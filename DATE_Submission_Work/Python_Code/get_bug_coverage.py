import os, sys, multiprocessing
import fnmatch
from optparse import OptionParser
from subprocess import Popen, PIPE
import regex as re
import pprint as pp

#bug_numbers = [1, 17, 18, 24, 25, 28, 29, 33, 34, 36, 37, 5, 8] # Excluded 26
bug_numbers = [28, 25, 37, 5] # Excluded 26
#bug_numbers = [1]
CACHE = 'Cache_bug'
NAME_OF_VCD = 'interface_vcd_dump.vcd'
SCOPE = '$scope'
UPSCOPE = '$upscope'
DUMPVARS = '$dumpvars'
END = '$end'

def get_file_names(correct_exec_dir):
    name_of_diags = []
    for root, dirNames, fileNames in os.walk(correct_exec_dir):
        for dirName in dirNames:
            if ':' in dirName:
                diag = dirName[:dirName.find(':')]
                name_of_diags.append(diag)
    return name_of_diags

def parse_vcd_file(name_of_vcd_file, name_of_diag):
    sig_tab = {}
    sig_val = {}
    sig_bit = {}
    vfile = open(name_of_vcd_file, 'r')
    vcd_file_contents = vfile.readlines()
    #print "Length of VCD File Contents: " + str(len(vcd_file_contents))
    scope_index = [idx for idx, x in enumerate(vcd_file_contents) if SCOPE in x]
    upscope_index = [idx for idx, x in enumerate(vcd_file_contents) if UPSCOPE in x]
    #print "Scope Index: " + str(scope_index) + " UPScope Index: " + str(upscope_index)
    for idx in range(scope_index[0] + 1, upscope_index[0]):
        line = vcd_file_contents[idx]
        line_content = line.split(' ')
        signal_name = line_content[4]
        signal_smbl = line_content[3]
        signal_bit  = line_content[2]
        sig_tab[signal_smbl] = signal_name
        sig_bit[signal_name] = int(signal_bit)
    vfile.close()
    pattern = re.compile(r'^#{1}[0-9]+')
    time_index = [idx for idx, x in enumerate(vcd_file_contents) if re.search(pattern, x)]
    for i in range(len(time_index) - 1):
        for j in range(time_index[i] + 1, time_index[i + 1]):
            if DUMPVARS in vcd_file_contents[j] or END in vcd_file_contents[j]:
                continue
            elif len(vcd_file_contents[j].lstrip().rstrip()) == 2:
                value = vcd_file_contents[j][0]
                smbl = vcd_file_contents[j][1]
                if sig_tab[smbl] not in sig_val.keys():
                    sig_val[sig_tab[smbl]] = [value]
                else:
                    sig_val[sig_tab[smbl]].append(value)
            else:
                sig_string_con = vcd_file_contents[j].lstrip().rstrip().split(' ')
                value = sig_string_con[0][1:]
                smbl = sig_string_con[1]
                if 'x' in value:
                    continue
                else:
                    if sig_tab[smbl] not in sig_val.keys():
                        sig_val[sig_tab[smbl]] = [hex(int(value[1:], 2))]
                    else:
                        sig_val[sig_tab[smbl]].append(hex(int(value[1:], 2)))
    print "VCD dump for Diag: " + name_of_diag
    #print pp.pprint(sig_val)
    return sig_val, sig_bit

def get_signal_vals(correct_exec_dir, name_of_diags):
    diag_signal_vals = {}
    status = {}
    for root, dirNames, fileNames in os.walk(correct_exec_dir):
        for dirName in dirNames:
            #print dirName
            if dirName[:dirName.find(':')] in name_of_diags:
                STATUS_LOG = open(root + '/' + dirName + '/status.log', 'r')
                STATUS_con = STATUS_LOG.readlines()
                index = [idx for idx, x in enumerate(STATUS_con) if 'FAIL' in x]
                if index != -1:
                    status[dirName[:dirName.find(':')]] = 'FAIL'
                else:
                    status[dirName[:dirName.find(':')]] = 'PASS'
                STATUS_LOG.close()

                name_of_vcd_file = root + '/' + dirName + '/' + NAME_OF_VCD
                signal_vals, sig_bit = parse_vcd_file(name_of_vcd_file, dirName[:dirName.find(':')])
                diag_signal_vals[dirName[:dirName.find(':')]] = signal_vals
                #exit(0)
    return diag_signal_vals, sig_bit, status

def compare_signal_dump(correct_signal_vals, buggy_signal_vals, sig_bit, status, bug_cache):
    msg_cov = open(bug_cache + '/' + 'msg_cov.txt', 'w')
    diag_names = correct_signal_vals.keys()
    for diag in diag_names:
        if status[diag] == 'PASS':
            continue
            # No need to compare. The diag in buggy design has passed. So no message has 
            # been affected
        else:
            msg_cov.write("Working on the diag: " + diag + '\n')
            signal_names = correct_signal_vals[diag].keys()
            correct_vals = correct_signal_vals[diag]
            buggy_vals = buggy_signal_vals[diag]
            #print correct_vals
            #print buggy_vals
            for signal in signal_names:
                #print "Comparing signal: " + signal
                # Comparing signals which are of width 1-bit
                if len(buggy_vals[signal]) <= len(correct_vals[signal]):
                    for i in range(len(buggy_vals[signal])):
                        if buggy_vals[signal][i] == correct_vals[signal][i]:
                            continue
                        else:
                            if sig_bit[signal] == 1:
                                msg_cov.write("Affected Signal: " + signal + ' @ sequence index: ' + str(i) + '\n')
                            else:
                                msg_cov.write("Affected Signal: " + signal + ' @ sequence index: ' + str(i) + ' Mismatched Value: ' + str(buggy_vals[signal][i]) + '\n')
                            break
                else:
                    msg_cov.write("Affected Signal (Length Mismatch): " + signal + '\n')
            msg_cov.write('\n\n')

    msg_cov.close()
    return

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-c", "--correct_exec", dest="correct_exec_dir")
    parser.add_option("-b", "--buggy_execution", dest="buggy_exec_dir")

    (options, args) = parser.parse_args()

    name_of_diags = get_file_names(options.correct_exec_dir)
    #print name_of_diags
    #name_of_diags = ['interrupt_DMU_CORE_BLK_enable1']
    # correct_signal_vals contain the correct signal values of all messages for all diags
    # correct_signal_vals is a dictionary where the keys of the dictionary are name of the
    # diags.
    print "Parsing Non-buggy Execution Dump...\n"
    correct_signal_vals, sig_bit, _ = get_signal_vals(options.correct_exec_dir, name_of_diags)
    Popen('mkdir -pv Correct_Val', shell=True, stdout=PIPE, stderr=PIPE) 
    for diag in name_of_diags:
        dump_file = open('Correct_Val/' + diag + '.txt', 'w')
        pp.pprint(correct_signal_vals[diag], stream=dump_file)
        dump_file.close()
    
    print "\n\n"

    for bug in bug_numbers:
        # Lets do the Bug-Message Coverage Analysis per bug
        print "Parsing Buggy Execution Dump for Bug ID: " + str(bug) + "\n"
        bug_cache = CACHE + '_' + str(bug)
        Popen('mkdir -pv ' + bug_cache, shell=True, stdout=PIPE, stderr=PIPE)
        buggy_exec_dir = options.buggy_exec_dir + '/' + bug_cache
        # buggy_signal_vals contain the buggy signal values of all messages for all diags
        # buggy_signal_vals is a dictionary where the keys of the dictionary are name of the
        # diags.
        buggy_signal_vals, _, status = get_signal_vals(buggy_exec_dir, name_of_diags)
        for diag in name_of_diags:
            dump_file = open(bug_cache + '/' + diag + '.txt', 'w')
            pp.pprint(buggy_signal_vals[diag], stream=dump_file)
            dump_file.close()
        
        print "\n"
        #print status
        compare_signal_dump(correct_signal_vals, buggy_signal_vals, sig_bit, status, bug_cache)
        # Technically, correct_signal_vals and buggy_signal_vals are two dictionaries
        # indexed by the same keys. We compare the same signal values between two dictionaries
        # We create a log file identifying the difference

        ############ Message Affecting Conditions ################
        '''
        1. For single bit we look for sequence match and the length. If buggy exec seq matches
        with non-buggy exec sequence and len(buggy_seq) <= len(non_buggy_seq) then continue
        2. If buggy exec seq does not match with non-buggy exec sequence and len(buggy_seq) <=
        len(non_buggy_seq) then report
        3. If len(buggy_seq) > len(non_buggy_seq) then report

        4. For multiple bit, we look for missmatched values and the length. If mismatched 
        value in buggy sequence, then report.
        5. If len(buggy_seq) <= len(non_buggy_seq) and no mismatch value, continue
        6. If len(buggy_seq) > len(non_buggy_seq) report
        '''

        



