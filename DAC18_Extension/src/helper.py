import numpy as np
import math as mt
import itertools as itools
import pprint as pp

colmap = {1: 'r', 
          2: 'g', 
          3: 'b', 
          4: 'm', 
          5: 'greenyellow',
          6: 'teal',
          7: 'lightpink',
          8: 'orchid',
          9: 'midnightblue',
          10: 'blueviolet',
          11: 'crimson',
          12: 'deeppink'
          }


##### Helper functions #####

def log2(x):
    try:
        return mt.log(x, 2)
    except ValueError:
        return 0.0

def euclidean(v1, v2):
    return sum((p - q) ** 2 for p, q in zip(v1, v2)) ** 0.5



##### Main functions #####

def read_trace(trace_file_name):
    
    sequence = []
    cycle = []
    
    t_file = open(trace_file_name, 'r')
    t_cont = t_file.readlines()
    t_file.close()
    
    for cont in t_cont:
        if cont[0] != '<':
            continue
        else:
            seq_ = cont[:cont.index('@')]
            cycle_ = cont[cont.index('@') + 1:]
            sequence.append(seq_)
            cycle.append(int(cycle_))
    
    #min_max_cycle = [min(cycle), max(cycle)] if cycle else []
            
    return sequence, cycle


def parse_trace(status, diag_names, test_num, BUGMSGLOC):

    sequence = {}
    cycle = {}
    
    for diag in diag_names:
        if status[diag] == 'FAIL':
            tracefile = BUGMSGLOC + '/Test' + str(test_num) + '_Part/' + diag + '_messages_part_obsv.log'
            seq, cycle_ = read_trace(tracefile)
            if not seq:
                #print('No sequence to analyze for Test: ' + str(test_num) + ' Diag: ' + diag + '\n')
                continue
            sequence[diag] = seq
            cycle[diag] = cycle_

            del seq, cycle_ 

    return sequence, cycle

def hamming_distance_(seq1, seq2):

    if len(seq1) != len(seq2):
        raise ValueError('Undefined for unequal length sequences')
    else:
        return sum(ele1 != ele2 for ele1, ele2 in zip(seq1, seq2))

def hamming_distance(aggr_seq):

    seq_pairs = itools.combinations(aggr_seq, 2)
    hdistance = [hamming_distance_(k[0], k[1]) for k in seq_pairs]

    print('Avg distance: ' + str(avg(hdistance)))

    return avg(hdistance)

def levenshtein_distance_(seq1, seq2):

    rows = len(seq1) + 1
    cols = len(seq2) + 1

    dist = np.zeros(shape=(rows, cols), dtype=np.int32)

    for i in range(1, rows):
        dist[i][0] = i

    for j in range(1, cols):
        dist[0][j] = j

    for col in range(1, cols):
        for row in range(1, rows):
            if seq1[row - 1] == seq2[col - 1]:
                cost = 0
            else:
                cost = 1

            dist[row][col] = min(dist[row - 1][col] + 1,\
                                 dist[row][col - 1] + 1, \
                                 dist[row - 1][col - 1] + cost)

    #print('seq1: ' + str(seq1) + ' seq2: ' + str(seq2) + ' : ' + str(dist[row][col]))

    return dist[row][col]

def levenshtein_distance(aggr_seq):

    seq_pairs = itools.combinations(aggr_seq, 2)
    seq_pairs_ = itools.combinations(aggr_seq, 2)
    
    if not list(seq_pairs):
        return 0.0
    
    ldistance = [levenshtein_distance_(k[0], k[1]) for k in seq_pairs_]
    
    #print('Avg distance: ' + str(avg(ldistance)))
    #print(2 * '\n')
    
    average = round(avg(ldistance), 4)

    return average

def avg(lst_num):

    return 1.0 * sum(lst_num) / len(lst_num)
