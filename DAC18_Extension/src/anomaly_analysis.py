from helper import euclidean
import numpy as np
import math as mt
import itertools as itools
import pprint as pp
from collections import Counter

from plot_figure import scatter3d, bar_diagram
from helper import levenshtein_distance, hamming_distance 
from sequence_analysis import create_aggrseq, calc_entropyaggrnew

def analyze_IP_BBV(IP_BBV):
    
    dim = len(IP_BBV)
    sim_matrix = np.zeros((dim, dim), dtype=int)

    for i in range(dim):
        for j in range(i, dim):
            ith_BBV = IP_BBV[i]
            jth_BBV = IP_BBV[j]
            dist = euclidean(ith_BBV, jth_BBV)
            sim_matrix[i][j] = dist

    return sim_matrix

def analyze_IP_BBV_(diag_names, test_num, cseq, cycle, granualrity):

    legal_ip_pair = []
    valid_msgs = []

    min_cycle_vector = []
    max_cycle_vector = []
    
    min_cycle = max_cycle = -1
    
    sequence = []
    cyl = []

    for diag in diag_names:
        try:
            sequence = cseq[diag]
            cyl = cycle[diag] 
        except KeyError:
            print('For Test: ' + str(test_num) + ' Diag: ' + diag + ' PASSED\n')
            continue
        '''
        if status[diag] == 'FAIL':
            tracefile = BUGMSGLOC + '/Test' + str(test_num) + '_Part/' + diag + '_messages_part_obsv.log'
            sequence = parse_trace(tracefile)
            if not sequence:
                #print('No sequence to analyze')
                continue
        '''
        for seq in sequence:
            ele = seq.split(',')
            src = ele[0][1:] #To remove the leading < we did [1:]
            dest = ele[1]
            msg = ele[3]
            pair = '<' + src + ', ' + dest + '>'
            if pair not in legal_ip_pair:
                legal_ip_pair.append(pair)
            if msg not in valid_msgs:
                valid_msgs.append(msg)
            
            del ele

        min_cycle_vector.append(cycle[diag][0])     ## Since the cycles are arranged in increasin order
                                                    ## in the list so 0th element is the min element
        max_cycle_vector.append(cycle[diag][-1])    ## The last element is the max element. Accessed via
                                                    ## -1 in the array
    
    if cyl:
        min_cycle = min(min_cycle_vector)
        max_cycle = max(max_cycle_vector)
    
    del cyl
    del sequence
    #del min_cycle_vector
    #del max_cycle_vector
    ###################### Identifying the legal IP interfaces that appears in all log ########
    print('For Test' + str(test_num) + ' legal IP pairs are: ' + str(len(legal_ip_pair)))
    print('For Test' + str(test_num) + ' Max_Cycle: ' + str(max_cycle) + ' Min_Cycle: ' + str(min_cycle))
    print('For Test' + str(test_num) + ' Granualarity: ' + str(granualrity))
    print('\n')
   
    print('Total cycle span: ' +  str(max_cycle - min_cycle))              ## Axis 1
    print('Legal IP-pairs: ' + ', '.join(legal_ip_pair))        ## Axis 2
    print('Valid messages: ' + ', '.join(valid_msgs))           ## Axis 3
    print('\n')

    return min_cycle, max_cycle, legal_ip_pair, valid_msgs


########################## Constructing IP_BBV on Raw Feature Values #############################
def construct_IP_BBV_Man(diag_names, status, test_num, cseq, cycle, granualrity, BUGMSGLOC, PIC_PATH):

    
    min_cycle, max_cycle, legal_ip_pair, valid_msgs = analyze_IP_BBV_(diag_names, test_num, \
            cseq, cycle, granualrity)

    if max_cycle == -1 and min_cycle == -1:
        IP_BBV = np.zeros(shape=(1,1,1,1), dtype=np.int32)
        return IP_BBV

    ## Derived feature: 1.1
    ## Finding the legal IP pair combinations
    ip_pair_comb_length = {}

    ## Derived feature: 1.2
    ## Finding the aggregation of unique message sequences that encompasses above mentioned legal IP pairs
    ## The unique sequences of length 2, 3, 4, ... for all the diag for a particular Test
    uniq_seq_length = {}        ## per cycle range
    uniq_seq_length_count = {}  ## per cycle range
    uniq_seq = {}               ## per sequence length

    ## Derived feature: 1.3
    ## Finding the entropy of unique message sequences that encompasses above mentioned legal IP pairs
    ## The entropy of all the unique sequences of length 2, 3, 4, ... for all the diag for a particular test
    entropy_seq_length = {}

    ## Derive feature: 3
    ## Finding how diverse the aggregation is. Using Levenshtein Distance / Hamming Distance
    ## The edit distance of all the unique sequences of length 2, 3, 4, ... for all the diag for a particular 
    ## test
    distance_seq_length = {}

    ## Message sequences for each cycle range in each of the diag. 
    ## Dictionary key is the length of the sequence
    cycle_span_seq_length = {}

    for length in range(2, len(legal_ip_pair) + 1):
        ip_pair_comb_length_ = itools.combinations(legal_ip_pair, length)
        print('Length of ' + str(length) + ' length IP pair combinations: ' + \
                str(len(list(ip_pair_comb_length_))))
        
        # Finding the unique sequences for all the diags for a given length
        unique_sequences_ = []
        
        # Total number of each of the unique sequences for all the diags for a given length
        seq_count = {}

        # Sequence Cycle Span for each of the individual diag
        seq_cycle_span_diag = {}

        for diag in diag_names:
            try:
                seq = cseq[diag]
                cyl = cycle[diag]
            except KeyError:
                continue

            seq_cycle_span_diag[diag] = {}

            seq_ = create_aggrseq(seq, length)
            cyl_ = create_aggrseq(cyl, length)


            #### Used for diag-->cycle-->message stats
            for idx1 in range(len(seq_)):
                span = cyl_[idx1]
                min_c_index = (span[0] - min_cycle) / granualrity if (span[0] - min_cycle) % granualrity == 0 \
                        else int(mt.floor(1.0 * (span[0] - min_cycle) / granualrity))

                max_c_index = (span[1] - min_cycle) / granualrity if (span[1] - min_cycle) % granualrity == 0 \
                        else int(mt.floor(1.0 * (span[1] - min_cycle) / granualrity))

                if min_c_index == max_c_index:
                    if min_c_index not in seq_cycle_span_diag[diag].keys():
                        seq_cycle_span_diag[diag][min_c_index] = [seq_[idx1]]
                    else:
                        seq_cycle_span_diag[diag][min_c_index].append(seq_[idx1])
                else:
                    if min_c_index not in seq_cycle_span_diag[diag].keys() and max_c_index not in \
                            seq_cycle_span_diag[diag].keys():
                        seq_cycle_span_diag[diag][min_c_index] = [seq_[idx1]]
                        seq_cycle_span_diag[diag][max_c_index] = [seq_[idx1]]
                    elif max_c_index not in seq_cycle_span_diag[diag].keys():
                        seq_cycle_span_diag[diag][min_c_index].append(seq_[idx1])
                        seq_cycle_span_diag[diag][max_c_index] = [seq_[idx1]]
                    else:
                        seq_cycle_span_diag[diag][min_c_index].append(seq_[idx1])
                        seq_cycle_span_diag[diag][max_c_index].append(seq_[idx1])
            #### Used for diag-->cycle-->message stats

            #### Used for message stats ####
            seq_count_diag = get_sequence_counts(seq_)
            unique_sequences__ = seq_count_diag.keys()
            unique_sequences_.extend(unique_sequences__)


            ## Here the key is a sequence
            for useq in unique_sequences__:
                if tuple(useq) not in seq_count.keys():
                    seq_count[tuple(useq)] = seq_count_diag[tuple(useq)]
                else:
                    seq_count[tuple(useq)] = seq_count[tuple(useq)] + seq_count_diag[tuple(useq)]

            del unique_sequences__
            del seq_count_diag
            #### Used for message stats ####
        
        unique_sequences_ = get_unique_sequences(unique_sequences_)

        ## Entropy calculation for all diags across all valida cycles
        ## Here the key is the cycle range index
        seq_diag = {}
        uniq_msgs = {}
        uniq_msgs_count = {}
        distance = {}
        entropies = {}
        
        for diag in diag_names:
            try:
                cycle_keys = seq_cycle_span_diag[diag].keys()
            except KeyError:
                continue
            for key in cycle_keys:
                if key not in seq_diag.keys():
                    seq_diag[key] = seq_cycle_span_diag[diag][key]
                else:
                    seq_diag[key] = seq_diag[key] + seq_cycle_span_diag[diag][key]
       
        del seq_cycle_span_diag
        
        #print('Cycle splitted message sequences:')
        #pp.pprint(seq_diag)
        #print(2 * '\n')

        for key in seq_diag.keys():
            entropies[key] = calc_entropyaggrnew(seq_diag[key])
        #print('Cycle splitted message sequence entropy:')
        #pp.pprint(entropies)
        #print(2 * '\n')

        for key in seq_diag.keys():
            uniq_msgs[key] = get_unique_sequences(seq_diag[key])
            uniq_msgs_count[key] = len(uniq_msgs[key])
        #print('Cycle splitted unique message sequences:')
        #pp.pprint(uniq_msgs)
        #print(2 * '\n')

        for key in uniq_msgs.keys():
            distance[key] = levenshtein_distance(uniq_msgs[key])
        #print('Cycle splitted unique message sequence L distance:')
        #pp.pprint(distance)
        #print(2 * '\n')

        ## Entropy calculation for all diags across all valida cycles

        uniq_seq_length[length] = uniq_msgs
        uniq_seq_length_count[length] = uniq_msgs_count
        entropy_seq_length[length] = entropies
        distance_seq_length[length] = distance
        cycle_span_seq_length[length] = seq_diag

        ip_pair_comb_length[length] = ip_pair_comb_length_
        uniq_seq[length] = unique_sequences_
        
        print('Length of ' + str(length) + ' length unique sequences: ' + \
                str(len(list(unique_sequences_))))
        print('\n\n')

        del ip_pair_comb_length_
        del seq_diag
        del distance
        del entropies
        del uniq_msgs
        del unique_sequences_

        #bar_diagram(seq_count, length, test_num, PIC_PATH)
    print('Entropy per length (indexed by cycle): ') 
    pp.pprint(entropy_seq_length)
    print(2 * '\n')

    print('L distance per length (indexed by cycle): ') 
    pp.pprint(distance_seq_length)
    print(2 * '\n')

    print('Unique message count per length (indexed by cycle): ')
    pp.pprint(uniq_seq_length_count)
    print(2 * '\n')

    #cycle_span = max_cycle - min_cycle

    #cyl_dim_rng = int(mt.ceil(1.0 * cycle_span / granualrity))
    
    zips = create_zips(uniq_seq_length_count, entropy_seq_length, distance_seq_length)

    IP_BBV = np.zeros(shape=(1,1,1,1), dtype=np.int32)

    return IP_BBV

########################## Constructing IP_BBV on Raw Feature Values #############################


def create_zips(uniq_seq_length_count, entropy_seq_length, distance_seq_length):
    
    zips = []

    seq_length = entropy_seq_length.keys()

    for length in seq_length:
        cycles = uniq_seq_length_count[length].keys()
        uniq_seq_length = [uniq_seq_length_count[length][cycle] for cycle in cycles]
        entropy = [entropy_seq_length[length][cycle] for cycle in cycles]
        distance = [distance_seq_length[length][cycle] for cycle in cycles]
        
        zips_ = zip(cycles, uniq_seq_length, entropy, distance)
        
        zips = zips + zips_
    
    print('Total zip elements: ' + str(len(zips)))
    pp.pprint(zips)
    return zips

def get_sequence_counts(seq_):
    
    c = Counter(tuple(x) for x in iter(seq_))

    return c

def get_unique_sequences(seq):
    
    useq = list(map(list, set(map(lambda i: tuple(i), seq))))

    return useq

########################## Constructing IP_BBV on Raw Feature Values #############################
def construct_IP_BBV_Raw(diag_names, status, test_num, cseq, cycle, granualrity, BUGMSGLOC, PIC_PATH):
    
    min_cycle, max_cycle, legal_ip_pair, valid_msgs = analyze_IP_BBV_(diag_names, test_num, \
            cseq, cycle, granualrity)

    if max_cycle == -1 and min_cycle == -1:
        IP_BBV = np.zeros(shape=(1,1,1,1), dtype=np.int32)
        return IP_BBV
    
    cycle_span = max_cycle - min_cycle

    diag_dim_rng = len(diag_names)                              ## Axis 0
    cyl_dim_rng = int(mt.ceil(1.0 * cycle_span / granualrity))  ## Axis 1
    lip_dim_rng = len(legal_ip_pair)                            ## Axis 2
    msg_dim_rng = len(valid_msgs)                               ## Axis 3

    IP_BBV = np.zeros(shape=(diag_dim_rng, cyl_dim_rng, lip_dim_rng, msg_dim_rng), dtype=np.int32)

    #lst_cycle_rngs = [[i * granualrity, (i + 1) * granualrity - 1] for i in range(cyl_dim_rng)]

    for idx1 in range(len(diag_names)):
    #for idx1 in range(1):
        print('Diag name: ' + diag_names[idx1] + '\n')
        #IP_BBV_ = [0] * len(legal_ip_pair)
        try:
            sequence = cseq[diag_names[idx1]]
            cyl = cycle[diag_names[idx1]]
        except KeyError:
            continue

        try:
            assert(len(sequence) == len(cyl))
        except AssertionError:
            print('Length of Sequence: ' + str(len(sequence)))
            print('Length of Cycle: ' + str(len(cyl)))
            print('Sequence and Cycle length are NOT same. Exiting')
            exit(-1)
        '''
        if status[diag] == 'FAIL':
            tracefile = BUGMSGLOC + '/Test' + str(test_num) + '_Part/' + diag + '_messages_part_obsv.log'
            sequence = parse_trace(tracefile)
            if not sequence:
                continue
        '''
        for idx2 in range(len(sequence)):
            seq = sequence[idx2]
            cyl_ = cyl[idx2]
            ele = seq.split(',')

            src = ele[0][1:] #To remove the leading < we did [1:]
            dest = ele[1]
            msg = ele[3]

            pair = '<' + src + ', ' + dest + '>'

            c_index = (cyl_ - min_cycle) / granualrity  if (cyl_ - min_cycle) % granualrity == 0 \
                    else int(mt.floor(1.0 * (cyl_ - min_cycle) / granualrity))
            l_index = legal_ip_pair.index(pair)
            m_index = valid_msgs.index(msg)
            
            #print('The seq::cycle is: ' + seq + ' ' + str(cyl_) + '\n')
            #print('The indices tuple is: <' + str(c_index) + ', ' + str(l_index) + ', ' + str(m_index) + '>\n')

            IP_BBV[idx1][c_index][l_index][m_index] = IP_BBV[idx1][c_index][l_index][m_index] + 1
        #scatter3d(test_num, IP_BBV, 'Cycle', 'Legal IP-pair', 'Messages', PIC_PATH) 


    print('The IP_BBV shape is: ' + str(IP_BBV.shape))
    #print(IP_BBV)
    scatter3d(test_num, IP_BBV, 'Cycle', 'Legal IP-pair', 'Messages', PIC_PATH) 
    return IP_BBV
########################## Constructing IP_BBV on Raw Feature Values #############################
