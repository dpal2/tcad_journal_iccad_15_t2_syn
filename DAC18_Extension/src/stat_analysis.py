import pandas as pd
from sklearn.cluster import KMeans, DBSCAN
from sklearn.metrics import calinski_harabaz_score
from sklearn.metrics import pairwise_distances
from sklearn.preprocessing import StandardScaler
from sklearn import metrics

from sklearn.covariance import EllipticEnvelope as EE
from sklearn.ensemble import IsolationForest as IF
from sklearn.neighbors import LocalOutlierFactor as LOF
from sklearn.manifold import TSNE

import matplotlib.pyplot as plt
from matplotlib import cm

from pyod.models.hbos import HBOS

from mpl_toolkits import mplot3d
from mpl_toolkits.mplot3d import Axes3D

import numpy as np


def calc_KMeans(X, PIC_PATH, test_num):
    
    np.random.seed(5)

    #X_ = np.array([np.array(x) for x in X])
    
    if X.ndim == 2:
        X_ = X
    else:
        nsamples, nx, ny, nz = X.shape
        X_ = X.reshape((nsamples, nx * ny * nz))

    for k in range(2, 12):
        title = str(k) + ' clusters'
        est = KMeans(n_clusters=k)
        fig = plt.figure(figsize=(4, 3))
        ax = Axes3D(fig, rect=[0, 0, 0.95, 1], elev=48, azim=134)
        est.fit(X_)
        labels = est.labels_

        ax.scatter(X_[:, 0], X_[:, 1], X_[:, 2],
                c=labels.astype(np.float), edgecolor='k')

        ax.set_xlabel('')
        ax.set_ylabel('')
        ax.set_zlabel('')
        ax.set_title(title)
        ax.dist = 12

        plt.savefig(PIC_PATH + '/KMeans_' + str(test_num) + '_' +  str(k) + '.pdf')
        plt.close()

    return

def calc_LOF(X, PIC_PATH, test_num):
    
    lof = LOF().fit(X)
    X_scores = lof.negative_outlier_factor_

    print('\n')
    print('The local outlier factor score for the supplied data (LOF): ')
    #print(lof)
    print(X_scores)

    return


def calc_IF(X, PIC_PATH, test_num):

    isf = IF()
    isf.fit(X)
    X_scores = isf.score_samples(X)
    print('\n')
    print('The local outlier factor score for the supplied data (IsolationForest): ')
    #print(lof)
    print(X_scores)

    return

def calc_HBOS(X, PIC_PATH, test_num):
    
    clf = HBOS()
    clf.fit(X)
    X_pred = clf.labels_
    X_scores = clf.decision_scores_

    print(X_pred)
    print(X_scores)

    return


def calc_TSNE(X, PIC_PATH, test_num):

    X_embedded = TSNE().fit_transform(X)
    print('\n')
    print('The embedded shape: ' + str(X_embedded.shape))
    
    
    return

def cluster_DBSCAN(X, PIC_PATH, test_num):
    

    X = StandardScaler().fit_transform(X)
    #print(X)

    db = DBSCAN(eps=0.5, min_samples=2).fit(X)
    core_samples_mask = np.zeros_like(db.labels_, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    labels = db.labels_

    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    unique_labels = set(labels)
    colors = [plt.cm.Spectral(each) \
            for each in np.linspace(0, 1, len(unique_labels))]
    '''
    fig = plt.figure()
    ax = plt.axes(projection='3d')

    for k, col in zip(unique_labels, colors):
        if k == -1:
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        ax.plot3D(xy[:, 0], xy[:, 1], xy[:, 2], 'o', markerfacecolor=tuple(col), \
                markeredgecolor='k', markersize=14)

        xy = X[class_member_mask & ~core_samples_mask]
        ax.plot3D(xy[:, 0], xy[:, 1], xy[:, 2], 'o', markerfacecolor=tuple(col), \
                markeredgecolor='k', markersize=6)

    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.savefig(PIC_PATH + '/3D_DBSCAN_Test_' + str(test_num) + '.pdf')
    plt.close()
    '''

    #fig, ax = plt.subplots(figsize=(5, 5))
    fig = plt.figure()

    for k, col in zip(unique_labels, colors):
        if k == -1:
            col = [0, 0, 0, 1]

        class_member_mask = (labels == k)

        xy = X[class_member_mask & core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col), \
                markeredgecolor='k', markersize=14)

        xy = X[class_member_mask & ~core_samples_mask]
        plt.plot(xy[:, 0], xy[:, 1], 'o', markerfacecolor=tuple(col), \
                markeredgecolor='k', markersize=6)

    plt.title('Estimated number of clusters: %d' % n_clusters_)
    plt.savefig(PIC_PATH + '/2D_DBSCAN_Test_' + str(test_num) + '.pdf')
    plt.close()

    rpt_file = open(PIC_PATH + '/DBSCAN_Test' + str(test_num) + '.rpt', 'w')
    rpt_file.write('Estimated number of clusters: %d' % n_clusters_ + '\n')
    rpt_file.write('Estimated number of noise points: %d' % n_noise_ + '\n')
    rpt_file.write("Silhouette Coefficient: %0.3f" % metrics.silhouette_score(X, labels) + '\n')
    rpt_file.close()


    return
