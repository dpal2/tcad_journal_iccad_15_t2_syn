from collections import Counter
from helper import log2

import pprint as pp

def create_subseq(seq):
    ## Just doing simple bisection 
    subseq = []
    length = len(seq)

    if length % 2 == 0:
        idx = int(length / 2)
        seq1 = seq[: idx]
        seq2 = seq[idx :]
        subseq = [seq1, seq2]
    else:
        idx = int((length + 1) / 2)
        seq1 = seq[: idx]
        seq2 = seq[idx - 1:]
        subseq = [seq1, seq2]
    return subseq

def create_aggrseq(seq, L):
    # Doing a moving window of length L
    #print(seq)
    sequences = []
    end_point = len(seq) - L  + 1
    
    for i in range(end_point):
        end = i + L
        seq_ = seq[i : end]
        sequences.append(seq_)

    #print(sequences)
    return sequences

def create_aggregates(sequences, aggrs):
    
    clusters = []
    length = len(sequences)
    seq_per_cluster = -1

    if length % aggrs == 0:
        seq_per_cluster = int(length / aggrs)
        for i in range(aggrs):
            cluster = sequences[i * seq_per_cluster : (i + 1) * seq_per_cluster]
            clusters.append(cluster)
            del cluster
    else:
        seq_per_cluster = int(length / aggrs) + 1
        for i in range(aggrs):
            if i == 0:
                cluster = sequences[i * seq_per_cluster : (i + 1) * seq_per_cluster]
            else:
                cluster = sequences[i * seq_per_cluster - 1 : (i + 1) * seq_per_cluster - 1]
            clusters.append(cluster)
            del cluster

    return clusters

def calc_entropyseq(seq):
    seq_entropy = []
    
    for i in range(len(seq)):
        seq_length = len(seq[i])
        #Get the unique elements
        u_ele = list(set(seq[i]))
        #Initializa a dictionary with the element count
        u_seq = {}
        for ele in u_ele:
            u_seq[ele] = 0
        #Fill the dictionary with element count
        for ele in seq[i]:
            u_seq[ele] += 1
        entropy = 0.0
        for ele in u_ele:
            p = 1.0 * u_seq[ele] / seq_length
            entropy = entropy + (-1.0) * p * log2(p)
        seq_entropy.append(entropy)
        
    return seq_entropy

def calc_entropyaggr(clusters):
    
    clus_entropy = []

    for i in range(len(clusters)):
        clus_length = len(clusters[i])
        c= Counter(tuple(x) for x in iter(clusters[i]))
        u_ele = c.keys()
        print(u_ele)
        entropy = 0.0
        for ele in u_ele:
            p = 1.0 * c[ele] / clus_length
            entropy = entropy + (-1.0) * p * log2(p)
        clus_entropy.append(entropy)

    return clus_entropy

def calc_entropyaggrnew(cluster):
    
    clus_length = len(cluster)

    c= Counter(tuple(x) for x in iter(cluster))
    u_ele = c.keys()
    entropy = 0.0
    for ele in u_ele:
        p = 1.0 * c[ele] / clus_length
        entropy = entropy + (-1.0) * p * log2(p)
    
    return round(entropy, 4)


def bisection(sequence, stop):
    #Here the sequence is just a single sequence of messages
    if len(sequence) <= stop:
        return sequence
    else:
        seq = create_subseq(sequence)
        #print('Sequences are: ' + str(seq))
        seq_entropy = calc_entropyseq(seq)
        #print('Sequence entropies are: ' + str(seq_entropy))
        next_sequence = seq[0] if seq_entropy[0] > seq_entropy[1] else seq[1]
        #print('Next Sequence to be analyzed: ' + str(next_sequence))
        return bisection(next_sequence, stop)

def aggregate(sequence, stop, n_aggr, length):
    #Here the sequence is aggregate of sequences
    if len(sequence[0]) <= stop:
        return sequence
    else:
        sequences = create_aggrseq(sequence, length)
        #print('Sequences are: ' + str(sequences))
        clusters = create_aggregates(sequences, n_aggr)
        print('Cluasters are: ' + str(clusters))
        clus_entropy = calc_entropyaggr(clusters)
        print('Cluster entropies are: ' + str(clus_entropy))
        max_clus_ent = max(clus_entropy)
        index = clus_entropy.index(max_clus_ent)
        print('Max entropy is: ' + str(max_clus_ent) + ' Max entropy index is: ' + str(index))
        next_sequences = clusters[index]
        #print('The set of next sequences are: ' + str(next_sequences))
        print(2 * '\n')
        return aggregate(next_sequences, stop, n_aggr * 2, \
                int(length / 2) if length % 2 == 0 else int((length + 1) / 2))
#        return aggregate(next_sequences, stop, n_aggr + 1, length - 1)

def find_culprit_seq(method, sequence, stop, n_aggr, length):
    

    c_sequence = []
    #Bisection method
    if method == 'bisection':
        c_sequence = bisection(sequence, stop)
    elif method == 'aggregate':
        c_sequence = aggregate([sequence], stop, n_aggr, length)
    elif method == 'clustering':
        cluster(sequence)

    return c_sequence
