import os

from argparse import ArgumentParser

from stat_analysis import calc_KMeans, calc_LOF, calc_IF, calc_HBOS, calc_TSNE, cluster_DBSCAN
from plot_figure import plot_similarity_matrix

from sequence_analysis import find_culprit_seq
from anomaly_analysis import analyze_IP_BBV, construct_IP_BBV_Raw, construct_IP_BBV_Man
from helper import parse_trace


NO_OF_TESTS = 1

#python src/automated_debugging.py -m anomaly

##### Main functions #####
if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("-f", "--tracefile", help="Absolute/Relative location of the message trace file", \
            dest="tracefile")
    parser.add_argument("-m", "--method", help="Method to apply on the traces", \
            dest="method", required=True, choices=['bisection', 'aggregate', 'anomaly'], \
            default='bisection')
    parser.add_argument("-s", "--stop", help="Stop length for the sequence exploration", \
            dest="stop", type=int, default=-1)
    parser.add_argument("-a", "--aggreagte", help="Number of aggregates that to start with", \
            dest="n_aggr", default=-1, type=int)
    parser.add_argument("-l", "--length", help="Length of the subsequences that to start with", \
            dest="length", default=-1, type=int)
    parser.add_argument("-g", '--granualrity', help='Granularity of the time axis to parse trace', \
            dest='granualrity', default=10000, type=int)

    args = parser.parse_args()

    if args.method == 'aggregate':
        if args.n_aggr == -1 or args.length == -1:
            print(2 * '\n')
            print('For aggregate method: ' + arg.method + ', please provide positive values for \
                    \'number of aggregation\' and \'length of sequence\'')
            print(2 * '\n')
            exit(-1)
        if args.stop == -1:
            print(2 * '\n')
            print('For aggregate method: ' + arg.method + ', please provide positive values for \
                    \'number of aggregation\' and \'length of sequence\'')
            print(2 * '\n')
            exit(-1)
    
    if args.method == 'anomaly':
        print('Using time-axis granularity of ' + str(args.granualrity) + ' cycles.\n')

    
    CURR_DIR = os.getcwd()
    RUNLOG = os.path.join(CURR_DIR, 'Run_Logs', args.method) 
    BUGMSGLOC = os.path.join(CURR_DIR, 'buggy_message_log_files')
    PIC_PATH = os.path.join(CURR_DIR, 'pic')
    
    for i in range(1, NO_OF_TESTS + 1):
        STATUS_FILE = BUGMSGLOC + '/Test' + str(i) + '_Complete/status.log'
        sFile = open(STATUS_FILE, 'r')
        sCon = sFile.readlines()
        sFile.close()
        status = {}
        diag_names = []
        for line in sCon:
            if '::' in line:
                key = line[:line.find('::')].strip()
                val = line[line.find('::') + 2:]
                status[key] = val.strip()
                diag_names.append(key)
       
        print('\n')

        sequence, cycle = parse_trace(status, diag_names, i, BUGMSGLOC)
        if args.method == 'anomaly':
            '''
            IP_BBV = construct_IP_BBV_Raw(diag_names, status, i, sequence, cycle, args.granualrity, BUGMSGLOC, \
                    PIC_PATH)
            '''
            IP_BBV = construct_IP_BBV_Man(diag_names, status, i, sequence, cycle, args.granualrity, BUGMSGLOC, \
                    PIC_PATH)
            #if IP_BBV.nonzero():
                #sim_matrix = analyze_IP_BBV(IP_BBV)
                #plot_similarity_matrix(sim_matrix, PIC_PATH, i)
                #cluster_DBSCAN(IP_BBV, PIC_PATH, i)
                #calc_LOF(IP_BBV, PIC_PATH, i)
                #calc_IF(IP_BBV, PIC_PATH, i)
                #calc_HBOS(IP_BBV, PIC_PATH, i)
                #calc_KMeans(IP_BBV, PIC_PATH, i)
        else:
            ALL_RPT = RUNLOG + '/Test' + str(i) + '/All_Diag.rpt' 
            ALL_RPTFile = open(ALL_RPT, 'w')
    
            for diag in diag_names:
                try:
                    seq = sequence[diag]
                except KeyError:
                    print('For Test: ' + str(test_num) + ' Diag: ' + diag + ' PASSED\n')
                    continue
                '''
                if status[diag] == 'FAIL':
                    tracefile = BUGMSGLOC + '/Test' + str(i) + '_Part/' + diag + '_messages_part_obsv.log'
                    sequence = parse_trace(tracefile)
                    if not sequence:
                        print('No sequence to analyze')
                        continue
                '''
                c_sequence = find_culprit_seq(args.method, seq, args.stop, args.n_aggr, args.length)
    
                rpt = RUNLOG + '/Test' + str(i) + '/' + diag + '.rpt'
                rptFile = open(rpt, 'w')
                rptFile.write('The set of possible culprit sequences are: ' + str(c_sequence))
                ALL_RPTFile.write('Diag: ' + diag + ' :: ' + str(c_sequence) + '\n')
                rptFile.close()
    
            ALL_RPTFile.close()
