import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
from helper import log2

import math as mt
from sklearn.neighbors.kde import KernelDensity

def plot_similarity_matrix(sim_matrix, PIC_PATH, test_num):
    
    fig, ax = plt.subplots(figsize=(5, 5))
    cax = plt.imshow(sim_matrix, cmap=cm.hot_r, interpolation='nearest')
    
    ax.set_xlabel('Indices of failed runs')
    ax.set_ylabel('Indices of failed runs')
    plt.tight_layout()

    
    min_val = sim_matrix.min()
    max_val = sim_matrix.max()

    cbar = fig.colorbar(cax, ticks=[min_val, max_val])
    cbar.ax.set_yticklabels([str(min_val), str(max_val)])
   
    plt.savefig(PIC_PATH + '/Sim_Mat_Test' + str(test_num) + '.pdf')
    plt.close()

    return

def scatter3d(test_num, Vector, xlabel, ylabel, zlabel, PIC_PATH):
    
    ndim = Vector.ndim
    shape = Vector.shape
    
    print('DIM: ' + str(ndim) + ' Shape: ' + str(shape))

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')

    if ndim == 3:
        xs = Vector[:, 1]
        ys = Vector[:, 2]
        zs = Vector[:, 3]
        
        ax.scatter(xs, ys, zs, c='r', marker='o')
    else:
        iterable = shape[0]
        for iter in range(iterable):
            Vector_ = Vector[iter, :, :, :]
            print('Dimension of sliced Vector_: ' + str(Vector_.shape) + ' nonzero-elements: ' +\
                    str(np.count_nonzero(Vector_)))
            z, x, y = Vector_.nonzero()

            pnt3d = ax.scatter(x, y, z, zdir='z', c=z, linewidth=None, edgecolor=None, marker='^')
            #ax.plot_surface(x, y, -z, cmap=cm.coolwarm, linewidth=0, antialiased=False)

            del z
            del x
            del y

        cbar = plt.colorbar(pnt3d)
    cbar.set_label("Values")
    
    ax.set_xlabel(xlabel)
    ax.set_ylabel(ylabel)
    ax.set_zlabel(zlabel)
    plt.savefig(PIC_PATH + '/Test' + str(test_num) + '_Scatter.pdf')
    #plt.show()

    return

def bar_diagram(data, length, test_num, PIC_PATH):

    categories = data.keys()
    values = [data[key] for key in categories]
    sval = sum(values)

    nvals = [1.0 * values[i] / sval for i in range(len(values))]
    categ = [str(i) for i in range(len(categories))]

    fig, ax = plt.subplots(figsize=(5,5))

    ax.bar(categ, nvals)
    #a = np.array(values).reshape(-1, 1)
    #kde = KernelDensity(kernel='gaussian', bandwidth=0.75).fit(a)
    #s = np.linspace(0, max(values))
    #e = kde.score_samples(s.reshape(-1, 1))
    #ax.plot(s, e)
    ax.set_xlabel('Categories')
    ax.set_ylabel('Values')

    plt.title('Bar diagram for unique message length: ' + str(length))

    plt.savefig(PIC_PATH + '/Test' + str(test_num) + '_' + str(length) + '_Bar.pdf')

    return

# python src/automated_debugging.py -m anomaly -g 100000
