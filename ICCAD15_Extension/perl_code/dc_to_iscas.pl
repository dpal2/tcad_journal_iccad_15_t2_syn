#!/usr/bin/env /usr/bin/perl 
#===============================================================================
#
#         FILE: dc_to_iscas.pl
#
#        USAGE: ./dc_to_iscas.pl  
#
#  DESCRIPTION: This script takes a Synopsys DC Compiler netlist and converts it into
#               a ISCAS bench format. 
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Debjit Pal ( dpal2@illinois.edu ) 
# ORGANIZATION: University of Illinois at Urbana-Champaign
#      VERSION: 1.0
#      CREATED: 03/05/15 12:57:15
#     REVISION: 1) Trying to consolidate different NOR, NAND, AND, OR gates in the
#                  single for loop for each kind of gate in the branch dpal2
#===============================================================================

use strict;
use warnings;
use utf8;
use autodie;
use File::Find;
use String::Util qw(trim);
use Getopt::Long;
use Path::Tiny qw(path);
use IO::Handle;
use Array::Utils qw(:all);
use List::MoreUtils qw(first_index);
use List::MoreUtils qw(uniq);
use 5.010;

# Declaration of my own functions
sub mytrim($);
sub stripchars;

my $file = "dc_netlist.v";
my $top = "top";
my $clock = "clock";
my $reset = "reset";
my $polarity = "-1";
my $verbose = '';
my $chars=" /[;";
my $in_flag = "-1";

GetOptions (    "file=s"    => \$file,      # File name
                "top=s"     => \$top,       # Name of the top module
                "clock=s"   => \$clock,     # Clock signal
                "reset=s"   => \$reset,     # Reset signal
                "polariy=s" => \$polarity,  # Polarity of the reset signals
                "verbose"   => \$verbose)   # Flag
or die ("Errior in command line arguments\n");
print "The Synopsys Design Compiler File received: $file\n";
print "The clock name is: $clock\n";
print "The reset name is: $reset\n";

# Module related details
my $module_name;
my @input_vector;
my @output_vector;

# Blackbox related details
my @lhs_signals;
my @rhs_signals;

# Logic elements related details for standard ISCAS format
my @dff_vector;
my @nand_vector;
my @and_vector;
my @nor_vector;
my @or_vector;
my @not_vector;

# Logic elements related details for Davoodi's format
my @dff_vector_ad;
my @nand_vector_ad;
my @and_vector_ad;
my @nor_vector_ad;
my @or_vector_ad;
my @not_vector_ad;

## push the clock and the reset explicitly in the input vector. This is needed as some of the DFF
## does not need the clock input and the reset input and may not get explicitly set as Input.
## If clock and reset is duplicated, uniq routine will take care of it.

push(@input_vector, $clock);
push(@input_vector, $reset);


## Flags
my $module_found = 0;

## Hash table to handle assign statement
my %assign_hash = ();

# Line Number of the File
my $line_number = 0;

my $map_name = File::Spec->splitpath(+$file);
my $map_filename = join(".", substr($map_name, 0, length($map_name) - 2), "map");
print "Map Filename is: $map_filename\n";
open(my $mh, '>', $map_filename) or die "Could not open file '$map_filename' $!";

my $inst_name = File::Spec->splitpath(+$file);
my $inst_filename = join(".", substr($inst_name, 0, length($inst_name) - 2), "inst");
print "DFF Instance Filename is: $inst_filename\n";
open(my $ih, '>', $inst_filename) or die "Could not open file '$inst_filename' $!";

my $file_handle = path($file)->openr;
while ( my $line = $file_handle->getline() ) {
    # Module Name
    $line_number = $line_number + 1;
    if( $line =~ m/^\bmodule\s/ ) {
        my @words = split(/ /, $line);
        $module_name = $words[1];
        print "Module Name Found: $module_name\n\n";
        if($module_name ne $top) {
            #print "UNEQUAL\n";
            $module_found = 0;
            next;
        }
        else {
            $module_found = 1;
        }
    }
    elsif ( $module_found == 0) {
        next;
    }
    # Input ports
    elsif ( $line =~ m/^\binput\s/ ) {
        my $vector_sig_names;
        chomp($line);
        #$line = mytrim($line);
        my $next_line = '';
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
        }
        my @words = split(/\s{1,}/, $line); 
        
        #print "#$words[0]#, #$words[1]#, #$words[2]#\n";

        if(index($words[1], "[") != -1) {
            #printf "Vector input seen.\n";
            my @vector_size = split(/:/, $words[1]);
            chomp($vector_size[0]);
            chomp($vector_size[1]);
            $vector_size[0] =~ s/\[//g;
            $vector_size[1] =~ s/\]//g;
            $vector_size[0] = $vector_size[1] - $vector_size[0] + 1;
            #print "Vector Size: $vector_size[0]\n";
            $words[2] =~ s/\;//g;
            #print "$words[2]\n";
            for(my $i = 0; $i < $vector_size[0]; $i++) {
                push(@input_vector, join("", $words[2], $i));
            }
        }
        else {
            #printf "Non-Vector input seen\n";
            my $index_val = index($line, "input");
            $index_val = $index_val + length("input");
            $vector_sig_names = substr($line, $index_val);
            $vector_sig_names =~ s/ //g;
            $vector_sig_names =~ s/\;//g;
            #print "Non-Vector signal names: $vector_sig_names\n";
            push(@input_vector, split(/,/, $vector_sig_names));
            #print @input_vector;
        }
    }
    #Output Ports
    elsif ( $line =~ m/^\boutput\s/ ) {
        my $vector_sig_names;
        chomp($line);
        #$line = mytrim($line);
        my $next_line = '';
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
        }
        my @words = split(/\s{1,}/, $line);
        #print "#$words[0]#, #$words[1]#, #$words[2]#\n";
        if(index($words[1], "[") != -1) {
            #printf "Vector output seen.\n";
            my @vector_size = split(/:/, $words[1]);
            chomp($vector_size[0]);
            chomp($vector_size[1]);
            $vector_size[0] =~ s/\[//g;
            $vector_size[1] =~ s/\]//g;
            $vector_size[0] = $vector_size[1] - $vector_size[0] + 1;
            #print "Vector Size: $vector_size[0]\n";
            $words[2] =~ s/\;//g;
            #print "$words[3]\n";
            for(my $i = 0; $i < $vector_size[0]; $i++) {
                push(@output_vector, join("", $words[2], $i));
            }
        }
        else {
            #printf "Non-Vector Output seen\n";
            my $index_val = index($line, "output");
            $index_val = $index_val + length("output");
            $vector_sig_names = substr($line, $index_val);
            $vector_sig_names =~ s/ //g;
            $vector_sig_names =~ s/\;//g;
            chomp($vector_sig_names);
            push(@output_vector, split(/,/, $vector_sig_names));
            #print @output_vector;
        }

    }

    ##################################################
    # Any output / port which is related with assignemnt
    # statements but not used anywhere in the netlist
    # needs to be removed. Else SigSeT will cry !!
    ##################################################

    elsif ( $line =~ m/^\bassign\s/ ){
        my $index;
        chomp($line);
        $line = mytrim($line);
        my @assign_words = split(/\s+/, $line);
        $assign_words[3] = stripchars($assign_words[3], $chars);
        $assign_words[3] =~ s/;//g;
        $assign_words[1] = stripchars($assign_words[1], $chars);
        my $net_name_to_be_removed = $assign_words[1];
        #print "Has Entries: ".$assign_words[1]." ".$assign_words[3]."\n";
        $assign_hash{$assign_words[1]} = $assign_words[3];
        #print $net_name_to_be_removed . "\n";
        #print "#".$net_name_to_be_removed."#" . "\n";
        my $output_size = scalar @output_vector;
        for(my $i = 0; $i < $output_size; $i++) {
            #print "#$output_vector[$i]#" . "#$net_name_to_be_removed#" . "\n";
            if($output_vector[$i] eq $net_name_to_be_removed) {
                splice @output_vector, $i, 1;
                #print join(", ", @output_vector) . "\n\n";
                last;
            }
        }
    }

    ##################################################
    # All Mux4 should be handled by this routine
    ##################################################
    
    elsif ( $line =~ m/^\bMux4\s/ ) {
        my $next_line = '';
        my $sel;
        my @in0;
        my @in1;
        my @in2;
        my @in3;
        my @out;
        my @select_vec;
        chomp($line);
        my @Mux4_config = (".Sel(",".Din0(",".Din1",".Din2", ".Din3", ".Dout");
        $line = mytrim($line);
        my @Mux4_Instantiation_Name = split(/ /, $line);
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
        }
        #print $line . "\n";
        my $sel_index_start = index($line, $Mux4_config[0]); my $sel_index_stop = index(substr($line, $sel_index_start + length($Mux4_config[0])), ")");
        #print "$in_index_start, $in_index_stop\n";
        $sel = substr($line, $sel_index_start + length($Mux4_config[0]), $sel_index_stop); $sel = mytrim($sel);
        if(index($sel, "/") != -1 or index($sel, "[") != -1) {
                $sel = stripchars($sel, $chars);
                $sel = substr($sel, 0, length($sel));
                # print $sel . "\n";
        }
		# Selecting line 
	 	for(my $i = 0; $i < 2; $i++) {
			push(@not_vector, $sel . $i . "c" . " = NOT(" . $sel . $i . ")\n"); ##\t\t # Mux select for $Mux4_Instantiation_Name[1]\n");
		}

        my $in0_index_start = index($line, $Mux4_config[1]); my $in0_index_stop = index(substr($line, $in0_index_start + length($Mux4_config[1])), ")");
        #print "$in_index_start, $in_index_stop\n";
        my $in0_ = substr($line, $in0_index_start + length($Mux4_config[1]), $in0_index_stop); $in0_ = mytrim($in0_);
		if(index($in0_, "/") != -1 or index($in0_, "[") != -1) {
                $in0_ = stripchars($in0_, $chars);
                $in0_ = substr($in0_, 0, length($in0_));
                #print $in0_ . "\n";
        }
		@in0 = split(/,/, $in0_); shift @in0; #print @in0;

        my $in1__index_start = index($line, $Mux4_config[2]); my $in1__index_stop = index(substr($line, $in1__index_start + length($Mux4_config[2])), ")");
        #print "$in_index_start, $in_index_stop\n";
        my $in1_ = substr($line, $in1__index_start + length($Mux4_config[1]), $in1__index_stop); $in1_ = mytrim($in1_);
		if(index($in1_, "/") != -1 or index($in1_, "[") != -1) {
                $in1_ = stripchars($in1_, $chars);
                $in1_ = substr($in1_, 0, length($in1_));
                #print $in1_ . "\n";
        }
		@in1 = split(/,/, $in1_); #print @in1;

        my $in2__index_start = index($line, $Mux4_config[3]); my $in2__index_stop = index(substr($line, $in2__index_start + length($Mux4_config[3])), ")");
        #print "$in_index_start, $in_index_stop\n";
        my $in2_ = substr($line, $in2__index_start + length($Mux4_config[1]), $in2__index_stop); $in2_ = mytrim($in2_);
		if(index($in2_, "/") != -1 or index($in2_, "[") != -1) {
                $in2_ = stripchars($in2_, $chars);
                $in2_ = substr($in2_, 0, length($in2_));
                #print "in2: " . $in2_ . "\n";
        }

        my $in3__index_start = index($line, $Mux4_config[4]); my $in3__index_stop = index(substr($line, $in3__index_start + length($Mux4_config[4])), ")");
        #print "$in_index_start, $in_index_stop\n";
        my $in3_ = substr($line, $in3__index_start + length($Mux4_config[1]), $in3__index_stop); $in3_ = mytrim($in3_);
		if(index($in3_, "/") != -1 or index($in3_, "[") != -1) {
                $in3_ = stripchars($in3_, $chars);
                $in3_ = substr($in3_, 0, length($in3_));
                #print $in3_ . "\n";
        }
		@in3 = split(/,/, $in3_); # print @in3;

        my $out_index_start = index($line, $Mux4_config[5]); my $out_index_stop = index(substr($line, $out_index_start + length($Mux4_config[5])), ")");
        #print "$in_index_start, $in_index_stop\n";
        my $out_ = substr($line, $out_index_start + length($Mux4_config[1]), $out_index_stop); $out_ = mytrim($out_);
		if(index($out_, "/") != -1 or index($out_, "[") != -1) {
                $out_ = stripchars($out_, $chars);
                $out_ = substr($out_, 0, length($out_));
                # print "out: " . $out_ . "\n";
        }
		
		my $count = 0;
		my $mux_temp_;
        my $range = 10000;
		my $random_number;
		for(my $i = 0; $i < 16; $i++) {
			my $bit = 16 - $i;
			my @or_inp;
			# Bit level Mux implementation
			$random_number = int(rand($range));
			$mux_temp_ = "temp" . $random_number . " = AND($in0[$i], cpuPCMUX1c, cpuPCMUX0c)\n";##\t\t # AND gate for bit combination $bit of MUX\n"; 
            push(@and_vector, $mux_temp_); $mux_temp_ = ''; push(@or_inp, "temp".$random_number);
			$random_number = int(rand($range));
			$mux_temp_ = "temp" . $random_number . " = AND($in1[$i], cpuPCMUX1c, cpuPCMUX0)\n"; ##\t\t # AND gate for bit combination $bit of MUX\n"; 
            push(@and_vector, $mux_temp_); $mux_temp_ = ''; push(@or_inp, "temp".$random_number);
			$random_number = int(rand($range));
			$mux_temp_ = "temp" . $random_number . " = AND(". join("", $in2_, $i) . ", cpuPCMUX1, cpuPCMUX0c)\n";##\t\t # AND gate for bit combination $bit of MUX\n"; 
            push(@and_vector, $mux_temp_); $mux_temp_ = ''; push(@or_inp, "temp".$random_number);
			$random_number = int(rand($range));
			$mux_temp_ = "temp" . $random_number . " = AND($in3[$i], cpuPCMUX1, cpuPCMUX0)\n";##   \t\t # AND gate for bit combination $bit of MUX\n"; 
            push(@and_vector, $mux_temp_); $mux_temp_ = ''; push(@or_inp, "temp".$random_number);
			$mux_temp_ = $out_.$i . " = OR(" . join(", ", @or_inp) . ")\n";##    \t\t # OR gate for bit combination $bit of MUX\n"; 
            push(@or_vector, $mux_temp_); $mux_temp_ = '';
		}
    }



    ##################################################
    # All Mux4 should be handled by this routine
    ##################################################
    
    ##################################################
    # All DFF (veriosn DFFR_*) should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bDFFR_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".CK(", ".RN(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "DFFR_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            elsif($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            push(@inputs, $in);
            if ( $i == 1) {
                push(@rhs_signals, $in);
            }
        }
        for(my $i = 4; $i <= 5; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #   $out = '';
                #   push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
        }
        else {
            $outputs[0] = $outputs[1] . "dummy";
            push(@lhs_signals, $outputs[0]);
            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
        }
        if(defined $outputs[1]) {
            push(@lhs_signals, $outputs[1]);
            push(@rhs_signals, $outputs[0]);

            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );

            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector_ad, $not_string_ad );
        }
    }

    ##################################################
    # All DFFR should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################

    ##################################################
    # All DFFS (veriosn DFFS_*) should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bDFFS_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".CK(", ".SN(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "DFFR_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 3; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            elsif($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            push(@inputs, $in);
            if ( $i == 1) {
                push(@rhs_signals, $in);
            }
        }
        for(my $i = 4; $i <= 5; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #    $out = '';
                #    push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
                push(@not_vector_ad, $not_string_ad);
            }
        }
        else {
            $outputs[0] = $outputs[1] . "dummy";
            push(@lhs_signals, $outputs[0]);
            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[2] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[2])";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
                push(@not_vector_ad, $not_string_ad);
            }
        }
        if(defined $outputs[1]) {
            push(@lhs_signals, $outputs[1]);
            push(@rhs_signals, $outputs[0]);

            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );

            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector_ad, $not_string_ad );
        }
    }

    ##################################################
    # All DFFS should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################


    ##################################################
    # All DFF (veriosn DFF_*) should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bDFF_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".CK(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "$DFF_Instantiation_Name[1]\n";
        #print "DFF_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            elsif($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            push(@inputs, $in);
            if ( $i == 1) {
                push(@rhs_signals, $in);
            }
        }
        # Weird way of coding but just kept for consistency
        for(my $i = 3; $i <= 4; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #   $out = '';
                #   push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        #if($outputs[0] ne '') {
        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $reset)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $reset)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                # There is no inputs[2]. Hence empty string concatenation
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $reset)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        else {
            $outputs[0] = $outputs[1] . "dummy";
            push(@lhs_signals, $outputs[0]);
            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $reset)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $reset)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $reset)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        if(defined $outputs[1]) {
            push(@lhs_signals, $outputs[1]);
            push(@rhs_signals, $outputs[0]);

            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );

            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector_ad, $not_string_ad );
        }
    }

    ##################################################
    # All DFF should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################


    ##################################################
    # All SDFF (veriosn SDFF_*) should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bSDFF_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".SI(", ".SE(", ".CK(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "DFF_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 4; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            elsif($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            push(@inputs, $in);
            if ( $i == 1) {
                push(@rhs_signals, $in);
            }
        }
        # Weird way of coding but just kept for consistency
        for(my $i = 5; $i <= 6; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #   $out = '';
                #   push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        #if($outputs[0] ne '') {
        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[3], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[3], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                # There is no inputs[2]. Hence empty string concatenation
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[3], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        else {
            $outputs[0] = $outputs[1] . "dummy";
            push(@lhs_signals, $outputs[0]);
            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[3], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[3], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[3], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        if(defined $outputs[1]) {
            push(@lhs_signals, $outputs[1]);
            push(@rhs_signals, $outputs[0]);

            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );

            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector_ad, $not_string_ad );
        }
    }

    ##################################################
    # All SDFF should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################

    ##################################################
    # All DLH (veriosn DLH_*) i.e. High enabled latch should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bDLH_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".G(", ".Q(", ".QN(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "DFF_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            elsif($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            push(@inputs, $in);
            if ( $i == 1 ) {
                push(@rhs_signals, $in);
            }
        }
        # Weird way of coding but just kept for consistency
        for(my $i = 3; $i <= 4; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #    #$out = '';
                #    #push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        #if($outputs[0] ne '') {
        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($clock, INPUT_SET_1, $inputs[1])\n" : "($outputs[0], ) = DFF_X1($clock, INPUT_SET_0, $inputs[1])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($clock, $inputs[0], $inputs[1])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        else {
            $outputs[0] = $outputs[1] . "dummy";
            push(@lhs_signals, $outputs[0]);
            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, $inputs[2])\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($clock, INPUT_SET_1, $inputs[1])\n" : "($outputs[0], ) = DFF_X1($clock, INPUT_SET_0, $inputs[1])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                #my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], $inputs[2])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($clock, $inputs[0], $inputs[1])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
            }
        }
        if(defined $outputs[1]) {
            push(@lhs_signals, $outputs[1]);
            push(@rhs_signals, $outputs[0]);
            
            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector, $not_string );

            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
            push(@not_vector_ad, $not_string_ad );
        }
    }

    ##################################################
    # All DLH should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################
    
    ##################################################
    # All DLL (veriosn DLL_*) i.e. low enabled latch should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bDLL_/ ) {
        my $next_line = '';
        my @inputs;
        my @outputs;
        chomp($line);
        my @dff_config= (".D(", ".GN(", ".Q(");
        $line = mytrim($line);
        my @DFF_Instantiation_Name = split(/ /, $line);
        print $ih "$DFF_Instantiation_Name[1]\n";
        #print "DFF_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print "Concatenated Line: #$line#\n";
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, $dff_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($dff_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($dff_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            if($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            elsif($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            push(@inputs, $in);
            if ( $i == 1 ) {
                push(@rhs_signals, $in);
            }
        }
        # Weird way of coding but just kept for consistency
        for(my $i = 3; $i <= 3; $i++) {
            my $out;
            my $out_index_start = index($line, $dff_config[$i-1]); 
            if($out_index_start == -1) {
                #if($i == 4) {
                #   out = '';
                #   push(@outputs, $out);
                #}
                push(@outputs, undef);
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($dff_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($dff_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                my $out_old = $out;
                $out = stripchars($out, $chars);
                print $mh "$out::$out_old\n";
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
            push(@outputs, $out);
        }

        if(defined $outputs[0]) {
            push(@lhs_signals, $outputs[0]);
            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
                $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[1] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[1])";
                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($clock, INPUT_SET_1, $blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($clock, INPUT_SET_0, $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad);
                push(@not_vector_ad, $not_string_ad);
            }
            else {
                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector, $dff_string );
                my $blif_reset_net = $inputs[1] . "dummy";
                my $not_string_ad = "$blif_reset_net = INV_X1($inputs[1])";
                my $dff_string_ad = "($outputs[0], ) = DFF_X1($clock, $inputs[0], $blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
                push(@dff_vector_ad, $dff_string_ad );
                push(@not_vector_ad, $not_string_ad );
            }
        }
#       Since No QN is present the following code will give concatenation warning in cases. Commented out
#        else {
#            $outputs[0] = $outputs[1] . "dummy";
#            push(@lhs_signals, $outputs[0]);
#            $in_flag = $inputs[0] eq "1'b1" ? 1 : 0;
#            if($inputs[0] eq "1'b1" or $inputs[0] eq "1'b0") {
#                my $dff_string = $inputs[0] eq "1'b1" ? "$outputs[0] = DFF(INPUT_SET_1)\n" : "$outputs[0] = DFF(INPUT_SET_0)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
#                push(@dff_vector, $dff_string );
#                my $dff_string_ad = $inputs[0] eq "1'b1" ? "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_1, blif_reset_net)\n" : "($outputs[0], ) = DFF_X1($inputs[1], INPUT_SET_0, blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
#                push(@dff_vector_ad, $dff_string_ad);
#            }
#            else {
#                my $dff_string = "$outputs[0] = DFF($inputs[0])\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
#                push(@dff_vector, $dff_string );
#                my $dff_string_ad = "($outputs[0], ) = DFF_X1($inputs[1], $inputs[0], blif_reset_net)\n"; ## \t\t# $DFF_Instantiation_Name[1]\n";
#                push(@dff_vector_ad, $dff_string_ad );
#            }
#        }
#        if(defined $outputs[1]) {
#            push(@lhs_signals, $outputs[1]);
#
#            my $not_string = "$outputs[1] = NOT($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
#            push(@not_vector, $not_string );
#
#            my $not_string_ad = "$outputs[1] = INV_X1($outputs[0])\n"; ## \t\t# QN of the Q of D Flipflop $DFF_Instantiation_Name[1]\n";
#            push(@not_vector_ad, $not_string_ad );
#        }
    }

    ##################################################
    # All DLH should be handled by this routine | DOUBLE CHECKED FOR DAVOODI
    ##################################################



    ##################################################
    # All TINV gates should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bTINV_X1\s/ ) {
        my $next_line = '';
        my @inputs;
        my $inputs_size;
        my $out = '';
        my @tinv_config = (".I(", ".EN(", ".ZN(");
        chomp($line);
        $line = mytrim($line);
        my @TINV_instantiation_name = split(/ /, $line);
        #print "TINV_X1 FOUND.\n";
        while( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= 2; $i++) {
            my $in_index_start = index($line, $tinv_config[$i-1]); my $in_index_stop = index(substr($line, $in_index_start + length($tinv_config[$i-1])), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length($tinv_config[$i-1]), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $in . "\n";
            }
            push(@inputs, $in);
            if ( $i == 1 ) {
                push(@rhs_signals, $in);
            }
        }
        # Very weird coding style here but as of now I am doing this for sake of unified flow
        for(my $i = 3; $i <= 3; $i++) {
            my $out_index_start = index($line, $tinv_config[$i-1]); 
            if($out_index_start == -1) {
                next;
            }
            my $out_index_stop = index(substr($line, $out_index_start + length($tinv_config[$i-1])), ")");
            #print "$out_index_start, $out_index_stop\n";
            $out = substr($line, $out_index_start + length($tinv_config[$i-1]), $out_index_stop); $out = mytrim($out); 
            if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
            }
        }
        # The follwoing random number generation is used to get name of some intermediate wires in the TINV translation 
        my $range = 10000;
        my $random_number = int(rand($range));
        my $intermediate_wire = "int" . $random_number;
        my $tinv_and_string =  $intermediate_wire . " = AND(" .  join(", ", @inputs) . ")\n";##\t\t# First level AND gate for TINV $TINV_instantiation_name[1]\n";
        push(@and_vector, $tinv_and_string);
        push(@not_vector, "$out = NOT($intermediate_wire)\n"); ##\t\t # Second level NOT gate for TINV $TINV_instantiation_name[1]\n");
       
        $inputs_size = scalar @inputs;
        my $tinv_and_string_ad =  $intermediate_wire . " = AND".$inputs_size."_X1(" .  join(", ", @inputs) . ")\n";##\t\t# First level AND gate for TINV $TINV_instantiation_name[1]\n";
        push(@and_vector_ad, $tinv_and_string_ad);
        push(@not_vector_ad, "$out = INV_X1($intermediate_wire)\n"); ##\t\t # Second level NOT gate for TINV $TINV_instantiation_name[1]\n");

    }

    ##################################################
    # All TINV gates should be handled by this routine
    ##################################################

    ##################################################
    # All INV gates should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bINV/ ) {
        my @words = split(/\s+/, $line);
        my $next_line = '';
        my $in = '';
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        #print "#$line#";
        #print "INV_X4 FOUND.\n";
        while( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        my $in_index_start = index($line, ".A("); my $in_index_stop = index(substr($line, $in_index_start + length(".A(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $in = substr($line, $in_index_start + length(".A("), $in_index_stop); $in = mytrim($in);
        if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
        }
        #print "From Line Number: $line_number, Pushed INPUT signal for INVERTER Instance $words[1]: $in\n";
        if($in eq "1'b0") {
            $in = "INPUT_SET_0";
        }
        elsif($in eq "1'b1") {
            $in = "INPUT_SET_1";
        }
        push(@rhs_signals, $in);
        #print "Current in pushed for INV: $in\n";
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out); #print $out . "\n";

        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        #print "Pushed OUTPUT signal for INVERTER Instance $words[1]: $out\n";
        push(@lhs_signals, $out);
        #print "$out = NOT($in)\n";
        push(@not_vector, "$out = NOT($in)\n");
        
        push(@not_vector_ad, "$out = INV_X1($in)\n");
    }
    
    ##################################################
    # All INV gates should be handled by this routine
    ##################################################

    ##################################################
    # All NAND gates should be handled by this routine
    ##################################################
    
    elsif ( $line =~ m/^\bNAND/ ) {
        my $next_line = '';
        my @inputs;
        my $inputs_size;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        my $nand_input_size = substr($line, length("NAND"), index($line, "_") - length("NAND"));
        # print $nand_input_size . "\n";
        # print "NAND2_X2 FOUND.\n";
        while( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= $nand_input_size; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            if($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            elsif($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            push(@inputs, $in);
            push(@rhs_signals, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        push(@lhs_signals, $out);
        my $nand_string =  $out . " = NAND(" .  join(", ", @inputs) . ")\n";
        push(@nand_vector, $nand_string );
        
        $inputs_size = scalar @inputs;
        my $nand_string_ad =  $out . " = NAND".$inputs_size."_X1(" .  join(", ", @inputs) . ")\n";
        push(@nand_vector_ad, $nand_string_ad );

    }

    ##################################################
    # All NAND gates should be handled by this routine
    ##################################################

    ##################################################
    # All AND gates should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bAND/ ) {
        my $next_line = '';
        my @inputs;
        my $inputs_size;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        my $and_input_size = substr($line, length("AND"), index($line, "_") - length("AND"));
        #print "AND2_X1 FOUND.\n";
        while( index($line, ";") == -1) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= $and_input_size; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
             if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            if($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            elsif($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            push(@inputs, $in);
            push(@rhs_signals, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        push(@lhs_signals, $out);
        my $and_string =  $out . " = AND(" .  join(", ", @inputs) . ")\n";
        push(@and_vector, $and_string );

        $inputs_size = scalar @inputs;
        my $and_string_ad =  $out . " = AND".$inputs_size."_X1(" .  join(", ", @inputs) . ")\n";
        push(@and_vector_ad, $and_string_ad );

    }

    ##################################################
    # All AND gates should be handled by this routine
    ##################################################

    ##################################################
    # All NOR gates should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bNOR/ ) {
        my $next_line = '';
        my @inputs;
        my $inputs_size;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        my $nor_input_size = substr($line, length("NOR"), index($line, "_") - length("NOR"));
        #print "NOR2_X1 FOUND.\n";
        while( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        for(my $i = 1; $i <= $nor_input_size; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            if($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            elsif($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            push(@inputs, $in);
            push(@rhs_signals, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        push(@lhs_signals, $out);
        my $nor_string =  $out . " = NOR(" .  join(", ", @inputs) . ")\n";
        push(@nor_vector, $nor_string );

        $inputs_size = scalar @inputs;
        my $nor_string_ad =  $out . " = NOR".$inputs_size."_X1(" .  join(", ", @inputs) . ")\n";
        push(@nor_vector_ad, $nor_string_ad );

    }

    ##################################################
    # All NOR gates should be handled by this routine
    ##################################################

    ##################################################
    # All OR gates should be handled by this routine
    ##################################################

    elsif ( $line =~ m/^\bOR/ ) {
        my $next_line = '';
        my @inputs;
        my $inputs_size;
        my $out = '';
        chomp($line);
        $line = mytrim($line);
        my $or_input_size = substr($line, length("OR"), index($line, "_") - length("OR"));
        #print "OR2_X1 FOUND.\n";
        while( index($line, ";") == -1 ) {
            $next_line = mytrim($file_handle->getline());
            $line = join(" ", $line, $next_line);
            #print "$line\n";
        }
        #print $line."\n";
        for(my $i = 1; $i <= $or_input_size; $i++) {
            my $in_index_start = index($line, ".A$i("); my $in_index_stop = index(substr($line, $in_index_start + length(".A$i(")), ")");
            #print "$in_index_start, $in_index_stop\n";
            my $in = substr($line, $in_index_start + length(".A$i("), $in_index_stop); $in = mytrim($in); 
            if(index($in, "/") != -1 or index($in, "[") != -1) {
                $in = stripchars($in, $chars);
                $in = substr($in, 0, length($in));
                #print $out . "\n";
            }
            if($in eq "1'b0") {
                $in = "INPUT_SET_0";
            }
            elsif($in eq "1'b1") {
                $in = "INPUT_SET_1";
            }
            push(@inputs, $in);
            push(@rhs_signals, $in);
        }
        my $out_index_start = index($line, ".ZN("); my $out_index_stop = index(substr($line, $out_index_start + length(".ZN(")), ")");
        #print "$in_index_start, $in_index_stop\n";
        $out = substr($line, $out_index_start + length(".ZN("), $out_index_stop); $out = mytrim($out);
        if(index($out, "/") != -1 or index($out, "[") != -1) {
                $out = stripchars($out, $chars);
                $out = substr($out, 0, length($out));
                #print $out . "\n";
        }
        push(@lhs_signals, $out);
        my $or_string =  $out . " = OR(" .  join(", ", @inputs) . ")\n";
        push(@or_vector, $or_string );

        $inputs_size = scalar @inputs;
        my $or_string_ad =  $out . " = OR".$inputs_size."_X1(" .  join(", ", @inputs) . ")\n";
        push(@or_vector_ad, $or_string_ad );

    }

    ##################################################
    # All OR gates should be handled by this routine
    ##################################################

#    else {
#        my $next_line = '';
#        chomp($line);
#        $line = mytrim($line);
#        while( index($line, ";") == -1 and $line !~ m/\bendmodule\b/i and length $line) {
#            $next_line = mytrim($file_handle->getline());
#            if ( length $next_line) {
#                $line = join(" ", $line, $next_line);
#            }
#        }
##        print $line."\n";
#        if ( $line !~ m/\bwire\s/ and $line !~ m/\btri\s/ and $line !~ m/\breg\s/ and $line!~ m/\bendmodule\b/ ) {
##            my $blackbox_module = substr($line, 0, index($line, " "));
#            my $blackbox_module = $line =~ s/\s.*//sr;
#            print $blackbox_module."\n";
#        }
#    }
#
}


close $mh;
######################### TO FIND UNIQUE RHS SIGNALS AND UNIQUE LHS SIGNALS ##########################
######################### ANY SIGNAL UNIQUE TO RHS SHOULD BE A PI           ##########################
######################### ANY SIGNAL UNIQUE TO lhs SHOULD BE A PO           ##########################

#if($in_flag ne "-1") {
#    my $in_add_string = $in_flag eq "1" ? "INPUT_SET_1" : "INPUT_SET_0";
#    push(@input_vector, $in_add_string);
#}

#print "The LHS Signals are: \n";

# TO avoid multiple input clock and reset signal
@input_vector = uniq @input_vector;


my @uniq_lhs_signals = uniq @lhs_signals;
foreach my $lhs_signal (@uniq_lhs_signals) {
    print "Unique LHS Signal: $lhs_signal\n";
}

#print "The RHS Signals are: \n";
my @uniq_rhs_signals = uniq @rhs_signals;
foreach my $rhs_signal (@uniq_rhs_signals) {
    print "Unique RHS Signal: $rhs_signal\n";
}

my %lhs = map{$_ => 1} @uniq_lhs_signals;
my %rhs = map{$_ => 1} @uniq_rhs_signals;
my %input_vector = map{$_ => 1} @input_vector;
my %output_vector = map{$_ => 1} @output_vector;

my @to_output_lhs = grep(!defined $rhs{$_}, @uniq_lhs_signals);
@to_output_lhs = grep(!defined $output_vector{$_}, @to_output_lhs);
print "To Output from LHS Unique: $_\n" foreach (@to_output_lhs);
push(@output_vector, @to_output_lhs);
print "\n\n";
my @to_input_rhs = grep(!defined $lhs{$_}, @uniq_rhs_signals);
@to_input_rhs = grep(!defined $input_vector{$_}, @to_input_rhs);
#print "To Input from RHS Unique: $_\n" foreach (@to_input_rhs);
push(@input_vector, @to_input_rhs);

######################### TO FIND UNIQUE RHS SIGNALS AND UNIQUE LHS SIGNALS ##########################
######################### ANY SIGNAL UNIQUE TO RHS SHOULD BE A PI           ##########################
######################### ANY SIGNAL UNIQUE TO lhs SHOULD BE A PO           ##########################


################ This part is to write into ISCAS89 format ############################

my $file_name = File::Spec->splitpath(+$file);
my $iscas_filename = join(".", substr($file_name, 0, length($file_name) - 2), "bench");
print "ISCAS File Name to write: $iscas_filename\n";
open(my $fh, '>', $iscas_filename) or die "Could not open file '$iscas_filename' $!";
my $and_gates = scalar @and_vector; my $or_gates = scalar @or_vector; my $nand_gates = scalar @nand_vector; my $nor_gates = scalar @nor_vector;
my $total_gates = $and_gates + $nand_gates + $or_gates + $nor_gates;

print $fh "# " . scalar @input_vector . " inputs\n";
print $fh "# " . scalar @output_vector . " outputs\n";
print $fh "# " . scalar @dff_vector . " D-type flipflops\n";
print $fh "# " . scalar @not_vector . " inverters\n";
print $fh "# $total_gates gates ($and_gates ANDs + $nand_gates NANDs + $or_gates ORs + $nor_gates NORs)\n";
print $fh "\n";
foreach my $input (@input_vector) {
    $input =~ s/\;//g;
    $input =  mytrim($input);
    if ($input =~ m/^\bblif_reset_net/) {
        next;
    }
    else {
        print $fh "INPUT($input)\n";
    }
}
print $fh "\n";
foreach my $output (@output_vector) {
    $output =~ s/\;//g;
    $output = mytrim($output);
    print $fh "OUTPUT($output)\n";
}
print $fh "\n";
foreach my $dff_string (@dff_vector) {
    print $fh "$dff_string";
}
print $fh "\n";
foreach my $not_string (@not_vector) {
    print $fh "$not_string";
}
print $fh "\n";
foreach my $and_string (@and_vector) {
    print $fh "$and_string";
}
print $fh "\n";
foreach my $or_string (@or_vector) {
    print $fh "$or_string";
}
print $fh "\n";
foreach my $nand_string (@nand_vector) {
    print $fh "$nand_string";
}
print $fh "\n";
foreach my $nor_string (@nor_vector) {
    print $fh "$nor_string";
}
#print $fh "\n";
close $fh;

################ This part is to write into ISCAS89 format ############################

######################## HANDLING VARIABLES OF ASSIGNMENT STATEMENTS FOR ISCAS89 FORMAT #######################

my @assign_keys = keys %assign_hash;
#print join("\n", @assign_keys);
my $replace_file = path($iscas_filename);
my $file_data = $replace_file->slurp_utf8;
#print $file_data;
foreach my $assign_key (@assign_keys) {
    #print "Key: ".$assign_key." and Data: ".$assign_hash{$assign_key}."\n";
    $file_data =~ s/$assign_key/$assign_hash{$assign_key}/g;
}
$replace_file->spew_utf8( $file_data );

######################## HANDLING VARIABLES OF ASSIGNMENT STATEMENTS FOR ISCAS89 FORMAT #######################


################ This part is to write into Davoodi format ############################

my $file_name_ad = File::Spec->splitpath(+$file);
my $iscas_filename_ad = join(".", substr($file_name_ad, 0, length($file_name_ad) - 2), "bench_ad");
print "ISCAS Davoodi File Name to write: $iscas_filename_ad\n";
open(my $fh_ad, '>', $iscas_filename_ad) or die "Could not open file '$iscas_filename_ad' $!";

#my $and_gates = scalar @and_vector; my $or_gates = scalar @or_vector; my $nand_gates = scalar @nand_vector; my $nor_gates = scalar @nor_vector; 
my $not_gates = scalar @not_vector;
my $total_instances = $total_gates + $not_gates + scalar @dff_vector;

print $fh_ad "## No of instances $total_instances\n";
print $fh_ad "## No of Nets 1\n";
print $fh_ad "## No of Inputs " . scalar @input_vector . "\n";
print $fh_ad "## No of Outputs " . scalar @output_vector . "\n";

print $fh_ad "\n\n\n\n";

foreach my $input (@input_vector) {
    $input =~ s/\;//g;
    $input =  mytrim($input);
    print $fh_ad "INPUT($input)\n";
}
foreach my $output (@output_vector) {
    $output =~ s/\;//g;
    $output = mytrim($output);
    print $fh_ad "OUTPUT($output)\n";
}
print $fh_ad "\n\n";
foreach my $dff_string_ad (@dff_vector_ad) {
    print $fh_ad "$dff_string_ad";
}
#print $fh_ad "\n";
foreach my $not_string_ad (@not_vector_ad) {
    print $fh_ad "$not_string_ad";
}
#print $fh_ad "\n";
foreach my $and_string_ad (@and_vector_ad) {
    print $fh_ad "$and_string_ad";
}
#print $fh_ad "\n";
foreach my $or_string_ad (@or_vector_ad) {
    print $fh_ad "$or_string_ad";
}
#print $fh_ad "\n";
foreach my $nand_string_ad (@nand_vector_ad) {
    print $fh_ad "$nand_string_ad";
}
#print $fh_ad "\n";
foreach my $nor_string_ad (@nor_vector_ad) {
    print $fh_ad "$nor_string_ad";
}
#print $fh_ad "\n";

close $fh_ad;

my $assign_filename_ad = join(".", substr($file_name_ad, 0, length($file_name_ad) - 2), "assign");
print "Assign Davoodi File Name to write: $assign_filename_ad\n";
open(my $fh_ad_as, '>', $assign_filename_ad) or die "Could not open file '$assign_filename_ad' $!";
print $fh_ad_as "$reset  $polarity";
close $fh_ad_as;

################ This part is to write into Davoodi format ############################

######################## HANDLING VARIABLES OF ASSIGNMENT STATEMENTS FOR ISCAS DAVOODI #######################

my $replace_file_ad = path($iscas_filename_ad);
my $file_data_ad = $replace_file_ad->slurp_utf8;
#print $file_data;
foreach my $assign_key (@assign_keys) {
    #print "Key: ".$assign_key." and Data: ".$assign_hash{$assign_key}."\n";
    $file_data_ad =~ s/$assign_key/$assign_hash{$assign_key}/g;
}
$replace_file_ad->spew_utf8( $file_data_ad );

######################## HANDLING VARIABLES OF ASSIGNMENT STATEMENTS FOR ISCAS DAVOODI #######################


######################## SUMMARY OF SIGNAL NAMES REPALCED FOR THE ASSIGN STATEMENTS ###################

print "\n\n";
print "Following  signal names changed as they belong to assign statements:\n";
foreach my $assign_key (@assign_keys) {
    print "$assign_key REPLACED BY => $assign_hash{$assign_key}\n";
}

######################## SUMMARY OF SIGNAL NAMES REPALCED FOR THE ASSIGN STATEMENTS ###################
#

## Trim function for removing any leading or trailing white spaces
sub mytrim($) {
  my $string = shift;
  if ( length $string ) {
      $string =~ s/^\s+//;
      $string =~ s/\s+$//;
  }
  return $string;
}

# This is a subroutine which removes every unnecessary charecter from a string including [] {} () \ / so that strings become ISCAS compatible
sub stripchars {
    my ($s, $chars) = @_;
    my $s_old = $s;
    $s =~ s/[$chars]//g;
    $s =~ s/\\//g;
    $s =~ s/\]//g;
	if(index($s, "{") != -1 or index($s, ")") != -1) {
		$s =~ s/{//g;
		$s =~ s/\}//g;
		$s =~ s/\(//g;
		$s =~ s/\)//g;
	}
	$s =~ s/1'b0,//g;
    #print "$s::$s_old\n";
    return $s;
}
