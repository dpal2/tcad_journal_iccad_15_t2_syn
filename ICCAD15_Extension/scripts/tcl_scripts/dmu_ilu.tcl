# All setup related things. They are collected from the following three files:
# 1. project_io_cfg.scr 2. target_lib.scr in the design/sys/synopsys/script directory
# 3. user_cfg.scr in the design/sys/iop/dmu

if {[info exists env(DESIGN_HOME)]} {
    set value $env(DESIGN_HOME)
} else {
    set env(DESIGN_HOME) "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension"
}

if {[info exists env(RTL_ROOT)]} {
    set value $env(RTL_ROOT)
} else {
    set env(RTL_ROOT) [concat $env(DESIGN_HOME)/rtl]
}

set output_syn_dir "../synthesized_netlist"

set search_path $env(RTL_ROOT)
set gate_lib_file "/home/debjit/Software/NangateOpenCellLibrary/ccs/NangateOpenCellLibrary.db"
set link_library $gate_lib_file

set wire_library_file {/home/debjit/Software/NangateOpenCellLibrary/ccs/NangateOpenCellLibrary.db}
set wire_library NangateOpenCellLibrary
set wire_model_name 05x05
set wireload_mode top

set link_library [concat * $link_library]
set symbol_library {}

set synthetic_library {dw_foundation.sldb}
set link_path [concat $link_library $synthetic_library]

set max_transitions 0.15
set max_fanout  6

set default_input_delay 0.15
set default_output_delay 0.2
set critical_range 0.30

set num_of_path 50

set top_module dmu_ilu

set compile_effort "medium"
set compile_flatten_all 1
set compile_no_new_cells_at_top_level false

set default_clk l1clk
set default_clk_freq 350
set default_setup_skew 0.0
set default_hold_skew 0.0
set default_clk_transition 0.05

set ideal_net_list {}
# The following setup has been done from run.scr from design/sys/synopsys/script

set hdlin_auto_save_templates false
set verilogout_single_bit false
set hdlout_internal_busses true
set bus_naming_style {%s[%d]}
set bus_inference_style $bus_naming_style

# All setup related things end here

# Read Designs

analyze -library WORK -format verilog {

    uncore/dmu_ilu/dmu_ilu_cib_defines.h
    uncore/dmu_ilu/pcie_csr_defines.h
    uncore/dmu_ilu/pcie.h
    uncore/dmu_ilu/dmu.h

    uncore/dmu_ilu/dmu_ilu_cib_addr_decode.v
    uncore/dmu_ilu/dmu_ilu_cib_cim.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_diagnos_entry.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_diagnos.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_int_en_entry.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_int_en.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_log_en_entry.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_log_en.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_log_err_entry.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_ilu_log_err.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_pec_int_en_entry.v
    uncore/dmu_ilu/dmu_ilu_cib_csr_pec_int_en.v
    uncore/dmu_ilu/dmu_ilu_cib_csrpipe_5.v
    uncore/dmu_ilu/dmu_ilu_cib_csrpipe_6.v
    uncore/dmu_ilu/dmu_ilu_cib_csr.v
    uncore/dmu_ilu/dmu_ilu_cib_default_grp.v
    uncore/dmu_ilu/dmu_ilu_cib_stage_mux_only.v
    uncore/dmu_ilu/dmu_ilu_cib.v
    uncore/dmu_ilu/dmu_ilu_eil_bufmgr.v
    uncore/dmu_ilu/dmu_ilu_eil_datafsm.v
    uncore/dmu_ilu/dmu_ilu_eil_rcdbldr.v
    uncore/dmu_ilu/dmu_ilu_eil_relgen.v
    uncore/dmu_ilu/dmu_ilu_eil.v
    uncore/dmu_ilu/dmu_ilu_eil_xfrfsm.v
    uncore/dmu_ilu/dmu_ilu_iil_bufmgr.v
    uncore/dmu_ilu/dmu_ilu_iil_crdtcnt.v
    uncore/dmu_ilu/dmu_ilu_iil_parchk.v
    uncore/dmu_ilu/dmu_ilu_iil_rcdbldr.v
    uncore/dmu_ilu/dmu_ilu_iil.v
    uncore/dmu_ilu/dmu_ilu_iil_xfrfsm.v
    uncore/dmu_ilu/dmu_ilu_isb.v
    uncore/dmu_ilu/dmu_ilu.v

    uncore/dmu_ilu/pcie_common_srq_qci.v
    uncore/dmu_ilu/pcie_common_srq_qcp.v
    uncore/dmu_ilu/pcie_common_srq_qdp.v
    uncore/dmu_ilu/pcie_common_srq.v

    uncore/dmu_ilu/pcie_common_dcd.v
    
    uncore/dmu_ilu/pcie_common_dcs_sdp.v
    uncore/dmu_ilu/pcie_common_dcs_osm.v
    uncore/dmu_ilu/pcie_common_dcs_ism.v
    uncore/dmu_ilu/pcie_common_dcs.v

    uncore/dmu_ilu/pcie_common_dcc.v
    
    uncore/dmu_ilu/pcie_common_dcb.v

    uncore/dmu_ilu/pcie_dcm_daemon.v

    uncore/dmu_ilu/csr_sw.v
    
    libs/cl_a1.v

}


elaborate $top_module -architecture verilog -library WORK
check_design -multiple_designs

current_design $top_module
link

# clock setup from run.scr from design/sys/synopsys/script

set clk_period [expr 1000.0 / $default_clk_freq / 1.0]
set high_time [expr $clk_period / 2.0]
create_clock $default_clk -period $clk_period -waveform [list 0 $high_time] -name $default_clk
set_clock_uncertainty -setup $default_setup_skew [find clock $default_clk]
set_clock_uncertainty -hold  $default_hold_skew [find clock $default_clk]
set_clock_transition $default_clk_transition [find clock $default_clk]
set_dont_touch_network $default_clk

set non_ideal_inputs [list]
set ideal_inputs [list]
foreach_in_collection input_object [all_inputs] {
   set input_name [get_object_name $input_object]
   set input_is_ideal [lsearch -exact $ideal_net_list $input_name]
   if {$input_is_ideal == -1} {
      lappend non_ideal_inputs $input_name
   } else {
      lappend ideal_inputs $input_name
   }
}

echo $ideal_inputs
echo $non_ideal_inputs

set_input_delay $default_input_delay -clock $default_clk $non_ideal_inputs
set_output_delay $default_output_delay -clock $default_clk [all_outputs]

set compile_seqmap_propagate_constants false
set dont_touch_nets_with_size_only_cells false

if {[info exists compile_flatten_all] && ($compile_flatten_all == 1)} {
    ungroup -flatten -all
}

#if {[info exists compile_effort]} {
#    compile -boundary_optimization -exact_map -ungroup_all -map_effort $compile_effort
#} else {
#    compile -boundary_optimization -exact_map
#}

if {[info exists compile_effort]} {
    compile -boundary_optimization -ungroup_all -map_effort $compile_effort
} else {
    compile -boundary_optimization
}

write_file -format verilog -output [format "%s%s%s%s" $output_syn_dir/ $top_module/ $top_module _flat.v]
write_sdf [format "%s%s%s%s" $output_syn_dir/ $top_module/ $top_module _base_gate.sdf]

set unmapped_designs [get_designs -filter "is_unmapped == true" $top_module]
if {  [sizeof_collection $unmapped_designs] != 0 } {
   echo "****************************************************"
   echo "* ERROR!!!! Compile finished with unmapped logic.  *"
   echo "****************************************************"
   echo $unmapped_designs
   echo "****************************************************"
   quit
}

quit
