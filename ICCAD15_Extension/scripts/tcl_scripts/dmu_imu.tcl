# All setup related things. They are collected from the following three files:
# 1. project_io_cfg.scr 2. target_lib.scr in the design/sys/synopsys/script directory
# 3. user_cfg.scr in the design/sys/iop/dmu

if {[info exists env(DESIGN_HOME)]} {
    set value $env(DESIGN_HOME)
} else {
    set env(DESIGN_HOME) "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension"
}

if {[info exists env(RTL_ROOT)]} {
    set value $env(RTL_ROOT)
} else {
    set env(RTL_ROOT) [concat $env(DESIGN_HOME)/rtl]
}

set output_syn_dir "../synthesized_netlist"

set search_path $env(RTL_ROOT)
set gate_lib_file "/home/debjit/Software/NangateOpenCellLibrary/ccs/NangateOpenCellLibrary.db"
set link_library $gate_lib_file

set wire_library_file {/home/debjit/Software/NangateOpenCellLibrary/ccs/NangateOpenCellLibrary.db}
set wire_library NangateOpenCellLibrary
set wire_model_name 05x05
set wireload_mode top

set link_library [concat * $link_library]
set symbol_library {}

set synthetic_library {dw_foundation.sldb}
set link_path [concat $link_library $synthetic_library]

set max_transitions 0.15
set max_fanout  6

set default_input_delay 0.15
set default_output_delay 0.2
set critical_range 0.30

set num_of_path 50

set root_module dmu_dmc
set top_module dmu_imu

set compile_effort "medium"
set compile_flatten_all 1
set compile_no_new_cells_at_top_level false

set default_clk clk
set default_clk_freq 350
set default_setup_skew 0.0
set default_hold_skew 0.0
set default_clk_transition 0.05

set ideal_net_list {}
# The following setup has been done from run.scr from design/sys/synopsys/script

set hdlin_auto_save_templates false
set verilogout_single_bit false
set hdlout_internal_busses true
set bus_naming_style {%s[%d]}
set bus_inference_style $bus_naming_style

# All setup related things end here

# Read Designs

analyze -library WORK -format verilog {

	uncore/dmu_dmc/dmu_imu/dmu.h
	uncore/dmu_dmc/dmu_imu/dmu_imu.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_defines.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_defines.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_defines.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_defines.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_defines.h
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_defines.h
	uncore/dmu_dmc/dmu_imu/pcie.h
	uncore/dmu_dmc/dmu_imu/pcie_csr_defines.h

    uncore/dmu_dmc/dmu_imu/csr_sw.v
    uncore/dmu_dmc/dmu_imu/pcie_dcm_daemon.v
    uncore/dmu_dmc/dmu_imu/pcie_common_frr_arbiter.v
    uncore/dmu_dmc/dmu_imu/dmu_common_simple_fifo.v

    uncore/dmu_dmc/dmu_imu/dmu_imu_dbg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_dms.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_base_address_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_base_address.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_head_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_head.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_tail_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr_eq_tail.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csrpipe_109.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csrpipe_5.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_fsm.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_eqs.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs_arb.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs_csm.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs_gc_cnt.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs_gc_fsm.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs_gc.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_gcs.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_dmc_interrupt_mask_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_dmc_interrupt_mask_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_eqs_error_log_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_eqs_error_log_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_error_log_en_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_error_log_en_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_int_en_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_int_en_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_logged_error_status_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_logged_error_status_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cnt0_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cnt0.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cnt1_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cnt1.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cntrl_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_perf_cntrl.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_rds_error_log_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_rds_error_log_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_scs_error_log_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_imu_scs_error_log_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_mem_64_pcie_offset_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_mem_64_pcie_offset_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_msi_32_addr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_msi_32_addr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_msi_64_addr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr_msi_64_addr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csrpipe_15.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csrpipe_5.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ics.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_irs.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_20_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_20.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_21_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_21.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_22_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_22.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_23_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_23.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_24_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_24.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_25_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_25.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_26_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_26.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_27_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_27.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_28_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_28.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_29_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_29.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_30_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_30.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_31_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_31.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_32_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_32.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_33_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_33.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_34_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_34.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_35_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_35.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_36_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_36.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_37_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_37.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_38_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_38.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_39_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_39.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_40_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_40.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_41_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_41.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_42_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_42.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_43_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_43.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_44_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_44.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_45_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_45.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_46_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_46.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_47_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_47.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_48_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_48.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_49_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_49.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_50_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_50.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_51_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_51.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_52_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_52.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_53_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_53.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_54_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_54.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_55_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_55.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_56_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_56.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_57_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_57.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_58_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_58.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_59_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_59.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_62_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_62.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_63_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_mapping_63.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_retry_timer_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr_interrupt_retry_timer.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csrpipe_5.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csrpipe_87.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_fsm.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_iss.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_ors.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_a_int_clr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_a_int_clr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_b_int_clr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_b_int_clr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_c_int_clr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_c_int_clr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_d_int_clr_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr_int_d_int_clr_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csrpipe_1.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csrpipe_5.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_intx.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_cor_mapping_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_cor_mapping.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_fatal_mapping_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_fatal_mapping.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_nonfatal_mapping_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_err_nonfatal_mapping.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csrpipe_1.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csrpipe_6.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_pme_to_ack_mapping_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_pme_to_ack_mapping.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_pm_pme_mapping_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr_pm_pme_mapping.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mess.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_mondo.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_addr_decode.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csr_int_mondo_data_0_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csr_int_mondo_data_0_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csr_int_mondo_data_1_reg_entry.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csr_int_mondo_data_1_reg.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csrpipe_1.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csrpipe_3.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_csr.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_stage_2_default_grp.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi_stage_mux_only.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds_msi.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rds.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_rss.v
	uncore/dmu_dmc/dmu_imu/dmu_imu_scs.v
	uncore/dmu_dmc/dmu_imu/dmu_imu.v

}


elaborate $top_module -architecture verilog -library WORK
check_design -multiple_designs

current_design $top_module
link

# clock setup from run.scr from design/sys/synopsys/script

set clk_period [expr 1000.0 / $default_clk_freq / 1.0]
set high_time [expr $clk_period / 2.0]
create_clock $default_clk -period $clk_period -waveform [list 0 $high_time] -name $default_clk
set_clock_uncertainty -setup $default_setup_skew [find clock $default_clk]
set_clock_uncertainty -hold  $default_hold_skew [find clock $default_clk]
set_clock_transition $default_clk_transition [find clock $default_clk]
set_dont_touch_network $default_clk

set non_ideal_inputs [list]
set ideal_inputs [list]
foreach_in_collection input_object [all_inputs] {
   set input_name [get_object_name $input_object]
   set input_is_ideal [lsearch -exact $ideal_net_list $input_name]
   if {$input_is_ideal == -1} {
      lappend non_ideal_inputs $input_name
   } else {
      lappend ideal_inputs $input_name
   }
}

echo $ideal_inputs
echo $non_ideal_inputs

set_input_delay $default_input_delay -clock $default_clk $non_ideal_inputs
set_output_delay $default_output_delay -clock $default_clk [all_outputs]

set compile_seqmap_propagate_constants false
set dont_touch_nets_with_size_only_cells false

if {[info exists compile_flatten_all] && ($compile_flatten_all == 1)} {
    ungroup -flatten -all
}

if {[info exists compile_effort]} {
    compile -boundary_optimization -ungroup_all -map_effort $compile_effort
} else {
    compile -boundary_optimization
}

#if {[info exists compile_effort]} {
#    compile -boundary_optimization -exact_map -ungroup_all -map_effort $compile_effort
#} else {
#    compile -boundary_optimization -exact_map
#}


write_file -format verilog -output [format "%s%s%s%s%s" $output_syn_dir/ $root_module/ $top_module/ $top_module _flat.v]

write_sdf [format "%s%s%s%s%s" $output_syn_dir/ $root_module/ $top_module/ $top_module _base_gate.sdf]

set unmapped_designs [get_designs -filter "is_unmapped == true" $top_module]
if {  [sizeof_collection $unmapped_designs] != 0 } {
   echo "****************************************************"
   echo "* ERROR!!!! Compile finished with unmapped logic.  *"
   echo "****************************************************"
   echo $unmapped_designs
   echo "****************************************************"
   quit
}

quit
