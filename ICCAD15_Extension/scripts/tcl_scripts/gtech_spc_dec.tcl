# All setup related things. They are collected from the following three files:
# 1. project_io_cfg.scr 2. target_lib.scr in the design/sys/synopsys/script directory
# 3. user_cfg.scr in the design/sys/iop/dmu

if {[info exists env(DESIGN_HOME)]} {
    set value $env(DESIGN_HOME)
} else {
    set env(DESIGN_HOME) "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension"
}

if {[info exists env(RTL_ROOT)]} {
    set value $env(RTL_ROOT)
} else {
    set env(RTL_ROOT) [concat $env(DESIGN_HOME)/rtl]
}

set output_syn_dir "../synthesized_netlist"

set search_path $env(RTL_ROOT)
set gate_lib_file "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/library_folder/gtech.db"
set link_library $gate_lib_file

set wire_library_file {/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/library_folder/gtech.db}
set wire_library gtech
set wire_model_name 05x05
set wireload_mode top

set link_library [concat * $link_library]
set symbol_library {}

set synthetic_library {dw_foundation.sldb}
set link_path [concat $link_library $synthetic_library]

set max_transitions 0.15
set max_fanout  6

set default_input_delay 0.15
set default_output_delay 0.2
set critical_range 0.30

set num_of_path 50

set top_module dec

set compile_effort "medium"
set compile_flatten_all 1
set compile_no_new_cells_at_top_level false

set default_clk l2clk
set default_clk_freq 1400
set default_setup_skew 0.0
set default_hold_skew 0.0
set default_clk_transition 0.05

set ideal_net_list {}
# The following setup has been done from run.scr from design/sys/synopsys/script

set hdlin_auto_save_templates false
set verilogout_single_bit false
set hdlout_internal_busses true
set bus_naming_style {%s[%d]}
set bus_inference_style $bus_naming_style

set dump_dir [concat $output_syn_dir/$top_module]
if {![file exists $dump_dir]} {
    sh mkdir -pv $dump_dir
}

# All setup related things end here

# Read Designs

analyze -library WORK -format verilog {

    libs/cl_a1.behV
    libs/cl_dp1.behV
    libs/cl_rtl_ext.v
    libs/cl_sc1.behV
    libs/cl_u1.behV

    core/spc_dec/dec_dcd_ctl.v
    core/spc_dec/dec_ded_ctl.v
    core/spc_dec/dec_del_ctl.v
    core/spc_dec/dec.v
}


elaborate $top_module -architecture verilog -library WORK
check_design -multiple_designs

current_design $top_module
link

# clock setup from run.scr from design/sys/synopsys/script

set clk_period [expr 1000.0 / $default_clk_freq / 1.0]
set high_time [expr $clk_period / 2.0]
create_clock $default_clk -period $clk_period -waveform [list 0 $high_time] -name $default_clk
set_clock_uncertainty -setup $default_setup_skew [find clock $default_clk]
set_clock_uncertainty -hold  $default_hold_skew [find clock $default_clk]
set_clock_transition $default_clk_transition [find clock $default_clk]
set_dont_touch_network $default_clk

set non_ideal_inputs [list]
set ideal_inputs [list]
foreach_in_collection input_object [all_inputs] {
   set input_name [get_object_name $input_object]
   set input_is_ideal [lsearch -exact $ideal_net_list $input_name]
   if {$input_is_ideal == -1} {
      lappend non_ideal_inputs $input_name
   } else {
      lappend ideal_inputs $input_name
   }
}

echo $ideal_inputs
echo $non_ideal_inputs

set_input_delay $default_input_delay -clock $default_clk $non_ideal_inputs
set_output_delay $default_output_delay -clock $default_clk [all_outputs]

set compile_seqmap_propagate_constants false
set dont_touch_nets_with_size_only_cells false

set_dont_use {gtech/GTECH_ADD_ABC}
set_dont_use {gtech/GTECH_ADD_AB}
set_dont_use {gtech/GTECH_AND_NOT}
set_dont_use {gtech/GTECH_AO21}
set_dont_use {gtech/GTECH_AO22}
set_dont_use {gtech/GTECH_AOI21}
set_dont_use {gtech/GTECH_AOI222}
set_dont_use {gtech/GTECH_AOI22}
set_dont_use {gtech/GTECH_AOI2N2}
set_dont_use {gtech/GTECH_BUF}
set_dont_use {gtech/GTECH_FD14}
set_dont_use {gtech/GTECH_FD18}
set_dont_use {gtech/GTECH_FD1S}
set_dont_use {gtech/GTECH_FD24}
set_dont_use {gtech/GTECH_FD28}
set_dont_use {gtech/GTECH_FD2S}
set_dont_use {gtech/GTECH_FD34}
set_dont_use {gtech/GTECH_FD38}
set_dont_use {gtech/GTECH_FD3S}
set_dont_use {gtech/GTECH_FD3}
set_dont_use {gtech/GTECH_FD44}
set_dont_use {gtech/GTECH_FD48}
set_dont_use {gtech/GTECH_FD4S}
set_dont_use {gtech/GTECH_FJK1S}
set_dont_use {gtech/GTECH_FJK1}
set_dont_use {gtech/GTECH_FJK2S}
set_dont_use {gtech/GTECH_FJK2}
set_dont_use {gtech/GTECH_FJK3S}
set_dont_use {gtech/GTECH_FJK3}
set_dont_use {gtech/GTECH_INBUF}
set_dont_use {gtech/GTECH_INOUTBUF}
set_dont_use {gtech/GTECH_ISO0_EN0}
set_dont_use {gtech/GTECH_ISO0_EN1}
set_dont_use {gtech/GTECH_ISO1_EN0}
set_dont_use {gtech/GTECH_ISO1_EN1}
set_dont_use {gtech/GTECH_ISOLATCH_EN0}
set_dont_use {gtech/GTECH_ISOLATCH_EN1}
set_dont_use {gtech/GTECH_LD1}
set_dont_use {gtech/GTECH_LD2_1}
set_dont_use {gtech/GTECH_LD2}
set_dont_use {gtech/GTECH_LD3}
set_dont_use {gtech/GTECH_LD4_1}
set_dont_use {gtech/GTECH_LD4}
set_dont_use {gtech/GTECH_LSR0}
set_dont_use {gtech/GTECH_MAJ23}
set_dont_use {gtech/GTECH_MUX2}
set_dont_use {gtech/GTECH_MUX4}
set_dont_use {gtech/GTECH_MUX8}
set_dont_use {gtech/GTECH_MUXI2}
set_dont_use {gtech/GTECH_OA21}
set_dont_use {gtech/GTECH_OA22}
set_dont_use {gtech/GTECH_OAI21}
set_dont_use {gtech/GTECH_OAI22}
set_dont_use {gtech/GTECH_OAI2N2}
set_dont_use {gtech/GTECH_ONE}
set_dont_use {gtech/GTECH_OR_NOT}
set_dont_use {gtech/GTECH_OUTBUF}
set_dont_use {gtech/GTECH_TBUF}
set_dont_use {gtech/GTECH_XNOR2}
set_dont_use {gtech/GTECH_XNOR3}
set_dont_use {gtech/GTECH_XNOR4}
set_dont_use {gtech/GTECH_XOR2}
set_dont_use {gtech/GTECH_XOR3}
set_dont_use {gtech/GTECH_XOR4}
set_dont_use {gtech/GTECH_ZERO}
set_dont_use {gtech/GTECH_AND3}
set_dont_use {gtech/GTECH_AND4}
set_dont_use {gtech/GTECH_AND5}
set_dont_use {gtech/GTECH_AND8}
set_dont_use {gtech/GTECH_NAND3}
set_dont_use {gtech/GTECH_NAND4}
set_dont_use {gtech/GTECH_NAND5}
set_dont_use {gtech/GTECH_NAND8}
set_dont_use {gtech/GTECH_NOR3}
set_dont_use {gtech/GTECH_NOR4}
set_dont_use {gtech/GTECH_NOR5}
set_dont_use {gtech/GTECH_NOR8}
set_dont_use {gtech/GTECH_OR3}
set_dont_use {gtech/GTECH_OR4}
set_dont_use {gtech/GTECH_OR5}
set_dont_use {gtech/GTECH_OR8}


if {[info exists compile_flatten_all] && ($compile_flatten_all == 1)} {
    ungroup -flatten -all
}

#if {[info exists compile_effort]} {
#    compile -boundary_optimization -exact_map -ungroup_all -map_effort $compile_effort
#} else {
#    compile -boundary_optimization -exact_map
#}

if {[info exists compile_effort]} {
    compile -boundary_optimization -ungroup_all -map_effort $compile_effort
} else {
    compile -boundary_optimization
}

write_file -format verilog -output [format "%s%s%s" $dump_dir/ $top_module _flat_gtech.v]

set unmapped_designs [get_designs -filter "is_unmapped == true" $top_module]
if {  [sizeof_collection $unmapped_designs] != 0 } {
   echo "****************************************************"
   echo "* ERROR!!!! Compile finished with unmapped logic.  *"
   echo "****************************************************"
   echo $unmapped_designs
   echo "****************************************************"
   quit
}

quit
