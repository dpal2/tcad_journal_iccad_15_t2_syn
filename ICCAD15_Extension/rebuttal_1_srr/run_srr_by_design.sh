#!/bin/sh


#########################################################
# AUTO-GENERATED SRR SCRIPT AT: 21-JUL-2018 03:50:12 AM #
#########################################################


###############################################################
##### State restoration ratio calculation for design: PMU #####
###############################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/HybrSel/pmu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_HybrSel.vcd pmu 1.0 HybrSel 256 300 1 || true

mv pmu_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/HybrSel/pmu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_HybrSel.vcd pmu 1.0 HybrSel 128 300 1 || true

mv pmu_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/HybrSel/pmu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_HybrSel.vcd pmu 1.0 HybrSel 64 300 1 || true

mv pmu_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/HybrSel/pmu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_HybrSel.vcd pmu 1.0 HybrSel 32 300 1 || true

mv pmu_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: SIGSET_1 <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/SigSeT_1/pmu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_SigSeT_1.vcd pmu 1.0 SigSeT_1 256 300 1 || true

mv pmu_SigSeT_1_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/SigSeT_1/pmu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_SigSeT_1.vcd pmu 1.0 SigSeT_1 128 300 1 || true

mv pmu_SigSeT_1_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/SigSeT_1/pmu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_SigSeT_1.vcd pmu 1.0 SigSeT_1 64 300 1 || true

mv pmu_SigSeT_1_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/SigSeT_1/pmu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_SigSeT_1.vcd pmu 1.0 SigSeT_1 32 300 1 || true

mv pmu_SigSeT_1_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/pmu/PRankNetlist/pmu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_PRankNetlist.vcd pmu 1.0 PRankNetlist 256 300 1 || true

mv pmu_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/pmu/PRankNetlist/pmu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_PRankNetlist.vcd pmu 1.0 PRankNetlist 128 300 1 || true

mv pmu_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/pmu/PRankNetlist/pmu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_PRankNetlist.vcd pmu 1.0 PRankNetlist 64 300 1 || true

mv pmu_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/pmu/PRankNetlist/pmu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_PRankNetlist.vcd pmu 1.0 PRankNetlist 32 300 1 || true

mv pmu_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




##########################################################################
##### State restoration ratio calculation for design: MCU_RDPCTL_CTL #####
##########################################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/HybrSel/mcu_rdpctl_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_HybrSel.vcd mcu_rdpctl_ctl 2.5 HybrSel 256 300 1 || true

mv mcu_rdpctl_ctl_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/HybrSel/mcu_rdpctl_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_HybrSel.vcd mcu_rdpctl_ctl 2.5 HybrSel 128 300 1 || true

mv mcu_rdpctl_ctl_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/HybrSel/mcu_rdpctl_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_HybrSel.vcd mcu_rdpctl_ctl 2.5 HybrSel 64 300 1 || true

mv mcu_rdpctl_ctl_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/HybrSel/mcu_rdpctl_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_HybrSel.vcd mcu_rdpctl_ctl 2.5 HybrSel 32 300 1 || true

mv mcu_rdpctl_ctl_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: SIGSET_1 <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/SigSeT_1/mcu_rdpctl_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_SigSeT_1.vcd mcu_rdpctl_ctl 2.5 SigSeT_1 256 300 1 || true

mv mcu_rdpctl_ctl_SigSeT_1_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/SigSeT_1/mcu_rdpctl_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_SigSeT_1.vcd mcu_rdpctl_ctl 2.5 SigSeT_1 128 300 1 || true

mv mcu_rdpctl_ctl_SigSeT_1_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/SigSeT_1/mcu_rdpctl_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_SigSeT_1.vcd mcu_rdpctl_ctl 2.5 SigSeT_1 64 300 1 || true

mv mcu_rdpctl_ctl_SigSeT_1_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/mcu_rdpctl_ctl/SigSeT_1/mcu_rdpctl_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_SigSeT_1.vcd mcu_rdpctl_ctl 2.5 SigSeT_1 32 300 1 || true

mv mcu_rdpctl_ctl_SigSeT_1_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/mcu_rdpctl_ctl/PRankNetlist/mcu_rdpctl_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_PRankNetlist.vcd mcu_rdpctl_ctl 2.5 PRankNetlist 256 300 1 || true

mv mcu_rdpctl_ctl_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/mcu_rdpctl_ctl/PRankNetlist/mcu_rdpctl_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_PRankNetlist.vcd mcu_rdpctl_ctl 2.5 PRankNetlist 128 300 1 || true

mv mcu_rdpctl_ctl_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/mcu_rdpctl_ctl/PRankNetlist/mcu_rdpctl_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_PRankNetlist.vcd mcu_rdpctl_ctl 2.5 PRankNetlist 64 300 1 || true

mv mcu_rdpctl_ctl_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/mcu_rdpctl_ctl/PRankNetlist/mcu_rdpctl_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/mcu_rdpctl_ctl_PRankNetlist.vcd mcu_rdpctl_ctl 2.5 PRankNetlist 32 300 1 || true

mv mcu_rdpctl_ctl_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




###################################################################
##### State restoration ratio calculation for design: DMU_DSN #####
###################################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/HybrSel/dmu_dsn_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_HybrSel.vcd dmu_dsn 3.0 HybrSel 256 300 1 || true

mv dmu_dsn_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/HybrSel/dmu_dsn_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_HybrSel.vcd dmu_dsn 3.0 HybrSel 128 300 1 || true

mv dmu_dsn_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/HybrSel/dmu_dsn_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_HybrSel.vcd dmu_dsn 3.0 HybrSel 64 300 1 || true

mv dmu_dsn_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/HybrSel/dmu_dsn_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_HybrSel.vcd dmu_dsn 3.0 HybrSel 32 300 1 || true

mv dmu_dsn_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: SIGSET_1 <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/SigSeT_1/dmu_dsn_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_SigSeT_1.vcd dmu_dsn 3.0 SigSeT_1 256 300 1 || true

mv dmu_dsn_SigSeT_1_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/SigSeT_1/dmu_dsn_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_SigSeT_1.vcd dmu_dsn 3.0 SigSeT_1 128 300 1 || true

mv dmu_dsn_SigSeT_1_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/SigSeT_1/dmu_dsn_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_SigSeT_1.vcd dmu_dsn 3.0 SigSeT_1 64 300 1 || true

mv dmu_dsn_SigSeT_1_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_dsn/SigSeT_1/dmu_dsn_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_SigSeT_1.vcd dmu_dsn 3.0 SigSeT_1 32 300 1 || true

mv dmu_dsn_SigSeT_1_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_dsn/PRankNetlist/dmu_dsn_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_PRankNetlist.vcd dmu_dsn 3.0 PRankNetlist 256 300 1 || true

mv dmu_dsn_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_dsn/PRankNetlist/dmu_dsn_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_PRankNetlist.vcd dmu_dsn 3.0 PRankNetlist 128 300 1 || true

mv dmu_dsn_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_dsn/PRankNetlist/dmu_dsn_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_PRankNetlist.vcd dmu_dsn 3.0 PRankNetlist 64 300 1 || true

mv dmu_dsn_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_dsn/PRankNetlist/dmu_dsn_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_dsn_PRankNetlist.vcd dmu_dsn 3.0 PRankNetlist 32 300 1 || true

mv dmu_dsn_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




###################################################################
##### State restoration ratio calculation for design: DMU_ILU #####
###################################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_ilu/HybrSel/dmu_ilu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_HybrSel.vcd dmu_ilu 3.0 HybrSel 256 300 1 || true

mv dmu_ilu_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_ilu/HybrSel/dmu_ilu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_HybrSel.vcd dmu_ilu 3.0 HybrSel 128 300 1 || true

mv dmu_ilu_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_ilu/HybrSel/dmu_ilu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_HybrSel.vcd dmu_ilu 3.0 HybrSel 64 300 1 || true

mv dmu_ilu_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_ilu/HybrSel/dmu_ilu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_HybrSel.vcd dmu_ilu 3.0 HybrSel 32 300 1 || true

mv dmu_ilu_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_ilu/PRankNetlist/dmu_ilu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_PRankNetlist.vcd dmu_ilu 3.0 PRankNetlist 256 300 1 || true

mv dmu_ilu_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_ilu/PRankNetlist/dmu_ilu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_PRankNetlist.vcd dmu_ilu 3.0 PRankNetlist 128 300 1 || true

mv dmu_ilu_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_ilu/PRankNetlist/dmu_ilu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_PRankNetlist.vcd dmu_ilu 3.0 PRankNetlist 64 300 1 || true

mv dmu_ilu_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_ilu/PRankNetlist/dmu_ilu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_ilu_PRankNetlist.vcd dmu_ilu 3.0 PRankNetlist 32 300 1 || true

mv dmu_ilu_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




#######################################################################
##### State restoration ratio calculation for design: NCU_FCD_CTL #####
#######################################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/HybrSel/ncu_fcd_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_HybrSel.vcd ncu_fcd_ctl 2.5 HybrSel 256 300 1 || true

mv ncu_fcd_ctl_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/HybrSel/ncu_fcd_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_HybrSel.vcd ncu_fcd_ctl 2.5 HybrSel 128 300 1 || true

mv ncu_fcd_ctl_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/HybrSel/ncu_fcd_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_HybrSel.vcd ncu_fcd_ctl 2.5 HybrSel 64 300 1 || true

mv ncu_fcd_ctl_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/HybrSel/ncu_fcd_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_HybrSel.vcd ncu_fcd_ctl 2.5 HybrSel 32 300 1 || true

mv ncu_fcd_ctl_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: SIGSET_1 <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/SigSeT_1/ncu_fcd_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_SigSeT_1.vcd ncu_fcd_ctl 2.5 SigSeT_1 256 300 1 || true

mv ncu_fcd_ctl_SigSeT_1_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/SigSeT_1/ncu_fcd_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_SigSeT_1.vcd ncu_fcd_ctl 2.5 SigSeT_1 128 300 1 || true

mv ncu_fcd_ctl_SigSeT_1_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/SigSeT_1/ncu_fcd_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_SigSeT_1.vcd ncu_fcd_ctl 2.5 SigSeT_1 64 300 1 || true

mv ncu_fcd_ctl_SigSeT_1_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/ncu_fcd_ctl/SigSeT_1/ncu_fcd_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_SigSeT_1.vcd ncu_fcd_ctl 2.5 SigSeT_1 32 300 1 || true

mv ncu_fcd_ctl_SigSeT_1_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/ncu_fcd_ctl/PRankNetlist/ncu_fcd_ctl_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_PRankNetlist.vcd ncu_fcd_ctl 2.5 PRankNetlist 256 300 1 || true

mv ncu_fcd_ctl_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/ncu_fcd_ctl/PRankNetlist/ncu_fcd_ctl_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_PRankNetlist.vcd ncu_fcd_ctl 2.5 PRankNetlist 128 300 1 || true

mv ncu_fcd_ctl_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/ncu_fcd_ctl/PRankNetlist/ncu_fcd_ctl_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_PRankNetlist.vcd ncu_fcd_ctl 2.5 PRankNetlist 64 300 1 || true

mv ncu_fcd_ctl_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/ncu_fcd_ctl/PRankNetlist/ncu_fcd_ctl_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/ncu_fcd_ctl_PRankNetlist.vcd ncu_fcd_ctl 2.5 PRankNetlist 32 300 1 || true

mv ncu_fcd_ctl_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




###################################################################
##### State restoration ratio calculation for design: DMU_CLU #####
###################################################################

#-->-->-->-->--> Tool used: HYBRSEL <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/HybrSel/dmu_clu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_HybrSel.vcd dmu_clu 3.0 HybrSel 256 300 1 || true

mv dmu_clu_HybrSel_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/HybrSel/dmu_clu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_HybrSel.vcd dmu_clu 3.0 HybrSel 128 300 1 || true

mv dmu_clu_HybrSel_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/HybrSel/dmu_clu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_HybrSel.vcd dmu_clu 3.0 HybrSel 64 300 1 || true

mv dmu_clu_HybrSel_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/HybrSel/dmu_clu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_HybrSel.vcd dmu_clu 3.0 HybrSel 32 300 1 || true

mv dmu_clu_HybrSel_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: SIGSET_1 <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/SigSeT_1/dmu_clu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_SigSeT_1.vcd dmu_clu 3.0 SigSeT_1 256 300 1 || true

mv dmu_clu_SigSeT_1_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/SigSeT_1/dmu_clu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_SigSeT_1.vcd dmu_clu 3.0 SigSeT_1 128 300 1 || true

mv dmu_clu_SigSeT_1_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/SigSeT_1/dmu_clu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_SigSeT_1.vcd dmu_clu 3.0 SigSeT_1 64 300 1 || true

mv dmu_clu_SigSeT_1_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/dmu_clu/SigSeT_1/dmu_clu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_SigSeT_1.vcd dmu_clu 3.0 SigSeT_1 32 300 1 || true

mv dmu_clu_SigSeT_1_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300


#-->-->-->-->--> Tool used: PRANKNETLIST <--<--<--<--<--#

#>>>>>>>>>> Coverage for buffer size: 256 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_clu/PRankNetlist/dmu_clu_flat_preprocessed_256_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_PRankNetlist.vcd dmu_clu 3.0 PRankNetlist 256 300 1 || true

mv dmu_clu_PRankNetlist_256.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/256_300
#>>>>>>>>>> Coverage for buffer size: 128 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_clu/PRankNetlist/dmu_clu_flat_preprocessed_128_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_PRankNetlist.vcd dmu_clu 3.0 PRankNetlist 128 300 1 || true

mv dmu_clu_PRankNetlist_128.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/128_300
#>>>>>>>>>> Coverage for buffer size: 64 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_clu/PRankNetlist/dmu_clu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_PRankNetlist.vcd dmu_clu 3.0 PRankNetlist 64 300 1 || true

mv dmu_clu_PRankNetlist_64.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/64_300
#>>>>>>>>>> Coverage for buffer size: 32 <<<<<<<<<<#
if [ -d "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300" ]; then
	 echo "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300 exists"
else
	 mkdir -pv "/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300"
fi

python ./RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_clu/dmu_clu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code/dmu_clu/PRankNetlist/dmu_clu_flat_preprocessed_32_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/dmu_clu_PRankNetlist.vcd dmu_clu 3.0 PRankNetlist 32 300 1 || true

mv dmu_clu_PRankNetlist_32.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr/32_300




