module l2t_dir_ctl ( aclk, bclk, dirlbf_lkup_en_c4_buf, dirlbf_inval_mask_c4_buf, dirlbf_rw_dec_c4_buf, dirlbf_rd_en_c4_buf, dirlbf_wr_en_c4_buf, dirlbf_rw_entry_c4_buf, dirlbf_dir_clear_c4_buf, l1clk, scan_in, scan_out, dir_rd_data_en_c4, dir_wr_data_en_c4, dir_cam_en_c4, dir_rw_entry_c4, dir_inval_mask_c4, dir_warm_rst_c4, select_panel0, select_panel1, select_panel2, select_panel3, l2t_mb0_run );
input [3:0] dirlbf_lkup_en_c4_buf;
input [7:0] dirlbf_inval_mask_c4_buf;
input [3:0] dirlbf_rw_dec_c4_buf;
input [5:0] dirlbf_rw_entry_c4_buf;
output [3:0] dir_rd_data_en_c4;
output [3:0] dir_wr_data_en_c4;
output [3:0] dir_cam_en_c4;
output [5:0] dir_rw_entry_c4;
output [7:0] dir_inval_mask_c4;
input aclk, bclk, dirlbf_rd_en_c4_buf, dirlbf_wr_en_c4_buf, dirlbf_dir_clear_c4_buf, l1clk, scan_in, l2t_mb0_run;
output scan_out, dir_warm_rst_c4, select_panel0, select_panel1, select_panel2, select_panel3;
wire   dir_warm_rst_c4, select_panel0, n5, n6, n7, n8, n10, n13, n14, n15;
wire   [3:0] rd_data_en_c5;
assign dir_cam_en_c4[3] = dirlbf_lkup_en_c4_buf[3];
assign dir_cam_en_c4[2] = dirlbf_lkup_en_c4_buf[2];
assign dir_cam_en_c4[1] = dirlbf_lkup_en_c4_buf[1];
assign dir_cam_en_c4[0] = dirlbf_lkup_en_c4_buf[0];
assign dir_rw_entry_c4[5] = dirlbf_rw_entry_c4_buf[5];
assign dir_rw_entry_c4[4] = dirlbf_rw_entry_c4_buf[4];
assign dir_rw_entry_c4[3] = dirlbf_rw_entry_c4_buf[3];
assign dir_rw_entry_c4[2] = dirlbf_rw_entry_c4_buf[2];
assign dir_rw_entry_c4[1] = dirlbf_rw_entry_c4_buf[1];
assign dir_rw_entry_c4[0] = dirlbf_rw_entry_c4_buf[0];
assign dir_inval_mask_c4[7] = dirlbf_inval_mask_c4_buf[7];
assign dir_inval_mask_c4[6] = dirlbf_inval_mask_c4_buf[6];
assign dir_inval_mask_c4[5] = dirlbf_inval_mask_c4_buf[5];
assign dir_inval_mask_c4[4] = dirlbf_inval_mask_c4_buf[4];
assign dir_inval_mask_c4[3] = dirlbf_inval_mask_c4_buf[3];
assign dir_inval_mask_c4[2] = dirlbf_inval_mask_c4_buf[2];
assign dir_inval_mask_c4[1] = dirlbf_inval_mask_c4_buf[1];
assign dir_inval_mask_c4[0] = dirlbf_inval_mask_c4_buf[0];
assign dir_warm_rst_c4 = dirlbf_dir_clear_c4_buf;
assign scan_out = select_panel0;
SDFF_X2 \ff_rd_data_en_c5_en/d0_0/q_reg[0]  ( .D(dirlbf_rd_en_c4_buf), .SI( 1'b0), .SE(n8), .CK(l1clk), .Q(rd_data_en_c5[0]) );
SDFF_X2 \ff_rd_data_en_c5_en/d0_0/q_reg[1]  ( .D(dirlbf_rd_en_c4_buf), .SI( 1'b0), .SE(n7), .CK(l1clk), .Q(rd_data_en_c5[1]) );
DFF_X1 \ff_rd_data_en_c5_en/d0_0/q_reg[2]  ( .D(l2t_mb0_run), .CK(l1clk), .QN(n10) );
SDFF_X2 \ff_rd_data_en_c5/d0_0/q_reg[0]  ( .D(dirlbf_rd_en_c4_buf), .SI(1'b0), .SE(n6), .CK(l1clk), .Q(rd_data_en_c5[2]) );
SDFF_X2 \ff_rd_data_en_c5/d0_0/q_reg[1]  ( .D(dirlbf_rd_en_c4_buf), .SI(1'b0), .SE(n5), .CK(l1clk), .Q(rd_data_en_c5[3]) );
DFF_X1 \ff_select_panel0/d0_0/q_reg[0]  ( .D(rd_data_en_c5[0]), .CK(l1clk), .Q(select_panel0) );
DFF_X1 \ff_select_panel0/d0_0/q_reg[1]  ( .D(rd_data_en_c5[1]), .CK(l1clk), .Q(select_panel1) );
DFF_X1 \ff_select_panel0/d0_0/q_reg[2]  ( .D(rd_data_en_c5[2]), .CK(l1clk), .Q(select_panel2) );
DFF_X1 \ff_select_panel0/d0_0/q_reg[3]  ( .D(rd_data_en_c5[3]), .CK(l1clk), .Q(select_panel3) );
NOR2_X1 U20 ( .A1(n5), .A2(n13), .ZN(dir_wr_data_en_c4[3]) );
NOR2_X1 U21 ( .A1(n6), .A2(n13), .ZN(dir_wr_data_en_c4[2]) );
NOR2_X1 U22 ( .A1(n7), .A2(n13), .ZN(dir_wr_data_en_c4[1]) );
NOR2_X1 U23 ( .A1(n8), .A2(n13), .ZN(dir_wr_data_en_c4[0]) );
NAND2_X1 U24 ( .A1(dirlbf_wr_en_c4_buf), .A2(n14), .ZN(n13) );
NAND2_X1 U25 ( .A1(dir_warm_rst_c4), .A2(n10), .ZN(n14) );
NOR2_X1 U26 ( .A1(n5), .A2(n15), .ZN(dir_rd_data_en_c4[3]) );
INV_X1 U27 ( .A(dirlbf_rw_dec_c4_buf[3]), .ZN(n5) );
NOR2_X1 U28 ( .A1(n6), .A2(n15), .ZN(dir_rd_data_en_c4[2]) );
INV_X1 U29 ( .A(dirlbf_rw_dec_c4_buf[2]), .ZN(n6) );
NOR2_X1 U30 ( .A1(n7), .A2(n15), .ZN(dir_rd_data_en_c4[1]) );
INV_X1 U31 ( .A(dirlbf_rw_dec_c4_buf[1]), .ZN(n7) );
NOR2_X1 U32 ( .A1(n8), .A2(n15), .ZN(dir_rd_data_en_c4[0]) );
INV_X1 U33 ( .A(dirlbf_rd_en_c4_buf), .ZN(n15) );
INV_X1 U34 ( .A(dirlbf_rw_dec_c4_buf[0]), .ZN(n8) );
endmodule

