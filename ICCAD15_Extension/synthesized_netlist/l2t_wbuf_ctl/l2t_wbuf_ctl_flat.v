/////////////////////////////////////////////////////////////
// Created by: Synopsys DC Expert(TM) in wire load mode
// Version   : L-2016.03
// Date      : Sat May 13 12:52:29 2017
/////////////////////////////////////////////////////////////


module l2t_wbuf_ctl ( tcu_pce_ov, tcu_aclk, tcu_bclk, tcu_scan_en, scan_in, 
        scan_out, l2clk, wmr_l, vlddir_dirty_evict_c3, arbdec_arbdp_inst_fb_c2, 
        misbuf_wbuf_mbid_c4, misbuf_hit_c4, misbuf_filbuf_mcu_pick, 
        csr_l2_bypass_mode_on, wb_cam_match_c2, wbuf_wbtag_write_wl_c4, 
        wbuf_wbtag_write_en_c4, wbuf_wb_read_wl, wbuf_wb_read_en, 
        wbuf_wbufrpt_leave_state0, arbadr_c1_addr_eq_wb_c4, 
        arb_wbuf_hit_off_c1, arb_wbuf_inst_vld_c2, mcu_l2t_wr_ack, 
        l2t_l2b_wbwr_wl_c6, l2t_l2b_wbwr_wen_c6, l2t_l2b_wbrd_wl_r0, 
        l2t_l2b_ev_dword_r0, l2t_l2b_evict_en_r0, l2t_mcu_wr_req, 
        wbuf_hit_unqual_c2, wbuf_misbuf_dep_rdy_en, wbuf_misbuf_dep_mbid, 
        wbuf_arb_full_px1, wbuf_rdmat_read_wl, wbuf_rdmat_read_en, 
        rdmat_pick_vec, rdmat_or_rdmat_valid, wbuf_wr_addr_sel, 
        wbuf_wb_or_rdma_wr_req_en, l2t_l2b_rdma_rdwl_r0, wbuf_reset_rdmat_vld, 
        wbuf_set_rdmat_acked, l2t_sii_wib_dequeue, l2t_siu_delay, 
        l2t_dbg_sii_wib_dequeue, l2t_mb2_run, l2t_mb2_rdmatag_rd_en, 
        l2t_mb2_wbtag_wr_en, l2t_mb2_wbtag_rd_en, l2t_mb2_addr, 
        wbuf_wbufrpt_next_state_1, cycle_count_less_than_7_din, 
        mcu_l2t_wr_ack_d1, wb_mbist_cam_hit, wb_mbist_cam_sel );
  input [4:0] misbuf_wbuf_mbid_c4;
  input [7:0] wb_cam_match_c2;
  output [7:0] wbuf_wbtag_write_wl_c4;
  output [7:0] wbuf_wb_read_wl;
  output [2:0] l2t_l2b_wbwr_wl_c6;
  output [3:0] l2t_l2b_wbwr_wen_c6;
  output [2:0] l2t_l2b_wbrd_wl_r0;
  output [2:0] l2t_l2b_ev_dword_r0;
  output [4:0] wbuf_misbuf_dep_mbid;
  output [3:0] wbuf_rdmat_read_wl;
  input [3:0] rdmat_pick_vec;
  output [1:0] l2t_l2b_rdma_rdwl_r0;
  output [3:0] wbuf_reset_rdmat_vld;
  output [3:0] wbuf_set_rdmat_acked;
  input [2:0] l2t_mb2_addr;
  input tcu_pce_ov, tcu_aclk, tcu_bclk, tcu_scan_en, scan_in, l2clk, wmr_l,
         vlddir_dirty_evict_c3, arbdec_arbdp_inst_fb_c2, misbuf_hit_c4,
         misbuf_filbuf_mcu_pick, csr_l2_bypass_mode_on,
         arbadr_c1_addr_eq_wb_c4, arb_wbuf_hit_off_c1, arb_wbuf_inst_vld_c2,
         mcu_l2t_wr_ack, rdmat_or_rdmat_valid, l2t_siu_delay, l2t_mb2_run,
         l2t_mb2_rdmatag_rd_en, l2t_mb2_wbtag_wr_en, l2t_mb2_wbtag_rd_en,
         wb_mbist_cam_sel;
  output scan_out, wbuf_wbtag_write_en_c4, wbuf_wb_read_en,
         wbuf_wbufrpt_leave_state0, l2t_l2b_evict_en_r0, l2t_mcu_wr_req,
         wbuf_hit_unqual_c2, wbuf_misbuf_dep_rdy_en, wbuf_arb_full_px1,
         wbuf_rdmat_read_en, wbuf_wr_addr_sel, wbuf_wb_or_rdma_wr_req_en,
         l2t_sii_wib_dequeue, l2t_dbg_sii_wib_dequeue,
         wbuf_wbufrpt_next_state_1, cycle_count_less_than_7_din,
         mcu_l2t_wr_ack_d1, wb_mbist_cam_hit;
  wire   \l2t_l2b_wbwr_wen_c6[3] , wbuf_arb_full_px1, l1clk,
         arb_wbuf_inst_vld_c3, arbdec_arbdp_inst_fb_c3,
         l2t_mb2_rdmatag_rd_en_r1, l2t_mb2_rdmatag_rd_en_r2,
         l2t_mb2_wbtag_rd_en_r1, l2t_mb2_wbtag_rd_en_r2,
         l2t_mb2_rdmatag_rd_en_r3, l2t_mb2_wbtag_rd_en_r3, l2t_mb2_run_r1,
         l2_bypass_mode_on_d1, l2t_mb2_wbtag_wr_en_r2, l2t_mb2_wbtag_wr_en_r1,
         wbtag_write_we_c5, wbtag_write_we_c52, bypass_en_c1, bypass_en_c2,
         wb_mbist_cam_hit_unreg, wb_mbist_cam_sel_r1, wbuf_hit_qual_c2,
         wbuf_hit_qual_c3, or_wb_mbid_vld, mcu_req_pending, \next_state[2] ,
         next_state_0, \cycle_count[3] , latched_wb_read_en,
         latched_rdma_read_en, cycle_count_less_than_7,
         l2t_sii_wib_dequeue_raw, l2t_sii_wib_dequeue_delay, \clkgen/c_0/l1en ,
         \ff_latched_wb_read_en/fdin[0] , \ff_latched_rdma_read_en/fdin[0] ,
         \ff_or_wb_mbid_vld/fdin[0] , \ff_mcu_req_pending/fdin[0] ,
         \ff_wbuf_arb_full_px1/fdin[0] , n10, n11, n12, n13, n15, n16, n17,
         n18, n30, n35, n36, n37, n38, n39, n40, n41, n42, n43, n44, n45, n47,
         n49, n51, n53, n55, n57, n59, n61, n63, n64, n65, n66, n67, n68, n69,
         n70, n75, n80, n81, n83, n84, n85, n86, n88, n89, n90, n91, n92, n93,
         n94, n95, n96, n97, n98, n99, n100, n101, n102, n103, n104, n105,
         n106, n107, n109, n110, n111, n112, n113, n114, n115, n116, n158,
         n173, n174, n175, n176, n177, n178, n179, n183, n184, n185, n186,
         n187, n188, n193, n194, n195, n196, n197, n198, n199, n200, n201,
         n202, n203, n204, n205, n206, n207, n208, n209, n210, n211, n212,
         n213, n214, n215, n216, n217, n218, n219, n220, n221, n222, n223,
         n224, n225, n226, n227, n228, n229, n230, n231, n232, n233, n234,
         n235, n236, n238, n239, n240, n241, n242, n243, n244, n245, n246,
         n255, n256, n257, n258, n259, n260, n262, n264, n266, n268, n270,
         n272, n274, n278, n283, n284, n285, n286, n287, n288, n289, n290,
         n291, n292, n293, n294, n297, n304, n305, n308, n315, n316, n317,
         n318, n319, n321, n322, n323, n325, n326, n328, n329, n331, n332,
         n334, n335, n337, n338, n340, n341, n343, n344, n346, n356, n357,
         n358, n359, n360, n361, n362, n363, n364, n365, n366, n367, n368,
         n369, n370, n371, n372, n373, n374, n375, n376, n377, n378, n379,
         n380, n381, n382, n383, n384, n385, n386, n387, n388, n389, n390,
         n391, n392, n393, n394, n395, n396, n397, n398, n399, n400, n401,
         n402, n403, n404, n405, n406, n414, n417, n422, n423, n424, n425,
         n426, n427, n428, n429, n430, n431, n432, n433, n434, n435, n436,
         n437, n438, n439, n440, n441, n442, n443, n444, n445, n446, n447,
         n448, n449, n450, n451, n452, n453, n454, n455, n456, n457, n458,
         n459, n460, n461, n462, n463, n464, n465, n466, n467, n468, n469,
         n470, n471, n472, n473, n474, n475, n476, n477, n478, n479, n480,
         n481, n482, n483, n484, n485, n486, n487, n488, n489, n490, n491,
         n492, n493, n494, n495, n496, n497, n498, n499, n500, n501, n502,
         n503, n504, n505, n506, n507, n508, n509, n605, n606, n613, n614,
         n635, n636, n637, n638, n639, n640, n641, n642, n643, n644, n645,
         n646, n647, n648, n649, n650, n652, n653, n654, n655, n656, n657,
         n658, n659, n660, n661, n662, n663, n664, n665, n666, n667, n668,
         n669, n670, n671, n672, n673, n674, n675, n676, n677, n678, n679,
         n680, n681, n682, n683, n684, n685, n686, n687, n688, n689, n690,
         n691, n692, n693, n694, n695, n696, n697, n698, n699, n700, n701,
         n702, n703, n704, n705, n706, n707, n708, n709, n710, n711, n712,
         n713, n714, n715, n716, n717, n718, n719, n720, n721, n722, n723,
         n724, n725, n726, n727, n729, n730, n731, n732, n733, n734, n735,
         n736, n737, n738, n739, n740, n741, n742, n743, n744, n745, n746,
         n747, n748, n749, n750, n751, n752, n753, n754, n755, n756, n757,
         n758, n759, n760, n761, n762, n763, n764, n765, n766, n767, n768,
         n769, n770, n771, n772, n773, n774, n775, n776, n777, n778, n779,
         n780, n781, n782, n783, n784, n785, n786, n787, n788, n789, n790,
         n791, n792, n793, n794, n795, n796, n797, n798, n799, n800, n801,
         n802, n803, n804, n805, n806, n807, n808, n809, n810, n811, n812,
         n813, n814, n815, n816, n817, n818, n819, n820, n821, n822, n823,
         n824, n825, n826, n827, n828, n829, n830, n831, n832, n833, n834,
         n835, n836, n837, n838, n839, n840, n841, n842, n843, n844, n845,
         n846, n847, n848, n849, n850, n851, n852, n853, n854, n855, n856,
         n857, n858, n859, n860, n861, n862, n863, n864, n865, n866, n867,
         n868, n869, n870, n871, n872, n873, n874, n875, n876, n877, n878,
         n879, n880, n881, n882, n883, n884, n885, n886, n887, n888, n889,
         n890, n891, n892, n893, n894, n895, n896, n897, n898, n899, n900,
         n901, n902, n903, n904, n905, n906, n907, n908, n909, n910, n911,
         n912, n913, n914, n915, n916, n917, n918, n919, n920, n921, n922,
         n923, n924, n925, n926, n927, n928, n929, n930, n931, n932, n933,
         n934, n935, n936, n937, n938, n939, n940, n941, n942, n943, n944,
         n945, n946, n947, n948, n949, n950, n951, n952, n953, n954, n955,
         n956, n957, n958, n959, n960, n961, n962, n963, n964, n965, n966,
         n967, n968, n969, n970, n971, n972, n973, n974, n975, n976, n977,
         n978, n979, n980, n981, n982, n983, n984, n985, n986, n987, n988,
         n989, n990, n991, n992, n993, n994, n995, n996, n997, n998, n999,
         n1000, n1001, n1002, n1003, n1004, n1005, n1006, n1007, n1008, n1009,
         n1010, n1011, n1012, n1013, n1014, n1015, n1016, n1017, n1018, n1019,
         n1020, n1021, n1022, n1023, n1028, n1029, n1030, n1031, n1032, n1033;
  wire   [7:0] wb_valid;
  wire   [2:0] l2t_mb2_addr_r3;
  wire   [2:0] l2t_mb2_addr_r1;
  wire   [2:0] l2t_mb2_addr_r2;
  wire   [7:0] wbtag_write_wl_c5;
  wire   [2:0] enc_write_wl_c5;
  wire   [2:0] enc_write_wl_c52;
  wire   [3:0] latched_rdmad_read_wl;
  wire   [7:0] latched_wb_read_wl;
  wire   [7:0] wb_acked;
  wire   [7:0] wb_cam_match_c2_d1;
  wire   [7:0] wb_cam_hit_vec_c2;
  wire   [7:0] wb_cam_hit_vec_c3;
  wire   [7:0] wb_cam_hit_vec_c4;
  wire   [4:0] mbid0;
  wire   [4:0] mbid1;
  wire   [4:0] mbid2;
  wire   [4:0] mbid3;
  wire   [4:0] mbid4;
  wire   [4:0] mbid5;
  wire   [4:0] mbid6;
  wire   [4:0] mbid7;
  wire   [7:0] wb_mbid_vld;
  wire   [2:0] state;
  wire   [2:0] lshift_quad_state;
  wire   [2:0] quad_state_in;
  wire   [3:0] lshift_quad0_state;
  wire   [3:0] quad0_state_in;
  wire   [3:0] lshift_quad1_state;
  wire   [3:0] quad1_state_in;
  wire   [3:0] lshift_quad2_state;
  wire   [3:0] quad2_state_in;
  wire   [3:0] wb_count;
  wire   [4:0] \ff_mbid0/fdin ;
  wire   [4:0] \ff_mbid1/fdin ;
  wire   [4:0] \ff_mbid2/fdin ;
  wire   [4:0] \ff_mbid3/fdin ;
  wire   [4:0] \ff_mbid4/fdin ;
  wire   [4:0] \ff_mbid5/fdin ;
  wire   [4:0] \ff_mbid6/fdin ;
  wire   [4:0] \ff_mbid7/fdin ;
  wire   [7:0] \ff_wb_valid/fdin ;
  wire   [7:0] \ff_wb_acked/fdin ;
  wire   [7:0] \ff_wb_mbid_vld/fdin ;
  wire   [7:0] \ff_latched_wb_read_wl/fdin ;
  wire   [1:0] \ff_l2t_l2b_rdma_rdwl_r0/fdin ;
  wire   [3:0] \ff_latched_rdmad_read_wl/fdin ;
  wire   [2:0] \ff_l2t_l2b_wbrd_wl_r0/fdin ;
  wire   [3:0] \ff_cycle_count/fdin ;
  wire   [3:0] \ff_wb_count/fdin ;
  assign l2t_l2b_wbwr_wen_c6[0] = \l2t_l2b_wbwr_wen_c6[3] ;
  assign l2t_l2b_wbwr_wen_c6[1] = \l2t_l2b_wbwr_wen_c6[3] ;
  assign l2t_l2b_wbwr_wen_c6[2] = \l2t_l2b_wbwr_wen_c6[3] ;
  assign l2t_l2b_wbwr_wen_c6[3] = \l2t_l2b_wbwr_wen_c6[3] ;
  assign scan_out = wbuf_arb_full_px1;

  DFF_X1 \clkgen/c_0/l1en_reg  ( .D(1'b1), .CK(n10), .Q(\clkgen/c_0/l1en ) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[1]  ( .D(l2t_mb2_addr_r2[0]), .CK(l1clk), .Q(l2t_mb2_addr_r3[0]), .QN(n36) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[3]  ( .D(l2t_mb2_addr_r2[2]), .CK(l1clk), .Q(l2t_mb2_addr_r3[2]), .QN(n37) );
  DFF_X1 \ff_arb_wbuf_hit_off_c2/d0_0/q_reg[0]  ( .D(arb_wbuf_hit_off_c1), 
        .CK(l1clk), .QN(n110) );
  DFF_X1 \ff_wbtag_write_we_c5/d0_0/q_reg[0]  ( .D(wbuf_wbtag_write_en_c4), 
        .CK(l1clk), .Q(wbtag_write_we_c5), .QN(n111) );
  DFF_X1 \ff_mcu_req_pending/d0_0/q_reg[0]  ( .D(\ff_mcu_req_pending/fdin[0] ), 
        .CK(l1clk), .Q(mcu_req_pending) );
  DFF_X1 \ff_state/d0_0/q_reg[1]  ( .D(wbuf_wbufrpt_next_state_1), .CK(l1clk), 
        .Q(state[1]) );
  DFF_X1 \ff_state/d0_0/q_reg[2]  ( .D(\next_state[2] ), .CK(l1clk), .Q(
        state[2]) );
  DFF_X1 \ff_cycle_count/d0_0/q_reg[0]  ( .D(\ff_cycle_count/fdin [0]), .CK(
        l1clk), .Q(l2t_l2b_ev_dword_r0[0]), .QN(n90) );
  DFF_X1 \ff_cycle_count/d0_0/q_reg[1]  ( .D(\ff_cycle_count/fdin [1]), .CK(
        l1clk), .Q(l2t_l2b_ev_dword_r0[1]), .QN(n91) );
  DFF_X1 \ff_cycle_count/d0_0/q_reg[2]  ( .D(\ff_cycle_count/fdin [2]), .CK(
        l1clk), .Q(l2t_l2b_ev_dword_r0[2]), .QN(n92) );
  DFF_X1 \ff_quad_state/d0_0/q_reg[1]  ( .D(quad_state_in[1]), .CK(l1clk), .Q(
        lshift_quad_state[2]), .QN(n89) );
  DFF_X1 \ff_quad_state/d0_0/q_reg[0]  ( .D(quad_state_in[0]), .CK(l1clk), .Q(
        lshift_quad_state[1]), .QN(n88) );
  DFF_X1 \ff_state/d0_0/q_reg[0]  ( .D(next_state_0), .CK(l1clk), .Q(state[0])
         );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[1]  ( .D(
        \ff_latched_wb_read_wl/fdin [1]), .CK(l1clk), .Q(latched_wb_read_wl[1]) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[1]  ( .D(\ff_wb_acked/fdin [1]), .CK(l1clk), 
        .Q(wb_acked[1]), .QN(n63) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[3]  ( .D(
        \ff_latched_wb_read_wl/fdin [3]), .CK(l1clk), .Q(latched_wb_read_wl[3]) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[3]  ( .D(\ff_wb_acked/fdin [3]), .CK(l1clk), 
        .Q(wb_acked[3]), .QN(n65) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[7]  ( .D(
        \ff_latched_wb_read_wl/fdin [7]), .CK(l1clk), .Q(latched_wb_read_wl[7]) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[7]  ( .D(\ff_wb_acked/fdin [7]), .CK(l1clk), 
        .Q(wb_acked[7]), .QN(n69) );
  DFF_X1 \ff_wbuf_hit_qual_c3/d0_0/q_reg[0]  ( .D(wbuf_hit_qual_c2), .CK(l1clk), .Q(wbuf_hit_qual_c3) );
  DFF_X1 \ff_wbuf_hit_qual_c4/d0_0/q_reg[0]  ( .D(wbuf_hit_qual_c3), .CK(l1clk), .QN(n112) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[0]  ( .D(\ff_wb_mbid_vld/fdin [0]), .CK(
        l1clk), .Q(wb_mbid_vld[0]), .QN(n669) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[0]  ( .D(
        \ff_latched_wb_read_wl/fdin [0]), .CK(l1clk), .Q(latched_wb_read_wl[0]) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[1]  ( .D(wbuf_wbtag_write_wl_c4[1]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[1]), .QN(n38) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[1]  ( .D(wb_cam_hit_vec_c2[1]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[1]) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[1]  ( .D(\ff_wb_mbid_vld/fdin [1]), .CK(
        l1clk), .Q(wb_mbid_vld[1]), .QN(n711) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[2]  ( .D(
        \ff_latched_wb_read_wl/fdin [2]), .CK(l1clk), .Q(latched_wb_read_wl[2]) );
  DFF_X1 \ff_quad0_state/d0_0/q_reg[0]  ( .D(quad0_state_in[0]), .CK(l1clk), 
        .Q(lshift_quad0_state[1]), .QN(n98) );
  DFF_X1 \ff_quad0_state/d0_0/q_reg[1]  ( .D(quad0_state_in[1]), .CK(l1clk), 
        .Q(lshift_quad0_state[2]), .QN(n652) );
  DFF_X1 \ff_quad0_state/d0_0/q_reg[2]  ( .D(quad0_state_in[2]), .CK(l1clk), 
        .Q(lshift_quad0_state[3]), .QN(n99) );
  DFF_X1 \ff_quad0_state/d0_0/q_reg[3]  ( .D(quad0_state_in[3]), .CK(l1clk), 
        .Q(lshift_quad0_state[0]), .QN(n653) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[2]  ( .D(wbuf_wbtag_write_wl_c4[2]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[2]), .QN(n39) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[3]  ( .D(wbuf_wbtag_write_wl_c4[3]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[3]), .QN(n40) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[3]  ( .D(\ff_wb_valid/fdin [3]), .CK(l1clk), 
        .Q(wb_valid[3]), .QN(n51) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[4]  ( .D(wbuf_wbtag_write_wl_c4[4]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[4]), .QN(n41) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[4]  ( .D(wb_cam_hit_vec_c2[4]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[4]) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[4]  ( .D(\ff_wb_mbid_vld/fdin [4]), .CK(
        l1clk), .Q(wb_mbid_vld[4]), .QN(n713) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[4]  ( .D(
        \ff_latched_wb_read_wl/fdin [4]), .CK(l1clk), .Q(latched_wb_read_wl[4]) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[5]  ( .D(wbuf_wbtag_write_wl_c4[5]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[5]), .QN(n42) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[5]  ( .D(wb_cam_hit_vec_c2[5]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[5]) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[5]  ( .D(\ff_wb_mbid_vld/fdin [5]), .CK(
        l1clk), .Q(wb_mbid_vld[5]), .QN(n657) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[5]  ( .D(
        \ff_latched_wb_read_wl/fdin [5]), .CK(l1clk), .Q(latched_wb_read_wl[5]) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[5]  ( .D(\ff_wb_valid/fdin [5]), .CK(l1clk), 
        .Q(wb_valid[5]), .QN(n55) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[6]  ( .D(wbuf_wbtag_write_wl_c4[6]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[6]), .QN(n43) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[6]  ( .D(wb_cam_hit_vec_c2[6]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[6]) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[6]  ( .D(\ff_wb_mbid_vld/fdin [6]), .CK(
        l1clk), .Q(wb_mbid_vld[6]), .QN(n712) );
  DFF_X1 \ff_latched_wb_read_wl/d0_0/q_reg[6]  ( .D(
        \ff_latched_wb_read_wl/fdin [6]), .CK(l1clk), .Q(latched_wb_read_wl[6]) );
  DFF_X1 \ff_quad1_state/d0_0/q_reg[0]  ( .D(quad1_state_in[0]), .CK(l1clk), 
        .Q(lshift_quad1_state[1]), .QN(n100) );
  DFF_X1 \ff_quad1_state/d0_0/q_reg[1]  ( .D(quad1_state_in[1]), .CK(l1clk), 
        .Q(lshift_quad1_state[2]), .QN(n655) );
  DFF_X1 \ff_quad1_state/d0_0/q_reg[2]  ( .D(quad1_state_in[2]), .CK(l1clk), 
        .Q(lshift_quad1_state[3]), .QN(n101) );
  DFF_X1 \ff_quad1_state/d0_0/q_reg[3]  ( .D(quad1_state_in[3]), .CK(l1clk), 
        .Q(lshift_quad1_state[0]), .QN(n654) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[7]  ( .D(wbuf_wbtag_write_wl_c4[7]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[7]), .QN(n44) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[7]  ( .D(\ff_wb_valid/fdin [7]), .CK(l1clk), 
        .Q(wb_valid[7]), .QN(n59) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[6]  ( .D(\ff_wb_acked/fdin [6]), .CK(l1clk), 
        .Q(wb_acked[6]), .QN(n68) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[5]  ( .D(\ff_wb_acked/fdin [5]), .CK(l1clk), 
        .Q(wb_acked[5]), .QN(n67) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[4]  ( .D(\ff_wb_acked/fdin [4]), .CK(l1clk), 
        .Q(wb_acked[4]), .QN(n66) );
  DFF_X1 \ff_l2t_l2b_wbrd_wl_r0/d0_0/q_reg[2]  ( .D(
        \ff_l2t_l2b_wbrd_wl_r0/fdin [2]), .CK(l1clk), .Q(l2t_l2b_wbrd_wl_r0[2]) );
  DFF_X1 \ff_mbid4/d0_0/q_reg[0]  ( .D(\ff_mbid4/fdin [0]), .CK(l1clk), .Q(
        mbid4[0]) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[3]  ( .D(wb_cam_hit_vec_c2[3]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[3]) );
  DFF_X1 \ff_mbid3/d0_0/q_reg[4]  ( .D(\ff_mbid3/fdin [4]), .CK(l1clk), .Q(
        mbid3[4]) );
  DFF_X1 \ff_mbid3/d0_0/q_reg[3]  ( .D(\ff_mbid3/fdin [3]), .CK(l1clk), .Q(
        mbid3[3]) );
  DFF_X1 \ff_mbid3/d0_0/q_reg[2]  ( .D(\ff_mbid3/fdin [2]), .CK(l1clk), .Q(
        mbid3[2]) );
  DFF_X1 \ff_mbid3/d0_0/q_reg[1]  ( .D(\ff_mbid3/fdin [1]), .CK(l1clk), .Q(
        mbid3[1]) );
  DFF_X1 \ff_mbid3/d0_0/q_reg[0]  ( .D(\ff_mbid3/fdin [0]), .CK(l1clk), .Q(
        mbid3[0]) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[2]  ( .D(\ff_wb_acked/fdin [2]), .CK(l1clk), 
        .Q(wb_acked[2]), .QN(n64) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[2]  ( .D(wb_cam_hit_vec_c2[2]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[2]) );
  DFF_X1 \ff_mbid2/d0_0/q_reg[4]  ( .D(\ff_mbid2/fdin [4]), .CK(l1clk), .Q(
        mbid2[4]) );
  DFF_X1 \ff_mbid2/d0_0/q_reg[3]  ( .D(\ff_mbid2/fdin [3]), .CK(l1clk), .Q(
        mbid2[3]) );
  DFF_X1 \ff_mbid2/d0_0/q_reg[2]  ( .D(\ff_mbid2/fdin [2]), .CK(l1clk), .Q(
        mbid2[2]) );
  DFF_X1 \ff_mbid2/d0_0/q_reg[1]  ( .D(\ff_mbid2/fdin [1]), .CK(l1clk), .Q(
        mbid2[1]) );
  DFF_X1 \ff_mbid2/d0_0/q_reg[0]  ( .D(\ff_mbid2/fdin [0]), .CK(l1clk), .Q(
        mbid2[0]) );
  DFF_X1 \ff_l2t_l2b_wbrd_wl_r0/d0_0/q_reg[1]  ( .D(
        \ff_l2t_l2b_wbrd_wl_r0/fdin [1]), .CK(l1clk), .Q(l2t_l2b_wbrd_wl_r0[1]), .QN(n978) );
  DFF_X1 \ff_mbid1/d0_0/q_reg[4]  ( .D(\ff_mbid1/fdin [4]), .CK(l1clk), .Q(
        mbid1[4]) );
  DFF_X1 \ff_mbid1/d0_0/q_reg[3]  ( .D(\ff_mbid1/fdin [3]), .CK(l1clk), .Q(
        mbid1[3]) );
  DFF_X1 \ff_mbid1/d0_0/q_reg[2]  ( .D(\ff_mbid1/fdin [2]), .CK(l1clk), .Q(
        mbid1[2]) );
  DFF_X1 \ff_mbid1/d0_0/q_reg[1]  ( .D(\ff_mbid1/fdin [1]), .CK(l1clk), .Q(
        mbid1[1]) );
  DFF_X1 \ff_mbid1/d0_0/q_reg[0]  ( .D(\ff_mbid1/fdin [0]), .CK(l1clk), .Q(
        mbid1[0]) );
  DFF_X1 \ff_wb_acked/d0_0/q_reg[0]  ( .D(\ff_wb_acked/fdin [0]), .CK(l1clk), 
        .Q(wb_acked[0]), .QN(n61) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[0]  ( .D(wb_cam_hit_vec_c2[0]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[0]) );
  DFF_X1 \ff_mbid0/d0_0/q_reg[4]  ( .D(\ff_mbid0/fdin [4]), .CK(l1clk), .Q(
        mbid0[4]) );
  DFF_X1 \ff_mbid0/d0_0/q_reg[3]  ( .D(\ff_mbid0/fdin [3]), .CK(l1clk), .Q(
        mbid0[3]) );
  DFF_X1 \ff_mbid0/d0_0/q_reg[2]  ( .D(\ff_mbid0/fdin [2]), .CK(l1clk), .Q(
        mbid0[2]) );
  DFF_X1 \ff_mbid0/d0_0/q_reg[1]  ( .D(\ff_mbid0/fdin [1]), .CK(l1clk), .Q(
        mbid0[1]) );
  DFF_X1 \ff_mbid0/d0_0/q_reg[0]  ( .D(\ff_mbid0/fdin [0]), .CK(l1clk), .Q(
        mbid0[0]) );
  DFF_X1 \ff_wb_cam_hit_vec_c3/d0_0/q_reg[7]  ( .D(wb_cam_hit_vec_c2[7]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c3[7]) );
  DFF_X1 \ff_mbid7/d0_0/q_reg[4]  ( .D(\ff_mbid7/fdin [4]), .CK(l1clk), .Q(
        mbid7[4]) );
  DFF_X1 \ff_mbid7/d0_0/q_reg[3]  ( .D(\ff_mbid7/fdin [3]), .CK(l1clk), .Q(
        mbid7[3]) );
  DFF_X1 \ff_mbid7/d0_0/q_reg[2]  ( .D(\ff_mbid7/fdin [2]), .CK(l1clk), .Q(
        mbid7[2]) );
  DFF_X1 \ff_mbid7/d0_0/q_reg[1]  ( .D(\ff_mbid7/fdin [1]), .CK(l1clk), .Q(
        mbid7[1]) );
  DFF_X1 \ff_mbid7/d0_0/q_reg[0]  ( .D(\ff_mbid7/fdin [0]), .CK(l1clk), .Q(
        mbid7[0]) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[7]  ( .D(\ff_wb_mbid_vld/fdin [7]), .CK(
        l1clk), .Q(wb_mbid_vld[7]), .QN(n718) );
  DFF_X1 \ff_l2t_l2b_wbrd_wl_r0/d0_0/q_reg[0]  ( .D(
        \ff_l2t_l2b_wbrd_wl_r0/fdin [0]), .CK(l1clk), .Q(l2t_l2b_wbrd_wl_r0[0]), .QN(n935) );
  DFF_X1 \ff_latched_rdmad_read_wl/d0_0/q_reg[1]  ( .D(
        \ff_latched_rdmad_read_wl/fdin [1]), .CK(l1clk), .Q(
        latched_rdmad_read_wl[1]), .QN(n95) );
  DFF_X1 \ff_quad2_state/d0_0/q_reg[0]  ( .D(quad2_state_in[0]), .CK(l1clk), 
        .Q(lshift_quad2_state[1]), .QN(n102) );
  DFF_X1 \ff_quad2_state/d0_0/q_reg[1]  ( .D(quad2_state_in[1]), .CK(l1clk), 
        .Q(lshift_quad2_state[2]), .QN(n686) );
  DFF_X1 \ff_quad2_state/d0_0/q_reg[2]  ( .D(quad2_state_in[2]), .CK(l1clk), 
        .Q(lshift_quad2_state[3]), .QN(n103) );
  DFF_X1 \ff_quad2_state/d0_0/q_reg[3]  ( .D(quad2_state_in[3]), .CK(l1clk), 
        .Q(lshift_quad2_state[0]), .QN(n688) );
  DFF_X1 \ff_latched_rdmad_read_wl/d0_0/q_reg[0]  ( .D(
        \ff_latched_rdmad_read_wl/fdin [0]), .CK(l1clk), .Q(
        latched_rdmad_read_wl[0]), .QN(n94) );
  DFF_X1 \ff_latched_rdmad_read_wl/d0_0/q_reg[2]  ( .D(
        \ff_latched_rdmad_read_wl/fdin [2]), .CK(l1clk), .Q(
        latched_rdmad_read_wl[2]), .QN(n96) );
  DFF_X1 \ff_latched_rdmad_read_wl/d0_0/q_reg[3]  ( .D(
        \ff_latched_rdmad_read_wl/fdin [3]), .CK(l1clk), .Q(
        latched_rdmad_read_wl[3]), .QN(n97) );
  DFF_X1 \ff_l2t_l2b_rdma_rdwl_r0/d0_0/q_reg[1]  ( .D(
        \ff_l2t_l2b_rdma_rdwl_r0/fdin [1]), .CK(l1clk), .Q(
        l2t_l2b_rdma_rdwl_r0[1]) );
  DFF_X1 \ff_l2t_l2b_rdma_rdwl_r0/d0_0/q_reg[0]  ( .D(
        \ff_l2t_l2b_rdma_rdwl_r0/fdin [0]), .CK(l1clk), .Q(
        l2t_l2b_rdma_rdwl_r0[0]), .QN(n911) );
  DFF_X1 \ff_latched_wb_read_en/d0_0/q_reg[0]  ( .D(
        \ff_latched_wb_read_en/fdin[0] ), .CK(l1clk), .Q(latched_wb_read_en), 
        .QN(n114) );
  DFF_X1 \ff_wb_count/d0_0/q_reg[0]  ( .D(\ff_wb_count/fdin [0]), .CK(l1clk), 
        .Q(wb_count[0]), .QN(n104) );
  DFF_X1 \ff_wb_count/d0_0/q_reg[1]  ( .D(\ff_wb_count/fdin [1]), .CK(l1clk), 
        .Q(wb_count[1]), .QN(n105) );
  DFF_X1 \ff_wb_count/d0_0/q_reg[2]  ( .D(\ff_wb_count/fdin [2]), .CK(l1clk), 
        .Q(wb_count[2]), .QN(n106) );
  DFF_X1 \ff_wb_count/d0_0/q_reg[3]  ( .D(\ff_wb_count/fdin [3]), .CK(l1clk), 
        .Q(wb_count[3]), .QN(n107) );
  DFF_X1 \ff_latched_rdma_read_en/d0_0/q_reg[0]  ( .D(
        \ff_latched_rdma_read_en/fdin[0] ), .CK(l1clk), .Q(
        latched_rdma_read_en), .QN(n662) );
  DFF_X1 \ff_wbuf_wr_addr_sel/d0_0/q_reg[0]  ( .D(n716), .CK(l1clk), .Q(
        wbuf_wr_addr_sel) );
  DFF_X1 \ff_wb_or_rdma_wr_req_en/d0_0/q_reg[0]  ( .D(
        wbuf_wbufrpt_leave_state0), .CK(l1clk), .Q(wbuf_wb_or_rdma_wr_req_en)
         );
  DFF_X1 \ff_cycle_count_in/d0_0/q_reg[0]  ( .D(cycle_count_less_than_7_din), 
        .CK(l1clk), .Q(cycle_count_less_than_7) );
  DFF_X1 \ff_wbuf_arb_full_px1/d0_0/q_reg[0]  ( .D(
        \ff_wbuf_arb_full_px1/fdin[0] ), .CK(l1clk), .Q(wbuf_arb_full_px1) );
  INV_X4 U4 ( .A(l2clk), .ZN(n10) );
  NAND2_X2 U74 ( .A1(n173), .A2(n174), .ZN(wbuf_misbuf_dep_mbid[4]) );
  NAND2_X2 U75 ( .A1(n1028), .A2(n175), .ZN(n174) );
  NAND4_X2 U76 ( .A1(n176), .A2(n177), .A3(n178), .A4(n179), .ZN(n175) );
  NAND2_X2 U77 ( .A1(mbid7[4]), .A2(n673), .ZN(n179) );
  NAND2_X2 U81 ( .A1(n183), .A2(n184), .ZN(n173) );
  NAND4_X2 U82 ( .A1(n185), .A2(n186), .A3(n187), .A4(n188), .ZN(n183) );
  NAND2_X2 U83 ( .A1(mbid3[4]), .A2(n674), .ZN(n188) );
  NAND2_X2 U87 ( .A1(n193), .A2(n194), .ZN(wbuf_misbuf_dep_mbid[3]) );
  NAND2_X2 U88 ( .A1(n1028), .A2(n195), .ZN(n194) );
  NAND4_X2 U89 ( .A1(n196), .A2(n197), .A3(n198), .A4(n199), .ZN(n195) );
  NAND2_X2 U90 ( .A1(mbid7[3]), .A2(n673), .ZN(n199) );
  NAND2_X2 U94 ( .A1(n200), .A2(n184), .ZN(n193) );
  NAND4_X2 U95 ( .A1(n201), .A2(n202), .A3(n203), .A4(n204), .ZN(n200) );
  NAND2_X2 U96 ( .A1(mbid3[3]), .A2(n674), .ZN(n204) );
  NAND2_X2 U100 ( .A1(n205), .A2(n206), .ZN(wbuf_misbuf_dep_mbid[2]) );
  NAND2_X2 U101 ( .A1(n1028), .A2(n207), .ZN(n206) );
  NAND4_X2 U102 ( .A1(n208), .A2(n209), .A3(n210), .A4(n211), .ZN(n207) );
  NAND2_X2 U103 ( .A1(mbid7[2]), .A2(n673), .ZN(n211) );
  NAND2_X2 U107 ( .A1(n212), .A2(n184), .ZN(n205) );
  NAND4_X2 U108 ( .A1(n213), .A2(n214), .A3(n215), .A4(n216), .ZN(n212) );
  NAND2_X2 U109 ( .A1(mbid3[2]), .A2(n674), .ZN(n216) );
  NAND2_X2 U113 ( .A1(n217), .A2(n218), .ZN(wbuf_misbuf_dep_mbid[1]) );
  NAND2_X2 U114 ( .A1(n1028), .A2(n219), .ZN(n218) );
  NAND4_X2 U115 ( .A1(n220), .A2(n221), .A3(n222), .A4(n223), .ZN(n219) );
  NAND2_X2 U116 ( .A1(mbid7[1]), .A2(n673), .ZN(n223) );
  NAND2_X2 U120 ( .A1(n224), .A2(n184), .ZN(n217) );
  NAND4_X2 U121 ( .A1(n225), .A2(n226), .A3(n227), .A4(n228), .ZN(n224) );
  NAND2_X2 U122 ( .A1(mbid3[1]), .A2(n674), .ZN(n228) );
  NAND2_X2 U126 ( .A1(n229), .A2(n230), .ZN(wbuf_misbuf_dep_mbid[0]) );
  NAND2_X2 U127 ( .A1(n1028), .A2(n231), .ZN(n230) );
  NAND4_X2 U128 ( .A1(n232), .A2(n233), .A3(n234), .A4(n235), .ZN(n231) );
  NAND2_X2 U129 ( .A1(mbid7[0]), .A2(n673), .ZN(n235) );
  NAND2_X2 U134 ( .A1(n236), .A2(n184), .ZN(n229) );
  NAND4_X2 U137 ( .A1(n238), .A2(n239), .A3(n240), .A4(n241), .ZN(n236) );
  NAND2_X2 U138 ( .A1(mbid3[0]), .A2(n674), .ZN(n241) );
  AND2_X2 U143 ( .A1(arb_wbuf_inst_vld_c2), .A2(wbuf_hit_unqual_c2), .ZN(
        wbuf_hit_qual_c2) );
  NAND2_X2 U144 ( .A1(n242), .A2(n243), .ZN(wbuf_hit_unqual_c2) );
  NAND2_X2 U145 ( .A1(n244), .A2(n110), .ZN(n243) );
  NAND2_X2 U146 ( .A1(n245), .A2(n246), .ZN(n244) );
  OR2_X2 U157 ( .A1(n256), .A2(vlddir_dirty_evict_c3), .ZN(n255) );
  AND3_X2 U158 ( .A1(arbdec_arbdp_inst_fb_c3), .A2(arb_wbuf_inst_vld_c3), .A3(
        l2_bypass_mode_on_d1), .ZN(n256) );
  AND2_X2 U159 ( .A1(wb_mbist_cam_sel_r1), .A2(n257), .ZN(
        wb_mbist_cam_hit_unreg) );
  NAND2_X2 U160 ( .A1(n258), .A2(n259), .ZN(n257) );
  NAND2_X2 U165 ( .A1(wbtag_write_wl_c5[7]), .A2(n109), .ZN(n260) );
  NAND2_X2 U168 ( .A1(wbtag_write_wl_c5[6]), .A2(n109), .ZN(n262) );
  NAND2_X2 U171 ( .A1(wbtag_write_wl_c5[5]), .A2(n109), .ZN(n264) );
  NAND2_X2 U174 ( .A1(wbtag_write_wl_c5[4]), .A2(n109), .ZN(n266) );
  NAND2_X2 U177 ( .A1(wbtag_write_wl_c5[3]), .A2(n109), .ZN(n268) );
  NAND2_X2 U180 ( .A1(wbtag_write_wl_c5[2]), .A2(n109), .ZN(n270) );
  NAND2_X2 U183 ( .A1(wbtag_write_wl_c5[1]), .A2(n109), .ZN(n272) );
  NAND2_X2 U186 ( .A1(wbtag_write_wl_c5[0]), .A2(n109), .ZN(n274) );
  NAND2_X2 U187 ( .A1(bypass_en_c2), .A2(n110), .ZN(n242) );
  NAND2_X2 U198 ( .A1(n284), .A2(n285), .ZN(quad2_state_in[3]) );
  NAND2_X2 U199 ( .A1(n286), .A2(lshift_quad2_state[0]), .ZN(n285) );
  NAND2_X2 U200 ( .A1(n86), .A2(lshift_quad2_state[3]), .ZN(n284) );
  NAND2_X2 U201 ( .A1(n287), .A2(n288), .ZN(quad2_state_in[2]) );
  NAND2_X2 U202 ( .A1(n286), .A2(lshift_quad2_state[3]), .ZN(n288) );
  NAND2_X2 U203 ( .A1(n86), .A2(lshift_quad2_state[2]), .ZN(n287) );
  NAND2_X2 U204 ( .A1(n289), .A2(n290), .ZN(quad2_state_in[1]) );
  NAND2_X2 U205 ( .A1(n286), .A2(lshift_quad2_state[2]), .ZN(n290) );
  NAND2_X2 U207 ( .A1(n86), .A2(lshift_quad2_state[1]), .ZN(n289) );
  NAND2_X2 U209 ( .A1(lshift_quad2_state[1]), .A2(n293), .ZN(n292) );
  NAND2_X2 U210 ( .A1(n86), .A2(lshift_quad2_state[0]), .ZN(n291) );
  NAND2_X2 U211 ( .A1(n727), .A2(n294), .ZN(n293) );
  OR4_X2 U212 ( .A1(wbuf_set_rdmat_acked[0]), .A2(wbuf_set_rdmat_acked[1]), 
        .A3(wbuf_set_rdmat_acked[2]), .A4(wbuf_set_rdmat_acked[3]), .ZN(n294)
         );
  NAND2_X2 U230 ( .A1(n278), .A2(n305), .ZN(n304) );
  OR4_X2 U231 ( .A1(latched_wb_read_wl[4]), .A2(latched_wb_read_wl[5]), .A3(
        latched_wb_read_wl[6]), .A4(latched_wb_read_wl[7]), .ZN(n305) );
  NAND2_X2 U245 ( .A1(n278), .A2(n316), .ZN(n315) );
  OR4_X2 U246 ( .A1(latched_wb_read_wl[0]), .A2(latched_wb_read_wl[1]), .A3(
        latched_wb_read_wl[2]), .A4(latched_wb_read_wl[3]), .ZN(n316) );
  NAND2_X2 U249 ( .A1(n318), .A2(n319), .ZN(l2t_sii_wib_dequeue) );
  NAND2_X2 U250 ( .A1(l2t_siu_delay), .A2(l2t_sii_wib_dequeue_delay), .ZN(n319) );
  NAND2_X2 U251 ( .A1(l2t_sii_wib_dequeue_raw), .A2(n30), .ZN(n318) );
  OR2_X2 U252 ( .A1(n85), .A2(cycle_count_less_than_7), .ZN(
        l2t_l2b_evict_en_r0) );
  NAND2_X2 U298 ( .A1(n366), .A2(n367), .ZN(n360) );
  NAND2_X2 U299 ( .A1(wb_count[0]), .A2(n105), .ZN(n366) );
  NAND2_X2 U319 ( .A1(n367), .A2(n386), .ZN(n376) );
  NAND2_X2 U320 ( .A1(n364), .A2(wb_count[0]), .ZN(n386) );
  AND2_X2 U323 ( .A1(n104), .A2(n385), .ZN(n388) );
  NAND2_X2 U328 ( .A1(latched_wb_read_en), .A2(n1033), .ZN(n389) );
  NAND2_X2 U329 ( .A1(n390), .A2(n391), .ZN(\ff_wb_acked/fdin [7]) );
  NAND2_X2 U332 ( .A1(n392), .A2(latched_wb_read_wl[7]), .ZN(n390) );
  NAND2_X2 U333 ( .A1(n393), .A2(n394), .ZN(\ff_wb_acked/fdin [6]) );
  NAND2_X2 U336 ( .A1(n392), .A2(latched_wb_read_wl[6]), .ZN(n393) );
  NAND2_X2 U337 ( .A1(n395), .A2(n396), .ZN(\ff_wb_acked/fdin [5]) );
  NAND2_X2 U340 ( .A1(n392), .A2(latched_wb_read_wl[5]), .ZN(n395) );
  NAND2_X2 U341 ( .A1(n397), .A2(n398), .ZN(\ff_wb_acked/fdin [4]) );
  NAND2_X2 U344 ( .A1(n392), .A2(latched_wb_read_wl[4]), .ZN(n397) );
  NAND2_X2 U345 ( .A1(n399), .A2(n400), .ZN(\ff_wb_acked/fdin [3]) );
  NAND2_X2 U348 ( .A1(n392), .A2(latched_wb_read_wl[3]), .ZN(n399) );
  NAND2_X2 U349 ( .A1(n401), .A2(n402), .ZN(\ff_wb_acked/fdin [2]) );
  NAND2_X2 U352 ( .A1(n392), .A2(latched_wb_read_wl[2]), .ZN(n401) );
  NAND2_X2 U353 ( .A1(n403), .A2(n404), .ZN(\ff_wb_acked/fdin [1]) );
  NAND2_X2 U356 ( .A1(n392), .A2(latched_wb_read_wl[1]), .ZN(n403) );
  NAND2_X2 U357 ( .A1(n405), .A2(n406), .ZN(\ff_wb_acked/fdin [0]) );
  NAND2_X2 U360 ( .A1(n392), .A2(latched_wb_read_wl[0]), .ZN(n405) );
  AND2_X2 U361 ( .A1(n278), .A2(n158), .ZN(n392) );
  NAND2_X2 U393 ( .A1(n423), .A2(n424), .ZN(\ff_mbid7/fdin [4]) );
  NAND2_X2 U394 ( .A1(mbid7[4]), .A2(n417), .ZN(n424) );
  NAND2_X2 U395 ( .A1(misbuf_wbuf_mbid_c4[4]), .A2(n18), .ZN(n423) );
  NAND2_X2 U396 ( .A1(n425), .A2(n426), .ZN(\ff_mbid7/fdin [3]) );
  NAND2_X2 U397 ( .A1(mbid7[3]), .A2(n417), .ZN(n426) );
  NAND2_X2 U398 ( .A1(misbuf_wbuf_mbid_c4[3]), .A2(n18), .ZN(n425) );
  NAND2_X2 U399 ( .A1(n427), .A2(n428), .ZN(\ff_mbid7/fdin [2]) );
  NAND2_X2 U400 ( .A1(mbid7[2]), .A2(n417), .ZN(n428) );
  NAND2_X2 U401 ( .A1(misbuf_wbuf_mbid_c4[2]), .A2(n18), .ZN(n427) );
  NAND2_X2 U402 ( .A1(n429), .A2(n430), .ZN(\ff_mbid7/fdin [1]) );
  NAND2_X2 U403 ( .A1(mbid7[1]), .A2(n417), .ZN(n430) );
  NAND2_X2 U404 ( .A1(misbuf_wbuf_mbid_c4[1]), .A2(n18), .ZN(n429) );
  NAND2_X2 U405 ( .A1(n431), .A2(n432), .ZN(\ff_mbid7/fdin [0]) );
  NAND2_X2 U406 ( .A1(mbid7[0]), .A2(n417), .ZN(n432) );
  NAND2_X2 U407 ( .A1(misbuf_wbuf_mbid_c4[0]), .A2(n18), .ZN(n431) );
  NAND2_X2 U408 ( .A1(wb_cam_hit_vec_c4[7]), .A2(n433), .ZN(n417) );
  NAND2_X2 U409 ( .A1(n434), .A2(n435), .ZN(\ff_mbid6/fdin [4]) );
  NAND2_X2 U410 ( .A1(mbid6[4]), .A2(n436), .ZN(n435) );
  NAND2_X2 U411 ( .A1(n17), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n434) );
  NAND2_X2 U412 ( .A1(n437), .A2(n438), .ZN(\ff_mbid6/fdin [3]) );
  NAND2_X2 U413 ( .A1(mbid6[3]), .A2(n436), .ZN(n438) );
  NAND2_X2 U414 ( .A1(n17), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n437) );
  NAND2_X2 U415 ( .A1(n439), .A2(n440), .ZN(\ff_mbid6/fdin [2]) );
  NAND2_X2 U416 ( .A1(mbid6[2]), .A2(n436), .ZN(n440) );
  NAND2_X2 U417 ( .A1(n17), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n439) );
  NAND2_X2 U418 ( .A1(n441), .A2(n442), .ZN(\ff_mbid6/fdin [1]) );
  NAND2_X2 U419 ( .A1(mbid6[1]), .A2(n436), .ZN(n442) );
  NAND2_X2 U420 ( .A1(n17), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n441) );
  NAND2_X2 U421 ( .A1(n443), .A2(n444), .ZN(\ff_mbid6/fdin [0]) );
  NAND2_X2 U422 ( .A1(mbid6[0]), .A2(n436), .ZN(n444) );
  NAND2_X2 U423 ( .A1(n17), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n443) );
  NAND2_X2 U424 ( .A1(wb_cam_hit_vec_c4[6]), .A2(n433), .ZN(n436) );
  NAND2_X2 U425 ( .A1(n445), .A2(n446), .ZN(\ff_mbid5/fdin [4]) );
  NAND2_X2 U426 ( .A1(mbid5[4]), .A2(n447), .ZN(n446) );
  NAND2_X2 U427 ( .A1(n16), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n445) );
  NAND2_X2 U428 ( .A1(n448), .A2(n449), .ZN(\ff_mbid5/fdin [3]) );
  NAND2_X2 U429 ( .A1(mbid5[3]), .A2(n447), .ZN(n449) );
  NAND2_X2 U430 ( .A1(n16), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n448) );
  NAND2_X2 U431 ( .A1(n450), .A2(n451), .ZN(\ff_mbid5/fdin [2]) );
  NAND2_X2 U432 ( .A1(mbid5[2]), .A2(n447), .ZN(n451) );
  NAND2_X2 U433 ( .A1(n16), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n450) );
  NAND2_X2 U434 ( .A1(n452), .A2(n453), .ZN(\ff_mbid5/fdin [1]) );
  NAND2_X2 U435 ( .A1(mbid5[1]), .A2(n447), .ZN(n453) );
  NAND2_X2 U436 ( .A1(n16), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n452) );
  NAND2_X2 U437 ( .A1(n454), .A2(n455), .ZN(\ff_mbid5/fdin [0]) );
  NAND2_X2 U438 ( .A1(mbid5[0]), .A2(n447), .ZN(n455) );
  NAND2_X2 U439 ( .A1(n16), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n454) );
  NAND2_X2 U440 ( .A1(wb_cam_hit_vec_c4[5]), .A2(n433), .ZN(n447) );
  NAND2_X2 U441 ( .A1(n456), .A2(n457), .ZN(\ff_mbid4/fdin [4]) );
  NAND2_X2 U442 ( .A1(mbid4[4]), .A2(n458), .ZN(n457) );
  NAND2_X2 U443 ( .A1(n15), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n456) );
  NAND2_X2 U444 ( .A1(n459), .A2(n460), .ZN(\ff_mbid4/fdin [3]) );
  NAND2_X2 U445 ( .A1(mbid4[3]), .A2(n458), .ZN(n460) );
  NAND2_X2 U446 ( .A1(n15), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n459) );
  NAND2_X2 U447 ( .A1(n461), .A2(n462), .ZN(\ff_mbid4/fdin [2]) );
  NAND2_X2 U448 ( .A1(mbid4[2]), .A2(n458), .ZN(n462) );
  NAND2_X2 U449 ( .A1(n15), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n461) );
  NAND2_X2 U450 ( .A1(n463), .A2(n464), .ZN(\ff_mbid4/fdin [1]) );
  NAND2_X2 U451 ( .A1(mbid4[1]), .A2(n458), .ZN(n464) );
  NAND2_X2 U452 ( .A1(n15), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n463) );
  NAND2_X2 U453 ( .A1(n465), .A2(n466), .ZN(\ff_mbid4/fdin [0]) );
  NAND2_X2 U454 ( .A1(mbid4[0]), .A2(n458), .ZN(n466) );
  NAND2_X2 U455 ( .A1(n15), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n465) );
  NAND2_X2 U456 ( .A1(wb_cam_hit_vec_c4[4]), .A2(n433), .ZN(n458) );
  NAND2_X2 U457 ( .A1(n467), .A2(n468), .ZN(\ff_mbid3/fdin [4]) );
  NAND2_X2 U458 ( .A1(mbid3[4]), .A2(n414), .ZN(n468) );
  NAND2_X2 U459 ( .A1(misbuf_wbuf_mbid_c4[4]), .A2(n744), .ZN(n467) );
  NAND2_X2 U460 ( .A1(n469), .A2(n470), .ZN(\ff_mbid3/fdin [3]) );
  NAND2_X2 U461 ( .A1(mbid3[3]), .A2(n414), .ZN(n470) );
  NAND2_X2 U462 ( .A1(misbuf_wbuf_mbid_c4[3]), .A2(n744), .ZN(n469) );
  NAND2_X2 U463 ( .A1(n471), .A2(n472), .ZN(\ff_mbid3/fdin [2]) );
  NAND2_X2 U464 ( .A1(mbid3[2]), .A2(n414), .ZN(n472) );
  NAND2_X2 U465 ( .A1(misbuf_wbuf_mbid_c4[2]), .A2(n744), .ZN(n471) );
  NAND2_X2 U466 ( .A1(n473), .A2(n474), .ZN(\ff_mbid3/fdin [1]) );
  NAND2_X2 U467 ( .A1(mbid3[1]), .A2(n414), .ZN(n474) );
  NAND2_X2 U468 ( .A1(misbuf_wbuf_mbid_c4[1]), .A2(n744), .ZN(n473) );
  NAND2_X2 U469 ( .A1(n475), .A2(n476), .ZN(\ff_mbid3/fdin [0]) );
  NAND2_X2 U470 ( .A1(mbid3[0]), .A2(n414), .ZN(n476) );
  NAND2_X2 U471 ( .A1(misbuf_wbuf_mbid_c4[0]), .A2(n744), .ZN(n475) );
  NAND2_X2 U472 ( .A1(wb_cam_hit_vec_c4[3]), .A2(n433), .ZN(n414) );
  NAND2_X2 U473 ( .A1(n477), .A2(n478), .ZN(\ff_mbid2/fdin [4]) );
  NAND2_X2 U474 ( .A1(mbid2[4]), .A2(n479), .ZN(n478) );
  NAND2_X2 U475 ( .A1(n13), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n477) );
  NAND2_X2 U476 ( .A1(n480), .A2(n481), .ZN(\ff_mbid2/fdin [3]) );
  NAND2_X2 U477 ( .A1(mbid2[3]), .A2(n479), .ZN(n481) );
  NAND2_X2 U478 ( .A1(n13), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n480) );
  NAND2_X2 U479 ( .A1(n482), .A2(n483), .ZN(\ff_mbid2/fdin [2]) );
  NAND2_X2 U480 ( .A1(mbid2[2]), .A2(n479), .ZN(n483) );
  NAND2_X2 U481 ( .A1(n13), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n482) );
  NAND2_X2 U482 ( .A1(n484), .A2(n485), .ZN(\ff_mbid2/fdin [1]) );
  NAND2_X2 U483 ( .A1(mbid2[1]), .A2(n479), .ZN(n485) );
  NAND2_X2 U484 ( .A1(n13), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n484) );
  NAND2_X2 U485 ( .A1(n486), .A2(n487), .ZN(\ff_mbid2/fdin [0]) );
  NAND2_X2 U486 ( .A1(mbid2[0]), .A2(n479), .ZN(n487) );
  NAND2_X2 U487 ( .A1(n13), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n486) );
  NAND2_X2 U488 ( .A1(wb_cam_hit_vec_c4[2]), .A2(n433), .ZN(n479) );
  NAND2_X2 U489 ( .A1(n488), .A2(n489), .ZN(\ff_mbid1/fdin [4]) );
  NAND2_X2 U490 ( .A1(mbid1[4]), .A2(n490), .ZN(n489) );
  NAND2_X2 U491 ( .A1(n12), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n488) );
  NAND2_X2 U492 ( .A1(n491), .A2(n492), .ZN(\ff_mbid1/fdin [3]) );
  NAND2_X2 U493 ( .A1(mbid1[3]), .A2(n490), .ZN(n492) );
  NAND2_X2 U494 ( .A1(n12), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n491) );
  NAND2_X2 U495 ( .A1(n493), .A2(n494), .ZN(\ff_mbid1/fdin [2]) );
  NAND2_X2 U496 ( .A1(mbid1[2]), .A2(n490), .ZN(n494) );
  NAND2_X2 U497 ( .A1(n12), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n493) );
  NAND2_X2 U498 ( .A1(n495), .A2(n496), .ZN(\ff_mbid1/fdin [1]) );
  NAND2_X2 U499 ( .A1(mbid1[1]), .A2(n490), .ZN(n496) );
  NAND2_X2 U500 ( .A1(n12), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n495) );
  NAND2_X2 U501 ( .A1(n497), .A2(n498), .ZN(\ff_mbid1/fdin [0]) );
  NAND2_X2 U502 ( .A1(mbid1[0]), .A2(n490), .ZN(n498) );
  NAND2_X2 U503 ( .A1(n12), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n497) );
  NAND2_X2 U504 ( .A1(wb_cam_hit_vec_c4[1]), .A2(n433), .ZN(n490) );
  NAND2_X2 U505 ( .A1(n499), .A2(n500), .ZN(\ff_mbid0/fdin [4]) );
  NAND2_X2 U506 ( .A1(mbid0[4]), .A2(n501), .ZN(n500) );
  NAND2_X2 U507 ( .A1(n11), .A2(misbuf_wbuf_mbid_c4[4]), .ZN(n499) );
  NAND2_X2 U508 ( .A1(n502), .A2(n503), .ZN(\ff_mbid0/fdin [3]) );
  NAND2_X2 U509 ( .A1(mbid0[3]), .A2(n501), .ZN(n503) );
  NAND2_X2 U510 ( .A1(n11), .A2(misbuf_wbuf_mbid_c4[3]), .ZN(n502) );
  NAND2_X2 U511 ( .A1(n504), .A2(n505), .ZN(\ff_mbid0/fdin [2]) );
  NAND2_X2 U512 ( .A1(mbid0[2]), .A2(n501), .ZN(n505) );
  NAND2_X2 U513 ( .A1(n11), .A2(misbuf_wbuf_mbid_c4[2]), .ZN(n504) );
  NAND2_X2 U514 ( .A1(n506), .A2(n507), .ZN(\ff_mbid0/fdin [1]) );
  NAND2_X2 U515 ( .A1(mbid0[1]), .A2(n501), .ZN(n507) );
  NAND2_X2 U516 ( .A1(n11), .A2(misbuf_wbuf_mbid_c4[1]), .ZN(n506) );
  NAND2_X2 U517 ( .A1(n508), .A2(n509), .ZN(\ff_mbid0/fdin [0]) );
  NAND2_X2 U518 ( .A1(mbid0[0]), .A2(n501), .ZN(n509) );
  NAND2_X2 U519 ( .A1(n11), .A2(misbuf_wbuf_mbid_c4[0]), .ZN(n508) );
  NAND2_X2 U520 ( .A1(wb_cam_hit_vec_c4[0]), .A2(n433), .ZN(n501) );
  NAND2_X2 U635 ( .A1(n103), .A2(n606), .ZN(n605) );
  NAND2_X2 U636 ( .A1(lshift_quad2_state[2]), .A2(n1031), .ZN(n606) );
  NAND2_X2 U647 ( .A1(n102), .A2(n614), .ZN(n613) );
  NAND2_X2 U648 ( .A1(lshift_quad2_state[0]), .A2(n1029), .ZN(n614) );
  NAND2_X2 U681 ( .A1(n635), .A2(n636), .ZN(\ff_cycle_count/fdin [3]) );
  OR2_X2 U684 ( .A1(n639), .A2(n92), .ZN(n638) );
  NAND2_X2 U685 ( .A1(n640), .A2(n641), .ZN(\ff_cycle_count/fdin [2]) );
  NAND2_X2 U688 ( .A1(n642), .A2(n643), .ZN(n639) );
  NAND2_X2 U690 ( .A1(n644), .A2(n645), .ZN(\ff_cycle_count/fdin [1]) );
  OR3_X2 U691 ( .A1(n726), .A2(n642), .A3(n91), .ZN(n645) );
  NAND2_X2 U695 ( .A1(n648), .A2(n649), .ZN(\ff_cycle_count/fdin [0]) );
  NAND2_X2 U697 ( .A1(\next_state[2] ), .A2(n90), .ZN(n648) );
  NAND4_X2 U698 ( .A1(n41), .A2(n42), .A3(n43), .A4(n44), .ZN(
        enc_write_wl_c5[2]) );
  NAND4_X2 U699 ( .A1(n39), .A2(n40), .A3(n43), .A4(n44), .ZN(
        enc_write_wl_c5[1]) );
  NAND4_X2 U700 ( .A1(n38), .A2(n40), .A3(n42), .A4(n44), .ZN(
        enc_write_wl_c5[0]) );
  NAND2_X2 U707 ( .A1(state[1]), .A2(mcu_l2t_wr_ack_d1), .ZN(n283) );
  AND2_X2 U708 ( .A1(arbadr_c1_addr_eq_wb_c4), .A2(wbuf_wbtag_write_en_c4), 
        .ZN(bypass_en_c1) );
  INV_X4 U716 ( .A(n417), .ZN(n18) );
  INV_X4 U728 ( .A(l2t_siu_delay), .ZN(n30) );
  INV_X4 U742 ( .A(n315), .ZN(n70) );
  INV_X4 U747 ( .A(n304), .ZN(n75) );
  INV_X4 U752 ( .A(n364), .ZN(n80) );
  INV_X4 U753 ( .A(n371), .ZN(n81) );
  INV_X4 U754 ( .A(\next_state[2] ), .ZN(n83) );
  INV_X4 U755 ( .A(n116), .ZN(n84) );
  INV_X4 U756 ( .A(n283), .ZN(n85) );
  INV_X4 U757 ( .A(n293), .ZN(n86) );
  INV_X4 U759 ( .A(n242), .ZN(n109) );
  DFF_X2 \ff_or_wb_mbid_vld/d0_0/q_reg[0]  ( .D(\ff_or_wb_mbid_vld/fdin[0] ), 
        .CK(l1clk), .Q(or_wb_mbid_vld), .QN(n113) );
  DFF_X2 \ff_l2t_mb2_run_r1/d0_0/q_reg[0]  ( .D(l2t_mb2_run), .CK(l1clk), .Q(
        l2t_mb2_run_r1), .QN(n35) );
  DFF_X2 \ff_wb_valid/d0_0/q_reg[6]  ( .D(\ff_wb_valid/fdin [6]), .CK(l1clk), 
        .Q(wb_valid[6]), .QN(n57) );
  DFF_X2 \ff_wb_valid/d0_0/q_reg[0]  ( .D(\ff_wb_valid/fdin [0]), .CK(l1clk), 
        .Q(wb_valid[0]), .QN(n45) );
  DFF_X2 \ff_cycle_count/d0_0/q_reg[3]  ( .D(\ff_cycle_count/fdin [3]), .CK(
        l1clk), .Q(\cycle_count[3] ), .QN(n93) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[4]  ( .D(\ff_wb_valid/fdin [4]), .CK(l1clk), 
        .Q(wb_valid[4]), .QN(n53) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[1]  ( .D(\ff_wb_valid/fdin [1]), .CK(l1clk), 
        .Q(wb_valid[1]), .QN(n47) );
  SDFF_X1 \ff_l2t_sii_wib_dequeue/d0_0/q_reg[0]  ( .D(latched_rdma_read_en), 
        .SI(1'b0), .SE(n158), .CK(l1clk), .Q(l2t_sii_wib_dequeue_raw) );
  SDFF_X1 \ff_wbtag_write_en_c4/d0_0/q_reg[0]  ( .D(n255), .SI(
        l2t_mb2_wbtag_wr_en_r2), .SE(l2t_mb2_run_r1), .CK(l1clk), .Q(
        wbuf_wbtag_write_en_c4) );
  SDFF_X1 \ff_l2t_dbg_sii_wib_dequeue/d0_0/q_reg[0]  ( .D(
        l2t_sii_wib_dequeue_raw), .SI(l2t_sii_wib_dequeue_delay), .SE(
        l2t_siu_delay), .CK(l1clk), .Q(l2t_dbg_sii_wib_dequeue) );
  DFF_X1 \ff_wb_valid/d0_0/q_reg[2]  ( .D(\ff_wb_valid/fdin [2]), .CK(l1clk), 
        .Q(wb_valid[2]), .QN(n49) );
  DFF_X1 \ff_wbtag_write_en_c4/d0_0/q_reg[2]  ( .D(l2t_mb2_wbtag_wr_en), .CK(
        l1clk), .Q(l2t_mb2_wbtag_wr_en_r1) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[0]  ( .D(wb_mbist_cam_sel), .CK(l1clk), 
        .Q(wb_mbist_cam_sel_r1) );
  DFF_X1 \ff_mcu_l2t_wr_ack_d1/d0_0/q_reg[0]  ( .D(mcu_l2t_wr_ack), .CK(l1clk), 
        .Q(mcu_l2t_wr_ack_d1) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[15]  ( .D(l2t_mb2_rdmatag_rd_en), .CK(
        l1clk), .Q(l2t_mb2_rdmatag_rd_en_r1) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[12]  ( .D(l2t_mb2_wbtag_rd_en), .CK(
        l1clk), .Q(l2t_mb2_wbtag_rd_en_r1) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[9]  ( .D(l2t_mb2_addr[2]), .CK(l1clk), 
        .Q(l2t_mb2_addr_r1[2]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[8]  ( .D(l2t_mb2_addr[1]), .CK(l1clk), 
        .Q(l2t_mb2_addr_r1[1]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[7]  ( .D(l2t_mb2_addr[0]), .CK(l1clk), 
        .Q(l2t_mb2_addr_r1[0]) );
  DFF_X1 \ff_l2_bypass_mode_on_d1/d0_0/q_reg[0]  ( .D(csr_l2_bypass_mode_on), 
        .CK(l1clk), .Q(l2_bypass_mode_on_d1) );
  DFF_X1 \ff_arbdp_inst_fb_c3/d0_0/q_reg[0]  ( .D(arbdec_arbdp_inst_fb_c2), 
        .CK(l1clk), .Q(arbdec_arbdp_inst_fb_c3) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[8]  ( .D(wb_cam_match_c2[7]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[7]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[7]  ( .D(wb_cam_match_c2[6]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[6]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[6]  ( .D(wb_cam_match_c2[5]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[5]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[5]  ( .D(wb_cam_match_c2[4]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[4]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[4]  ( .D(wb_cam_match_c2[3]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[3]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[3]  ( .D(wb_cam_match_c2[2]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[2]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[2]  ( .D(wb_cam_match_c2[1]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[1]) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[1]  ( .D(wb_cam_match_c2[0]), .CK(
        l1clk), .Q(wb_cam_match_c2_d1[0]) );
  DFF_X1 \ff_arb_wbuf_inst_vld_c3/d0_0/q_reg[0]  ( .D(arb_wbuf_inst_vld_c2), 
        .CK(l1clk), .Q(arb_wbuf_inst_vld_c3) );
  DFF_X1 \ff_wbtag_write_we_c6/d0_0/q_reg[0]  ( .D(wbtag_write_we_c52), .CK(
        l1clk), .Q(\l2t_l2b_wbwr_wen_c6[3] ) );
  DFF_X1 \ff_wbtag_write_en_c4/d0_0/q_reg[1]  ( .D(l2t_mb2_wbtag_wr_en_r1), 
        .CK(l1clk), .Q(l2t_mb2_wbtag_wr_en_r2) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[7]  ( .D(wb_cam_hit_vec_c3[7]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[7]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[6]  ( .D(wb_cam_hit_vec_c3[6]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[6]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[5]  ( .D(wb_cam_hit_vec_c3[5]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[5]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[4]  ( .D(wb_cam_hit_vec_c3[4]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[4]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[3]  ( .D(wb_cam_hit_vec_c3[3]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[3]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[2]  ( .D(wb_cam_hit_vec_c3[2]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[2]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[1]  ( .D(wb_cam_hit_vec_c3[1]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[1]) );
  DFF_X1 \ff_wb_cam_hit_vec_c4/d0_0/q_reg[0]  ( .D(wb_cam_hit_vec_c3[0]), .CK(
        l1clk), .Q(wb_cam_hit_vec_c4[0]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[14]  ( .D(l2t_mb2_rdmatag_rd_en_r1), 
        .CK(l1clk), .Q(l2t_mb2_rdmatag_rd_en_r2) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[13]  ( .D(l2t_mb2_rdmatag_rd_en_r2), 
        .CK(l1clk), .Q(l2t_mb2_rdmatag_rd_en_r3) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[11]  ( .D(l2t_mb2_wbtag_rd_en_r1), .CK(
        l1clk), .Q(l2t_mb2_wbtag_rd_en_r2) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[10]  ( .D(l2t_mb2_wbtag_rd_en_r2), .CK(
        l1clk), .Q(l2t_mb2_wbtag_rd_en_r3) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[6]  ( .D(l2t_mb2_addr_r1[2]), .CK(l1clk), .Q(l2t_mb2_addr_r2[2]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[5]  ( .D(l2t_mb2_addr_r1[1]), .CK(l1clk), .Q(l2t_mb2_addr_r2[1]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[4]  ( .D(l2t_mb2_addr_r1[0]), .CK(l1clk), .Q(l2t_mb2_addr_r2[0]) );
  DFF_X1 \ff_l2t_mb2_run_r1/d0_0/q_reg[2]  ( .D(l2t_mb2_addr_r2[1]), .CK(l1clk), .Q(l2t_mb2_addr_r3[1]) );
  DFF_X1 \ff_enc_write_wl_c6/d0_0/q_reg[2]  ( .D(enc_write_wl_c52[2]), .CK(
        l1clk), .Q(l2t_l2b_wbwr_wl_c6[2]) );
  DFF_X1 \ff_enc_write_wl_c6/d0_0/q_reg[1]  ( .D(enc_write_wl_c52[1]), .CK(
        l1clk), .Q(l2t_l2b_wbwr_wl_c6[1]) );
  DFF_X1 \ff_enc_write_wl_c6/d0_0/q_reg[0]  ( .D(enc_write_wl_c52[0]), .CK(
        l1clk), .Q(l2t_l2b_wbwr_wl_c6[0]) );
  DFF_X1 \ff_l2t_mcu_wr_req/d0_0/q_reg[0]  ( .D(wbuf_wb_or_rdma_wr_req_en), 
        .CK(l1clk), .Q(l2t_mcu_wr_req) );
  DFF_X1 \ff_wbtag_write_we_c52/d0_0/q_reg[0]  ( .D(wbtag_write_we_c5), .CK(
        l1clk), .Q(wbtag_write_we_c52) );
  DFF_X1 \ff_l2t_sii_wib_dequeue_delay/d0_0/q_reg[0]  ( .D(
        l2t_sii_wib_dequeue_raw), .CK(l1clk), .Q(l2t_sii_wib_dequeue_delay) );
  DFF_X1 \ff_enc_write_wl_c52/d0_0/q_reg[1]  ( .D(enc_write_wl_c5[1]), .CK(
        l1clk), .Q(enc_write_wl_c52[1]) );
  DFF_X1 \ff_enc_write_wl_c52/d0_0/q_reg[0]  ( .D(enc_write_wl_c5[0]), .CK(
        l1clk), .Q(enc_write_wl_c52[0]) );
  DFF_X1 \ff_enc_write_wl_c52/d0_0/q_reg[2]  ( .D(enc_write_wl_c5[2]), .CK(
        l1clk), .Q(enc_write_wl_c52[2]) );
  DFF_X1 \ff_bypass_en_c2/d0_0/q_reg[0]  ( .D(bypass_en_c1), .CK(l1clk), .Q(
        bypass_en_c2) );
  DFF_X1 \ff_wb_cam_match_c2/d0_0/q_reg[9]  ( .D(wb_mbist_cam_hit_unreg), .CK(
        l1clk), .Q(wb_mbist_cam_hit) );
  DFF_X1 \ff_quad_state/d0_0/q_reg[2]  ( .D(quad_state_in[2]), .CK(l1clk), .Q(
        lshift_quad_state[0]), .QN(n885) );
  DFF_X1 \ff_wbtag_write_wl_c5/d0_0/q_reg[0]  ( .D(wbuf_wbtag_write_wl_c4[0]), 
        .CK(l1clk), .Q(wbtag_write_wl_c5[0]) );
  DFF_X1 \ff_mbid6/d0_0/q_reg[4]  ( .D(\ff_mbid6/fdin [4]), .CK(l1clk), .Q(
        mbid6[4]) );
  DFF_X1 \ff_mbid6/d0_0/q_reg[3]  ( .D(\ff_mbid6/fdin [3]), .CK(l1clk), .Q(
        mbid6[3]) );
  DFF_X1 \ff_mbid6/d0_0/q_reg[2]  ( .D(\ff_mbid6/fdin [2]), .CK(l1clk), .Q(
        mbid6[2]) );
  DFF_X1 \ff_mbid6/d0_0/q_reg[1]  ( .D(\ff_mbid6/fdin [1]), .CK(l1clk), .Q(
        mbid6[1]) );
  DFF_X1 \ff_mbid6/d0_0/q_reg[0]  ( .D(\ff_mbid6/fdin [0]), .CK(l1clk), .Q(
        mbid6[0]) );
  DFF_X1 \ff_mbid5/d0_0/q_reg[4]  ( .D(\ff_mbid5/fdin [4]), .CK(l1clk), .Q(
        mbid5[4]) );
  DFF_X1 \ff_mbid5/d0_0/q_reg[3]  ( .D(\ff_mbid5/fdin [3]), .CK(l1clk), .Q(
        mbid5[3]) );
  DFF_X1 \ff_mbid5/d0_0/q_reg[2]  ( .D(\ff_mbid5/fdin [2]), .CK(l1clk), .Q(
        mbid5[2]) );
  DFF_X1 \ff_mbid5/d0_0/q_reg[1]  ( .D(\ff_mbid5/fdin [1]), .CK(l1clk), .Q(
        mbid5[1]) );
  DFF_X1 \ff_mbid5/d0_0/q_reg[0]  ( .D(\ff_mbid5/fdin [0]), .CK(l1clk), .Q(
        mbid5[0]) );
  DFF_X1 \ff_mbid4/d0_0/q_reg[4]  ( .D(\ff_mbid4/fdin [4]), .CK(l1clk), .Q(
        mbid4[4]) );
  DFF_X1 \ff_mbid4/d0_0/q_reg[3]  ( .D(\ff_mbid4/fdin [3]), .CK(l1clk), .Q(
        mbid4[3]) );
  DFF_X1 \ff_mbid4/d0_0/q_reg[2]  ( .D(\ff_mbid4/fdin [2]), .CK(l1clk), .Q(
        mbid4[2]) );
  DFF_X1 \ff_mbid4/d0_0/q_reg[1]  ( .D(\ff_mbid4/fdin [1]), .CK(l1clk), .Q(
        mbid4[1]) );
  DFF_X1 \reset_flop/d0_0/q_reg[0]  ( .D(wmr_l), .CK(l1clk), .Q(n730), .QN(
        n726) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[3]  ( .D(\ff_wb_mbid_vld/fdin [3]), .CK(
        l1clk), .Q(wb_mbid_vld[3]), .QN(n720) );
  DFF_X1 \ff_wb_mbid_vld/d0_0/q_reg[2]  ( .D(\ff_wb_mbid_vld/fdin [2]), .CK(
        l1clk), .Q(wb_mbid_vld[2]), .QN(n719) );
  NAND2_X1 U761 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n1021), .ZN(n786) );
  NAND2_X2 U762 ( .A1(n35), .A2(n1021), .ZN(n1022) );
  NAND2_X2 U763 ( .A1(lshift_quad_state[0]), .A2(n656), .ZN(n827) );
  NAND2_X4 U764 ( .A1(n700), .A2(n667), .ZN(n891) );
  NAND4_X1 U765 ( .A1(n35), .A2(n993), .A3(n992), .A4(n1000), .ZN(n994) );
  AND2_X4 U766 ( .A1(n676), .A2(n1031), .ZN(n656) );
  NAND2_X1 U767 ( .A1(n874), .A2(n873), .ZN(n721) );
  AND2_X4 U768 ( .A1(n35), .A2(n1018), .ZN(n658) );
  AND2_X4 U769 ( .A1(n35), .A2(n706), .ZN(n659) );
  AND2_X4 U770 ( .A1(lshift_quad1_state[1]), .A2(n35), .ZN(n660) );
  AND3_X4 U771 ( .A1(n966), .A2(n35), .A3(rdmat_pick_vec[0]), .ZN(n661) );
  NAND2_X1 U772 ( .A1(lshift_quad2_state[3]), .A2(n676), .ZN(n912) );
  INV_X4 U773 ( .A(n722), .ZN(n724) );
  NOR3_X2 U774 ( .A1(rdmat_pick_vec[0]), .A2(rdmat_pick_vec[2]), .A3(
        rdmat_pick_vec[3]), .ZN(n676) );
  NAND2_X2 U775 ( .A1(n945), .A2(n724), .ZN(n879) );
  NOR2_X1 U776 ( .A1(n332), .A2(wb_valid[5]), .ZN(n872) );
  NAND2_X2 U777 ( .A1(wb_valid[5]), .A2(n724), .ZN(n874) );
  NAND3_X1 U778 ( .A1(n1016), .A2(n1017), .A3(n658), .ZN(n1019) );
  NAND2_X2 U779 ( .A1(wb_mbid_vld[5]), .A2(or_wb_mbid_vld), .ZN(n873) );
  NAND2_X2 U780 ( .A1(n885), .A2(n886), .ZN(n663) );
  NAND2_X2 U781 ( .A1(n664), .A2(n884), .ZN(n887) );
  INV_X4 U782 ( .A(n663), .ZN(n664) );
  NAND3_X2 U783 ( .A1(n705), .A2(n875), .A3(n678), .ZN(n886) );
  NAND4_X1 U784 ( .A1(n883), .A2(n675), .A3(n882), .A4(n881), .ZN(n884) );
  NAND2_X2 U785 ( .A1(wb_valid[0]), .A2(n724), .ZN(n817) );
  NAND2_X2 U786 ( .A1(n819), .A2(n817), .ZN(n665) );
  NAND3_X2 U787 ( .A1(n780), .A2(n841), .A3(n666), .ZN(n781) );
  INV_X4 U788 ( .A(n665), .ZN(n666) );
  NAND2_X1 U789 ( .A1(n779), .A2(or_wb_mbid_vld), .ZN(n780) );
  NAND2_X1 U790 ( .A1(lshift_quad0_state[3]), .A2(n883), .ZN(n835) );
  INV_X4 U791 ( .A(n974), .ZN(n667) );
  NAND2_X4 U792 ( .A1(n783), .A2(n89), .ZN(n1017) );
  INV_X4 U793 ( .A(n989), .ZN(n725) );
  INV_X4 U794 ( .A(n49), .ZN(n668) );
  OR2_X2 U795 ( .A1(n57), .A2(n722), .ZN(n865) );
  AND2_X4 U796 ( .A1(n874), .A2(n873), .ZN(n678) );
  AND2_X2 U797 ( .A1(n874), .A2(n873), .ZN(n675) );
  INV_X8 U798 ( .A(n962), .ZN(n974) );
  NAND2_X1 U799 ( .A1(n659), .A2(n974), .ZN(n969) );
  NAND2_X1 U800 ( .A1(n776), .A2(n865), .ZN(n1018) );
  AND4_X4 U801 ( .A1(n866), .A2(n865), .A3(lshift_quad_state[2]), .A4(n864), 
        .ZN(n705) );
  AND2_X4 U802 ( .A1(n1017), .A2(n721), .ZN(n679) );
  OR2_X4 U803 ( .A1(n731), .A2(tcu_scan_en), .ZN(l1clk) );
  NAND2_X1 U804 ( .A1(n661), .A2(n974), .ZN(n967) );
  INV_X1 U805 ( .A(n724), .ZN(n670) );
  AND2_X4 U806 ( .A1(n879), .A2(n878), .ZN(n677) );
  AND3_X4 U807 ( .A1(wb_valid[2]), .A2(wb_valid[3]), .A3(n695), .ZN(n693) );
  AND3_X4 U808 ( .A1(wb_cam_match_c2[2]), .A2(n668), .A3(n64), .ZN(n694) );
  AND3_X4 U809 ( .A1(wb_cam_match_c2[6]), .A2(wb_valid[6]), .A3(n68), .ZN(n683) );
  AND3_X4 U810 ( .A1(wb_cam_match_c2[0]), .A2(wb_valid[0]), .A3(n61), .ZN(n696) );
  AND3_X4 U811 ( .A1(n63), .A2(wb_cam_match_c2[1]), .A3(wb_valid[1]), .ZN(n699) );
  AND3_X4 U812 ( .A1(wb_cam_match_c2[4]), .A2(wb_valid[4]), .A3(n66), .ZN(n682) );
  AND3_X4 U813 ( .A1(wb_cam_match_c2[5]), .A2(wb_valid[5]), .A3(n67), .ZN(n701) );
  AND3_X4 U814 ( .A1(wb_cam_match_c2[3]), .A2(n65), .A3(wb_valid[3]), .ZN(n697) );
  AND3_X4 U815 ( .A1(n714), .A2(wb_valid[5]), .A3(n693), .ZN(n702) );
  NAND2_X1 U816 ( .A1(n720), .A2(n744), .ZN(n745) );
  NAND2_X1 U817 ( .A1(n863), .A2(n675), .ZN(n803) );
  INV_X4 U818 ( .A(n59), .ZN(n671) );
  NAND2_X1 U819 ( .A1(n676), .A2(n1031), .ZN(n672) );
  NAND2_X2 U820 ( .A1(n660), .A2(n680), .ZN(n1010) );
  NOR2_X2 U821 ( .A1(n729), .A2(n85), .ZN(n116) );
  NOR2_X2 U822 ( .A1(n729), .A2(n86), .ZN(n286) );
  NOR2_X2 U823 ( .A1(n729), .A2(n75), .ZN(n297) );
  NOR2_X2 U824 ( .A1(n729), .A2(n70), .ZN(n308) );
  NOR3_X2 U825 ( .A1(n646), .A2(n1033), .A3(n729), .ZN(\next_state[2] ) );
  NOR2_X2 U826 ( .A1(n283), .A2(n729), .ZN(n278) );
  AND3_X2 U827 ( .A1(n898), .A2(n897), .A3(n896), .ZN(n673) );
  AND3_X2 U828 ( .A1(n894), .A2(n893), .A3(n892), .ZN(n674) );
  INV_X8 U829 ( .A(n158), .ZN(n1033) );
  NOR2_X2 U830 ( .A1(n385), .A2(n376), .ZN(n384) );
  INV_X4 U831 ( .A(n730), .ZN(n729) );
  INV_X4 U832 ( .A(n722), .ZN(n723) );
  AND2_X2 U833 ( .A1(n1017), .A2(n1008), .ZN(n680) );
  AND2_X2 U834 ( .A1(n776), .A2(n865), .ZN(n681) );
  INV_X4 U835 ( .A(n933), .ZN(n1004) );
  INV_X4 U836 ( .A(n1001), .ZN(n853) );
  NOR2_X2 U837 ( .A1(misbuf_filbuf_mcu_pick), .A2(mcu_req_pending), .ZN(n771)
         );
  NOR2_X2 U838 ( .A1(n112), .A2(misbuf_hit_c4), .ZN(n433) );
  NOR4_X2 U839 ( .A1(n80), .A2(wb_count[0]), .A3(wb_count[1]), .A4(wb_count[2]), .ZN(n370) );
  NOR2_X2 U840 ( .A1(n877), .A2(n876), .ZN(n882) );
  NOR3_X2 U841 ( .A1(n90), .A2(n92), .A3(n91), .ZN(n637) );
  NAND2_X2 U842 ( .A1(wb_count[0]), .A2(n365), .ZN(n371) );
  NOR4_X2 U843 ( .A1(n360), .A2(n361), .A3(n362), .A4(n363), .ZN(n359) );
  NOR2_X2 U844 ( .A1(wb_count[2]), .A2(n364), .ZN(n362) );
  NOR2_X2 U845 ( .A1(n105), .A2(n80), .ZN(n363) );
  NAND2_X2 U846 ( .A1(n778), .A2(n777), .ZN(n784) );
  INV_X4 U847 ( .A(n790), .ZN(n778) );
  NOR2_X2 U848 ( .A1(state[2]), .A2(n85), .ZN(n646) );
  NOR4_X2 U849 ( .A1(rdmat_or_rdmat_valid), .A2(wb_valid[0]), .A3(wb_valid[2]), 
        .A4(n945), .ZN(n769) );
  NOR2_X2 U850 ( .A1(wbuf_wbufrpt_leave_state0), .A2(mcu_req_pending), .ZN(
        n422) );
  NOR3_X2 U851 ( .A1(n376), .A2(n377), .A3(n378), .ZN(n375) );
  NOR2_X2 U852 ( .A1(wb_count[1]), .A2(n364), .ZN(n377) );
  NAND3_X2 U853 ( .A1(n88), .A2(n827), .A3(n826), .ZN(n1000) );
  NAND2_X2 U854 ( .A1(wb_valid[7]), .A2(n723), .ZN(n864) );
  OR2_X2 U855 ( .A1(n863), .A2(n684), .ZN(n1013) );
  AND3_X4 U856 ( .A1(n867), .A2(n875), .A3(lshift_quad1_state[3]), .ZN(n684)
         );
  NOR3_X2 U857 ( .A1(n729), .A2(state[0]), .A3(n1033), .ZN(n317) );
  NAND2_X2 U858 ( .A1(n685), .A2(n686), .ZN(n918) );
  NAND2_X2 U859 ( .A1(n613), .A2(n1032), .ZN(n685) );
  NAND2_X2 U860 ( .A1(n687), .A2(n688), .ZN(n925) );
  NAND2_X2 U861 ( .A1(n605), .A2(n1030), .ZN(n687) );
  NOR3_X2 U862 ( .A1(n83), .A2(\cycle_count[3] ), .A3(n637), .ZN(
        cycle_count_less_than_7_din) );
  AND3_X2 U863 ( .A1(n971), .A2(n35), .A3(rdmat_pick_vec[2]), .ZN(n689) );
  NOR2_X2 U864 ( .A1(n97), .A2(n283), .ZN(wbuf_set_rdmat_acked[3]) );
  NOR2_X2 U865 ( .A1(n96), .A2(n283), .ZN(wbuf_set_rdmat_acked[2]) );
  NOR2_X2 U866 ( .A1(n95), .A2(n283), .ZN(wbuf_set_rdmat_acked[1]) );
  NOR2_X2 U867 ( .A1(n94), .A2(n283), .ZN(wbuf_set_rdmat_acked[0]) );
  AND3_X2 U868 ( .A1(n59), .A2(n57), .A3(n55), .ZN(n770) );
  AND2_X2 U869 ( .A1(n447), .A2(n657), .ZN(n690) );
  NAND2_X2 U870 ( .A1(n802), .A2(n655), .ZN(n863) );
  AND2_X2 U871 ( .A1(l2t_mb2_addr_r3[1]), .A2(l2t_mb2_run_r1), .ZN(n691) );
  NOR3_X2 U872 ( .A1(n729), .A2(n387), .A3(n388), .ZN(\ff_wb_count/fdin [0])
         );
  NOR2_X2 U873 ( .A1(n385), .A2(n104), .ZN(n387) );
  NOR2_X2 U874 ( .A1(n369), .A2(n370), .ZN(n368) );
  NOR3_X2 U875 ( .A1(n105), .A2(n106), .A3(n371), .ZN(n369) );
  NAND3_X2 U876 ( .A1(n727), .A2(n346), .A3(wb_acked[0]), .ZN(n406) );
  NAND3_X2 U877 ( .A1(n727), .A2(n340), .A3(wb_acked[2]), .ZN(n402) );
  NAND3_X2 U878 ( .A1(n727), .A2(n334), .A3(wb_acked[4]), .ZN(n398) );
  NAND3_X2 U879 ( .A1(n727), .A2(n331), .A3(wb_acked[5]), .ZN(n396) );
  NAND3_X2 U880 ( .A1(n727), .A2(n328), .A3(wb_acked[6]), .ZN(n394) );
  NAND3_X2 U881 ( .A1(n727), .A2(n325), .A3(wb_acked[7]), .ZN(n391) );
  NAND3_X2 U882 ( .A1(n727), .A2(n337), .A3(wb_acked[3]), .ZN(n400) );
  NAND3_X2 U883 ( .A1(n727), .A2(n343), .A3(wb_acked[1]), .ZN(n404) );
  NAND3_X2 U884 ( .A1(n1012), .A2(n1011), .A3(n1010), .ZN(wbuf_wb_read_wl[4])
         );
  NOR2_X2 U885 ( .A1(n383), .A2(n81), .ZN(n382) );
  NOR2_X2 U886 ( .A1(wb_count[0]), .A2(n80), .ZN(n383) );
  NOR2_X2 U887 ( .A1(n646), .A2(n647), .ZN(n642) );
  INV_X4 U888 ( .A(n726), .ZN(n727) );
  NAND3_X2 U889 ( .A1(n773), .A2(n654), .A3(n774), .ZN(n790) );
  NAND2_X2 U890 ( .A1(n803), .A2(n101), .ZN(n1016) );
  NAND3_X2 U891 ( .A1(n727), .A2(n639), .A3(l2t_l2b_ev_dword_r0[2]), .ZN(n640)
         );
  AND2_X2 U892 ( .A1(n910), .A2(rdmat_pick_vec[3]), .ZN(n692) );
  NAND3_X2 U893 ( .A1(n822), .A2(n653), .A3(n821), .ZN(n852) );
  NAND3_X2 U894 ( .A1(n637), .A2(n93), .A3(\next_state[2] ), .ZN(n636) );
  NAND3_X2 U895 ( .A1(n727), .A2(n638), .A3(\cycle_count[3] ), .ZN(n635) );
  NOR2_X2 U896 ( .A1(n115), .A2(n84), .ZN(wbuf_wbufrpt_next_state_1) );
  NOR2_X2 U897 ( .A1(n725), .A2(state[1]), .ZN(n115) );
  NOR2_X2 U898 ( .A1(n321), .A2(n729), .ZN(\ff_wbuf_arb_full_px1/fdin[0] ) );
  NOR2_X2 U899 ( .A1(n322), .A2(wb_count[3]), .ZN(n321) );
  NOR2_X2 U900 ( .A1(n323), .A2(n106), .ZN(n322) );
  NOR2_X2 U901 ( .A1(n81), .A2(wb_count[1]), .ZN(n323) );
  NOR2_X2 U902 ( .A1(n372), .A2(n729), .ZN(\ff_wb_count/fdin [2]) );
  NOR3_X2 U903 ( .A1(n373), .A2(n370), .A3(n374), .ZN(n372) );
  NOR3_X2 U904 ( .A1(n105), .A2(wb_count[2]), .A3(n371), .ZN(n374) );
  NOR2_X2 U905 ( .A1(n375), .A2(n106), .ZN(n373) );
  NOR2_X2 U906 ( .A1(n356), .A2(n729), .ZN(\ff_wb_count/fdin [3]) );
  NOR2_X2 U907 ( .A1(n357), .A2(n358), .ZN(n356) );
  NOR2_X2 U908 ( .A1(n359), .A2(n107), .ZN(n358) );
  NOR2_X2 U909 ( .A1(wb_count[3]), .A2(n368), .ZN(n357) );
  NOR2_X2 U910 ( .A1(n379), .A2(n729), .ZN(\ff_wb_count/fdin [1]) );
  NOR2_X2 U911 ( .A1(n380), .A2(n381), .ZN(n379) );
  NOR2_X2 U912 ( .A1(wb_count[1]), .A2(n382), .ZN(n381) );
  NOR2_X2 U913 ( .A1(n384), .A2(n105), .ZN(n380) );
  AND3_X2 U914 ( .A1(n945), .A2(wb_valid[0]), .A3(n944), .ZN(n695) );
  NAND2_X2 U915 ( .A1(n1004), .A2(n35), .ZN(n1005) );
  AND2_X2 U916 ( .A1(n436), .A2(n712), .ZN(n698) );
  AND2_X2 U917 ( .A1(n725), .A2(n35), .ZN(n700) );
  NOR4_X2 U918 ( .A1(wb_cam_match_c2_d1[3]), .A2(wb_cam_match_c2_d1[2]), .A3(
        wb_cam_match_c2_d1[1]), .A4(wb_cam_match_c2_d1[0]), .ZN(n258) );
  NOR4_X2 U919 ( .A1(wb_cam_match_c2_d1[7]), .A2(wb_cam_match_c2_d1[6]), .A3(
        wb_cam_match_c2_d1[5]), .A4(wb_cam_match_c2_d1[4]), .ZN(n259) );
  NOR2_X2 U920 ( .A1(n41), .A2(n111), .ZN(n335) );
  AND2_X2 U921 ( .A1(n703), .A2(n45), .ZN(n831) );
  NAND2_X2 U922 ( .A1(wbtag_write_wl_c5[0]), .A2(wbtag_write_we_c5), .ZN(n703)
         );
  AND3_X2 U923 ( .A1(wb_cam_match_c2[7]), .A2(n69), .A3(n671), .ZN(n704) );
  NOR2_X2 U924 ( .A1(n38), .A2(n111), .ZN(n344) );
  NOR2_X2 U925 ( .A1(n42), .A2(n111), .ZN(n332) );
  NOR2_X2 U926 ( .A1(n43), .A2(n111), .ZN(n329) );
  NOR2_X2 U927 ( .A1(n39), .A2(n111), .ZN(n341) );
  NOR2_X2 U928 ( .A1(n44), .A2(n111), .ZN(n326) );
  NOR2_X2 U929 ( .A1(n40), .A2(n111), .ZN(n338) );
  INV_X4 U930 ( .A(n781), .ZN(n883) );
  AND2_X2 U931 ( .A1(n914), .A2(rdmat_pick_vec[1]), .ZN(n706) );
  AND2_X2 U932 ( .A1(n746), .A2(n745), .ZN(n707) );
  NOR2_X2 U933 ( .A1(n35), .A2(l2t_mb2_addr_r3[1]), .ZN(n708) );
  AND2_X2 U934 ( .A1(n743), .A2(n742), .ZN(n709) );
  NOR2_X2 U935 ( .A1(n711), .A2(n723), .ZN(n710) );
  INV_X4 U936 ( .A(n710), .ZN(n878) );
  INV_X1 U937 ( .A(n876), .ZN(n867) );
  NAND3_X1 U938 ( .A1(n1017), .A2(n1018), .A3(n1016), .ZN(n987) );
  INV_X4 U939 ( .A(n989), .ZN(wbuf_wbufrpt_leave_state0) );
  INV_X4 U940 ( .A(n53), .ZN(n714) );
  INV_X1 U941 ( .A(wbuf_wb_read_en), .ZN(n715) );
  INV_X4 U942 ( .A(n715), .ZN(n716) );
  INV_X1 U943 ( .A(n677), .ZN(n717) );
  NOR2_X2 U944 ( .A1(n365), .A2(n364), .ZN(n385) );
  AND2_X4 U945 ( .A1(wbuf_wbtag_write_en_c4), .A2(n389), .ZN(n365) );
  NAND3_X1 U946 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n985), .A3(n936), .ZN(
        n937) );
  NAND2_X2 U947 ( .A1(or_wb_mbid_vld), .A2(n825), .ZN(n875) );
  OR2_X1 U948 ( .A1(n719), .A2(n723), .ZN(n820) );
  NAND3_X1 U949 ( .A1(n35), .A2(wb_valid[0]), .A3(n47), .ZN(n959) );
  OR2_X1 U950 ( .A1(n720), .A2(n723), .ZN(n842) );
  NAND2_X1 U951 ( .A1(n689), .A2(n974), .ZN(n972) );
  NAND2_X1 U952 ( .A1(n973), .A2(n972), .ZN(wbuf_rdmat_read_wl[2]) );
  NAND2_X2 U953 ( .A1(wb_valid[3]), .A2(n724), .ZN(n841) );
  NOR2_X1 U954 ( .A1(n338), .A2(wb_valid[3]), .ZN(n862) );
  NAND2_X1 U955 ( .A1(n842), .A2(n841), .ZN(n857) );
  NAND3_X1 U956 ( .A1(lshift_quad0_state[0]), .A2(n841), .A3(n842), .ZN(n832)
         );
  NAND3_X1 U957 ( .A1(n842), .A2(n841), .A3(lshift_quad0_state[0]), .ZN(n843)
         );
  NOR2_X1 U958 ( .A1(wb_valid[4]), .A2(wb_valid[3]), .ZN(n768) );
  NAND3_X2 U959 ( .A1(n692), .A2(n35), .A3(n974), .ZN(n975) );
  NAND4_X1 U960 ( .A1(n987), .A2(n986), .A3(n985), .A4(n984), .ZN(n988) );
  INV_X4 U961 ( .A(n113), .ZN(n722) );
  NAND4_X1 U962 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n992), .A3(n993), .A4(
        n1000), .ZN(n829) );
  NAND3_X1 U963 ( .A1(n1001), .A2(n1000), .A3(n999), .ZN(n979) );
  NAND4_X1 U964 ( .A1(n35), .A2(n1001), .A3(n1000), .A4(n999), .ZN(n1002) );
  NAND2_X1 U965 ( .A1(wb_mbid_vld[0]), .A2(n670), .ZN(n818) );
  NAND2_X1 U966 ( .A1(wb_mbid_vld[4]), .A2(n670), .ZN(n775) );
  NAND2_X1 U967 ( .A1(n670), .A2(wb_mbid_vld[7]), .ZN(n798) );
  NAND2_X1 U968 ( .A1(n933), .A2(n986), .ZN(n934) );
  NAND2_X1 U969 ( .A1(n965), .A2(n907), .ZN(\ff_latched_rdma_read_en/fdin[0] )
         );
  NOR2_X1 U970 ( .A1(n344), .A2(wb_valid[1]), .ZN(n840) );
  NAND3_X2 U971 ( .A1(n770), .A2(n769), .A3(n768), .ZN(n772) );
  NAND2_X4 U972 ( .A1(n887), .A2(n672), .ZN(n962) );
  NAND3_X1 U973 ( .A1(l2t_l2b_ev_dword_r0[0]), .A2(n91), .A3(\next_state[2] ), 
        .ZN(n644) );
  NAND3_X1 U974 ( .A1(l2t_l2b_ev_dword_r0[0]), .A2(n727), .A3(n646), .ZN(n649)
         );
  NAND4_X1 U975 ( .A1(\next_state[2] ), .A2(l2t_l2b_ev_dword_r0[1]), .A3(
        l2t_l2b_ev_dword_r0[0]), .A4(n92), .ZN(n641) );
  NOR2_X1 U976 ( .A1(n365), .A2(n105), .ZN(n378) );
  NOR2_X1 U977 ( .A1(n365), .A2(n106), .ZN(n361) );
  NAND2_X1 U978 ( .A1(n365), .A2(n104), .ZN(n367) );
  NAND2_X1 U979 ( .A1(n158), .A2(n91), .ZN(n643) );
  NOR2_X1 U980 ( .A1(n97), .A2(n158), .ZN(wbuf_reset_rdmat_vld[3]) );
  NOR2_X1 U981 ( .A1(n96), .A2(n158), .ZN(wbuf_reset_rdmat_vld[2]) );
  NOR2_X1 U982 ( .A1(n95), .A2(n158), .ZN(wbuf_reset_rdmat_vld[1]) );
  NOR2_X1 U983 ( .A1(n94), .A2(n158), .ZN(wbuf_reset_rdmat_vld[0]) );
  NOR2_X1 U984 ( .A1(n1033), .A2(l2t_l2b_ev_dword_r0[0]), .ZN(n647) );
  NOR3_X2 U985 ( .A1(n158), .A2(wbuf_wbtag_write_en_c4), .A3(n114), .ZN(n364)
         );
  NOR2_X2 U986 ( .A1(l2t_l2b_ev_dword_r0[1]), .A2(l2t_l2b_ev_dword_r0[0]), 
        .ZN(n650) );
  NAND4_X1 U987 ( .A1(n1009), .A2(n1017), .A3(n35), .A4(n1008), .ZN(n1012) );
  NAND4_X4 U988 ( .A1(state[2]), .A2(l2t_l2b_ev_dword_r0[2]), .A3(n650), .A4(
        \cycle_count[3] ), .ZN(n158) );
  AND2_X2 U989 ( .A1(\clkgen/c_0/l1en ), .A2(l2clk), .ZN(n731) );
  INV_X4 U990 ( .A(rdmat_pick_vec[3]), .ZN(n1029) );
  NAND3_X2 U991 ( .A1(n292), .A2(n727), .A3(n291), .ZN(quad2_state_in[0]) );
  INV_X4 U992 ( .A(rdmat_pick_vec[0]), .ZN(n1032) );
  INV_X4 U993 ( .A(rdmat_pick_vec[1]), .ZN(n1031) );
  INV_X4 U994 ( .A(rdmat_pick_vec[2]), .ZN(n1030) );
  NAND2_X2 U995 ( .A1(n85), .A2(lshift_quad_state[0]), .ZN(n733) );
  NAND2_X2 U996 ( .A1(n283), .A2(lshift_quad_state[1]), .ZN(n732) );
  NAND3_X2 U997 ( .A1(n727), .A2(n733), .A3(n732), .ZN(quad_state_in[0]) );
  NAND2_X2 U998 ( .A1(n278), .A2(lshift_quad_state[1]), .ZN(n735) );
  NAND2_X2 U999 ( .A1(n116), .A2(lshift_quad_state[2]), .ZN(n734) );
  NAND2_X2 U1000 ( .A1(n735), .A2(n734), .ZN(quad_state_in[1]) );
  NAND2_X2 U1001 ( .A1(n278), .A2(lshift_quad_state[2]), .ZN(n737) );
  NAND2_X2 U1002 ( .A1(n116), .A2(lshift_quad_state[0]), .ZN(n736) );
  NAND2_X2 U1003 ( .A1(n737), .A2(n736), .ZN(quad_state_in[2]) );
  INV_X4 U1004 ( .A(n501), .ZN(n11) );
  INV_X4 U1005 ( .A(n490), .ZN(n12) );
  INV_X4 U1006 ( .A(n479), .ZN(n13) );
  INV_X4 U1007 ( .A(n458), .ZN(n15) );
  INV_X4 U1008 ( .A(n447), .ZN(n16) );
  INV_X4 U1009 ( .A(n436), .ZN(n17) );
  NAND2_X2 U1010 ( .A1(n479), .A2(n719), .ZN(n748) );
  INV_X4 U1011 ( .A(n748), .ZN(n738) );
  NAND2_X2 U1012 ( .A1(wb_acked[2]), .A2(wb_mbid_vld[2]), .ZN(n893) );
  INV_X4 U1013 ( .A(n893), .ZN(n899) );
  NOR3_X2 U1014 ( .A1(n738), .A2(n729), .A3(n899), .ZN(
        \ff_wb_mbid_vld/fdin [2]) );
  NAND2_X2 U1015 ( .A1(n458), .A2(n713), .ZN(n747) );
  INV_X4 U1016 ( .A(n747), .ZN(n739) );
  NAND2_X2 U1017 ( .A1(wb_acked[4]), .A2(wb_mbid_vld[4]), .ZN(n896) );
  INV_X4 U1018 ( .A(n896), .ZN(n904) );
  NOR3_X2 U1019 ( .A1(n739), .A2(n726), .A3(n904), .ZN(
        \ff_wb_mbid_vld/fdin [4]) );
  NAND2_X2 U1020 ( .A1(n490), .A2(n711), .ZN(n750) );
  INV_X4 U1021 ( .A(n750), .ZN(n740) );
  NAND2_X2 U1022 ( .A1(wb_acked[1]), .A2(wb_mbid_vld[1]), .ZN(n894) );
  INV_X4 U1023 ( .A(n894), .ZN(n900) );
  NOR3_X2 U1024 ( .A1(n740), .A2(n726), .A3(n900), .ZN(
        \ff_wb_mbid_vld/fdin [1]) );
  NAND2_X2 U1025 ( .A1(n501), .A2(n669), .ZN(n749) );
  INV_X4 U1026 ( .A(n749), .ZN(n741) );
  NAND2_X2 U1027 ( .A1(wb_acked[0]), .A2(wb_mbid_vld[0]), .ZN(n892) );
  INV_X4 U1028 ( .A(n892), .ZN(n901) );
  NOR3_X2 U1029 ( .A1(n741), .A2(n729), .A3(n901), .ZN(
        \ff_wb_mbid_vld/fdin [0]) );
  NAND2_X2 U1030 ( .A1(wb_acked[6]), .A2(wb_mbid_vld[6]), .ZN(n897) );
  INV_X4 U1031 ( .A(n897), .ZN(n902) );
  NOR3_X2 U1032 ( .A1(n698), .A2(n726), .A3(n902), .ZN(
        \ff_wb_mbid_vld/fdin [6]) );
  NAND2_X2 U1033 ( .A1(wb_acked[5]), .A2(wb_mbid_vld[5]), .ZN(n898) );
  INV_X4 U1034 ( .A(n898), .ZN(n903) );
  NOR3_X2 U1035 ( .A1(n690), .A2(n726), .A3(n903), .ZN(
        \ff_wb_mbid_vld/fdin [5]) );
  NAND2_X2 U1036 ( .A1(n69), .A2(wb_mbid_vld[7]), .ZN(n743) );
  NAND2_X2 U1037 ( .A1(n718), .A2(n18), .ZN(n742) );
  NOR2_X2 U1038 ( .A1(n729), .A2(n709), .ZN(\ff_wb_mbid_vld/fdin [7]) );
  NAND2_X2 U1039 ( .A1(n65), .A2(wb_mbid_vld[3]), .ZN(n746) );
  INV_X4 U1040 ( .A(n414), .ZN(n744) );
  NOR2_X2 U1041 ( .A1(n729), .A2(n707), .ZN(\ff_wb_mbid_vld/fdin [3]) );
  NAND2_X2 U1042 ( .A1(n747), .A2(n896), .ZN(n754) );
  NAND2_X2 U1043 ( .A1(n748), .A2(n893), .ZN(n753) );
  NAND2_X2 U1044 ( .A1(n749), .A2(n892), .ZN(n752) );
  NAND2_X2 U1045 ( .A1(n750), .A2(n894), .ZN(n751) );
  NAND4_X2 U1046 ( .A1(n754), .A2(n753), .A3(n752), .A4(n751), .ZN(n758) );
  NAND2_X2 U1047 ( .A1(n709), .A2(n707), .ZN(n757) );
  NOR2_X2 U1048 ( .A1(n902), .A2(n698), .ZN(n756) );
  NOR2_X2 U1049 ( .A1(n903), .A2(n690), .ZN(n755) );
  NOR4_X2 U1050 ( .A1(n758), .A2(n757), .A3(n756), .A4(n755), .ZN(n759) );
  NOR2_X2 U1051 ( .A1(n726), .A2(n759), .ZN(\ff_or_wb_mbid_vld/fdin[0] ) );
  NAND2_X2 U1052 ( .A1(n75), .A2(lshift_quad1_state[3]), .ZN(n761) );
  NAND2_X2 U1053 ( .A1(n297), .A2(lshift_quad1_state[0]), .ZN(n760) );
  NAND2_X2 U1054 ( .A1(n761), .A2(n760), .ZN(quad1_state_in[3]) );
  NAND2_X2 U1055 ( .A1(n75), .A2(lshift_quad1_state[0]), .ZN(n763) );
  NAND2_X2 U1056 ( .A1(n304), .A2(lshift_quad1_state[1]), .ZN(n762) );
  NAND3_X2 U1057 ( .A1(n727), .A2(n763), .A3(n762), .ZN(quad1_state_in[0]) );
  NAND2_X2 U1058 ( .A1(n75), .A2(lshift_quad1_state[1]), .ZN(n765) );
  NAND2_X2 U1059 ( .A1(n297), .A2(lshift_quad1_state[2]), .ZN(n764) );
  NAND2_X2 U1060 ( .A1(n765), .A2(n764), .ZN(quad1_state_in[1]) );
  NAND2_X2 U1061 ( .A1(n75), .A2(lshift_quad1_state[2]), .ZN(n767) );
  NAND2_X2 U1062 ( .A1(n297), .A2(lshift_quad1_state[3]), .ZN(n766) );
  NAND2_X2 U1063 ( .A1(n767), .A2(n766), .ZN(quad1_state_in[2]) );
  NAND3_X4 U1064 ( .A1(n772), .A2(state[0]), .A3(n771), .ZN(n989) );
  NAND2_X2 U1065 ( .A1(wb_mbid_vld[6]), .A2(or_wb_mbid_vld), .ZN(n776) );
  NAND2_X2 U1066 ( .A1(n681), .A2(lshift_quad1_state[3]), .ZN(n774) );
  NAND3_X2 U1067 ( .A1(lshift_quad1_state[2]), .A2(n681), .A3(n678), .ZN(n773)
         );
  NAND2_X2 U1068 ( .A1(wb_valid[4]), .A2(n724), .ZN(n866) );
  NAND2_X2 U1069 ( .A1(n775), .A2(n866), .ZN(n1008) );
  INV_X4 U1070 ( .A(n1008), .ZN(n801) );
  NAND4_X2 U1071 ( .A1(lshift_quad1_state[1]), .A2(n801), .A3(n681), .A4(n675), 
        .ZN(n777) );
  NAND2_X2 U1072 ( .A1(n864), .A2(n798), .ZN(n789) );
  NAND2_X2 U1073 ( .A1(n827), .A2(n88), .ZN(n782) );
  NAND2_X2 U1074 ( .A1(wb_valid[2]), .A2(n724), .ZN(n819) );
  NAND3_X2 U1075 ( .A1(n669), .A2(n720), .A3(n719), .ZN(n779) );
  NAND3_X2 U1076 ( .A1(n677), .A2(n782), .A3(n883), .ZN(n783) );
  NAND3_X2 U1077 ( .A1(n784), .A2(n789), .A3(n1017), .ZN(n986) );
  INV_X4 U1078 ( .A(n986), .ZN(n1021) );
  NAND2_X2 U1079 ( .A1(latched_wb_read_wl[7]), .A2(n989), .ZN(n785) );
  NAND2_X2 U1080 ( .A1(n786), .A2(n785), .ZN(\ff_latched_wb_read_wl/fdin [7])
         );
  NAND2_X2 U1081 ( .A1(latched_wb_read_wl[7]), .A2(n1033), .ZN(n325) );
  NOR2_X2 U1082 ( .A1(n326), .A2(n671), .ZN(n788) );
  INV_X4 U1083 ( .A(n325), .ZN(n787) );
  NOR3_X2 U1084 ( .A1(n788), .A2(n726), .A3(n787), .ZN(\ff_wb_valid/fdin [7])
         );
  INV_X4 U1085 ( .A(n789), .ZN(n791) );
  NAND2_X2 U1086 ( .A1(n791), .A2(n790), .ZN(n1007) );
  NAND2_X2 U1087 ( .A1(n100), .A2(n1007), .ZN(n792) );
  NAND2_X2 U1088 ( .A1(n680), .A2(n792), .ZN(n984) );
  INV_X4 U1089 ( .A(n984), .ZN(n793) );
  NAND2_X2 U1090 ( .A1(n793), .A2(wbuf_wbufrpt_leave_state0), .ZN(n795) );
  NAND2_X2 U1091 ( .A1(latched_wb_read_wl[4]), .A2(n989), .ZN(n794) );
  NAND2_X2 U1092 ( .A1(n795), .A2(n794), .ZN(\ff_latched_wb_read_wl/fdin [4])
         );
  NAND2_X2 U1093 ( .A1(latched_wb_read_wl[4]), .A2(n1033), .ZN(n334) );
  NOR2_X2 U1094 ( .A1(n335), .A2(n714), .ZN(n797) );
  INV_X4 U1095 ( .A(n334), .ZN(n796) );
  NOR3_X2 U1096 ( .A1(n797), .A2(n726), .A3(n796), .ZN(\ff_wb_valid/fdin [4])
         );
  NAND3_X2 U1097 ( .A1(lshift_quad1_state[0]), .A2(n864), .A3(n798), .ZN(n799)
         );
  NAND2_X2 U1098 ( .A1(n100), .A2(n799), .ZN(n800) );
  NAND2_X2 U1099 ( .A1(n801), .A2(n800), .ZN(n802) );
  INV_X4 U1100 ( .A(n987), .ZN(n804) );
  NAND2_X2 U1101 ( .A1(n804), .A2(wbuf_wbufrpt_leave_state0), .ZN(n806) );
  NAND2_X2 U1102 ( .A1(latched_wb_read_wl[6]), .A2(n989), .ZN(n805) );
  NAND2_X2 U1103 ( .A1(n806), .A2(n805), .ZN(\ff_latched_wb_read_wl/fdin [6])
         );
  NAND2_X2 U1104 ( .A1(latched_wb_read_wl[6]), .A2(n1033), .ZN(n328) );
  NOR2_X2 U1105 ( .A1(n329), .A2(wb_valid[6]), .ZN(n808) );
  INV_X4 U1106 ( .A(n328), .ZN(n807) );
  NOR3_X2 U1107 ( .A1(n808), .A2(n726), .A3(n807), .ZN(\ff_wb_valid/fdin [6])
         );
  NAND2_X2 U1108 ( .A1(n70), .A2(lshift_quad0_state[2]), .ZN(n810) );
  NAND2_X2 U1109 ( .A1(n308), .A2(lshift_quad0_state[3]), .ZN(n809) );
  NAND2_X2 U1110 ( .A1(n810), .A2(n809), .ZN(quad0_state_in[2]) );
  NAND2_X2 U1111 ( .A1(n70), .A2(lshift_quad0_state[3]), .ZN(n812) );
  NAND2_X2 U1112 ( .A1(n308), .A2(lshift_quad0_state[0]), .ZN(n811) );
  NAND2_X2 U1113 ( .A1(n812), .A2(n811), .ZN(quad0_state_in[3]) );
  NAND2_X2 U1114 ( .A1(n70), .A2(lshift_quad0_state[0]), .ZN(n814) );
  NAND2_X2 U1115 ( .A1(n315), .A2(lshift_quad0_state[1]), .ZN(n813) );
  NAND3_X2 U1116 ( .A1(n727), .A2(n814), .A3(n813), .ZN(quad0_state_in[0]) );
  NAND2_X2 U1117 ( .A1(n70), .A2(lshift_quad0_state[1]), .ZN(n816) );
  NAND2_X2 U1118 ( .A1(n308), .A2(lshift_quad0_state[2]), .ZN(n815) );
  NAND2_X2 U1119 ( .A1(n816), .A2(n815), .ZN(quad0_state_in[1]) );
  NAND2_X2 U1120 ( .A1(n818), .A2(n817), .ZN(n992) );
  INV_X4 U1121 ( .A(n857), .ZN(n823) );
  NAND2_X2 U1122 ( .A1(n820), .A2(n819), .ZN(n1001) );
  NAND2_X2 U1123 ( .A1(n853), .A2(lshift_quad0_state[3]), .ZN(n822) );
  NAND3_X2 U1124 ( .A1(lshift_quad0_state[2]), .A2(n853), .A3(n677), .ZN(n821)
         );
  NAND2_X2 U1125 ( .A1(n823), .A2(n852), .ZN(n824) );
  NAND2_X2 U1126 ( .A1(n98), .A2(n824), .ZN(n993) );
  NAND3_X2 U1127 ( .A1(n712), .A2(n718), .A3(n713), .ZN(n825) );
  NAND4_X2 U1128 ( .A1(n705), .A2(n678), .A3(n656), .A4(n875), .ZN(n826) );
  NAND2_X2 U1129 ( .A1(latched_wb_read_wl[0]), .A2(n989), .ZN(n828) );
  NAND2_X2 U1130 ( .A1(n829), .A2(n828), .ZN(\ff_latched_wb_read_wl/fdin [0])
         );
  NAND2_X2 U1131 ( .A1(latched_wb_read_wl[0]), .A2(n1033), .ZN(n346) );
  INV_X4 U1132 ( .A(n346), .ZN(n830) );
  NOR3_X2 U1133 ( .A1(n831), .A2(n729), .A3(n830), .ZN(\ff_wb_valid/fdin [0])
         );
  INV_X4 U1134 ( .A(n992), .ZN(n854) );
  NAND2_X2 U1135 ( .A1(n98), .A2(n832), .ZN(n833) );
  NAND2_X2 U1136 ( .A1(n854), .A2(n833), .ZN(n834) );
  NAND3_X2 U1137 ( .A1(n835), .A2(n652), .A3(n834), .ZN(n836) );
  NAND3_X2 U1138 ( .A1(n836), .A2(n717), .A3(n1000), .ZN(n936) );
  INV_X4 U1139 ( .A(n936), .ZN(n996) );
  NAND2_X2 U1140 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n996), .ZN(n838) );
  NAND2_X2 U1141 ( .A1(latched_wb_read_wl[1]), .A2(n989), .ZN(n837) );
  NAND2_X2 U1142 ( .A1(n838), .A2(n837), .ZN(\ff_latched_wb_read_wl/fdin [1])
         );
  NAND2_X2 U1143 ( .A1(latched_wb_read_wl[1]), .A2(n1033), .ZN(n343) );
  INV_X4 U1144 ( .A(n343), .ZN(n839) );
  NOR3_X2 U1145 ( .A1(n840), .A2(n729), .A3(n839), .ZN(\ff_wb_valid/fdin [1])
         );
  NAND2_X2 U1146 ( .A1(n992), .A2(n652), .ZN(n845) );
  NAND3_X2 U1147 ( .A1(n98), .A2(n652), .A3(n843), .ZN(n844) );
  NAND3_X2 U1148 ( .A1(n677), .A2(n845), .A3(n844), .ZN(n846) );
  NAND2_X2 U1149 ( .A1(n99), .A2(n846), .ZN(n999) );
  INV_X4 U1150 ( .A(n979), .ZN(n847) );
  NAND2_X2 U1151 ( .A1(n847), .A2(wbuf_wbufrpt_leave_state0), .ZN(n849) );
  NAND2_X2 U1152 ( .A1(latched_wb_read_wl[2]), .A2(n989), .ZN(n848) );
  NAND2_X2 U1153 ( .A1(n849), .A2(n848), .ZN(\ff_latched_wb_read_wl/fdin [2])
         );
  NAND2_X2 U1154 ( .A1(latched_wb_read_wl[2]), .A2(n1033), .ZN(n340) );
  NOR2_X2 U1155 ( .A1(n341), .A2(n668), .ZN(n851) );
  INV_X4 U1156 ( .A(n340), .ZN(n850) );
  NOR3_X2 U1157 ( .A1(n851), .A2(n729), .A3(n850), .ZN(\ff_wb_valid/fdin [2])
         );
  INV_X4 U1158 ( .A(n852), .ZN(n856) );
  NAND4_X2 U1159 ( .A1(lshift_quad0_state[1]), .A2(n677), .A3(n854), .A4(n853), 
        .ZN(n855) );
  NAND2_X2 U1160 ( .A1(n856), .A2(n855), .ZN(n858) );
  NAND3_X2 U1161 ( .A1(n858), .A2(n857), .A3(n1000), .ZN(n933) );
  NAND2_X2 U1162 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n1004), .ZN(n860) );
  NAND2_X2 U1163 ( .A1(latched_wb_read_wl[3]), .A2(n989), .ZN(n859) );
  NAND2_X2 U1164 ( .A1(n860), .A2(n859), .ZN(\ff_latched_wb_read_wl/fdin [3])
         );
  NAND2_X2 U1165 ( .A1(latched_wb_read_wl[3]), .A2(n1033), .ZN(n337) );
  INV_X4 U1166 ( .A(n337), .ZN(n861) );
  NOR3_X2 U1167 ( .A1(n862), .A2(n729), .A3(n861), .ZN(\ff_wb_valid/fdin [3])
         );
  NAND3_X2 U1168 ( .A1(n866), .A2(n865), .A3(n864), .ZN(n876) );
  NAND2_X2 U1169 ( .A1(n679), .A2(n1013), .ZN(n985) );
  INV_X4 U1170 ( .A(n985), .ZN(n868) );
  NAND2_X2 U1171 ( .A1(n868), .A2(wbuf_wbufrpt_leave_state0), .ZN(n870) );
  NAND2_X2 U1172 ( .A1(latched_wb_read_wl[5]), .A2(n989), .ZN(n869) );
  NAND2_X2 U1173 ( .A1(n870), .A2(n869), .ZN(\ff_latched_wb_read_wl/fdin [5])
         );
  NAND2_X2 U1174 ( .A1(latched_wb_read_wl[5]), .A2(n1033), .ZN(n331) );
  INV_X4 U1175 ( .A(n331), .ZN(n871) );
  NOR3_X2 U1176 ( .A1(n872), .A2(n729), .A3(n871), .ZN(\ff_wb_valid/fdin [5])
         );
  NOR2_X2 U1177 ( .A1(n317), .A2(wbuf_wbufrpt_leave_state0), .ZN(next_state_0)
         );
  NOR3_X2 U1178 ( .A1(n729), .A2(n422), .A3(n1033), .ZN(
        \ff_mcu_req_pending/fdin[0] ) );
  INV_X4 U1179 ( .A(n875), .ZN(n877) );
  INV_X4 U1180 ( .A(n879), .ZN(n880) );
  NOR3_X2 U1181 ( .A1(n710), .A2(n880), .A3(n88), .ZN(n881) );
  NAND2_X2 U1182 ( .A1(n989), .A2(n114), .ZN(n889) );
  NAND2_X2 U1183 ( .A1(l2t_mb2_wbtag_rd_en_r3), .A2(l2t_mb2_run_r1), .ZN(n963)
         );
  NAND2_X2 U1184 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n963), .ZN(n888) );
  NAND2_X2 U1185 ( .A1(n889), .A2(n888), .ZN(n890) );
  NAND2_X2 U1186 ( .A1(n891), .A2(n890), .ZN(\ff_latched_wb_read_en/fdin[0] )
         );
  NOR4_X2 U1187 ( .A1(n683), .A2(n704), .A3(n682), .A4(n701), .ZN(n245) );
  NOR4_X2 U1188 ( .A1(n694), .A2(n696), .A3(n699), .A4(n697), .ZN(n246) );
  NAND2_X2 U1189 ( .A1(mbid2[0]), .A2(n899), .ZN(n238) );
  NAND2_X2 U1190 ( .A1(mbid1[0]), .A2(n900), .ZN(n239) );
  NAND2_X2 U1191 ( .A1(mbid0[0]), .A2(n901), .ZN(n240) );
  NAND2_X2 U1192 ( .A1(wb_acked[3]), .A2(wb_mbid_vld[3]), .ZN(n895) );
  NAND2_X2 U1193 ( .A1(n674), .A2(n895), .ZN(n184) );
  NAND2_X2 U1194 ( .A1(mbid6[0]), .A2(n902), .ZN(n232) );
  NAND2_X2 U1195 ( .A1(mbid5[0]), .A2(n903), .ZN(n233) );
  NAND2_X2 U1196 ( .A1(mbid4[0]), .A2(n904), .ZN(n234) );
  INV_X4 U1197 ( .A(n184), .ZN(n1028) );
  NAND2_X2 U1198 ( .A1(mbid2[1]), .A2(n899), .ZN(n225) );
  NAND2_X2 U1199 ( .A1(mbid1[1]), .A2(n900), .ZN(n226) );
  NAND2_X2 U1200 ( .A1(mbid0[1]), .A2(n901), .ZN(n227) );
  NAND2_X2 U1201 ( .A1(mbid6[1]), .A2(n902), .ZN(n220) );
  NAND2_X2 U1202 ( .A1(mbid5[1]), .A2(n903), .ZN(n221) );
  NAND2_X2 U1203 ( .A1(mbid4[1]), .A2(n904), .ZN(n222) );
  NAND2_X2 U1204 ( .A1(mbid2[2]), .A2(n899), .ZN(n213) );
  NAND2_X2 U1205 ( .A1(mbid1[2]), .A2(n900), .ZN(n214) );
  NAND2_X2 U1206 ( .A1(mbid0[2]), .A2(n901), .ZN(n215) );
  NAND2_X2 U1207 ( .A1(mbid6[2]), .A2(n902), .ZN(n208) );
  NAND2_X2 U1208 ( .A1(mbid5[2]), .A2(n903), .ZN(n209) );
  NAND2_X2 U1209 ( .A1(mbid4[2]), .A2(n904), .ZN(n210) );
  NAND2_X2 U1210 ( .A1(mbid2[3]), .A2(n899), .ZN(n201) );
  NAND2_X2 U1211 ( .A1(mbid1[3]), .A2(n900), .ZN(n202) );
  NAND2_X2 U1212 ( .A1(mbid0[3]), .A2(n901), .ZN(n203) );
  NAND2_X2 U1213 ( .A1(mbid6[3]), .A2(n902), .ZN(n196) );
  NAND2_X2 U1214 ( .A1(mbid5[3]), .A2(n903), .ZN(n197) );
  NAND2_X2 U1215 ( .A1(mbid4[3]), .A2(n904), .ZN(n198) );
  NAND2_X2 U1216 ( .A1(mbid2[4]), .A2(n899), .ZN(n185) );
  NAND2_X2 U1217 ( .A1(mbid1[4]), .A2(n900), .ZN(n186) );
  NAND2_X2 U1218 ( .A1(mbid0[4]), .A2(n901), .ZN(n187) );
  NAND2_X2 U1219 ( .A1(mbid6[4]), .A2(n902), .ZN(n176) );
  NAND2_X2 U1220 ( .A1(mbid5[4]), .A2(n903), .ZN(n177) );
  NAND2_X2 U1221 ( .A1(mbid4[4]), .A2(n904), .ZN(n178) );
  NAND2_X2 U1222 ( .A1(n700), .A2(n974), .ZN(n965) );
  NAND2_X2 U1223 ( .A1(n989), .A2(n662), .ZN(n906) );
  NAND2_X2 U1224 ( .A1(l2t_mb2_rdmatag_rd_en_r3), .A2(l2t_mb2_run_r1), .ZN(
        n964) );
  NAND2_X2 U1225 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n964), .ZN(n905) );
  NAND2_X2 U1226 ( .A1(n906), .A2(n905), .ZN(n907) );
  INV_X4 U1227 ( .A(n925), .ZN(n909) );
  NAND4_X2 U1228 ( .A1(lshift_quad2_state[1]), .A2(n1030), .A3(n1032), .A4(
        n1031), .ZN(n908) );
  NAND2_X2 U1229 ( .A1(n909), .A2(n908), .ZN(n910) );
  NAND3_X2 U1230 ( .A1(n692), .A2(wbuf_wbufrpt_leave_state0), .A3(n974), .ZN(
        n922) );
  NAND2_X2 U1231 ( .A1(n989), .A2(n911), .ZN(n916) );
  INV_X4 U1232 ( .A(n918), .ZN(n913) );
  NAND2_X2 U1233 ( .A1(n913), .A2(n912), .ZN(n914) );
  NAND2_X2 U1234 ( .A1(n706), .A2(n974), .ZN(n929) );
  NAND2_X2 U1235 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n929), .ZN(n915) );
  NAND2_X2 U1236 ( .A1(n916), .A2(n915), .ZN(n917) );
  NAND2_X2 U1237 ( .A1(n922), .A2(n917), .ZN(\ff_l2t_l2b_rdma_rdwl_r0/fdin [0]) );
  NAND2_X2 U1238 ( .A1(n918), .A2(n1031), .ZN(n919) );
  NAND2_X2 U1239 ( .A1(n103), .A2(n919), .ZN(n971) );
  NAND4_X2 U1240 ( .A1(wbuf_wbufrpt_leave_state0), .A2(rdmat_pick_vec[2]), 
        .A3(n974), .A4(n971), .ZN(n924) );
  NAND2_X2 U1241 ( .A1(l2t_l2b_rdma_rdwl_r0[1]), .A2(n989), .ZN(n920) );
  NAND3_X2 U1242 ( .A1(n922), .A2(n924), .A3(n920), .ZN(
        \ff_l2t_l2b_rdma_rdwl_r0/fdin [1]) );
  NAND2_X2 U1243 ( .A1(latched_rdmad_read_wl[3]), .A2(n989), .ZN(n921) );
  NAND2_X2 U1244 ( .A1(n922), .A2(n921), .ZN(
        \ff_latched_rdmad_read_wl/fdin [3]) );
  NAND2_X2 U1245 ( .A1(latched_rdmad_read_wl[2]), .A2(n989), .ZN(n923) );
  NAND2_X2 U1246 ( .A1(n924), .A2(n923), .ZN(
        \ff_latched_rdmad_read_wl/fdin [2]) );
  NAND2_X2 U1247 ( .A1(n925), .A2(n1029), .ZN(n926) );
  NAND2_X2 U1248 ( .A1(n102), .A2(n926), .ZN(n966) );
  NAND4_X2 U1249 ( .A1(wbuf_wbufrpt_leave_state0), .A2(rdmat_pick_vec[0]), 
        .A3(n974), .A4(n966), .ZN(n928) );
  NAND2_X2 U1250 ( .A1(latched_rdmad_read_wl[0]), .A2(n989), .ZN(n927) );
  NAND2_X2 U1251 ( .A1(n928), .A2(n927), .ZN(
        \ff_latched_rdmad_read_wl/fdin [0]) );
  INV_X4 U1252 ( .A(n929), .ZN(n930) );
  NAND2_X2 U1253 ( .A1(n930), .A2(wbuf_wbufrpt_leave_state0), .ZN(n932) );
  NAND2_X2 U1254 ( .A1(latched_rdmad_read_wl[1]), .A2(n989), .ZN(n931) );
  NAND2_X2 U1255 ( .A1(n932), .A2(n931), .ZN(
        \ff_latched_rdmad_read_wl/fdin [1]) );
  NAND2_X2 U1256 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n934), .ZN(n983) );
  NAND2_X2 U1257 ( .A1(n989), .A2(n935), .ZN(n938) );
  NAND2_X2 U1258 ( .A1(n938), .A2(n937), .ZN(n939) );
  NAND2_X2 U1259 ( .A1(n983), .A2(n939), .ZN(\ff_l2t_l2b_wbrd_wl_r0/fdin [0])
         );
  NAND2_X2 U1260 ( .A1(n704), .A2(n110), .ZN(n940) );
  NAND2_X2 U1261 ( .A1(n260), .A2(n940), .ZN(wb_cam_hit_vec_c2[7]) );
  NAND2_X2 U1262 ( .A1(n696), .A2(n110), .ZN(n941) );
  NAND2_X2 U1263 ( .A1(n274), .A2(n941), .ZN(wb_cam_hit_vec_c2[0]) );
  NAND2_X2 U1264 ( .A1(n694), .A2(n110), .ZN(n942) );
  NAND2_X2 U1265 ( .A1(n270), .A2(n942), .ZN(wb_cam_hit_vec_c2[2]) );
  NAND2_X2 U1266 ( .A1(n697), .A2(n110), .ZN(n943) );
  NAND2_X2 U1267 ( .A1(n268), .A2(n943), .ZN(wb_cam_hit_vec_c2[3]) );
  NAND2_X2 U1268 ( .A1(l2t_mb2_addr_r3[0]), .A2(n691), .ZN(n976) );
  INV_X4 U1269 ( .A(n976), .ZN(n953) );
  NAND2_X2 U1270 ( .A1(l2t_mb2_addr_r3[2]), .A2(n953), .ZN(n1023) );
  INV_X4 U1271 ( .A(n47), .ZN(n945) );
  INV_X4 U1272 ( .A(l2t_mb2_run_r1), .ZN(n944) );
  NAND3_X2 U1273 ( .A1(n59), .A2(wb_valid[6]), .A3(n702), .ZN(n946) );
  NAND2_X2 U1274 ( .A1(n1023), .A2(n946), .ZN(wbuf_wbtag_write_wl_c4[7]) );
  NAND2_X2 U1275 ( .A1(n683), .A2(n110), .ZN(n947) );
  NAND2_X2 U1276 ( .A1(n262), .A2(n947), .ZN(wb_cam_hit_vec_c2[6]) );
  NAND2_X2 U1277 ( .A1(n36), .A2(n691), .ZN(n973) );
  INV_X4 U1278 ( .A(n973), .ZN(n955) );
  NAND2_X2 U1279 ( .A1(n955), .A2(l2t_mb2_addr_r3[2]), .ZN(n1020) );
  NAND2_X2 U1280 ( .A1(n702), .A2(n57), .ZN(n948) );
  NAND2_X2 U1281 ( .A1(n1020), .A2(n948), .ZN(wbuf_wbtag_write_wl_c4[6]) );
  NAND2_X2 U1282 ( .A1(n701), .A2(n110), .ZN(n949) );
  NAND2_X2 U1283 ( .A1(n264), .A2(n949), .ZN(wb_cam_hit_vec_c2[5]) );
  NAND2_X2 U1284 ( .A1(n708), .A2(l2t_mb2_addr_r3[0]), .ZN(n970) );
  INV_X4 U1285 ( .A(n970), .ZN(n958) );
  NAND2_X2 U1286 ( .A1(n958), .A2(l2t_mb2_addr_r3[2]), .ZN(n1015) );
  NAND3_X2 U1287 ( .A1(n55), .A2(n714), .A3(n693), .ZN(n950) );
  NAND2_X2 U1288 ( .A1(n1015), .A2(n950), .ZN(wbuf_wbtag_write_wl_c4[5]) );
  NAND2_X2 U1289 ( .A1(n682), .A2(n110), .ZN(n951) );
  NAND2_X2 U1290 ( .A1(n266), .A2(n951), .ZN(wb_cam_hit_vec_c2[4]) );
  NAND2_X2 U1291 ( .A1(n708), .A2(n36), .ZN(n968) );
  INV_X4 U1292 ( .A(n968), .ZN(n960) );
  NAND2_X2 U1293 ( .A1(n960), .A2(l2t_mb2_addr_r3[2]), .ZN(n1011) );
  NAND2_X2 U1294 ( .A1(n53), .A2(n693), .ZN(n952) );
  NAND2_X2 U1295 ( .A1(n1011), .A2(n952), .ZN(wbuf_wbtag_write_wl_c4[4]) );
  NAND2_X2 U1296 ( .A1(n37), .A2(n953), .ZN(n1006) );
  NAND3_X2 U1297 ( .A1(n51), .A2(n668), .A3(n695), .ZN(n954) );
  NAND2_X2 U1298 ( .A1(n1006), .A2(n954), .ZN(wbuf_wbtag_write_wl_c4[3]) );
  NAND2_X2 U1299 ( .A1(n37), .A2(n955), .ZN(n1003) );
  NAND2_X2 U1300 ( .A1(n49), .A2(n695), .ZN(n956) );
  NAND2_X2 U1301 ( .A1(n1003), .A2(n956), .ZN(wbuf_wbtag_write_wl_c4[2]) );
  NAND2_X2 U1302 ( .A1(n110), .A2(n699), .ZN(n957) );
  NAND2_X2 U1303 ( .A1(n272), .A2(n957), .ZN(wb_cam_hit_vec_c2[1]) );
  NAND2_X2 U1304 ( .A1(n37), .A2(n958), .ZN(n998) );
  NAND2_X2 U1305 ( .A1(n998), .A2(n959), .ZN(wbuf_wbtag_write_wl_c4[1]) );
  NAND2_X2 U1306 ( .A1(n37), .A2(n960), .ZN(n995) );
  NAND2_X2 U1307 ( .A1(n45), .A2(n35), .ZN(n961) );
  NAND2_X2 U1308 ( .A1(n995), .A2(n961), .ZN(wbuf_wbtag_write_wl_c4[0]) );
  NAND2_X2 U1309 ( .A1(n963), .A2(n891), .ZN(wbuf_wb_read_en) );
  NAND2_X2 U1310 ( .A1(n965), .A2(n964), .ZN(wbuf_rdmat_read_en) );
  NAND2_X2 U1311 ( .A1(n968), .A2(n967), .ZN(wbuf_rdmat_read_wl[0]) );
  NAND2_X2 U1312 ( .A1(n970), .A2(n969), .ZN(wbuf_rdmat_read_wl[1]) );
  NAND2_X2 U1313 ( .A1(n976), .A2(n975), .ZN(wbuf_rdmat_read_wl[3]) );
  NAND2_X2 U1314 ( .A1(wb_acked[7]), .A2(wb_mbid_vld[7]), .ZN(n977) );
  NAND3_X2 U1315 ( .A1(n673), .A2(n1028), .A3(n977), .ZN(
        wbuf_misbuf_dep_rdy_en) );
  NAND2_X2 U1316 ( .A1(n989), .A2(n978), .ZN(n981) );
  NAND3_X2 U1317 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n987), .A3(n979), .ZN(
        n980) );
  NAND2_X2 U1318 ( .A1(n981), .A2(n980), .ZN(n982) );
  NAND2_X2 U1319 ( .A1(n983), .A2(n982), .ZN(\ff_l2t_l2b_wbrd_wl_r0/fdin [1])
         );
  NAND2_X2 U1320 ( .A1(wbuf_wbufrpt_leave_state0), .A2(n988), .ZN(n991) );
  NAND2_X2 U1321 ( .A1(l2t_l2b_wbrd_wl_r0[2]), .A2(n989), .ZN(n990) );
  NAND2_X2 U1322 ( .A1(n991), .A2(n990), .ZN(\ff_l2t_l2b_wbrd_wl_r0/fdin [2])
         );
  NAND2_X2 U1323 ( .A1(n995), .A2(n994), .ZN(wbuf_wb_read_wl[0]) );
  NAND2_X2 U1324 ( .A1(n996), .A2(n35), .ZN(n997) );
  NAND2_X2 U1325 ( .A1(n998), .A2(n997), .ZN(wbuf_wb_read_wl[1]) );
  NAND2_X2 U1326 ( .A1(n1003), .A2(n1002), .ZN(wbuf_wb_read_wl[2]) );
  NAND2_X2 U1327 ( .A1(n1005), .A2(n1006), .ZN(wbuf_wb_read_wl[3]) );
  INV_X4 U1328 ( .A(n1007), .ZN(n1009) );
  NAND3_X2 U1329 ( .A1(n35), .A2(n1013), .A3(n679), .ZN(n1014) );
  NAND2_X2 U1330 ( .A1(n1015), .A2(n1014), .ZN(wbuf_wb_read_wl[5]) );
  NAND2_X2 U1331 ( .A1(n1019), .A2(n1020), .ZN(wbuf_wb_read_wl[6]) );
  NAND2_X2 U1332 ( .A1(n1022), .A2(n1023), .ZN(wbuf_wb_read_wl[7]) );
endmodule

