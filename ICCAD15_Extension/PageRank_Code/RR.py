
#//////////////////////////////////////////////////
#//		Email ID: saima1@illinois.edu, rjiang3@illinois.edu
#//		Purpose: Evalutaion platform
#//////////////////////////////////////////////////
from __future__ import division
import os
import sys
import operator
import itertools
import math
import re
import numpy as np
import networkx as nx
import array
from datetime import datetime
import random
from random import shuffle
import regex as re
#import matlat python plot funtionality
import multiprocessing as mps
from types import *
import pprint as pp

NUMBER_OF_PROCESSES = mps.cpu_count()

# Timezones for the different VCD files
TIMEZONE = {
        'dmu_ilu': {'HybrSel' : 30075, 'PRankNetlist': 42250},
        'dmu_clu': {'SigSeT_1': 93100, 'HybrSel': 62900, 'PRankNetlist': 47100},
        'dmu_dsn': {'SigSeT_1': 42700, 'HybrSel': 17700, 'PRankNetlist': 56844},
        'mcu_rdpctl_ctl': {'SigSeT_1': 60550, 'HybrSel': 60000, 'PRankNetlist': 60000},
        'pmu': {'SigSeT_1': 40000, 'HybrSel': 39340, 'PRankNetlist': 59540},
        'ncu_fcd_ctl': {'SigSeT_1': 216030 , 'HybrSel': 222330, 'PRankNetlist': 12345}
        }

# Length of cycles traced
CYCLELENGTH = int(sys.argv[8])
## VCD parsing code for the Restoration Ratio calculation ##
SCOPE = '$scope'
UPSCOPE = '$upscope'
DUMPVARS = '$dumpvars'
END = '$end'

def do_get_signal_in_parallel(selected, vcd_file_contents, mode):

    signal_val = {}

    total_xvals_traced = 0

    task_queue = mps.Queue()
    done_queue = mps.Queue()

    TASKS = [(get_signal_val_parallel, (selected_signal, vcd_file_contents, TIMEZONE[sys.argv[4]][sys.argv[6]] * 100, float(sys.argv[5]) * 100, mode)) for selected_signal in selected]

    for task in TASKS:
        task_queue.put(task)

    start_time = datetime.now()

    for i in range(NUMBER_OF_PROCESSES):
        mps.Process(target=worker, args=(task_queue, done_queue)).start()

    ## Unordered results
    for i in range(len(TASKS)):
        # value_pair is a tuple. First element is the signal value indexed by the selected signal name
        # Second element is the total number of 'x' values traced for that selected signal
        value_pair = done_queue.get()
        signal_val.update(value_pair[0])
        total_xvals_traced = total_xvals_traced + value_pair[1]

    for i in range(NUMBER_OF_PROCESSES):
        task_queue.put('STOP')

    end_time = datetime.now()

    print '\n'

    print "Total time spent for collecting signal values: " + str((end_time - start_time).total_seconds())
    print "Total number of 'x' values traced: " + str(total_xvals_traced)

    return signal_val, total_xvals_traced

def worker(inputQ, outputQ):
    for func, args in iter(inputQ.get, 'STOP'):
        result = calculate(func, args)
        outputQ.put(result)

def calculate(func, args):
    result = func(*args)
    return result

def get_signal_val_parallel(selected_signal, vcd_file_contents, STARTTIME, clk_period, mode):
    print "Getting signal values from the trace file for the signal: " + selected_signal + " via Process ID: " + mps.current_process().name
    return get_signal_val(selected_signal, vcd_file_contents, STARTTIME, clk_period, mode)

def get_signal_val(selected_signal, vcd_file_contents, STARTTIME, clk_period, mode):
    '''
    Should return a NumPy array with the values of the signal
    '''
    sig_tab = {}
    sig_val = {}
    
    sig_val_temp = {}
    
    signal = [-1] * CYCLELENGTH
    ENDTIME = STARTTIME + int(clk_period * CYCLELENGTH)
    
    scope_index = [idx for idx, x in enumerate(vcd_file_contents) if SCOPE in x]
    upscope_index = [idx for idx, x in enumerate(vcd_file_contents) if UPSCOPE in x]
    for idx in range(scope_index[0] + 1, upscope_index[0]):
        line = vcd_file_contents[idx]
        line_content = line.split(' ')
        signal_name = line_content[4]
        if signal_name == selected_signal:
            signal_smbl = line_content[3]
            sig_tab[signal_smbl] = signal_name
            #print sig_tab
            break
    pattern = re.compile(r'^#{1}[0-9]+')
    time_index = [idx for idx, x in enumerate(vcd_file_contents) if re.search(pattern, x)]
    for i in range(len(time_index) - 1):
        if int(vcd_file_contents[time_index[i]][1:]) > ENDTIME:
            break
        for j in range(time_index[i] + 1, time_index[i + 1]):
            if DUMPVARS in vcd_file_contents[j] or END in vcd_file_contents[j]:
                continue
            else:
                value = vcd_file_contents[j][0]
                smbl = vcd_file_contents[j][1:].lstrip().rstrip()
                if smbl == sig_tab.keys()[0]:
                    if sig_tab[smbl] not in sig_val.keys():
                        #print time_index[i]
                        sig_val[sig_tab[smbl]]= [value + ':' + vcd_file_contents[time_index[i]][1:].lstrip().rstrip()]
                    else:
                        sig_val[sig_tab[smbl]].append(value + ':' + vcd_file_contents[time_index[i]][1:].lstrip().rstrip())
            #else:
            #    print vcd_file_contents[j]
            #    sig_string_con = vcd_file_contents[j].lstrip().rstrip().split(' ')
            #    value = sig_string_con[0][1:]
            #    smbl = sig_string_con[1]
            #    if 'x' in value:
            #        continue
            #    else:
            #        if sig_tab[smbl] not in sig_val.keys():
            #            sig_val[sig_tab[smbl]] = [hex(int(value[1:], 2))]
            #        else:
            #            sig_val[sig_tab[smbl]].append(hex(int(value[1:], 2)))
    #print selected_signal
    sig_val_list = sig_val[sig_val.keys()[0]]
    time = [int(data[data.index(':') + 1:]) for data in sig_val_list]
    #print time
    #print sig_val_list
    closestSTARTTIME = min(time, key=lambda x:abs(x - STARTTIME))
    closestENDTIME = min(time, key=lambda x:abs(x - ENDTIME))

    if closestSTARTTIME == closestENDTIME:
        ele = sig_val_list[time.index(closestSTARTTIME)]
        val = ele[:ele.find(':')]
        if val == 'x':
            signal = [-10] * CYCLELENGTH
        else:
            signal = [int(val)] * CYCLELENGTH
    elif closestSTARTTIME < closestENDTIME:
        curr_ele_ptr = 1
        extract_ele = sig_val_list[time.index(closestSTARTTIME) : time.index(closestENDTIME) + 1]
        extract_time = time[time.index(closestSTARTTIME) : time.index(closestENDTIME) + 1]
        for cycle in range(CYCLELENGTH):
            curr_time = STARTTIME + cycle * clk_period
            if curr_time < extract_time[curr_ele_ptr]:
                val = extract_ele[curr_ele_ptr - 1][:extract_ele[curr_ele_ptr - 1].find(':')]
                if val == 'x':
                    signal[cycle] = -10
                else:
                    signal[cycle] = int(val)
            elif curr_time >= extract_time[curr_ele_ptr]:
                val = extract_ele[curr_ele_ptr][:extract_ele[curr_ele_ptr].find(':')]
                if val == 'x':
                    signal[cycle] = -10
                else:
                    signal[cycle] = int(val)

                if curr_ele_ptr < len(extract_time) - 1:
                    curr_ele_ptr = curr_ele_ptr + 1

    #print signal
    #print STARTTIME, ENDTIME
    #print signal
    #exit(0)
    #return np.array(sig_val[sig_val.keys()[0]])
    xval_length = len([idx for idx, sig_val in enumerate(signal) if sig_val == -10])

    if mode == 0:
        return np.array(signal), xval_length
    elif mode == 1:
        sig_val_temp[selected_signal] = np.array(signal)
        return (sig_val_temp,  xval_length)
        


#a[curr_idx:curr_idx + length] = ( 0 for _ in range(length))

## VCD parsing code for the Restoration Ratio calculation ##

start=datetime.now()
print "Creation of circuit graph starts at time "+str(start)+"!"
#open data file
netlist = open(sys.argv[1], 'r')

#declare a directed graph
graph=nx.DiGraph();

#//////////////////////////////////////////////////
#//                     step1:
#//	 processes the simulation data
#//////////////////////////////////////////////////
maxcount=1000000
'''
####parse the simulation file
file = open(sys.argv[3], 'r')
simulation_file=[]
for line in file:
    simulation_file.append(line)
selected_value={}
maxcount=50000000
currentline=0
for line in simulation_file:
	line= line.split(' ')
	line[1]= line[1].replace("[","")
	line[len(line)-1]=line[len(line)-1].replace(']\n','')
	signal_name=line[0]
	selected_value[signal_name]=[]
	for i in range (1, len(line)):
		selected_value[line[0]].append(int(line[i].strip(',')))
totalcycles=len(selected_value[line[0]])


totalcycles = 1000;

Benchname=sys.argv[3]
if "S38417" in Benchname:
	flag=1
if "S38584" in Benchname:
	flag= 2
if "S35932" in Benchname:
	flag= 3
if "USB" in Benchname:
	flag= 4
'''
#totalcycles = 100
totalcycles = CYCLELENGTH
#############################=========================================================step 2 
#totalcycles=
dictin={}
dictout={}
input=[]
output=[]
comb=[]
seq=[]
inputlist=[]
outputlist=[]
ini_values=[-1]*totalcycles
flags=[0]*totalcycles
INPUT=[]
OUTPUT=[]
DFF=[]
AND=[]
NOT=[]
OR=[]
NOR=[]
NAND=[]
TRI=[]
#read lines from netlist
for linenumber, line in enumerate (netlist):
	line=line.split(' ')
	if 'INPUT' in line[0]:
  		item=line[0].replace('INPUT(','')
		item= item.replace(')\n','')
		item= item.replace(')\r\n','')
		graph.add_node(item,type='input', value = [-10]*totalcycles,inputnumber=0)
		INPUT.append(item)
	elif 'OUTPUT' in line[0]:
  		item=line[0].replace('OUTPUT(','')
		item= item.replace(')\n','')
		INitem= item.replace(')\r\n','')
		graph.add_node(item,type='output', value = [-10]*totalcycles,inputnumber=0)
		OUTPUT.append(item)
        elif len(line) == 1:
            continue
	else:
		#print line
		#if len(line)<3:
			#continue
		#print line
		if 'DFF' in line[2]:
			#print line[0]
			node=line[0] 
			
			dictin[node]=[]
			item = line[0]
 	 		INitem=line[2].replace('DFF(','')
			INitem=INitem.replace(')\n','')
			INitem=INitem.replace(')\r\n','')
			dictin[node].append(INitem)
			graph.add_node(node,type='dff',value = [-10]*totalcycles,inputnumber=len(line)-2)
			DFF.append(node)
			graph.add_edge(INitem,item,edgestatus = [0]*totalcycles)
			#print graph.node[node]['type'],graph.node[node]['inputnumber']
			

		elif 'NOT' in line[2]:
			node=line[0] 
			dictin[node]=[]
			item = line[0]
 	 		INitem=line[2].replace('NOT(','')
			INitem= INitem.replace(')\n','')
			INitem= INitem.replace(')\r\n','')
			dictin[node].append(INitem)
			graph.add_node(node,type='not',value = [-10]*totalcycles,inputnumber=len(line)-2)
			NOT.append(node)
			graph.add_edge(INitem,item,edgestatus = [0]*totalcycles)
			#print graph.node[node]['type'],graph.node[node]['inputnumber']
		

		elif 'NAND' in line[2]:

			node=line[0] 
			NAND.append(node)
			dictin[node]=[]
			graph.add_node(node,type='nand',value = [-10]*totalcycles,inputnumber=len(line)-2)
			
			for i in range (2,len(line)):
				line[i]=line[i].replace('NAND(','')
				line[i]=line[i].replace(')\n','')
				line[i]=line[i].replace(')\r\n','')
				line[i]=line[i].replace(',','')
				dictin[node].append(line[i])
				graph.add_edge(line[i],node,edgestatus = [0]*totalcycles)
			#print graph.node[node]['type'],graph.node[node]['inputnumber']

		elif 'NOR' in line[2]:
			node=line[0] 
			NOR.append(node)
			dictin[node]=[]
			graph.add_node(node,type='nor',value = [-10]*totalcycles,inputnumber=len(line)-2)
			for i in range (2,len(line)):
				line[i]=line[i].replace('NOR(','')
				line[i]=line[i].replace(')\r\n','')
				line[i]=line[i].replace(')\n','')
				line[i]=line[i].replace(',','')
				dictin[node].append(line[i])
				graph.add_edge(line[i],node,edgestatus = [0]*totalcycles)
		#	print graph.node[node]['type'],graph.node[node]['inputnumber']

		elif 'AND' in line[2]:
			node=line[0]
			AND.append(node)
			graph.add_node(node,type='and',value = [-10]*totalcycles,inputnumber=len(line)-2)
			dictin[node]=[]
			for i in range (2,len(line)):
				line[i]=line[i].replace('AND(','')
				line[i]=line[i].replace(')\n','')
				line[i]=line[i].replace(')\r\n','')

				line[i]=line[i].replace(',','')
				dictin[node].append(line[i])
				graph.add_edge(line[i],node,edgestatus = [0]*totalcycles)
			#print graph.node[node]['type'],graph.node[node]['inputnumber']

		elif 'OR' in line[2]:

			node=line[0]
			OR.append(node)
			graph.add_node(node,type='or',value = [-10]*totalcycles,inputnumber=len(line)-2)
			dictin[node]=[]
			for i in range (2,len(line)):
				line[i]=line[i].replace('OR(','')
				line[i]=line[i].replace(')\n','')
				line[i]=line[i].replace(')\r\n','')
				line[i]=line[i].replace(',','')
				dictin[node].append(line[i])
				graph.add_edge(line[i],node,edgestatus = [0]*totalcycles)
			#print graph.node[node]['type'],graph.node[node]['inputnumber']




print '~~~~~~~graph created!~~~~~~~~~```'

#dangling_nodes = [n for n in graph if graph.out_degree(n) == 0.0]
#pp.pprint(dangling_nodes)

#exit(0)

#for nodes in graph:
	#print graph.node[nodes], graph.predecessors(nodes),graph.successors(nodes)


#--------step2: force some lists of values to specific nodes
#--------       and initialize the queue for BFS------------
#--------      
# FEEDING INPUTS INTO THE NODES
file = open(sys.argv[2], 'r')
selection_file=[]
for line in file:
    selection_file.append(line)
selected=[]
# Gettng signal name from the selected signal final
index = 0
for line in selection_file:
	line = line.split(' ')

	if len(line)==1:
		PR=0
		items = line[0].strip("('")
		items = items.strip("',")
		items = items.strip("\n")
		selected.append(items)
	else:
                if index > int(sys.argv[7]):
                    break
		PR=1
		item = line[4].strip("('")
		item = item.strip("',")
		selected.append(item)
                index = index + 1
		

# 20%
#if PR:
#	bufferlength = bufferlength = int(round(0.2* len (selected)))
#else :
#	bufferlength = len(selected)


bufferlength = len(selected)
print bufferlength
tem_select=[]
for i in range (0,bufferlength):
	tem_select.append(selected[i])
selected=tem_select

# Putting values in the signals for the amount of cycles simulated. 
# this is where the values needs to be parsed from the VCD for our case analysis

# selected signal from the gate level netlist name
#for selected_signal in selected:
#	graph.node[selected_signal]['value']=np.random.randint(2, size=totalcycles)


#vfile = open(name_of_vcd_file, 'r')
vfile = open(sys.argv[3], 'r')
vcd_file_contents = vfile.readlines()
vfile.close()

# This variables keep track of unknown values in the trace buffer
total_xvals_traced = 0

mode = int(sys.argv[9])

if mode == 0:
    for selected_signal in selected:
        clk_period = float(sys.argv[5])
        print "Getting signal values for signal: " + selected_signal
        STARTTIME = TIMEZONE[sys.argv[4]][sys.argv[6]]
        signal_val_array, xval_length = get_signal_val(selected_signal, vcd_file_contents, STARTTIME * 100, clk_period * 100, mode)
        #print "Signal: " + selected_signal
        #print signal_val_array
        #print "\n"
        graph.node[selected_signal]['value'] = signal_val_array
        total_xvals_traced = xval_length + total_xvals_traced
        #exit(0)
    print "Total number of 'x' values traced: " + str(total_xvals_traced)
elif mode == 1:
    # signal_val is a dictionary 
    print "Retrieving signal values from trace file using: " + str(NUMBER_OF_PROCESSES) + " cores"
    signal_val, total_xvals_traced = do_get_signal_in_parallel(selected, vcd_file_contents, 1)
    for selected_signal in selected:
        graph.node[selected_signal]['value'] = signal_val[selected_signal]



inputlist=[]
timelist=[]
for x in selected:
	for y in range(0, totalcycles):
		inputlist.append(x)
		timelist.append(y)
print 'The total cycles of simulation is: ',totalcycles

#print inputlist, timelist	


#--------------------------------------------------------------
#------- step3: find all definite values of each node 
#---
#--  

#--------------------------------------------------------------
#------- step3: find all definite values of each node 
#---
#--  

# signal of what? output or input?
# signal is input

def findSucc(signal,Gatetype):
	if Gatetype=='and':
		if signal==0:
			return 0
		else :
			return -1

	elif Gatetype == 'nand':
		if signal ==0:
			return 1
		else :
			return -1

	elif Gatetype == 'or':
		if signal == 1:
			return 1
		else :
			return -1

	elif Gatetype == 'nor':
		if signal == 1:
			return 0
		else :
			return -1

	elif Gatetype == 'dff':
		return signal
	elif Gatetype == 'not':
		if signal== 1:
			return 0
		elif signal == 0:
			return 1
		else :
			return -1
	else:
		return -1 

	
# signal of what? output or input?
# signal is output

def findPred(signal, Gatetype):
	if Gatetype=='and':
		if signal==1:
			return 1
		else :
			return -1

	elif Gatetype == 'nand':
		if signal ==0:
			return 1
		else :
			return -1

	elif Gatetype == 'or':
		if signal == 0:
			return 0
		else :
			return -1

	elif Gatetype == 'nor':
		if signal == 1:
			return 0
		else :
			return -1

	elif Gatetype == 'dff':
		return signal
	elif Gatetype == 'not':
		if signal== 1:
			return 0
		elif signal == 0:
			return 1
		else :

			return -1
	else:
		return -1






print ('Start Calculating...')
# For what loopcount is used?
# until convergence is used
loopcount=0
while len(inputlist) and maxcount:

	loopcount+=1
	maxcount-=1
	currentNode= inputlist.pop(0)
	currentTime= timelist.pop(0)
	#find succ and pred
	currentSucc= graph.successors(currentNode)  	
	currentPred= graph.predecessors(currentNode)
        # What's happening per 10000 cycle?
	if loopcount % 10000 == 0:
		print('.'),len(inputlist)
#	print graph.node['G15'],currentNode,currentPred,currentSucc,'newloop'
#	print graph.node['G8']['value'],'G8',graph.node['G8']['type']
	#print graph.node['G12']['value'],'G12',graph.node['G12']['type']
	#print graph.node['G9']['value'],'G9',graph.node['G9']['type']#print graph.node[currentNode]['value'][currentTime]
	#print  inputlist, currentNode, currentPred, currentSucc 
	if currentPred:
		
		for Pred in currentPred:
			#if loopcount == 11 :
			#	print 'line 415','in5:',graph.node['in5']['value'],', in16:',graph.node['in16']['value']
			Pred_val = findPred(graph.node[currentNode]['value'][currentTime], graph.node[currentNode]['type'])


			if Pred_val != -1 and graph.node[currentNode]['type']!= 'dff' and graph.node[Pred]['value'][currentTime]==-10:
				graph.node[Pred]['value'][currentTime]= Pred_val
				#if currentTime>=0 and graph.edge[Pred][currentNode]['edgestatus'][currentTime]==0 :
				inputlist.append(Pred)
				timelist.append(currentTime)
				graph.edge[Pred][currentNode]['edgestatus'][currentTime]=1
				#print 'append in pred'
			elif Pred_val != -1 and graph.node[currentNode]['type']== 'dff' and currentTime>0 and graph.node[Pred]['value'][currentTime-1]==-10:

				graph.node[Pred]['value'][currentTime-1]= Pred_val
				#if currentTime>0 and graph.edge[Pred][currentNode]['edgestatus'][currentTime]==0 :
				inputlist.append(Pred)
				timelist.append(currentTime-1)
				#print 'append in pred'
				graph.edge[Pred][currentNode]['edgestatus'][currentTime]=1

			#if loopcount == 11 :
			#	print 'line 425','in5:',graph.node['in5']['value'],', in16:',graph.node['in16']['value'], currentNode
			#	print graph.node[currentNode]['value']
			#	print ';;;',currentNode, currentTime, Pred,currentPred, Pred_val
					######################sqeeze for 2-inputs logic gates
			if  graph.node[currentNode]['type']=='tri':
				print 'tri'
	val=-1	
	if  graph.node[currentNode]['inputnumber']==2:
				totalinput=graph.node[currentNode]['value'][currentTime]
				
				# the sum of the 4 signals related to the 3-input logic gate range from -40 to 4. A sqeeze method can be used when we know three out of four signals
				for preds in currentPred:
					totalinput = totalinput+graph.node[preds]['value'][currentTime]
				#totalinput= graph.node[currentNode]['value'][currentTime]+graph.node[currentPred[0]]['value'][currentTime]
				#totalinput+=graph.node[currentPred[1]]['value'][currentTime]
			#	print totalinput
	
				if totalinput>=-12 and totalinput<0 :#there are two signals known out of three
					if graph.node[currentNode]['value'][currentTime]==-10: #the output in unkown
						if totalinput==-10: #two known inputs are both 0
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=1		
						elif totalinput==-9 :#two known inputs are 1,0  
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0
                                                # You mean 1, 1?
						elif totalinput==-8 :#two known inputs are 1,0  
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						
						inputlist.append(currentNode)
						timelist.append(currentTime)
		
					elif graph.node[currentNode]['value'][currentTime]==0: #the output is known to be 0
						#and has 0, nand 1,1    or 0,0    nor has 1
						if totalinput==-10: #the known signal is 0 
							if graph.node[currentNode]['type']=='and': #out 0 in1 0 in2 unknown
								val=-1							
							elif graph.node[currentNode]['type']=='nand':#out 0 -> in1=in2=1 not possible
								val=-1
															
							elif graph.node[currentNode]['type']=='or':
								val=0							
							elif graph.node[currentNode]['type']=='nor':# out 0 means in1 Or in2
								val=1		
						elif totalinput==-9 :#the known signal is 1
							if graph.node[currentNode]['type']=='and':
								val=0						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=1	
							elif graph.node[currentNode]['type']=='or': #in1 or in2 = 0
								val=-1

							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)
								#print 'append in n3'
								graph.edge[preds][currentNode]['edgestatus'][currentTime]=1
					elif graph.node[currentNode]['value'][currentTime]==1: #the output is known to be 1
						#and 1,1   nand has 0 ,  or has 1,  or 0,0
						if totalinput==-9: #the known signal is 0 
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=1							
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=0	
						elif totalinput==-8 :#the known signal is 1
							if graph.node[currentNode]['type']=='and': #A and B ==1 A=1
								val=1						
							elif graph.node[currentNode]['type']=='nand': # A and B == 0 A=1
								val=0	
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1
							elif graph.node[currentNode]['type']=='nor':#A or B == 1 A=1
								val=-1
																								
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)
								#print 'append in n3'
								graph.edge[preds][currentNode]['edgestatus'][currentTime]=1
						graph.edge[currentPred[1]][currentNode]['edgestatus'][currentTime]=1
#########################################sqeeze for 3-inputs logic gates
	#print graph.node['G15'],graph.predecessors('G15'),graph.successors('G15'),'pred'
	#print graph.node['G8']['value']
	#print graph.node['G12']['value']
	#print graph.node['G9']['value']#print graph.node[currentNode]['value'][currentTime]
	if  graph.node[currentNode]['inputnumber']==3:

				#print Pred,'###',loopcount,currentNode,currentTime,'G2', graph.node['G2']['value']
				totalinput=graph.node[currentNode]['value'][currentTime]
				# the sum of the 4 signals related to the 3-input logic gate range from -40 to 4. A sqeeze method can be used when we know three out of four signals
				for preds in currentPred:
					totalinput = totalinput+graph.node[preds]['value'][currentTime]
				
			
				if totalinput>=-12 and totalinput<0 :#there 3 two signals known out of 4
					if graph.node[currentNode]['value'][currentTime]==-10: #the output in unknown
						if totalinput==-10: #two known inputs are 0,0,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=1		
						elif totalinput==-9 :#two known inputs are 1,0,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0
						elif totalinput==-8 :#two known inputs are 1,1,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0						
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						elif totalinput==-7 :#two known inputs are 1,1,1 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=1						
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						
						inputlist.append(currentNode)
						timelist.append(currentTime)
		
					elif graph.node[currentNode]['value'][currentTime]==0: #the output is known to be 0
					# and_inputs are has a zero, nand inputs are 1,1,1, or inputs are 0,0,0,   nor inputs has a one 

						if totalinput==-10: #the known signals are 0,0
							if graph.node[currentNode]['type']=='and': #out 0 in1 0 in2 unknown
								val=-1							
							elif graph.node[currentNode]['type']=='nand':#out 0 -> in1=in2=1 not possible
								val=-1
															
							elif graph.node[currentNode]['type']=='or':
								val=0							
							elif graph.node[currentNode]['type']=='nor':# out 0 means in1 Or in2
								val=1		
						elif totalinput==-9 :#the known signal is 1,0
							if graph.node[currentNode]['type']=='and':
								val=-1						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=-1	
																				
							elif graph.node[currentNode]['type']=='or': 
								val=-1

							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1	
						elif totalinput==-8 :#the known signal is 1,1
							if graph.node[currentNode]['type']=='and':
								val=0						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=1	
							elif graph.node[currentNode]['type']=='or': #in1 or in2 = 0
								val=-1
							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)
								#print 'append in n3'
								graph.edge[preds][currentNode]['edgestatus'][currentTime]=1
					elif graph.node[currentNode]['value'][currentTime]==1: #the output in known to be 1
					# and: 1,1,1   nand has zero, or has 1, nor 0,0,0
						if totalinput==-9: #the known signal are 0,0
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=1							
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=0	
						if totalinput==-8: #the known signal is 0,1
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1						
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=-1	
																					
						elif totalinput==-7 :#the known signal is 1,1
							if graph.node[currentNode]['type']=='and': #A and B ==1 A=1
								val=1						
							elif graph.node[currentNode]['type']=='nand': # A and B == 0 A=1
								val=0	
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1
							elif graph.node[currentNode]['type']=='nor':#A or B == 1 A=1
								val=-1 

	
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)
								#print 'append in n3'
								graph.edge[preds][currentNode]['edgestatus'][currentTime]=1
				#print Pred,'after',loopcount,currentNode,currentTime,'G2', graph.node['G2']['value']
#########################################sqeeze for 4-inputs logic gates
	if  graph.node[currentNode]['inputnumber']==4:

				totalinput=graph.node[currentNode]['value'][currentTime]
				# the sum of the 5 signals related to the 4-input logic gate range from -50 to 5. A sqeeze method can be used when we know 4 out of 5 signals
				for preds in currentPred:
					#print preds
					totalinput+= graph.node[preds]['value'][currentTime]
							
				if totalinput>=-12 and totalinput<0 :#there 4 two signals known out of 5
					if graph.node[currentNode]['value'][currentTime]==-10: #the output in unknown
						if totalinput==-10: #two known inputs are 0,0,0,0
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=1		
						elif totalinput==-9 :#two known inputs are 1,0,0,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0
						elif totalinput==-8 :#two known inputs are 1,1,0,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0						
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						elif totalinput==-7 :#two known inputs are 1,1,1,0 
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=0						
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						elif totalinput==-7 :#two known inputs are 1,1,1,1
							if graph.node[currentNode]['type']=='and':
								graph.node[currentNode]['value'][currentTime]=1						
							elif graph.node[currentNode]['type']=='nand':
								graph.node[currentNode]['value'][currentTime]=0							
							elif graph.node[currentNode]['type']=='or':
								graph.node[currentNode]['value'][currentTime]=1							
							elif graph.node[currentNode]['type']=='nor':
								graph.node[currentNode]['value'][currentTime]=0	
						inputlist.append(currentNode)
						timelist.append(currentTime)
		
					elif graph.node[currentNode]['value'][currentTime]==0: #the output is known to be 0
					# and_inputs are has a zero, nand inputs are 1,1,1,1 or inputs are 0,0,0,0   nor inputs has a one 

						if totalinput==-10: #the known signals are 0,0,0
							if graph.node[currentNode]['type']=='and': #
								val=-1							
							elif graph.node[currentNode]['type']=='nand':#
								val=-1
															
							elif graph.node[currentNode]['type']=='or':
								val=0							
							elif graph.node[currentNode]['type']=='nor':# out 0 means in1 Or in2
								val=1		
						elif totalinput==-9 :#the known signal is 1,0,0
							if graph.node[currentNode]['type']=='and':
								val=-1						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=-1	
																				
							elif graph.node[currentNode]['type']=='or': 
								val=-1

							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1	

						elif totalinput==-8 :#the known signal is 1,1,0
							if graph.node[currentNode]['type']=='and':
								val=-1						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=-1	
																				
							elif graph.node[currentNode]['type']=='or': 
								val=-1

							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1	
						elif totalinput==-7 :#the known signal is 1,1,1
							if graph.node[currentNode]['type']=='and':
								val=0						
							elif graph.node[currentNode]['type']=='nand': # in1 and in2 =1 
								val=1	
							elif graph.node[currentNode]['type']=='or': #in1 or in2 = 0
								val=-1
							elif graph.node[currentNode]['type']=='nor':
								val=-1 #in1 or in2 =1
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:			
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)

					elif graph.node[currentNode]['value'][currentTime]==1: #the output in known to be 1
					# and: 1,1,1,1   nand has zero, or has 1, nor 0,0,0,0
						if totalinput==-9: #the known signal are 0,0,0
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=1							
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=0	
						if totalinput==-8: #the known signal is 0,0,1
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1						
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=-1	
									
						if totalinput==-7: #the known signal is 0,1,1
							if graph.node[currentNode]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[currentNode]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1						
							elif graph.node[currentNode]['type']=='nor':#A or B == 0 
								val=-1	
																					
						elif totalinput==-6 :#the known signal is 1,1,1
							if graph.node[currentNode]['type']=='and': #A and B ==1 A=1
								val=1						
							elif graph.node[currentNode]['type']=='nand': # A and B == 0 A=1
								val=0	
							elif graph.node[currentNode]['type']=='or': #A or B == 1 A=0
								val=-1
							elif graph.node[currentNode]['type']=='nor':#A or B == 1 A=1
								val=-1 

	
						for preds in currentPred:
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:			
								graph.node[preds]['value'][currentTime]=val
								#if currentTime>=0  and graph.edge[preds][currentNode]['edgestatus'][currentTime]==0:
								inputlist.append(preds)
								timelist.append(currentTime)
							#print 'append in n4'

	#print graph.node['G15'],graph.predecessors('G15'),graph.successors('G15'),'sqeeze'
	#print graph.node['G8']['value']
	#print graph.node['G12']['value']
	#print graph.node['G9']['value']#print graph.node[currentNode]['value'][currentTime]

	if currentSucc:
		for Succ in currentSucc:
			Succ_val = findSucc(graph.node[currentNode]['value'][currentTime], graph.node[Succ]['type'])

		#	if Succ_val == -1 :
			#	succ_pred=graph.predecessors(Succ)
			#	for succ_preds in succ_pred:
			#		tem_val = findSucc(graph.node[succ_preds]['value'][currentTime], graph.node[Succ]['type'])
			#		if tem_val != -1:
			#			Succ_val = tem_val
						#print '!!!!!!!!!!!!!!!!!!!!!!'
			
			if Succ_val != -1 and  graph.node[Succ]['type'] != 'dff' and graph.node[Succ]['value'][currentTime]==-10:
				graph.node[Succ]['value'][currentTime]= Succ_val
				#if currentTime<= totalcycles-1  and graph.edge[currentNode][Succ]['edgestatus'][currentTime]==0:
				inputlist.append(Succ)
				timelist.append(currentTime)
				#print 'append in succ'
				graph.edge[currentNode][Succ]['edgestatus'][currentTime]=1
			elif Succ_val != -1 and  graph.node[Succ]['type'] == 'dff' and currentTime<totalcycles-1 and graph.node[Succ]['value'][currentTime+1]== -10:
				graph.node[Succ]['value'][currentTime+1]= Succ_val
				#if currentTime< totalcycles-1  and graph.edge[currentNode][Succ]['edgestatus'][currentTime]==0:
				inputlist.append(Succ)
				timelist.append(currentTime+1)
				graph.edge[currentNode][Succ]['edgestatus'][currentTime]=1
				#print 'append in succ'


			val=-1
			if  graph.node[Succ]['inputnumber']==2:
				totalinput=graph.node[Succ]['value'][currentTime]
				#totalinput=0
				for preds in graph.predecessors(Succ):
					totalinput = totalinput+graph.node[preds]['value'][currentTime]

				if totalinput>=-12 and totalinput<0 :#there are two signals known out of three

					if graph.node[Succ]['value'][currentTime]==-10: #the output in unkown
						
						if totalinput==-10: #two known inputs are both 0
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=1		
						elif totalinput==-9 :#two known inputs are 1,0  
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0
						elif totalinput==-8 :#two known inputs are 1,0  
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0	
									
						inputlist.append(Succ)
						timelist.append(currentTime)
		
					elif graph.node[Succ]['value'][currentTime]==0: #the output is known to be 0
						#and has 0, nand 1,1    or 0,0    nor has 1
						if totalinput==-10: #the known signal is 0 
							if graph.node[Succ]['type']=='and': #out 0 in1 0 in2 unknown
								val=-1							
							elif graph.node[Succ]['type']=='nand':#out 0 -> in1=in2=1 not possible
								val=-1
															
							elif graph.node[Succ]['type']=='or':
								val=0							
							elif graph.node[Succ]['type']=='nor':# out 0 means in1 Or in2
								val=1		
						elif totalinput==-9 :#the known signal is 1
							if graph.node[Succ]['type']=='and':
								val=0						
							elif graph.node[Succ]['type']=='nand': # in1 and in2 =1 
								val=1	
							elif graph.node[Succ]['type']=='or': #in1 or in2 = 0
								val=-1

							elif graph.node[Succ]['type']=='nor':
								val=-1 #in1 or in2 =1
																								
						for preds in graph.predecessors(Succ):
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								inputlist.append(preds)
								timelist.append(currentTime)

					elif graph.node[Succ]['value'][currentTime]==1: #the output is known to be 1
						#and 1,1   nand has 0 ,  or has 1,  or 0,0
						if totalinput==-9: #the known signal is 0 
							if graph.node[Succ]['type']=='and': # A and B ==1 A=0
								val=-1		
																				
							elif graph.node[Succ]['type']=='nand':# A and B == 0 A=0
								val=-1
							elif graph.node[Succ]['type']=='or': #A or B == 1 A=0
								val=1							
							elif graph.node[Succ]['type']=='nor':#A or B == 0 
								val=0	


						elif totalinput==-8 :#the known signal is 1
							if graph.node[Succ]['type']=='and': #A and B ==1 A=1
								val=1						
							elif graph.node[Succ]['type']=='nand': # A and B == 0 A=1
								val=0	
							elif graph.node[Succ]['type']=='or': #A or B == 1 A=0
								val=-1
							elif graph.node[Succ]['type']=='nor':#A or B == 1 A=1
								val=-1
																								
						for preds in graph.predecessors(Succ):
							if graph.node[preds]['value'][currentTime]==-10 and val!=-1:				
								graph.node[preds]['value'][currentTime]=val
								inputlist.append(preds)
								timelist.append(currentTime)



		
			elif  graph.node[Succ]['inputnumber']==3:
				totalinput= graph.node[Succ]['value'][currentTime]
				for preds in graph.predecessors(Succ):
					totalinput = totalinput+graph.node[preds]['value'][currentTime]
				if graph.node[Succ]['value'][currentTime]==-10 and totalinput>=0:
						if totalinput==0: 
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=1		
						elif totalinput==1 :
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0
						elif totalinput==2 :#
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0						
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0	
						elif totalinput==3 :
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=1						
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0	
						inputlist.append(Succ)
						timelist.append(currentTime)	
						
			elif  graph.node[Succ]['inputnumber']==4:
				totalinput= graph.node[Succ]['value'][currentTime]
				for preds in graph.predecessors(Succ):
					totalinput = totalinput+graph.node[preds]['value'][currentTime]
				if graph.node[Succ]['value'][currentTime]==-10 and totalinput>=0:
						if totalinput==0: #two known inputs are 0,0,0,0
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=1		
						elif totalinput==1 :#two known inputs are 1,0,0,0 
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0
						elif totalinput==2 :#two known inputs are 1,1,0,0 
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0						
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0	
						elif totalinput==3 :#two known inputs are 1,1,1,0 
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=0						
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0	
						elif totalinput==4 :#two known inputs are 1,1,1,1
							if graph.node[Succ]['type']=='and':
								graph.node[Succ]['value'][currentTime]=1						
							elif graph.node[Succ]['type']=='nand':
								graph.node[Succ]['value'][currentTime]=0							
							elif graph.node[Succ]['type']=='or':
								graph.node[Succ]['value'][currentTime]=1							
							elif graph.node[Succ]['type']=='nor':
								graph.node[Succ]['value'][currentTime]=0
						inputlist.append(Succ)
						timelist.append(currentTime)



## BIG BLUNDER

restored=0

# Total number of signals traced [number of bits multiplied by the number of cycles]
total_signals = len(selected) * totalcycles

# No idea what is this
if total_signals==0:
	total_signals =1

percent90=[]
percent80=[]
percent70=[]
percent60=[]
percent50=[]
percent10=[]
# No idea what is this

#print graph.nodes(data=True)
for gates in DFF:
	value = graph.node[gates]['value']
	signal_covered=0
	signal_total_restored=0
	for values in range (0, totalcycles):
		if value[values]!=-10:
			signal_total_restored+=1
			restored+=1

file_name = sys.argv[4] + '_' + sys.argv[6] + '_' + sys.argv[7] + '_' + str(CYCLELENGTH) + '.txt'
fo = open(file_name, "w")
print "Name of the file: ", fo.name

line = fo.write('The selected flops are:\n\n' + '\n'.join(selected))
#for items in selected:
#    line = fo.write(items)
#    line = fo.write('\n')

#line = fo.write('The restored signals are:\n')			
#signallist=[]

# restored only stores the restored Flip-Flops. Why RR is calculate based on that?
RR = restored / (total_signals - total_xvals_traced)

#restored_all= restored
#for gates in graph:
#	value = graph.node[gates]['value']
#	
#	for values in value:
#
#		if values!=-10:
#			if gates not in signallist:
#				signallist.append(gates);
#				line = fo.write(gates+ "\n")
#			restored_all+=1	

# RR = (restored_all + (total_signals - total_xvals_traced)) / (total_signals - total_xvals_traced)

fo.write('\n\n\n')
fo.write('The Trace Buffer Width is: ' + str(len(selected)) + '\n')
fo.write('The Trace Buffer Depth is: ' + str(CYCLELENGTH) + '\n')
fo.write('Total traced signals: ' + str(total_signals) + '\n')
fo.write('Total no of \'x\' values traced: ' + str(total_xvals_traced) + '\n')
fo.write('Total DFF restored: ' + str(restored) + '\n')
#fo.write('Total all signals (flops + gates) restored: ' + str(restored_all) + '\n')
fo.write('The RR is: ' + str(RR) + '\n')
fo.close()	

#print "\n\n\n\n\nThe RR is:",RR, ',',restored,'signals are restored,', 'total traced signasl', total_signals, 'restored_all=',restored_all
#print 'total number of iterations are:',loopcount

#print graph.node['CPUdata15'], graph.predecessors('CPUdata15'),graph.successors('CUPdata15')

# python RR.py /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection/pmu/SigSeT_1/pmu_flat_preprocessed_64_selected_signal.txt /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files/pmu_SigSeT_1.vcd
