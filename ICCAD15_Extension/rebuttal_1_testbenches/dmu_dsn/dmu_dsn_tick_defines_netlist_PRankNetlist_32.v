`ifdef PRankNetlist_32
	`define SIG0 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[0]
	`define SIG1 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[0]
	`define SIG2 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_pa[2]
	`define SIG3 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmu_hdr_vld_d1
	`define SIG4 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma[0]
	`define SIG5 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_sel
	`define SIG6 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_int[0]
	`define SIG7 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[1]
	`define SIG8 dmu_dsn_cov_bench.dmu_dsn_.j2d_p_cmd[3]
	`define SIG9 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma[1]
	`define SIG10 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[1]
	`define SIG11 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.stall_d1
	`define SIG12 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma [3]
	`define SIG13 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma [2]
	`define SIG14 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[15]
	`define SIG15 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[9]
	`define SIG16 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[6]
	`define SIG17 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [0]
	`define SIG18 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[2]
	`define SIG19 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[7]
	`define SIG20 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_ctag_d1[9]
	`define SIG21 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_int[1]
	`define SIG22 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_en
	`define SIG23 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[2]
	`define SIG24 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_dep.state[1]
	`define SIG25 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[2]
	`define SIG26 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[3]
	`define SIG27 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[14]
	`define SIG28 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[13]
	`define SIG29 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[10]
	`define SIG30 dmu_dsn_cov_bench.dmu_dsn_.j2d_p_addr[3]
	`define SIG31 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[3]
`endif
