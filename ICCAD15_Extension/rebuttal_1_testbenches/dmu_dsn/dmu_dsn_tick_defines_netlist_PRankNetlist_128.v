`ifdef PRankNetlist_128
	`define SIG0 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[0]
	`define SIG1 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[0]
	`define SIG2 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_pa[2]
	`define SIG3 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmu_hdr_vld_d1
	`define SIG4 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma[0]
	`define SIG5 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_sel
	`define SIG6 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_int[0]
	`define SIG7 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[1]
	`define SIG8 dmu_dsn_cov_bench.dmu_dsn_.j2d_p_cmd[3]
	`define SIG9 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma[1]
	`define SIG10 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[1]
	`define SIG11 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.stall_d1
	`define SIG12 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma [3]
	`define SIG13 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma [2]
	`define SIG14 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[15]
	`define SIG15 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[9]
	`define SIG16 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[6]
	`define SIG17 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [0]
	`define SIG18 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[2]
	`define SIG19 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[7]
	`define SIG20 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_ctag_d1[9]
	`define SIG21 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_int[1]
	`define SIG22 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_en
	`define SIG23 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[2]
	`define SIG24 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_dep.state[1]
	`define SIG25 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[2]
	`define SIG26 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[3]
	`define SIG27 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[14]
	`define SIG28 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[13]
	`define SIG29 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[10]
	`define SIG30 dmu_dsn_cov_bench.dmu_dsn_.j2d_p_addr[3]
	`define SIG31 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[3]
	`define SIG32 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[5]
	`define SIG33 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[7]
	`define SIG34 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.mondo_fifo.wr_ptr[0]
	`define SIG35 dmu_dsn_cov_bench.dmu_dsn_.j2d_p_addr[0]
	`define SIG36 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[1]
	`define SIG37 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag_ecc[3]
	`define SIG38 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.sio_dmc_tag[12]
	`define SIG39 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_ctag_d1[11]
	`define SIG40 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_ctag_d1[8]
	`define SIG41 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.dbg_stall_dma [4]
	`define SIG42 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[6]
	`define SIG43 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.pio_byte_cnt[4]
	`define SIG44 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_ctag_d1[10]
	`define SIG45 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.mondo_fifo.rd_ptr[0]
	`define SIG46 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.mondo_fifo.rd_ptr[1]
	`define SIG47 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[3]
	`define SIG48 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[9]
	`define SIG49 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[12]
	`define SIG50 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[15]
	`define SIG51 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[16]
	`define SIG52 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[13]
	`define SIG53 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[14]
	`define SIG54 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[20]
	`define SIG55 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[21]
	`define SIG56 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[18]
	`define SIG57 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[19]
	`define SIG58 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[17]
	`define SIG59 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[0]
	`define SIG60 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[1]
	`define SIG61 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[6]
	`define SIG62 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[5]
	`define SIG63 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[4]
	`define SIG64 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[2]
	`define SIG65 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[7]
	`define SIG66 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[11]
	`define SIG67 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[10]
	`define SIG68 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[8]
	`define SIG69 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[26]
	`define SIG70 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[29]
	`define SIG71 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[22]
	`define SIG72 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[24]
	`define SIG73 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[23]
	`define SIG74 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[25]
	`define SIG75 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[28]
	`define SIG76 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[31]
	`define SIG77 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[30]
	`define SIG78 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.data_d1[27]
	`define SIG79 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.enable_pe_err[0]
	`define SIG80 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.vld_d1
	`define SIG81 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_dep.state[0]
	`define SIG82 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.stall
	`define SIG83 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[33]
	`define SIG84 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[32]
	`define SIG85 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[41]
	`define SIG86 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[39]
	`define SIG87 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[40]
	`define SIG88 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[37]
	`define SIG89 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[38]
	`define SIG90 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[36]
	`define SIG91 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[35]
	`define SIG92 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[69]
	`define SIG93 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[25]
	`define SIG94 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[57]
	`define SIG95 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[53]
	`define SIG96 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[121]
	`define SIG97 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[67]
	`define SIG98 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[19]
	`define SIG99 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[16]
	`define SIG100 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[93]
	`define SIG101 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[91]
	`define SIG102 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[97]
	`define SIG103 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[95]
	`define SIG104 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[96]
	`define SIG105 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[99]
	`define SIG106 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[100]
	`define SIG107 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[102]
	`define SIG108 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[42]
	`define SIG109 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[41]
	`define SIG110 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[48]
	`define SIG111 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[46]
	`define SIG112 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[80]
	`define SIG113 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[119]
	`define SIG114 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[115]
	`define SIG115 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[117]
	`define SIG116 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[112]
	`define SIG117 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[113]
	`define SIG118 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[71]
	`define SIG119 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[87]
	`define SIG120 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[85]
	`define SIG121 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[81]
	`define SIG122 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[49]
	`define SIG123 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[35]
	`define SIG124 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[37]
	`define SIG125 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[31]
	`define SIG126 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[110]
	`define SIG127 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ctl.d2j_data_d1[17]
`endif
