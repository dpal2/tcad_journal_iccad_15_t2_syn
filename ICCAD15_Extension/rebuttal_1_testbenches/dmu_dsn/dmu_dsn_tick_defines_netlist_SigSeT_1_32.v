`ifdef SigSeT_1_32
	`define SIG0 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_full
	`define SIG1 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[1]
	`define SIG2 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[3]
	`define SIG3 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_vec0_d1
	`define SIG4 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[0]
	`define SIG5 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_vec[0]
	`define SIG6 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[2]
	`define SIG7 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_tail[0]
	`define SIG8 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_tail[1]
	`define SIG9 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.stall
	`define SIG10 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.state[2]
	`define SIG11 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_empty
	`define SIG12 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[0]
	`define SIG13 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.ack_buf_vld
	`define SIG14 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_out32.outdata_vec[0]
	`define SIG15 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_out32.stall_d1
	`define SIG16 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.state[1]
	`define SIG17 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.state[0]
	`define SIG18 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_en
	`define SIG19 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf_head[1]
	`define SIG20 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.skid_buf1_sel
	`define SIG21 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf1[0]
	`define SIG22 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.buf0[0]
	`define SIG23 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_pkt.state[1]
	`define SIG24 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_pkt.state[0]
	`define SIG25 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.stall_d1
	`define SIG26 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [0]
	`define SIG27 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [1]
	`define SIG28 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [2]
	`define SIG29 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [3]
	`define SIG30 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ccc_fsm.timer [4]
	`define SIG31 dmu_dsn_cov_bench.dmu_dsn_.dmu_dsn_ucb_flow.dmu_dsn_ucb_in32.indata_buf[117]
`endif
