`ifdef SigSeT_1_256
	`define SIG0 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.wptr [1]
	`define SIG1 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.wptr [2]
	`define SIG2 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.wptr [0]
	`define SIG3 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.wptr [3]
	`define SIG4 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [18]
	`define SIG5 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data_in[13]
	`define SIG6 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_scrub_data_valid.d0_0.q[0]
	`define SIG7 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.rptr [1]
	`define SIG8 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.rptr [0]
	`define SIG9 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.rptr [2]
	`define SIG10 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.rptr [3]
	`define SIG11 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_ddp_data_valid.d0_0.q[0]
	`define SIG12 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_l2t_data_valid.d0_0.q[1]
	`define SIG13 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_l2t_data_valid.d0_0.q[0]
	`define SIG14 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[21]
	`define SIG15 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fifo_deq_d1.d0_0.q[2]
	`define SIG16 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fifo_deq_d1.d0_0.q[1]
	`define SIG17 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fifo_deq_d1.d0_0.q[3]
	`define SIG18 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fifo_deq_d1.d0_0.q[4]
	`define SIG19 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[3]
	`define SIG20 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[2]
	`define SIG21 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[4]
	`define SIG22 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_crc_err_dly.d0_0.q[1]
	`define SIG23 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_data_cnt.d0_0.q[0]
	`define SIG24 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[0]
	`define SIG25 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[20]
	`define SIG26 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_pa_err.d0_0.q[0]
	`define SIG27 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_rddata_state[0]
	`define SIG28 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_scrub_ecc_err.d0_0.q[0]
	`define SIG29 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_scrub_ecc_err.d0_0.q[1]
	`define SIG30 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[5]
	`define SIG31 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_scrub_wren.d0_0.q[0]
	`define SIG32 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[23]
	`define SIG33 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_dummy_data_valid.d0_0.q[0]
	`define SIG34 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_dummy_data_valid.d0_0.q[1]
	`define SIG35 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fifo_deq_d1.d0_0.q[0]
	`define SIG36 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[36]
	`define SIG37 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_syndrome_dly.d0_0.q[0]
	`define SIG38 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_ecc_multi_err_d1[1]
	`define SIG39 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_ecc_single_err_d1[1]
	`define SIG40 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_ecc_single_err_d1[0]
	`define SIG41 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_rd_req_id[0]
	`define SIG42 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_rd_req_id[2]
	`define SIG43 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_rd_req_id[1]
	`define SIG44 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[12]
	`define SIG45 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[6]
	`define SIG46 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[9]
	`define SIG47 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[11]
	`define SIG48 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[8]
	`define SIG49 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[13]
	`define SIG50 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[7]
	`define SIG51 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_fifo_data[10]
	`define SIG52 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fbd_recov_err.d0_0.q[1]
	`define SIG53 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[0]
	`define SIG54 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.pff_secc_int_en.d0_0.q[0]
	`define SIG55 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_secc_cnt_intr.d0_0.q[0]
	`define SIG56 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[1]
	`define SIG57 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[2]
	`define SIG58 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[3]
	`define SIG59 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[4]
	`define SIG60 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[5]
	`define SIG61 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[6]
	`define SIG62 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[7]
	`define SIG63 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[8]
	`define SIG64 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[9]
	`define SIG65 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[10]
	`define SIG66 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[11]
	`define SIG67 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[12]
	`define SIG68 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[13]
	`define SIG69 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[14]
	`define SIG70 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_cnt[15]
	`define SIG71 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_fbd_recov_err.d0_0.q[0]
	`define SIG72 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[22]
	`define SIG73 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_rd_dummy_req0.d0_0.q[0]
	`define SIG74 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_dummy_req0.d0_0.q[5]
	`define SIG75 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[19]
	`define SIG76 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[4]
	`define SIG77 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[2]
	`define SIG78 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[0]
	`define SIG79 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[6]
	`define SIG80 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[7]
	`define SIG81 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[5]
	`define SIG82 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[3]
	`define SIG83 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq1_clear_ent[1]
	`define SIG84 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[6]
	`define SIG85 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[0]
	`define SIG86 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[4]
	`define SIG87 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[2]
	`define SIG88 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[7]
	`define SIG89 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[1]
	`define SIG90 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[5]
	`define SIG91 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_drq0_clear_ent[3]
	`define SIG92 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_rd_dummy_req1.d0_0.q[0]
	`define SIG93 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_dummy_req1.d0_0.q[5]
	`define SIG94 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[24]
	`define SIG95 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_qword_id.d0_0.q[0]
	`define SIG96 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_fbd_unrecov_err[0]
	`define SIG97 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [18]
	`define SIG98 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [18]
	`define SIG99 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [18]
	`define SIG100 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [18]
	`define SIG101 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [18]
	`define SIG102 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [18]
	`define SIG103 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [18]
	`define SIG104 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent11 [18]
	`define SIG105 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent4 [18]
	`define SIG106 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent5 [18]
	`define SIG107 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent12 [18]
	`define SIG108 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent13 [18]
	`define SIG109 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent0 [18]
	`define SIG110 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent8 [18]
	`define SIG111 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent9 [18]
	`define SIG112 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[18]
	`define SIG113 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_dummy_priority.d0_0.q[0]
	`define SIG114 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[16]
	`define SIG115 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_crc_recov_err.d0_0.q[0]
	`define SIG116 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_fbd_unrecov_err[1]
	`define SIG117 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_rddata_state[1]
	`define SIG118 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [10]
	`define SIG119 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_crc_err_dly.d0_0.q[5]
	`define SIG120 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_sts_reg[25]
	`define SIG121 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [12]
	`define SIG122 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [8]
	`define SIG123 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [8]
	`define SIG124 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [8]
	`define SIG125 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [8]
	`define SIG126 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [8]
	`define SIG127 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [8]
	`define SIG128 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [8]
	`define SIG129 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent4 [8]
	`define SIG130 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent5 [8]
	`define SIG131 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent12 [8]
	`define SIG132 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent13 [8]
	`define SIG133 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [8]
	`define SIG134 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent0 [8]
	`define SIG135 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent8 [8]
	`define SIG136 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent9 [8]
	`define SIG137 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [2]
	`define SIG138 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [2]
	`define SIG139 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [2]
	`define SIG140 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [2]
	`define SIG141 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [2]
	`define SIG142 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [2]
	`define SIG143 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [2]
	`define SIG144 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent4 [2]
	`define SIG145 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent5 [2]
	`define SIG146 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent12 [2]
	`define SIG147 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent13 [2]
	`define SIG148 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [2]
	`define SIG149 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent0 [2]
	`define SIG150 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent8 [2]
	`define SIG151 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent9 [2]
	`define SIG152 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [19]
	`define SIG153 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[10]
	`define SIG154 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[28]
	`define SIG155 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[8]
	`define SIG156 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[8]
	`define SIG157 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[11]
	`define SIG158 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[29]
	`define SIG159 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[9]
	`define SIG160 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[9]
	`define SIG161 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[12]
	`define SIG162 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[30]
	`define SIG163 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[10]
	`define SIG164 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[10]
	`define SIG165 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[13]
	`define SIG166 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[31]
	`define SIG167 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[11]
	`define SIG168 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[11]
	`define SIG169 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[14]
	`define SIG170 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[32]
	`define SIG171 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[12]
	`define SIG172 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[12]
	`define SIG173 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[15]
	`define SIG174 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[33]
	`define SIG175 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[13]
	`define SIG176 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[13]
	`define SIG177 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[16]
	`define SIG178 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[34]
	`define SIG179 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[14]
	`define SIG180 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[14]
	`define SIG181 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[17]
	`define SIG182 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[35]
	`define SIG183 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[15]
	`define SIG184 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[15]
	`define SIG185 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[2]
	`define SIG186 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[20]
	`define SIG187 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[0]
	`define SIG188 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[0]
	`define SIG189 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[3]
	`define SIG190 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[21]
	`define SIG191 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[1]
	`define SIG192 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[1]
	`define SIG193 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[4]
	`define SIG194 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[22]
	`define SIG195 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[2]
	`define SIG196 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[2]
	`define SIG197 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[5]
	`define SIG198 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[23]
	`define SIG199 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[3]
	`define SIG200 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[3]
	`define SIG201 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[6]
	`define SIG202 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[24]
	`define SIG203 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[4]
	`define SIG204 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[4]
	`define SIG205 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[7]
	`define SIG206 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[25]
	`define SIG207 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[5]
	`define SIG208 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[5]
	`define SIG209 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[8]
	`define SIG210 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[26]
	`define SIG211 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[6]
	`define SIG212 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[6]
	`define SIG213 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[9]
	`define SIG214 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl_err_retry_reg[27]
	`define SIG215 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl0_syndrome[7]
	`define SIG216 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.rdpctl1_syndrome[7]
	`define SIG217 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_crc_err_dly.d0_0.q[4]
	`define SIG218 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [0]
	`define SIG219 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [0]
	`define SIG220 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [0]
	`define SIG221 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [0]
	`define SIG222 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [0]
	`define SIG223 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [0]
	`define SIG224 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [0]
	`define SIG225 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent4 [0]
	`define SIG226 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent5 [0]
	`define SIG227 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent12 [0]
	`define SIG228 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent13 [0]
	`define SIG229 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent1 [0]
	`define SIG230 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent0 [0]
	`define SIG231 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent8 [0]
	`define SIG232 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent9 [0]
	`define SIG233 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.ff_crc_err_dly.d0_0.q[0]
	`define SIG234 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [10]
	`define SIG235 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [10]
	`define SIG236 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [10]
	`define SIG237 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [10]
	`define SIG238 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [10]
	`define SIG239 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [10]
	`define SIG240 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [10]
	`define SIG241 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent11 [10]
	`define SIG242 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent4 [10]
	`define SIG243 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent5 [10]
	`define SIG244 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent12 [10]
	`define SIG245 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent13 [10]
	`define SIG246 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent0 [10]
	`define SIG247 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent8 [10]
	`define SIG248 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent9 [10]
	`define SIG249 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent6 [19]
	`define SIG250 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent7 [19]
	`define SIG251 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent14 [19]
	`define SIG252 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent15 [19]
	`define SIG253 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent2 [19]
	`define SIG254 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent3 [19]
	`define SIG255 mcu_rdpctl_ctl_cov_bench.mcu_rdpctl_ctl_.otq.ent10 [19]
`endif
