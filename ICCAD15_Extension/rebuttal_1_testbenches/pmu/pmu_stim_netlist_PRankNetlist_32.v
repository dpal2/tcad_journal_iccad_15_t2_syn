`ifdef PRankNetlist_32
	initial begin
		#0 pdp_asi_din_to_pctl31 = 1'b0;
	end
	assign `SIG13 = pdp_asi_din_to_pctl31;

	initial begin
		#0 pmu_pct_ctltlu_fl_w10 = 1'b0;
	end
	assign `SIG31 = pmu_pct_ctltlu_fl_w10;

	initial begin
		#0 pmu_pdp_dppdp_asi_din34 = 1'b0;
	end
	assign `SIG22 = pmu_pdp_dppdp_asi_din34;

	initial begin
		#0 pmu_pdp_dppdp_asi_din36 = 1'b0;
		#59640.0 pmu_pdp_dppdp_asi_din36 = 1'b1;
		#24000.0 pmu_pdp_dppdp_asi_din36 = 1'b0;
	end
	assign `SIG20 = pmu_pdp_dppdp_asi_din36;

	initial begin
		#0 pmu_pdp_dppdp_asi_din37 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din37 = 1'b1;
		#100.0 pmu_pdp_dppdp_asi_din37 = 1'b0;
		#24000.0 pmu_pdp_dppdp_asi_din37 = 1'b1;
	end
	assign `SIG25 = pmu_pdp_dppdp_asi_din37;

	initial begin
		#0 pmu_pdp_dppdp_asi_din39 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din39 = 1'b1;
		#24100.0 pmu_pdp_dppdp_asi_din39 = 1'b0;
	end
	assign `SIG26 = pmu_pdp_dppdp_asi_din39;

	initial begin
		#0 pmu_pct_ctlmem_typ_w0 = 1'b0;
		#59540.0 pmu_pct_ctlmem_typ_w0 = 1'b1;
	end
	assign `SIG7 = pmu_pct_ctlmem_typ_w0;

	initial begin
		#0 pmu_pct_ctlmem_typ_w1 = 1'b0;
		#59540.0 pmu_pct_ctlmem_typ_w1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlmem_typ_w1;

	initial begin
		#0 n5843 = 1'b0;
		#59540.0 n5843 = 1'b1;
		#24100.0 n5843 = 1'b0;
	end
	assign `SIG14 = n5843;

	initial begin
		#0 pmu_pdp_dppdp_asi_din_53 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din_53 = 1'b1;
		#24100.0 pmu_pdp_dppdp_asi_din_53 = 1'b0;
	end
	assign `SIG21 = pmu_pdp_dppdp_asi_din_53;

	initial begin
		#0 n5832 = 1'b0;
		#83640.0 n5832 = 1'b1;
	end
	assign `SIG11 = n5832;

	initial begin
		#0 pct_incr_pic_w16 = 1'b0;
		#59540.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
	end
	assign `SIG3 = pct_incr_pic_w16;

	initial begin
		#0 pct_incr_pic_w14 = 1'b0;
		#59540.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#22400.0 pct_incr_pic_w14 = 1'b0;
	end
	assign `SIG1 = pct_incr_pic_w14;

	initial begin
		#0 pct_incr_pic_w15 = 1'b0;
		#59540.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#22500.0 pct_incr_pic_w15 = 1'b0;
	end
	assign `SIG0 = pct_incr_pic_w15;

	initial begin
		#0 pct_incr_pic_w13 = 1'b0;
		#59640.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#22400.0 pct_incr_pic_w13 = 1'b0;
	end
	assign `SIG2 = pct_incr_pic_w13;

	initial begin
		#0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#59540.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlpic_std_w17;

	initial begin
		#0 pdp_asi_din_to_pctl14 = 1'b0;
	end
	assign `SIG6 = pdp_asi_din_to_pctl14;

	initial begin
		#0 pdp_asi_din_to_pctl15 = 1'b0;
		#59540.0 pdp_asi_din_to_pctl15 = 1'b1;
		#100.0 pdp_asi_din_to_pctl15 = 1'b0;
		#24000.0 pdp_asi_din_to_pctl15 = 1'b1;
	end
	assign `SIG10 = pdp_asi_din_to_pctl15;

	initial begin
		#0 pdp_asi_din_to_pctl18 = 1'b0;
	end
	assign `SIG19 = pdp_asi_din_to_pctl18;

	initial begin
		#0 n6158 = 1'b0;
	end
	assign `SIG12 = n6158;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#59540.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
		#100.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#24000.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
	end
	assign `SIG15 = pdp_asi_ctlin_to_pctl_4_02;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
	end
	assign `SIG16 = pdp_asi_ctlin_to_pctl_4_01;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
	end
	assign `SIG17 = pdp_asi_ctlin_to_pctl_4_00;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
	end
	assign `SIG18 = pdp_asi_ctlin_to_pctl_4_04;

	initial begin
		#0 pmu_pct_ctlifu_fl_b0 = 1'b0;
		#73740.0 pmu_pct_ctlifu_fl_b0 = 1'b1;
	end
	assign `SIG23 = pmu_pct_ctlifu_fl_b0;

	initial begin
		#0 pmu_pct_ctlifu_fl_b1 = 1'b0;
	end
	assign `SIG24 = pmu_pct_ctlifu_fl_b1;

	initial begin
		#0 pmu_pct_ctldec_valid_m0 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctldec_valid_m0;

	initial begin
		#0 pdp_asi_din_to_pctl27 = 1'b0;
	end
	assign `SIG27 = pdp_asi_din_to_pctl27;

	initial begin
		#0 pdp_asi_din_to_pctl29 = 1'b0;
	end
	assign `SIG9 = pdp_asi_din_to_pctl29;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_84 = 1'b0;
		#73640.0 pdp_asi_ctlin_to_pctl_15_84 = 1'b1;
	end
	assign `SIG28 = pdp_asi_ctlin_to_pctl_15_84;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_85 = 1'b0;
	end
	assign `SIG29 = pdp_asi_ctlin_to_pctl_15_85;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
		#59540.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b1;
		#14100.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
	end
	assign `SIG30 = pdp_asi_ctlin_to_pctl_15_80;

	initial begin
		 #89540.0 $finish;
	end

`endif
