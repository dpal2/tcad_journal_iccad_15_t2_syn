`ifdef HybrSel_128
	`define SIG0 pmu_cov_bench.pmu_.pmu_pct_ctl.pwrm.d0_0.q[1]
	`define SIG1 pmu_cov_bench.pmu_.pct_incr_pic_w1[6]
	`define SIG2 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [26]
	`define SIG3 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [29]
	`define SIG4 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [24]
	`define SIG5 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [25]
	`define SIG6 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [31]
	`define SIG7 pmu_cov_bench.pmu_.pmu_pdp_dp.pic4.c0_0.l1en
	`define SIG8 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[52]
	`define SIG9 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [28]
	`define SIG10 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[51]
	`define SIG11 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[19]
	`define SIG12 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [10]
	`define SIG13 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[35]
	`define SIG14 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [7]
	`define SIG15 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[3]
	`define SIG16 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [31]
	`define SIG17 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[2]
	`define SIG18 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[11]
	`define SIG19 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [14]
	`define SIG20 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[43]
	`define SIG21 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[37]
	`define SIG22 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[32]
	`define SIG23 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[34]
	`define SIG24 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[36]
	`define SIG25 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[25]
	`define SIG26 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [15]
	`define SIG27 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[44]
	`define SIG28 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[12]
	`define SIG29 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [15]
	`define SIG30 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[38]
	`define SIG31 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [21]
	`define SIG32 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[50]
	`define SIG33 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[24]
	`define SIG34 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [16]
	`define SIG35 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [17]
	`define SIG36 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [8]
	`define SIG37 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [8]
	`define SIG38 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [5]
	`define SIG39 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [29]
	`define SIG40 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [20]
	`define SIG41 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [14]
	`define SIG42 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[26]
	`define SIG43 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[18]
	`define SIG44 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [16]
	`define SIG45 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [21]
	`define SIG46 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [6]
	`define SIG47 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[10]
	`define SIG48 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [28]
	`define SIG49 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[45]
	`define SIG50 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [9]
	`define SIG51 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [27]
	`define SIG52 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [23]
	`define SIG53 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [27]
	`define SIG54 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [9]
	`define SIG55 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [22]
	`define SIG56 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[17]
	`define SIG57 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[0]
	`define SIG58 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [7]
	`define SIG59 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[33]
	`define SIG60 pmu_cov_bench.pmu_.pmu_pdp_dp.pich0 [22]
	`define SIG61 pmu_cov_bench.pmu_.pmu_pdp_dp.picl0 [4]
	`define SIG62 pmu_cov_bench.pmu_.pmu_pdp_dp.pic0.d0_0.q[1]
	`define SIG63 pmu_cov_bench.pmu_.pmu_pdp_dp.picl7 [30]
	`define SIG64 pmu_cov_bench.pmu_.pmu_pdp_dp.pich7 [23]
	`define SIG65 pmu_cov_bench.pmu_.pmu_pdp_dp.pich6 [30]
	`define SIG66 pmu_cov_bench.pmu_.pmu_pdp_dp.pic3.d0_0.q[13]
	`define SIG67 pmu_cov_bench.pmu_.pmu_pct_ctl.pmu_pmen
	`define SIG68 pmu_cov_bench.pmu_.pmu_pct_ctl.tg1_e [12]
	`define SIG69 pmu_cov_bench.pmu_.pmu_pct_ctl.lsu_tid_b [2]
	`define SIG70 pmu_cov_bench.pmu_.pmu_pct_ctl.tg1_e [11]
	`define SIG71 pmu_cov_bench.pmu_.pmu_pct_ctl.th_m [0]
	`define SIG72 pmu_cov_bench.pmu_.pmu_pct_ctl.tg1_e [10]
	`define SIG73 pmu_cov_bench.pmu_.pmu_pct_ctl.dec_valid_m[0]
	`define SIG74 pmu_cov_bench.pmu_.pmu_pct_ctl.pwrm.d0_0.q[2]
	`define SIG75 pmu_cov_bench.pmu_.pmu_pct_ctl.x7_1 [0]
	`define SIG76 pmu_cov_bench.pmu_.pmu_pct_ctl.x6_1 [0]
	`define SIG77 pmu_cov_bench.pmu_.pmu_pct_ctl.x0_1 [1]
	`define SIG78 pmu_cov_bench.pmu_.pmu_pct_ctl.x1_1 [1]
	`define SIG79 pmu_cov_bench.pmu_.pmu_pct_ctl.x5_1 [0]
	`define SIG80 pmu_cov_bench.pmu_.pmu_pct_ctl.x3_1 [0]
	`define SIG81 pmu_cov_bench.pmu_.pmu_pct_ctl.x2_1 [1]
	`define SIG82 pmu_cov_bench.pmu_.pmu_pct_ctl.x7_1 [1]
	`define SIG83 pmu_cov_bench.pmu_.pmu_pct_ctl.x0_1 [0]
	`define SIG84 pmu_cov_bench.pmu_.pmu_pct_ctl.x4_1 [0]
	`define SIG85 pmu_cov_bench.pmu_.pmu_pct_ctl.x1_1 [0]
	`define SIG86 pmu_cov_bench.pmu_.pmu_pct_ctl.x2_1 [0]
	`define SIG87 pmu_cov_bench.pmu_.pmu_pct_ctl.x3_1 [1]
	`define SIG88 pmu_cov_bench.pmu_.pmu_pct_ctl.x5_1 [1]
	`define SIG89 pmu_cov_bench.pmu_.pmu_pct_ctl.x4_1 [1]
	`define SIG90 pmu_cov_bench.pmu_.pmu_pct_ctl.x6_1 [1]
	`define SIG91 pmu_cov_bench.pmu_.pmu_pct_ctl.rc4
	`define SIG92 pmu_cov_bench.pmu_.pmu_rngl_cdbus[64]
	`define SIG93 pmu_cov_bench.pmu_.pmu_pct_ctl.events.d0_0.q[1]
	`define SIG94 pmu_cov_bench.pmu_.pmu_pct_ctl.des
	`define SIG95 pmu_cov_bench.pmu_.pmu_pct_ctl.events.d0_0.q[3]
	`define SIG96 pmu_cov_bench.pmu_.pmu_pct_ctl.aes_d1
	`define SIG97 pmu_cov_bench.pmu_.pmu_pct_ctl.hash
	`define SIG98 pmu_cov_bench.pmu_.pmu_rngl_cdbus[62]
	`define SIG99 pmu_cov_bench.pmu_.pmu_rngl_cdbus[55]
	`define SIG100 pmu_cov_bench.pmu_.pmu_pdp_dp.asi_din.d0_0.q[30]
	`define SIG101 pmu_cov_bench.pmu_.pmu_pct_ctl.fl_w [0]
	`define SIG102 pmu_cov_bench.pmu_.pmu_rngl_cdbus[13]
	`define SIG103 pmu_cov_bench.pmu_.pmu_pct_ctl.tlu_pmu_trap_mask_m [0]
	`define SIG104 pmu_cov_bench.pmu_.pmu_pct_ctl.tg0_e [10]
	`define SIG105 pmu_cov_bench.pmu_.pmu_pct_ctl.tg0_e [12]
	`define SIG106 pmu_cov_bench.pmu_.pmu_pct_ctl.ti_e2m.d0_0.q[1]
	`define SIG107 pmu_cov_bench.pmu_.pmu_pct_ctl.tlu_pmu_trap_mask_m [1]
	`define SIG108 pmu_cov_bench.pmu_.pmu_pct_ctl.fl_w [1]
	`define SIG109 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr5_read [22]
	`define SIG110 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr6_read [22]
	`define SIG111 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr0_read [22]
	`define SIG112 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr7_read [22]
	`define SIG113 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr6_read [23]
	`define SIG114 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr1_read [22]
	`define SIG115 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr2_read [22]
	`define SIG116 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr3_read [22]
	`define SIG117 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr0_read [7]
	`define SIG118 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr4_read [22]
	`define SIG119 pmu_cov_bench.pmu_.pmu_pct_ctl.spares.spare11_flop.q
	`define SIG120 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr5_read [9]
	`define SIG121 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr7_read [7]
	`define SIG122 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr6_read [9]
	`define SIG123 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr2_read [15]
	`define SIG124 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr0_read [9]
	`define SIG125 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr1_read [5]
	`define SIG126 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr1_read [9]
	`define SIG127 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr2_read [9]
`endif
