`ifdef SigSeT_1_128
	initial begin
		#0 n686dummy = 1'b0;
		#40000.0 n686dummy = 1'bx;
	end
	assign `SIG39 = n686dummy;

	initial begin
		#0 pmu_pct_ctlpcr6_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read3 = 1'bx;
	end
	assign `SIG21 = pmu_pct_ctlpcr6_read3;

	initial begin
		#0 pmu_pct_ctlpcr6_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read2 = 1'bx;
	end
	assign `SIG37 = pmu_pct_ctlpcr6_read2;

	initial begin
		#0 pmu_pct_ctlpcr6_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read1 = 1'bx;
	end
	assign `SIG30 = pmu_pct_ctlpcr6_read1;

	initial begin
		#0 pmu_pct_ctltg0_e12 = 1'b0;
		#40000.0 pmu_pct_ctltg0_e12 = 1'b1;
	end
	assign `SIG85 = pmu_pct_ctltg0_e12;

	initial begin
		#0 pmu_pct_ctlskip_wakeup = 1'b0;
		#40000.0 pmu_pct_ctlskip_wakeup = 1'bx;
	end
	assign `SIG106 = pmu_pct_ctlskip_wakeup;

	initial begin
		#0 pmu_pdp_dppic07_w2c0_0l1en = 1'b0;
		#40000.0 pmu_pdp_dppic07_w2c0_0l1en = 1'bx;
	end
	assign `SIG114 = pmu_pdp_dppic07_w2c0_0l1en;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m3 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
	end
	assign `SIG45 = pmu_pct_ctlph_br_may_trap_m3;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m2 = 1'b0;
	end
	assign `SIG49 = pmu_pct_ctlph_br_may_trap_m2;

	initial begin
		#0 pmu_pct_ctlatid1 = 1'b0;
		#40000.0 pmu_pct_ctlatid1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlatid1;

	initial begin
		#0 pmu_pct_ctlatid2 = 1'b0;
		#40000.0 pmu_pct_ctlatid2 = 1'b1;
		#13700.0 pmu_pct_ctlatid2 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctlatid2;

	initial begin
		#0 pmu_pct_ctlatid3 = 1'b0;
	end
	assign `SIG9 = pmu_pct_ctlatid3;

	initial begin
		#0 pmu_pct_ctlatid4 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlatid4;

	initial begin
		#0 pmu_pct_ctlatid5 = 1'b0;
	end
	assign `SIG6 = pmu_pct_ctlatid5;

	initial begin
		#0 pmu_pct_ctlatid6 = 1'b0;
	end
	assign `SIG2 = pmu_pct_ctlatid6;

	initial begin
		#0 pmu_pct_ctlatid7 = 1'b0;
	end
	assign `SIG3 = pmu_pct_ctlatid7;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m2 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m2 = 1'bx;
	end
	assign `SIG58 = pmu_pct_ctll2dm_wrap_m2;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m7 = 1'b0;
	end
	assign `SIG51 = pmu_pct_ctlph_br_may_trap_m7;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m6 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m6 = 1'bx;
	end
	assign `SIG57 = pmu_pct_ctlph_br_may_trap_m6;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m7 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m7 = 1'bx;
	end
	assign `SIG61 = pmu_pct_ctll2dm_wrap_m7;

	initial begin
		#0 pmu_pct_ctlpmu_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlpmu_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG86 = pmu_pct_ctlpmu_busy_clkgenc_0l1en;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m4 = 1'b0;
	end
	assign `SIG69 = pmu_pct_ctlph_br_may_trap_m4;

	initial begin
		#0 pmu_pct_ctltrap_m1 = 1'b0;
	end
	assign `SIG46 = pmu_pct_ctltrap_m1;

	initial begin
		#0 pmu_pct_ctltrap_m0 = 1'b0;
	end
	assign `SIG64 = pmu_pct_ctltrap_m0;

	initial begin
		#0 pmu_pct_ctltrap_m3 = 1'b0;
	end
	assign `SIG44 = pmu_pct_ctltrap_m3;

	initial begin
		#0 pmu_pct_ctltrap_m2 = 1'b0;
	end
	assign `SIG48 = pmu_pct_ctltrap_m2;

	initial begin
		#0 pmu_pct_ctltrap_m5 = 1'b0;
	end
	assign `SIG52 = pmu_pct_ctltrap_m5;

	initial begin
		#0 pmu_pct_ctltrap_m4 = 1'b0;
	end
	assign `SIG68 = pmu_pct_ctltrap_m4;

	initial begin
		#0 pmu_pct_ctltrap_m7 = 1'b0;
	end
	assign `SIG50 = pmu_pct_ctltrap_m7;

	initial begin
		#0 pmu_pct_ctltrap_m6 = 1'b0;
	end
	assign `SIG56 = pmu_pct_ctltrap_m6;

	initial begin
		#0 pmu_pct_ctlpl_incr_m6 = 1'b0;
	end
	assign `SIG97 = pmu_pct_ctlpl_incr_m6;

	initial begin
		#0 n5843 = 1'b0;
	end
	assign `SIG71 = n5843;

	initial begin
		#0 pmu_pct_ctltrap_hold4 = 1'b0;
	end
	assign `SIG80 = pmu_pct_ctltrap_hold4;

	initial begin
		#0 pmu_pct_ctltrap_hold5 = 1'b0;
		#40000.0 pmu_pct_ctltrap_hold5 = 1'bx;
		#23800.0 pmu_pct_ctltrap_hold5 = 1'b0;
	end
	assign `SIG83 = pmu_pct_ctltrap_hold5;

	initial begin
		#0 pmu_pct_ctltrap_hold6 = 1'b0;
		#53800.0 pmu_pct_ctltrap_hold6 = 1'bx;
	end
	assign `SIG77 = pmu_pct_ctltrap_hold6;

	initial begin
		#0 pmu_pct_ctltrap_hold7 = 1'b0;
	end
	assign `SIG79 = pmu_pct_ctltrap_hold7;

	initial begin
		#0 n6614 = 1'b0;
		#40100.0 n6614 = 1'b1;
		#23600.0 n6614 = 1'b0;
	end
	assign `SIG7 = n6614;

	initial begin
		#0 pmu_pct_ctltrap_hold1 = 1'b0;
		#63800.0 pmu_pct_ctltrap_hold1 = 1'bx;
	end
	assign `SIG78 = pmu_pct_ctltrap_hold1;

	initial begin
		#0 pmu_pct_ctltrap_hold2 = 1'b0;
	end
	assign `SIG82 = pmu_pct_ctltrap_hold2;

	initial begin
		#0 pmu_pct_ctltrap_hold3 = 1'b0;
	end
	assign `SIG76 = pmu_pct_ctltrap_hold3;

	initial begin
		#0 pmu_pct_ctlpcr1_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read1 = 1'bx;
	end
	assign `SIG28 = pmu_pct_ctlpcr1_read1;

	initial begin
		#0 pmu_pct_ctlpcr1_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read3 = 1'bx;
	end
	assign `SIG26 = pmu_pct_ctlpcr1_read3;

	initial begin
		#0 pmu_pct_ctlpc_wr = 1'b0;
	end
	assign `SIG0 = pmu_pct_ctlpc_wr;

	initial begin
		#0 pct_incr_pic_w14 = 1'b0;
		#40100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
	end
	assign `SIG109 = pct_incr_pic_w14;

	initial begin
		#0 pmu_pct_ctlpcr5_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read1 = 1'bx;
	end
	assign `SIG31 = pmu_pct_ctlpcr5_read1;

	initial begin
		#0 pmu_pct_ctlpcr5_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read2 = 1'bx;
	end
	assign `SIG38 = pmu_pct_ctlpcr5_read2;

	initial begin
		#0 pmu_pct_ctlpcr5_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read3 = 1'bx;
	end
	assign `SIG23 = pmu_pct_ctlpcr5_read3;

	initial begin
		#0 pmu_pct_ctlpl_brm6 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm6 = 1'bx;
	end
	assign `SIG126 = pmu_pct_ctlpl_brm6;

	initial begin
		#0 pmu_pct_ctlpl_brm7 = 1'b0;
	end
	assign `SIG118 = pmu_pct_ctlpl_brm7;

	initial begin
		#0 pmu_pct_ctlpl_brm5 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm5 = 1'bx;
		#13800.0 pmu_pct_ctlpl_brm5 = 1'b0;
	end
	assign `SIG127 = pmu_pct_ctlpl_brm5;

	initial begin
		#0 pmu_pct_ctlpl_brm2 = 1'b0;
	end
	assign `SIG123 = pmu_pct_ctlpl_brm2;

	initial begin
		#0 pmu_pct_ctlpl_brm3 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm3 = 1'bx;
		#13800.0 pmu_pct_ctlpl_brm3 = 1'b0;
	end
	assign `SIG117 = pmu_pct_ctlpl_brm3;

	initial begin
		#0 pmu_pct_ctlpl_brm1 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm1 = 1'bx;
	end
	assign `SIG120 = pmu_pct_ctlpl_brm1;

	initial begin
		#0 pct_incr_pic_w15 = 1'b0;
		#40100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#22100.0 pct_incr_pic_w15 = 1'b0;
	end
	assign `SIG111 = pct_incr_pic_w15;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m3 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m3 = 1'bx;
	end
	assign `SIG63 = pmu_pct_ctll2dm_wrap_m3;

	initial begin
		#0 pct_incr_pic_w12 = 1'b0;
		#40000.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#22500.0 pct_incr_pic_w12 = 1'b0;
	end
	assign `SIG110 = pct_incr_pic_w12;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
		#63600.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b1;
	end
	assign `SIG87 = pdp_asi_ctlin_to_pctl_15_80;

	initial begin
		#0 pmu_pct_ctlph_brm2 = 1'b0;
	end
	assign `SIG122 = pmu_pct_ctlph_brm2;

	initial begin
		#0 pmu_pct_ctlph_brm3 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm3 = 1'bx;
		#13800.0 pmu_pct_ctlph_brm3 = 1'b0;
	end
	assign `SIG116 = pmu_pct_ctlph_brm3;

	initial begin
		#0 pmu_pct_ctlph_brm1 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm1 = 1'bx;
	end
	assign `SIG121 = pmu_pct_ctlph_brm1;

	initial begin
		#0 pmu_pct_ctlph_brm6 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm6 = 1'bx;
	end
	assign `SIG124 = pmu_pct_ctlph_brm6;

	initial begin
		#0 pmu_pct_ctlph_brm7 = 1'b0;
	end
	assign `SIG119 = pmu_pct_ctlph_brm7;

	initial begin
		#0 pmu_pct_ctlph_brm5 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm5 = 1'bx;
		#13800.0 pmu_pct_ctlph_brm5 = 1'b0;
	end
	assign `SIG125 = pmu_pct_ctlph_brm5;

	initial begin
		#0 pct_incr_pic_w13 = 1'b0;
		#40000.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
	end
	assign `SIG113 = pct_incr_pic_w13;

	initial begin
		#0 pmu_pct_ctlpcr3_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read2 = 1'bx;
	end
	assign `SIG43 = pmu_pct_ctlpcr3_read2;

	initial begin
		#0 pmu_pct_ctlpcr3_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read3 = 1'bx;
	end
	assign `SIG25 = pmu_pct_ctlpcr3_read3;

	initial begin
		#0 pmu_pct_ctlpcr3_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read1 = 1'bx;
	end
	assign `SIG33 = pmu_pct_ctlpcr3_read1;

	initial begin
		#0 pmu_pct_ctlpcr4_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read1 = 1'bx;
	end
	assign `SIG32 = pmu_pct_ctlpcr4_read1;

	initial begin
		#0 pmu_pct_ctlpcr4_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read3 = 1'bx;
	end
	assign `SIG24 = pmu_pct_ctlpcr4_read3;

	initial begin
		#0 pct_incr_pic_w16 = 1'b0;
		#40100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#22200.0 pct_incr_pic_w16 = 1'b0;
	end
	assign `SIG112 = pct_incr_pic_w16;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_82 = 1'b0;
	end
	assign `SIG88 = pdp_asi_ctlin_to_pctl_15_82;

	initial begin
		#0 pmu_pct_ctlph_incr_m3 = 1'b0;
	end
	assign `SIG91 = pmu_pct_ctlph_incr_m3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr1 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr1 = 1'bx;
	end
	assign `SIG15 = pmu_pct_ctlsleep_cntr1;

	initial begin
		#0 pmu_pct_ctlsleep_cntr0 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr0 = 1'bx;
	end
	assign `SIG14 = pmu_pct_ctlsleep_cntr0;

	initial begin
		#0 pmu_pct_ctlsleep_cntr3 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr3 = 1'bx;
	end
	assign `SIG11 = pmu_pct_ctlsleep_cntr3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr2 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr2 = 1'bx;
	end
	assign `SIG12 = pmu_pct_ctlsleep_cntr2;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_81 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_81 = 1'b1;
		#13600.0 pdp_asi_ctlin_to_pctl_15_81 = 1'b0;
	end
	assign `SIG89 = pdp_asi_ctlin_to_pctl_15_81;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m1 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m1 = 1'bx;
	end
	assign `SIG60 = pmu_pct_ctll2dm_wrap_m1;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m0 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m0 = 1'bx;
	end
	assign `SIG74 = pmu_pct_ctll2dm_wrap_m0;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m1 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m1 = 1'bx;
	end
	assign `SIG47 = pmu_pct_ctlph_br_may_trap_m1;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m0 = 1'b0;
		#40100.0 pmu_pct_ctlph_br_may_trap_m0 = 1'bx;
		#23700.0 pmu_pct_ctlph_br_may_trap_m0 = 1'b0;
	end
	assign `SIG65 = pmu_pct_ctlph_br_may_trap_m0;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m5 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m5 = 1'bx;
	end
	assign `SIG59 = pmu_pct_ctll2dm_wrap_m5;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m4 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m4 = 1'bx;
	end
	assign `SIG75 = pmu_pct_ctll2dm_wrap_m4;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m5 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
	end
	assign `SIG53 = pmu_pct_ctlph_br_may_trap_m5;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m6 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m6 = 1'bx;
	end
	assign `SIG62 = pmu_pct_ctll2dm_wrap_m6;

	initial begin
		#0 pmu_pct_ctlph_incr_m2 = 1'b0;
	end
	assign `SIG99 = pmu_pct_ctlph_incr_m2;

	initial begin
		#0 pmu_pct_ctltrap_hold0 = 1'b0;
		#40000.0 pmu_pct_ctltrap_hold0 = 1'bx;
		#23800.0 pmu_pct_ctltrap_hold0 = 1'b0;
	end
	assign `SIG81 = pmu_pct_ctltrap_hold0;

	initial begin
		#0 pmu_pct_ctlph_incr_m0 = 1'b0;
	end
	assign `SIG105 = pmu_pct_ctlph_incr_m0;

	initial begin
		#0 pmu_pct_ctlph_incr_m1 = 1'b0;
	end
	assign `SIG101 = pmu_pct_ctlph_incr_m1;

	initial begin
		#0 pmu_pct_ctlph_incr_m6 = 1'b0;
	end
	assign `SIG95 = pmu_pct_ctlph_incr_m6;

	initial begin
		#0 pmu_pct_ctlph_incr_m7 = 1'b0;
	end
	assign `SIG93 = pmu_pct_ctlph_incr_m7;

	initial begin
		#0 pmu_pct_ctlph_incr_m4 = 1'b0;
	end
	assign `SIG103 = pmu_pct_ctlph_incr_m4;

	initial begin
		#0 pmu_pct_ctlph_incr_m5 = 1'b0;
	end
	assign `SIG96 = pmu_pct_ctlph_incr_m5;

	initial begin
		#0 pmu_pct_ctlpcr7_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read2 = 1'bx;
	end
	assign `SIG36 = pmu_pct_ctlpcr7_read2;

	initial begin
		#0 pmu_pct_ctlpcr7_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read3 = 1'bx;
	end
	assign `SIG20 = pmu_pct_ctlpcr7_read3;

	initial begin
		#0 pmu_pct_ctlpcr7_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read1 = 1'bx;
	end
	assign `SIG29 = pmu_pct_ctlpcr7_read1;

	initial begin
		#0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG1 = pmu_pct_ctlov_busy_clkgenc_0l1en;

	initial begin
		#0 n6158 = 1'b0;
		#63600.0 n6158 = 1'b1;
	end
	assign `SIG16 = n6158;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
		#100.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#23500.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
	end
	assign `SIG72 = pdp_asi_ctlin_to_pctl_4_02;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b1;
		#23600.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
	end
	assign `SIG73 = pdp_asi_ctlin_to_pctl_4_01;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b1;
	end
	assign `SIG55 = pdp_asi_ctlin_to_pctl_4_00;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b1;
	end
	assign `SIG70 = pdp_asi_ctlin_to_pctl_4_04;

	initial begin
		#0 pmu_pct_ctlpcr0_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read1 = 1'bx;
	end
	assign `SIG35 = pmu_pct_ctlpcr0_read1;

	initial begin
		#0 pmu_pct_ctlpcr0_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read3 = 1'bx;
	end
	assign `SIG27 = pmu_pct_ctlpcr0_read3;

	initial begin
		#0 pmu_pct_ctlpcr0_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read2 = 1'bx;
	end
	assign `SIG42 = pmu_pct_ctlpcr0_read2;

	initial begin
		#0 n6096 = 1'b0;
		#63600.0 n6096 = 1'b1;
	end
	assign `SIG54 = n6096;

	initial begin
		#0 pct_incr_pic_w11 = 1'b0;
		#40000.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#22400.0 pct_incr_pic_w11 = 1'b0;
	end
	assign `SIG108 = pct_incr_pic_w11;

	initial begin
		#0 n6627dummy = 1'b0;
		#40000.0 n6627dummy = 1'bx;
	end
	assign `SIG41 = n6627dummy;

	initial begin
		#0 n6144 = 1'b0;
		#40000.0 n6144 = 1'bx;
	end
	assign `SIG10 = n6144;

	initial begin
		#0 pmu_pct_ctlpl_incr_m7 = 1'b0;
	end
	assign `SIG92 = pmu_pct_ctlpl_incr_m7;

	initial begin
		#0 pmu_pct_ctlpl_incr_m4 = 1'b0;
	end
	assign `SIG102 = pmu_pct_ctlpl_incr_m4;

	initial begin
		#0 pmu_pct_ctlpl_incr_m5 = 1'b0;
	end
	assign `SIG94 = pmu_pct_ctlpl_incr_m5;

	initial begin
		#0 pmu_pct_ctlpl_incr_m2 = 1'b0;
	end
	assign `SIG98 = pmu_pct_ctlpl_incr_m2;

	initial begin
		#0 pmu_pct_ctlpl_incr_m3 = 1'b0;
	end
	assign `SIG90 = pmu_pct_ctlpl_incr_m3;

	initial begin
		#0 pmu_pct_ctlpl_incr_m0 = 1'b0;
	end
	assign `SIG104 = pmu_pct_ctlpl_incr_m0;

	initial begin
		#0 pmu_pct_ctlpl_incr_m1 = 1'b0;
	end
	assign `SIG100 = pmu_pct_ctlpl_incr_m1;

	initial begin
		#0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#40100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#22300.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
	end
	assign `SIG107 = pmu_pct_ctlpic_st_dout0;

	initial begin
		#0 pmu_pct_ctltg1_e11 = 1'b0;
		#53700.0 pmu_pct_ctltg1_e11 = 1'b1;
	end
	assign `SIG115 = pmu_pct_ctltg1_e11;

	initial begin
		#0 pmu_pct_ctltg1_e12 = 1'b0;
		#40000.0 pmu_pct_ctltg1_e12 = 1'b1;
	end
	assign `SIG84 = pmu_pct_ctltg1_e12;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_84 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_84 = 1'b1;
	end
	assign `SIG66 = pdp_asi_ctlin_to_pctl_15_84;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_85 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_85 = 1'b1;
	end
	assign `SIG67 = pdp_asi_ctlin_to_pctl_15_85;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_86 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_15_86 = 1'b1;
	end
	assign `SIG19 = pdp_asi_ctlin_to_pctl_15_86;

	initial begin
		#0 pmu_pct_ctlpcr2_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read3 = 1'bx;
	end
	assign `SIG22 = pmu_pct_ctlpcr2_read3;

	initial begin
		#0 pmu_pct_ctlpcr2_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read2 = 1'bx;
	end
	assign `SIG40 = pmu_pct_ctlpcr2_read2;

	initial begin
		#0 pmu_pct_ctlpcr2_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read1 = 1'bx;
	end
	assign `SIG34 = pmu_pct_ctlpcr2_read1;

	initial begin
		#0 pmu_pdp_dppicpcr_muxpsel0 = 1'b0;
	end
	assign `SIG17 = pmu_pdp_dppicpcr_muxpsel0;

	initial begin
		#0 pmu_pct_ctlp_wr = 1'b0;
	end
	assign `SIG18 = pmu_pct_ctlp_wr;

	initial begin
		#0 pmu_pct_ctlpc_rd = 1'b0;
	end
	assign `SIG13 = pmu_pct_ctlpc_rd;

	initial begin
		 #70000.0 $finish;
	end

`endif
