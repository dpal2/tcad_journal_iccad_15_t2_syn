`ifdef SigSeT_1_32
	`define SIG0 pmu_cov_bench.pmu_.pmu_pct_ctl.pc_wr
	`define SIG1 pmu_cov_bench.pmu_.pmu_pct_ctl.ov_busy_clkgen.c_0.l1en
	`define SIG2 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [6]
	`define SIG3 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [7]
	`define SIG4 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [2]
	`define SIG5 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [4]
	`define SIG6 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [5]
	`define SIG7 pmu_cov_bench.pmu_.pmu_pct_ctl.asi.d0_0.q[2]
	`define SIG8 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [1]
	`define SIG9 pmu_cov_bench.pmu_.pmu_pct_ctl.atid [3]
	`define SIG10 pmu_cov_bench.pmu_.pmu_pct_ctl.pwrm.d0_0.q[1]
	`define SIG11 pmu_cov_bench.pmu_.pmu_pct_ctl.sleep_cntr [3]
	`define SIG12 pmu_cov_bench.pmu_.pmu_pct_ctl.sleep_cntr [2]
	`define SIG13 pmu_cov_bench.pmu_.pmu_pct_ctl.pc_rd
	`define SIG14 pmu_cov_bench.pmu_.pmu_pct_ctl.sleep_cntr [0]
	`define SIG15 pmu_cov_bench.pmu_.pmu_pct_ctl.sleep_cntr [1]
	`define SIG16 pmu_cov_bench.pmu_.pmu_pdp_dp.asi_din.d0_0.q[63]
	`define SIG17 pmu_cov_bench.pmu_.pmu_pdp_dp.picpcr_mux.psel0
	`define SIG18 pmu_cov_bench.pmu_.pmu_pct_ctl.p_wr
	`define SIG19 pmu_cov_bench.pmu_.pdp_asi_ctlin_to_pctl_15_8[6]
	`define SIG20 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr7_read [3]
	`define SIG21 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr6_read [3]
	`define SIG22 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr2_read [3]
	`define SIG23 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr5_read [3]
	`define SIG24 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr4_read [3]
	`define SIG25 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr3_read [3]
	`define SIG26 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr1_read [3]
	`define SIG27 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr0_read [3]
	`define SIG28 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr1_read [1]
	`define SIG29 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr7_read [1]
	`define SIG30 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr6_read [1]
	`define SIG31 pmu_cov_bench.pmu_.pmu_pct_ctl.pcr5_read [1]
`endif
