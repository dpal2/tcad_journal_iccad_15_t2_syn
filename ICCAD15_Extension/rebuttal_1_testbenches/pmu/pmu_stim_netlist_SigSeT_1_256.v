`ifdef SigSeT_1_256
	initial begin
		#0 pmu_pdp_dppicl07_w24 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w24 = 1'bx;
	end
	assign `SIG240 = pmu_pdp_dppicl07_w24;

	initial begin
		#0 n686dummy = 1'b0;
		#40000.0 n686dummy = 1'bx;
	end
	assign `SIG39 = n686dummy;

	initial begin
		#0 pmu_pdp_dppich330 = 1'b0;
		#40000.0 pmu_pdp_dppich330 = 1'bx;
	end
	assign `SIG232 = pmu_pdp_dppich330;

	initial begin
		#0 n6670dummy = 1'b0;
		#53600.0 n6670dummy = 1'b1;
	end
	assign `SIG177 = n6670dummy;

	initial begin
		#0 pmu_pct_ctlpcr6_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read3 = 1'bx;
	end
	assign `SIG21 = pmu_pct_ctlpcr6_read3;

	initial begin
		#0 pmu_pct_ctlpcr6_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read2 = 1'bx;
	end
	assign `SIG37 = pmu_pct_ctlpcr6_read2;

	initial begin
		#0 pmu_pct_ctlpcr6_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read1 = 1'bx;
	end
	assign `SIG30 = pmu_pct_ctlpcr6_read1;

	initial begin
		#0 pmu_pct_ctltg0_e10 = 1'b0;
		#40000.0 pmu_pct_ctltg0_e10 = 1'b1;
		#100.0 pmu_pct_ctltg0_e10 = 1'b0;
		#23600.0 pmu_pct_ctltg0_e10 = 1'b1;
	end
	assign `SIG135 = pmu_pct_ctltg0_e10;

	initial begin
		#0 pmu_pct_ctltg0_e11 = 1'b0;
		#40000.0 pmu_pct_ctltg0_e11 = 1'b1;
		#13700.0 pmu_pct_ctltg0_e11 = 1'b0;
	end
	assign `SIG132 = pmu_pct_ctltg0_e11;

	initial begin
		#0 pmu_pct_ctltg0_e12 = 1'b0;
		#40000.0 pmu_pct_ctltg0_e12 = 1'b1;
	end
	assign `SIG85 = pmu_pct_ctltg0_e12;

	initial begin
		#0 pmu_pct_ctlhpriv6 = 1'b0;
		#63700.0 pmu_pct_ctlhpriv6 = 1'b1;
	end
	assign `SIG253 = pmu_pct_ctlhpriv6;

	initial begin
		#0 pmu_pdp_dppicl07_w211 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w211 = 1'bx;
	end
	assign `SIG250 = pmu_pdp_dppicl07_w211;

	initial begin
		#0 pmu_pdp_dppicl07_w210 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w210 = 1'bx;
	end
	assign `SIG251 = pmu_pdp_dppicl07_w210;

	initial begin
		#0 pmu_pdp_dppicl07_w213 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w213 = 1'bx;
	end
	assign `SIG139 = pmu_pdp_dppicl07_w213;

	initial begin
		#0 pmu_pdp_dppicl07_w212 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w212 = 1'bx;
	end
	assign `SIG210 = pmu_pdp_dppicl07_w212;

	initial begin
		#0 pmu_lsu_dtmiss_trap_mttt = 1'b0;
		#40100.0 pmu_lsu_dtmiss_trap_mttt = 1'bx;
		#23700.0 pmu_lsu_dtmiss_trap_mttt = 1'b0;
	end
	assign `SIG136 = pmu_lsu_dtmiss_trap_mttt;

	initial begin
		#0 pmu_pdp_dppich07_w220 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w220 = 1'bx;
	end
	assign `SIG247 = pmu_pdp_dppich07_w220;

	initial begin
		#0 pmu_pdp_dppich730 = 1'b0;
		#40000.0 pmu_pdp_dppich730 = 1'bx;
	end
	assign `SIG228 = pmu_pdp_dppich730;

	initial begin
		#0 pmu_pdp_dppich630 = 1'b0;
		#40000.0 pmu_pdp_dppich630 = 1'bx;
	end
	assign `SIG227 = pmu_pdp_dppich630;

	initial begin
		#0 pmu_pct_ctlskip_wakeup = 1'b0;
		#40000.0 pmu_pct_ctlskip_wakeup = 1'bx;
	end
	assign `SIG106 = pmu_pct_ctlskip_wakeup;

	initial begin
		#0 pmu_rngl_cdbus12 = 1'b0;
		#40000.0 pmu_rngl_cdbus12 = 1'b1;
		#23700.0 pmu_rngl_cdbus12 = 1'b0;
	end
	assign `SIG156 = pmu_rngl_cdbus12;

	initial begin
		#0 pmu_pdp_dppich430 = 1'b0;
		#40000.0 pmu_pdp_dppich430 = 1'bx;
	end
	assign `SIG236 = pmu_pdp_dppich430;

	initial begin
		#0 pmu_pct_ctlpcr6_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read18 = 1'bx;
	end
	assign `SIG211 = pmu_pct_ctlpcr6_read18;

	initial begin
		#0 pmu_pct_ctlpcr2_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read31 = 1'bx;
	end
	assign `SIG220 = pmu_pct_ctlpcr2_read31;

	initial begin
		#0 n6693dummy = 1'b0;
		#40000.0 n6693dummy = 1'b1;
		#100.0 n6693dummy = 1'b0;
		#23500.0 n6693dummy = 1'b1;
	end
	assign `SIG163 = n6693dummy;

	initial begin
		#0 pmu_pdp_dppic07_w2c0_0l1en = 1'b0;
		#40000.0 pmu_pdp_dppic07_w2c0_0l1en = 1'bx;
	end
	assign `SIG114 = pmu_pdp_dppic07_w2c0_0l1en;

	initial begin
		#0 pmu_pdp_dppich530 = 1'b0;
		#40000.0 pmu_pdp_dppich530 = 1'bx;
	end
	assign `SIG237 = pmu_pdp_dppich530;

	initial begin
		#0 pmu_rngl_cdbus62 = 1'b0;
		#53700.0 pmu_rngl_cdbus62 = 1'b1;
	end
	assign `SIG252 = pmu_rngl_cdbus62;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m3 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
	end
	assign `SIG45 = pmu_pct_ctlph_br_may_trap_m3;

	initial begin
		#0 n6674dummy = 1'b0;
		#53600.0 n6674dummy = 1'b1;
	end
	assign `SIG199 = n6674dummy;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m2 = 1'b0;
	end
	assign `SIG49 = pmu_pct_ctlph_br_may_trap_m2;

	initial begin
		#0 pmu_pct_ctlatid1 = 1'b0;
		#40000.0 pmu_pct_ctlatid1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlatid1;

	initial begin
		#0 pmu_pct_ctlatid2 = 1'b0;
		#40000.0 pmu_pct_ctlatid2 = 1'b1;
		#13700.0 pmu_pct_ctlatid2 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctlatid2;

	initial begin
		#0 pmu_pct_ctlatid3 = 1'b0;
	end
	assign `SIG9 = pmu_pct_ctlatid3;

	initial begin
		#0 pmu_pct_ctlatid4 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlatid4;

	initial begin
		#0 pmu_pct_ctlatid5 = 1'b0;
	end
	assign `SIG6 = pmu_pct_ctlatid5;

	initial begin
		#0 pmu_pct_ctlatid6 = 1'b0;
	end
	assign `SIG2 = pmu_pct_ctlatid6;

	initial begin
		#0 pmu_pct_ctlatid7 = 1'b0;
	end
	assign `SIG3 = pmu_pct_ctlatid7;

	initial begin
		#0 pmu_pdp_dppicl230 = 1'b0;
		#40000.0 pmu_pdp_dppicl230 = 1'bx;
	end
	assign `SIG234 = pmu_pdp_dppicl230;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m2 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m2 = 1'bx;
	end
	assign `SIG58 = pmu_pct_ctll2dm_wrap_m2;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m7 = 1'b0;
	end
	assign `SIG51 = pmu_pct_ctlph_br_may_trap_m7;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m6 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m6 = 1'bx;
	end
	assign `SIG57 = pmu_pct_ctlph_br_may_trap_m6;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m7 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m7 = 1'bx;
	end
	assign `SIG61 = pmu_pct_ctll2dm_wrap_m7;

	initial begin
		#0 pmu_pct_ctlpmu_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlpmu_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG86 = pmu_pct_ctlpmu_busy_clkgenc_0l1en;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m4 = 1'b0;
	end
	assign `SIG69 = pmu_pct_ctlph_br_may_trap_m4;

	initial begin
		#0 pmu_pct_ctlpcr5_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read18 = 1'bx;
	end
	assign `SIG223 = pmu_pct_ctlpcr5_read18;

	initial begin
		#0 pmu_pct_ctltrap_m1 = 1'b0;
	end
	assign `SIG46 = pmu_pct_ctltrap_m1;

	initial begin
		#0 pmu_pct_ctltrap_m0 = 1'b0;
	end
	assign `SIG64 = pmu_pct_ctltrap_m0;

	initial begin
		#0 pmu_pct_ctltrap_m3 = 1'b0;
	end
	assign `SIG44 = pmu_pct_ctltrap_m3;

	initial begin
		#0 pmu_pct_ctltrap_m2 = 1'b0;
	end
	assign `SIG48 = pmu_pct_ctltrap_m2;

	initial begin
		#0 pmu_pct_ctltrap_m5 = 1'b0;
	end
	assign `SIG52 = pmu_pct_ctltrap_m5;

	initial begin
		#0 pmu_pct_ctltrap_m4 = 1'b0;
	end
	assign `SIG68 = pmu_pct_ctltrap_m4;

	initial begin
		#0 pmu_pct_ctltrap_m7 = 1'b0;
	end
	assign `SIG50 = pmu_pct_ctltrap_m7;

	initial begin
		#0 pmu_pct_ctltrap_m6 = 1'b0;
	end
	assign `SIG56 = pmu_pct_ctltrap_m6;

	initial begin
		#0 n6697dummy = 1'b0;
		#53600.0 n6697dummy = 1'b1;
	end
	assign `SIG141 = n6697dummy;

	initial begin
		#0 pmu_pct_ctlpl_incr_m6 = 1'b0;
	end
	assign `SIG97 = pmu_pct_ctlpl_incr_m6;

	initial begin
		#0 n5843 = 1'b0;
	end
	assign `SIG71 = n5843;

	initial begin
		#0 n6664dummy = 1'b0;
		#40000.0 n6664dummy = 1'b1;
		#23600.0 n6664dummy = 1'b0;
	end
	assign `SIG197 = n6664dummy;

	initial begin
		#0 n6691dummy = 1'b0;
		#40000.0 n6691dummy = 1'b1;
		#100.0 n6691dummy = 1'b0;
		#23500.0 n6691dummy = 1'b1;
	end
	assign `SIG191 = n6691dummy;

	initial begin
		#0 pmu_pct_ctltrap_hold4 = 1'b0;
	end
	assign `SIG80 = pmu_pct_ctltrap_hold4;

	initial begin
		#0 pmu_pct_ctltrap_hold5 = 1'b0;
		#40000.0 pmu_pct_ctltrap_hold5 = 1'bx;
		#23800.0 pmu_pct_ctltrap_hold5 = 1'b0;
	end
	assign `SIG83 = pmu_pct_ctltrap_hold5;

	initial begin
		#0 pmu_pct_ctltrap_hold6 = 1'b0;
		#53800.0 pmu_pct_ctltrap_hold6 = 1'bx;
	end
	assign `SIG77 = pmu_pct_ctltrap_hold6;

	initial begin
		#0 pmu_pct_ctltrap_hold7 = 1'b0;
	end
	assign `SIG79 = pmu_pct_ctltrap_hold7;

	initial begin
		#0 n6614 = 1'b0;
		#40100.0 n6614 = 1'b1;
		#23600.0 n6614 = 1'b0;
	end
	assign `SIG7 = n6614;

	initial begin
		#0 pmu_pct_ctltrap_hold1 = 1'b0;
		#63800.0 pmu_pct_ctltrap_hold1 = 1'bx;
	end
	assign `SIG78 = pmu_pct_ctltrap_hold1;

	initial begin
		#0 pmu_pct_ctltrap_hold2 = 1'b0;
	end
	assign `SIG82 = pmu_pct_ctltrap_hold2;

	initial begin
		#0 pmu_pct_ctltrap_hold3 = 1'b0;
	end
	assign `SIG76 = pmu_pct_ctltrap_hold3;

	initial begin
		#0 pdp_asi_din_to_pctl27 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl27 = 1'b1;
		#13600.0 pdp_asi_din_to_pctl27 = 1'b0;
	end
	assign `SIG171 = pdp_asi_din_to_pctl27;

	initial begin
		#0 pmu_rngl_cdbus0 = 1'b0;
		#53700.0 pmu_rngl_cdbus0 = 1'b1;
	end
	assign `SIG244 = pmu_rngl_cdbus0;

	initial begin
		#0 n6662dummy = 1'b0;
	end
	assign `SIG153 = n6662dummy;

	initial begin
		#0 n6689dummy = 1'b0;
		#40000.0 n6689dummy = 1'b1;
		#13600.0 n6689dummy = 1'b0;
	end
	assign `SIG195 = n6689dummy;

	initial begin
		#0 pmu_rngl_cdbus25 = 1'b0;
		#40000.0 pmu_rngl_cdbus25 = 1'b1;
	end
	assign `SIG152 = pmu_rngl_cdbus25;

	initial begin
		#0 pmu_pdp_dppich030 = 1'b0;
		#40000.0 pmu_pdp_dppich030 = 1'bx;
	end
	assign `SIG224 = pmu_pdp_dppich030;

	initial begin
		#0 pmu_pct_ctlpcr1_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read1 = 1'bx;
	end
	assign `SIG28 = pmu_pct_ctlpcr1_read1;

	initial begin
		#0 pmu_pct_ctlpcr1_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read3 = 1'bx;
	end
	assign `SIG26 = pmu_pct_ctlpcr1_read3;

	initial begin
		#0 pdp_asi_din_to_pctl4 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl4 = 1'b1;
	end
	assign `SIG193 = pdp_asi_din_to_pctl4;

	initial begin
		#0 pmu_pct_ctlpc_wr = 1'b0;
	end
	assign `SIG0 = pmu_pct_ctlpc_wr;

	initial begin
		#0 n6703dummy = 1'b0;
		#40000.0 n6703dummy = 1'b1;
		#13600.0 n6703dummy = 1'b0;
	end
	assign `SIG245 = n6703dummy;

	initial begin
		#0 pmu_pct_ctlpcr2_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read18 = 1'bx;
	end
	assign `SIG219 = pmu_pct_ctlpcr2_read18;

	initial begin
		#0 n6687dummy = 1'b0;
		#40000.0 n6687dummy = 1'b1;
		#23600.0 n6687dummy = 1'b0;
	end
	assign `SIG189 = n6687dummy;

	initial begin
		#0 n5833 = 1'b0;
		#40000.0 n5833 = 1'b1;
	end
	assign `SIG169 = n5833;

	initial begin
		#0 n5832 = 1'b0;
	end
	assign `SIG181 = n5832;

	initial begin
		#0 n5837 = 1'b0;
	end
	assign `SIG165 = n5837;

	initial begin
		#0 n6677dummy = 1'b0;
	end
	assign `SIG161 = n6677dummy;

	initial begin
		#0 pct_incr_pic_w14 = 1'b0;
		#40100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
	end
	assign `SIG109 = pct_incr_pic_w14;

	initial begin
		#0 pmu_pct_ctlpcr5_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read1 = 1'bx;
	end
	assign `SIG31 = pmu_pct_ctlpcr5_read1;

	initial begin
		#0 pmu_pct_ctlpcr5_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read2 = 1'bx;
	end
	assign `SIG38 = pmu_pct_ctlpcr5_read2;

	initial begin
		#0 pmu_pct_ctlpcr5_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read3 = 1'bx;
	end
	assign `SIG23 = pmu_pct_ctlpcr5_read3;

	initial begin
		#0 pmu_pct_ctlpl_brm6 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm6 = 1'bx;
	end
	assign `SIG126 = pmu_pct_ctlpl_brm6;

	initial begin
		#0 pmu_pct_ctlpl_brm7 = 1'b0;
	end
	assign `SIG118 = pmu_pct_ctlpl_brm7;

	initial begin
		#0 pmu_pct_ctlpl_brm4 = 1'b0;
	end
	assign `SIG131 = pmu_pct_ctlpl_brm4;

	initial begin
		#0 pmu_pct_ctlpl_brm5 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm5 = 1'bx;
		#13800.0 pmu_pct_ctlpl_brm5 = 1'b0;
	end
	assign `SIG127 = pmu_pct_ctlpl_brm5;

	initial begin
		#0 pmu_pct_ctlpl_brm2 = 1'b0;
	end
	assign `SIG123 = pmu_pct_ctlpl_brm2;

	initial begin
		#0 pmu_pct_ctlpl_brm3 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm3 = 1'bx;
		#13800.0 pmu_pct_ctlpl_brm3 = 1'b0;
	end
	assign `SIG117 = pmu_pct_ctlpl_brm3;

	initial begin
		#0 pmu_pct_ctlpl_brm0 = 1'b0;
		#40100.0 pmu_pct_ctlpl_brm0 = 1'bx;
		#23700.0 pmu_pct_ctlpl_brm0 = 1'b0;
	end
	assign `SIG128 = pmu_pct_ctlpl_brm0;

	initial begin
		#0 pmu_pct_ctlpl_brm1 = 1'b0;
		#40000.0 pmu_pct_ctlpl_brm1 = 1'bx;
	end
	assign `SIG120 = pmu_pct_ctlpl_brm1;

	initial begin
		#0 pmu_pct_ctlpcr5_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read31 = 1'bx;
	end
	assign `SIG222 = pmu_pct_ctlpcr5_read31;

	initial begin
		#0 n6685dummy = 1'b0;
		#63600.0 n6685dummy = 1'b1;
	end
	assign `SIG187 = n6685dummy;

	initial begin
		#0 n6666dummy = 1'b0;
		#40000.0 n6666dummy = 1'b1;
	end
	assign `SIG173 = n6666dummy;

	initial begin
		#0 pmu_pct_ctlhpriv7 = 1'b0;
	end
	assign `SIG255 = pmu_pct_ctlhpriv7;

	initial begin
		#0 pmu_pct_ctlpcr4_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read18 = 1'bx;
	end
	assign `SIG212 = pmu_pct_ctlpcr4_read18;

	initial begin
		#0 pct_incr_pic_w15 = 1'b0;
		#40100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#22100.0 pct_incr_pic_w15 = 1'b0;
	end
	assign `SIG111 = pct_incr_pic_w15;

	initial begin
		#0 pmu_pct_ctlpcr0_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read18 = 1'bx;
	end
	assign `SIG221 = pmu_pct_ctlpcr0_read18;

	initial begin
		#0 pmu_rngl_cdbus28 = 1'b0;
		#40000.0 pmu_rngl_cdbus28 = 1'b1;
	end
	assign `SIG168 = pmu_rngl_cdbus28;

	initial begin
		#0 pmu_rngl_cdbus9 = 1'b0;
		#40000.0 pmu_rngl_cdbus9 = 1'b1;
		#23700.0 pmu_rngl_cdbus9 = 1'b0;
	end
	assign `SIG186 = pmu_rngl_cdbus9;

	initial begin
		#0 pmu_rngl_cdbus8 = 1'b0;
		#63700.0 pmu_rngl_cdbus8 = 1'b1;
	end
	assign `SIG188 = pmu_rngl_cdbus8;

	initial begin
		#0 pmu_rngl_cdbus7 = 1'b0;
		#53700.0 pmu_rngl_cdbus7 = 1'b1;
	end
	assign `SIG194 = pmu_rngl_cdbus7;

	initial begin
		#0 pmu_rngl_cdbus6 = 1'b0;
		#40100.0 pmu_rngl_cdbus6 = 1'b1;
		#23600.0 pmu_rngl_cdbus6 = 1'b0;
	end
	assign `SIG190 = pmu_rngl_cdbus6;

	initial begin
		#0 pmu_rngl_cdbus5 = 1'b0;
		#40100.0 pmu_rngl_cdbus5 = 1'b1;
		#23600.0 pmu_rngl_cdbus5 = 1'b0;
	end
	assign `SIG162 = pmu_rngl_cdbus5;

	initial begin
		#0 pmu_rngl_cdbus29 = 1'b0;
	end
	assign `SIG166 = pmu_rngl_cdbus29;

	initial begin
		#0 pmu_rngl_cdbus3 = 1'b0;
		#40000.0 pmu_rngl_cdbus3 = 1'b1;
		#13700.0 pmu_rngl_cdbus3 = 1'b0;
	end
	assign `SIG147 = pmu_rngl_cdbus3;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m3 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m3 = 1'bx;
	end
	assign `SIG63 = pmu_pct_ctll2dm_wrap_m3;

	initial begin
		#0 pmu_rngl_cdbus1 = 1'b0;
		#40000.0 pmu_rngl_cdbus1 = 1'b1;
		#100.0 pmu_rngl_cdbus1 = 1'b0;
		#23600.0 pmu_rngl_cdbus1 = 1'b1;
	end
	assign `SIG205 = pmu_rngl_cdbus1;

	initial begin
		#0 pmu_pdp_dppicl07_w229 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w229 = 1'bx;
	end
	assign `SIG242 = pmu_pdp_dppicl07_w229;

	initial begin
		#0 pmu_pdp_dppich07_w230 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w230 = 1'bx;
	end
	assign `SIG144 = pmu_pdp_dppich07_w230;

	initial begin
		#0 pct_incr_pic_w12 = 1'b0;
		#40000.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#22500.0 pct_incr_pic_w12 = 1'b0;
	end
	assign `SIG110 = pct_incr_pic_w12;

	initial begin
		#0 n6668dummy = 1'b0;
		#40000.0 n6668dummy = 1'b1;
		#100.0 n6668dummy = 1'b0;
		#23500.0 n6668dummy = 1'b1;
	end
	assign `SIG175 = n6668dummy;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
		#63600.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b1;
	end
	assign `SIG87 = pdp_asi_ctlin_to_pctl_15_80;

	initial begin
		#0 pmu_pct_ctlph_brm2 = 1'b0;
	end
	assign `SIG122 = pmu_pct_ctlph_brm2;

	initial begin
		#0 pmu_pct_ctlph_brm3 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm3 = 1'bx;
		#13800.0 pmu_pct_ctlph_brm3 = 1'b0;
	end
	assign `SIG116 = pmu_pct_ctlph_brm3;

	initial begin
		#0 pmu_pct_ctlph_brm0 = 1'b0;
		#40100.0 pmu_pct_ctlph_brm0 = 1'bx;
		#23700.0 pmu_pct_ctlph_brm0 = 1'b0;
	end
	assign `SIG129 = pmu_pct_ctlph_brm0;

	initial begin
		#0 pmu_pct_ctlph_brm1 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm1 = 1'bx;
	end
	assign `SIG121 = pmu_pct_ctlph_brm1;

	initial begin
		#0 pmu_pct_ctlph_brm6 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm6 = 1'bx;
	end
	assign `SIG124 = pmu_pct_ctlph_brm6;

	initial begin
		#0 pmu_pct_ctlph_brm7 = 1'b0;
	end
	assign `SIG119 = pmu_pct_ctlph_brm7;

	initial begin
		#0 pmu_pct_ctlph_brm4 = 1'b0;
	end
	assign `SIG130 = pmu_pct_ctlph_brm4;

	initial begin
		#0 pmu_pct_ctlph_brm5 = 1'b0;
		#40000.0 pmu_pct_ctlph_brm5 = 1'bx;
		#13800.0 pmu_pct_ctlph_brm5 = 1'b0;
	end
	assign `SIG125 = pmu_pct_ctlph_brm5;

	initial begin
		#0 pmu_pct_ctlpcr7_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read18 = 1'bx;
	end
	assign `SIG218 = pmu_pct_ctlpcr7_read18;

	initial begin
		#0 pmu_rngl_cdbus10 = 1'b0;
		#40100.0 pmu_rngl_cdbus10 = 1'b1;
		#23600.0 pmu_rngl_cdbus10 = 1'b0;
	end
	assign `SIG158 = pmu_rngl_cdbus10;

	initial begin
		#0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#40100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#22300.0 pmu_pct_ctlpic_std_w17 = 1'b0;
	end
	assign `SIG140 = pmu_pct_ctlpic_std_w17;

	initial begin
		#0 pmu_rngl_cdbus13 = 1'b0;
		#40000.0 pmu_rngl_cdbus13 = 1'b1;
	end
	assign `SIG160 = pmu_rngl_cdbus13;

	initial begin
		#0 pct_incr_pic_w13 = 1'b0;
		#40000.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
	end
	assign `SIG113 = pct_incr_pic_w13;

	initial begin
		#0 pmu_pct_ctlpcr3_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read18 = 1'bx;
	end
	assign `SIG207 = pmu_pct_ctlpcr3_read18;

	initial begin
		#0 pdp_asi_din_to_pctl31 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl31 = 1'b1;
		#23600.0 pdp_asi_din_to_pctl31 = 1'b0;
	end
	assign `SIG146 = pdp_asi_din_to_pctl31;

	initial begin
		#0 n6672dummy = 1'b0;
		#53600.0 n6672dummy = 1'b1;
	end
	assign `SIG179 = n6672dummy;

	initial begin
		#0 pmu_pct_ctlpcr3_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read2 = 1'bx;
	end
	assign `SIG43 = pmu_pct_ctlpcr3_read2;

	initial begin
		#0 pmu_pct_ctlpcr3_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read3 = 1'bx;
	end
	assign `SIG25 = pmu_pct_ctlpcr3_read3;

	initial begin
		#0 pmu_rngl_cdbus11 = 1'b0;
		#40000.0 pmu_rngl_cdbus11 = 1'b1;
		#13700.0 pmu_rngl_cdbus11 = 1'b0;
	end
	assign `SIG202 = pmu_rngl_cdbus11;

	initial begin
		#0 pmu_pct_ctlpcr3_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read1 = 1'bx;
	end
	assign `SIG33 = pmu_pct_ctlpcr3_read1;

	initial begin
		#0 pmu_rngl_cdbus17 = 1'b0;
		#40000.0 pmu_rngl_cdbus17 = 1'b1;
	end
	assign `SIG200 = pmu_rngl_cdbus17;

	initial begin
		#0 pmu_rngl_cdbus16 = 1'b0;
	end
	assign `SIG180 = pmu_rngl_cdbus16;

	initial begin
		#0 pmu_rngl_cdbus15 = 1'b0;
		#40000.0 pmu_rngl_cdbus15 = 1'b1;
	end
	assign `SIG182 = pmu_rngl_cdbus15;

	initial begin
		#0 pmu_rngl_cdbus14 = 1'b0;
		#40100.0 pmu_rngl_cdbus14 = 1'b1;
		#23600.0 pmu_rngl_cdbus14 = 1'b0;
	end
	assign `SIG184 = pmu_rngl_cdbus14;

	initial begin
		#0 pmu_pct_ctlpcr4_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read1 = 1'bx;
	end
	assign `SIG32 = pmu_pct_ctlpcr4_read1;

	initial begin
		#0 pmu_pct_ctlpcr4_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read3 = 1'bx;
	end
	assign `SIG24 = pmu_pct_ctlpcr4_read3;

	initial begin
		#0 pmu_rngl_cdbus18 = 1'b0;
		#40000.0 pmu_rngl_cdbus18 = 1'b1;
		#23700.0 pmu_rngl_cdbus18 = 1'b0;
	end
	assign `SIG154 = pmu_rngl_cdbus18;

	initial begin
		#0 pdp_asi_din_to_pctl1 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl1 = 1'b1;
		#100.0 pdp_asi_din_to_pctl1 = 1'b0;
		#23500.0 pdp_asi_din_to_pctl1 = 1'b1;
	end
	assign `SIG204 = pdp_asi_din_to_pctl1;

	initial begin
		#0 pct_incr_pic_w16 = 1'b0;
		#40100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#22200.0 pct_incr_pic_w16 = 1'b0;
	end
	assign `SIG112 = pct_incr_pic_w16;

	initial begin
		#0 pmu_pdp_dppich07_w221 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w221 = 1'bx;
	end
	assign `SIG248 = pmu_pdp_dppich07_w221;

	initial begin
		#0 pmu_pdp_dppich07_w222 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w222 = 1'bx;
	end
	assign `SIG209 = pmu_pdp_dppich07_w222;

	initial begin
		#0 pmu_pdp_dppich07_w223 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w223 = 1'bx;
	end
	assign `SIG138 = pmu_pdp_dppich07_w223;

	initial begin
		#0 n6377dummy = 1'b0;
		#40000.0 n6377dummy = 1'bx;
	end
	assign `SIG254 = n6377dummy;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_82 = 1'b0;
	end
	assign `SIG88 = pdp_asi_ctlin_to_pctl_15_82;

	initial begin
		#0 n6660dummy = 1'b0;
		#40000.0 n6660dummy = 1'b1;
	end
	assign `SIG151 = n6660dummy;

	initial begin
		#0 pmu_pct_ctlph_incr_m3 = 1'b0;
	end
	assign `SIG91 = pmu_pct_ctlph_incr_m3;

	initial begin
		#0 pmu_pdp_dppicl530 = 1'b0;
		#40000.0 pmu_pdp_dppicl530 = 1'bx;
	end
	assign `SIG226 = pmu_pdp_dppicl530;

	initial begin
		#0 pmu_pct_ctlpcr1_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read31 = 1'bx;
	end
	assign `SIG215 = pmu_pct_ctlpcr1_read31;

	initial begin
		#0 pmu_pct_ctlsleep_cntr1 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr1 = 1'bx;
	end
	assign `SIG15 = pmu_pct_ctlsleep_cntr1;

	initial begin
		#0 pmu_pct_ctlsleep_cntr0 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr0 = 1'bx;
	end
	assign `SIG14 = pmu_pct_ctlsleep_cntr0;

	initial begin
		#0 pmu_pct_ctlsleep_cntr3 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr3 = 1'bx;
	end
	assign `SIG11 = pmu_pct_ctlsleep_cntr3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr2 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr2 = 1'bx;
	end
	assign `SIG12 = pmu_pct_ctlsleep_cntr2;

	initial begin
		#0 pmu_pct_ctlpcr1_read18 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read18 = 1'bx;
	end
	assign `SIG206 = pmu_pct_ctlpcr1_read18;

	initial begin
		#0 pmu_rngl_cdbus19 = 1'b0;
		#40000.0 pmu_rngl_cdbus19 = 1'b1;
		#13700.0 pmu_rngl_cdbus19 = 1'b0;
	end
	assign `SIG198 = pmu_rngl_cdbus19;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_81 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_81 = 1'b1;
		#13600.0 pdp_asi_ctlin_to_pctl_15_81 = 1'b0;
	end
	assign `SIG89 = pdp_asi_ctlin_to_pctl_15_81;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m1 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m1 = 1'bx;
	end
	assign `SIG60 = pmu_pct_ctll2dm_wrap_m1;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m0 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m0 = 1'bx;
	end
	assign `SIG74 = pmu_pct_ctll2dm_wrap_m0;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m1 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m1 = 1'bx;
	end
	assign `SIG47 = pmu_pct_ctlph_br_may_trap_m1;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m0 = 1'b0;
		#40100.0 pmu_pct_ctlph_br_may_trap_m0 = 1'bx;
		#23700.0 pmu_pct_ctlph_br_may_trap_m0 = 1'b0;
	end
	assign `SIG65 = pmu_pct_ctlph_br_may_trap_m0;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m5 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m5 = 1'bx;
	end
	assign `SIG59 = pmu_pct_ctll2dm_wrap_m5;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m4 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m4 = 1'bx;
	end
	assign `SIG75 = pmu_pct_ctll2dm_wrap_m4;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m5 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
	end
	assign `SIG53 = pmu_pct_ctlph_br_may_trap_m5;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m6 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m6 = 1'bx;
	end
	assign `SIG62 = pmu_pct_ctll2dm_wrap_m6;

	initial begin
		#0 pmu_pct_ctlpcr4_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read31 = 1'bx;
	end
	assign `SIG213 = pmu_pct_ctlpcr4_read31;

	initial begin
		#0 pmu_pdp_dppich230 = 1'b0;
		#40000.0 pmu_pdp_dppich230 = 1'bx;
	end
	assign `SIG229 = pmu_pdp_dppich230;

	initial begin
		#0 pmu_rngl_cdbus26 = 1'b0;
	end
	assign `SIG150 = pmu_rngl_cdbus26;

	initial begin
		#0 pmu_rngl_cdbus27 = 1'b0;
		#40000.0 pmu_rngl_cdbus27 = 1'b1;
		#13700.0 pmu_rngl_cdbus27 = 1'b0;
	end
	assign `SIG170 = pmu_rngl_cdbus27;

	initial begin
		#0 pmu_rngl_cdbus24 = 1'b0;
		#63700.0 pmu_rngl_cdbus24 = 1'b1;
	end
	assign `SIG196 = pmu_rngl_cdbus24;

	initial begin
		#0 pmu_pdp_dppicl07_w230 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w230 = 1'bx;
	end
	assign `SIG143 = pmu_pdp_dppicl07_w230;

	initial begin
		#0 pmu_rngl_cdbus22 = 1'b0;
		#40100.0 pmu_rngl_cdbus22 = 1'b1;
		#23600.0 pmu_rngl_cdbus22 = 1'b0;
	end
	assign `SIG174 = pmu_rngl_cdbus22;

	initial begin
		#0 pmu_rngl_cdbus23 = 1'b0;
	end
	assign `SIG172 = pmu_rngl_cdbus23;

	initial begin
		#0 pmu_rngl_cdbus20 = 1'b0;
		#40000.0 pmu_rngl_cdbus20 = 1'b1;
		#13700.0 pmu_rngl_cdbus20 = 1'b0;
	end
	assign `SIG178 = pmu_rngl_cdbus20;

	initial begin
		#0 pmu_rngl_cdbus21 = 1'b0;
		#40000.0 pmu_rngl_cdbus21 = 1'b1;
		#13700.0 pmu_rngl_cdbus21 = 1'b0;
	end
	assign `SIG176 = pmu_rngl_cdbus21;

	initial begin
		#0 pmu_pct_ctlph_incr_m2 = 1'b0;
	end
	assign `SIG99 = pmu_pct_ctlph_incr_m2;

	initial begin
		#0 pmu_pct_ctltrap_hold0 = 1'b0;
		#40000.0 pmu_pct_ctltrap_hold0 = 1'bx;
		#23800.0 pmu_pct_ctltrap_hold0 = 1'b0;
	end
	assign `SIG81 = pmu_pct_ctltrap_hold0;

	initial begin
		#0 pmu_pct_ctlph_incr_m0 = 1'b0;
	end
	assign `SIG105 = pmu_pct_ctlph_incr_m0;

	initial begin
		#0 pmu_pct_ctlph_incr_m1 = 1'b0;
	end
	assign `SIG101 = pmu_pct_ctlph_incr_m1;

	initial begin
		#0 pmu_pct_ctlph_incr_m6 = 1'b0;
	end
	assign `SIG95 = pmu_pct_ctlph_incr_m6;

	initial begin
		#0 pmu_pct_ctlph_incr_m7 = 1'b0;
	end
	assign `SIG93 = pmu_pct_ctlph_incr_m7;

	initial begin
		#0 pmu_pct_ctlph_incr_m4 = 1'b0;
	end
	assign `SIG103 = pmu_pct_ctlph_incr_m4;

	initial begin
		#0 pmu_pct_ctlph_incr_m5 = 1'b0;
	end
	assign `SIG96 = pmu_pct_ctlph_incr_m5;

	initial begin
		#0 pdp_asi_din_to_pctl17 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl17 = 1'b1;
	end
	assign `SIG201 = pdp_asi_din_to_pctl17;

	initial begin
		#0 pdp_asi_din_to_pctl14 = 1'b0;
		#40100.0 pdp_asi_din_to_pctl14 = 1'b1;
		#23500.0 pdp_asi_din_to_pctl14 = 1'b0;
	end
	assign `SIG185 = pdp_asi_din_to_pctl14;

	initial begin
		#0 pdp_asi_din_to_pctl15 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl15 = 1'b1;
	end
	assign `SIG183 = pdp_asi_din_to_pctl15;

	initial begin
		#0 pmu_pct_ctlpcr7_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read2 = 1'bx;
	end
	assign `SIG36 = pmu_pct_ctlpcr7_read2;

	initial begin
		#0 pmu_pct_ctlpcr7_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read3 = 1'bx;
	end
	assign `SIG20 = pmu_pct_ctlpcr7_read3;

	initial begin
		#0 pmu_pct_ctlpcr7_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read1 = 1'bx;
	end
	assign `SIG29 = pmu_pct_ctlpcr7_read1;

	initial begin
		#0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG1 = pmu_pct_ctlov_busy_clkgenc_0l1en;

	initial begin
		#0 pmu_pct_ctlpcr7_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read31 = 1'bx;
	end
	assign `SIG214 = pmu_pct_ctlpcr7_read31;

	initial begin
		#0 pmu_pdp_dppicl430 = 1'b0;
		#40000.0 pmu_pdp_dppicl430 = 1'bx;
	end
	assign `SIG230 = pmu_pdp_dppicl430;

	initial begin
		#0 pdp_asi_din_to_pctl18 = 1'b0;
		#40000.0 pdp_asi_din_to_pctl18 = 1'b1;
		#23600.0 pdp_asi_din_to_pctl18 = 1'b0;
	end
	assign `SIG155 = pdp_asi_din_to_pctl18;

	initial begin
		#0 pmu_pdp_dppicl130 = 1'b0;
		#40000.0 pmu_pdp_dppicl130 = 1'bx;
	end
	assign `SIG239 = pmu_pdp_dppicl130;

	initial begin
		#0 n6679dummy = 1'b0;
		#63600.0 n6679dummy = 1'b1;
	end
	assign `SIG157 = n6679dummy;

	initial begin
		#0 n6158 = 1'b0;
		#63600.0 n6158 = 1'b1;
	end
	assign `SIG16 = n6158;

	initial begin
		#0 pmu_pdp_dppicl730 = 1'b0;
		#40000.0 pmu_pdp_dppicl730 = 1'bx;
	end
	assign `SIG225 = pmu_pdp_dppicl730;

	initial begin
		#0 pmu_pct_ctlasi_ctl_ndata = 1'b0;
		#40000.0 pmu_pct_ctlasi_ctl_ndata = 1'b1;
		#100.0 pmu_pct_ctlasi_ctl_ndata = 1'b0;
		#23600.0 pmu_pct_ctlasi_ctl_ndata = 1'b1;
	end
	assign `SIG134 = pmu_pct_ctlasi_ctl_ndata;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
		#100.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#23500.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
	end
	assign `SIG72 = pdp_asi_ctlin_to_pctl_4_02;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b1;
		#23600.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
	end
	assign `SIG73 = pdp_asi_ctlin_to_pctl_4_01;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b1;
	end
	assign `SIG55 = pdp_asi_ctlin_to_pctl_4_00;

	initial begin
		#0 pmu_pdp_dppicl630 = 1'b0;
		#40000.0 pmu_pdp_dppicl630 = 1'bx;
	end
	assign `SIG233 = pmu_pdp_dppicl630;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b1;
	end
	assign `SIG70 = pdp_asi_ctlin_to_pctl_4_04;

	initial begin
		#0 pmu_pct_ctlpcr0_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read1 = 1'bx;
	end
	assign `SIG35 = pmu_pct_ctlpcr0_read1;

	initial begin
		#0 pmu_pct_ctlpcr0_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read3 = 1'bx;
	end
	assign `SIG27 = pmu_pct_ctlpcr0_read3;

	initial begin
		#0 pmu_pct_ctlpcr0_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read2 = 1'bx;
	end
	assign `SIG42 = pmu_pct_ctlpcr0_read2;

	initial begin
		#0 n6699dummy = 1'b0;
		#40000.0 n6699dummy = 1'b1;
	end
	assign `SIG142 = n6699dummy;

	initial begin
		#0 n6096 = 1'b0;
		#63600.0 n6096 = 1'b1;
	end
	assign `SIG54 = n6096;

	initial begin
		#0 pct_picl07_add_w23 = 1'b0;
		#40100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#100.0 pct_picl07_add_w23 = 1'b0;
		#100.0 pct_picl07_add_w23 = 1'bx;
		#17400.0 pct_picl07_add_w23 = 1'b0;
	end
	assign `SIG241 = pct_picl07_add_w23;

	initial begin
		#0 pct_incr_pic_w11 = 1'b0;
		#40000.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#100.0 pct_incr_pic_w11 = 1'b0;
		#100.0 pct_incr_pic_w11 = 1'b1;
		#22400.0 pct_incr_pic_w11 = 1'b0;
	end
	assign `SIG108 = pct_incr_pic_w11;

	initial begin
		#0 pmu_pct_ctlpcr6_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read31 = 1'bx;
	end
	assign `SIG217 = pmu_pct_ctlpcr6_read31;

	initial begin
		#0 pmu_pdp_dppicl330 = 1'b0;
		#40000.0 pmu_pdp_dppicl330 = 1'bx;
	end
	assign `SIG235 = pmu_pdp_dppicl330;

	initial begin
		#0 pmu_pdp_dppich07_w229 = 1'b0;
		#40000.0 pmu_pdp_dppich07_w229 = 1'bx;
	end
	assign `SIG243 = pmu_pdp_dppich07_w229;

	initial begin
		#0 pmu_rngl_cdbus31 = 1'b0;
		#40000.0 pmu_rngl_cdbus31 = 1'b1;
		#23700.0 pmu_rngl_cdbus31 = 1'b0;
	end
	assign `SIG149 = pmu_rngl_cdbus31;

	initial begin
		#0 pmu_rngl_cdbus30 = 1'b0;
	end
	assign `SIG164 = pmu_rngl_cdbus30;

	initial begin
		#0 n6681dummy = 1'b0;
		#53600.0 n6681dummy = 1'b1;
	end
	assign `SIG203 = n6681dummy;

	initial begin
		#0 pmu_pdp_dppicl030 = 1'b0;
		#40000.0 pmu_pdp_dppicl030 = 1'bx;
	end
	assign `SIG238 = pmu_pdp_dppicl030;

	initial begin
		#0 n6627dummy = 1'b0;
		#40000.0 n6627dummy = 1'bx;
	end
	assign `SIG41 = n6627dummy;

	initial begin
		#0 n6144 = 1'b0;
		#40000.0 n6144 = 1'bx;
	end
	assign `SIG10 = n6144;

	initial begin
		#0 pmu_pct_ctlpl_incr_m7 = 1'b0;
	end
	assign `SIG92 = pmu_pct_ctlpl_incr_m7;

	initial begin
		#0 pmu_pct_ctlpl_incr_m4 = 1'b0;
	end
	assign `SIG102 = pmu_pct_ctlpl_incr_m4;

	initial begin
		#0 pmu_pct_ctlpl_incr_m5 = 1'b0;
	end
	assign `SIG94 = pmu_pct_ctlpl_incr_m5;

	initial begin
		#0 pmu_pct_ctlpl_incr_m2 = 1'b0;
	end
	assign `SIG98 = pmu_pct_ctlpl_incr_m2;

	initial begin
		#0 pmu_pct_ctlpl_incr_m3 = 1'b0;
	end
	assign `SIG90 = pmu_pct_ctlpl_incr_m3;

	initial begin
		#0 pmu_pct_ctlpl_incr_m0 = 1'b0;
	end
	assign `SIG104 = pmu_pct_ctlpl_incr_m0;

	initial begin
		#0 pmu_pct_ctlpl_incr_m1 = 1'b0;
	end
	assign `SIG100 = pmu_pct_ctlpl_incr_m1;

	initial begin
		#0 n6683dummy = 1'b0;
		#40000.0 n6683dummy = 1'b1;
		#100.0 n6683dummy = 1'b0;
		#23500.0 n6683dummy = 1'b1;
	end
	assign `SIG159 = n6683dummy;

	initial begin
		#0 pdp_asi_din_to_pctl29 = 1'b0;
	end
	assign `SIG167 = pdp_asi_din_to_pctl29;

	initial begin
		#0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#40100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
		#100.0 pmu_pct_ctlpic_st_dout0 = 1'b0;
		#22300.0 pmu_pct_ctlpic_st_dout0 = 1'b1;
	end
	assign `SIG107 = pmu_pct_ctlpic_st_dout0;

	initial begin
		#0 pmu_pct_ctltg1_e11 = 1'b0;
		#53700.0 pmu_pct_ctltg1_e11 = 1'b1;
	end
	assign `SIG115 = pmu_pct_ctltg1_e11;

	initial begin
		#0 pmu_pct_ctltg1_e10 = 1'b0;
	end
	assign `SIG133 = pmu_pct_ctltg1_e10;

	initial begin
		#0 pmu_pct_ctltg1_e12 = 1'b0;
		#40000.0 pmu_pct_ctltg1_e12 = 1'b1;
	end
	assign `SIG84 = pmu_pct_ctltg1_e12;

	initial begin
		#0 pmu_pdp_dppich130 = 1'b0;
		#40000.0 pmu_pdp_dppich130 = 1'bx;
	end
	assign `SIG231 = pmu_pdp_dppich130;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_84 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_84 = 1'b1;
	end
	assign `SIG66 = pdp_asi_ctlin_to_pctl_15_84;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_85 = 1'b0;
		#40000.0 pdp_asi_ctlin_to_pctl_15_85 = 1'b1;
	end
	assign `SIG67 = pdp_asi_ctlin_to_pctl_15_85;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_86 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_15_86 = 1'b1;
	end
	assign `SIG19 = pdp_asi_ctlin_to_pctl_15_86;

	initial begin
		#0 pmu_rngl_cdbus4 = 1'b0;
		#40000.0 pmu_rngl_cdbus4 = 1'b1;
	end
	assign `SIG192 = pmu_rngl_cdbus4;

	initial begin
		#0 pmu_pct_ctlpcr2_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read3 = 1'bx;
	end
	assign `SIG22 = pmu_pct_ctlpcr2_read3;

	initial begin
		#0 pmu_pct_ctlpcr2_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read2 = 1'bx;
	end
	assign `SIG40 = pmu_pct_ctlpcr2_read2;

	initial begin
		#0 pmu_pct_ctlpcr2_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read1 = 1'bx;
	end
	assign `SIG34 = pmu_pct_ctlpcr2_read1;

	initial begin
		#0 pmu_pdp_dppicpcr_muxpsel0 = 1'b0;
	end
	assign `SIG17 = pmu_pdp_dppicpcr_muxpsel0;

	initial begin
		#0 pmu_lsu_dcmiss_trap_mttt = 1'b0;
		#40100.0 pmu_lsu_dcmiss_trap_mttt = 1'bx;
		#23700.0 pmu_lsu_dcmiss_trap_mttt = 1'b0;
	end
	assign `SIG137 = pmu_lsu_dcmiss_trap_mttt;

	initial begin
		#0 pmu_pdp_dppicl07_w29 = 1'b0;
		#40000.0 pmu_pdp_dppicl07_w29 = 1'bx;
	end
	assign `SIG249 = pmu_pdp_dppicl07_w29;

	initial begin
		#0 pmu_rngl_cdbus2 = 1'b0;
	end
	assign `SIG148 = pmu_rngl_cdbus2;

	initial begin
		#0 pmu_pct_ctlpcr3_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read31 = 1'bx;
	end
	assign `SIG216 = pmu_pct_ctlpcr3_read31;

	initial begin
		#0 pmu_rngl_cdbus49 = 1'b0;
		#40000.0 pmu_rngl_cdbus49 = 1'b1;
		#23700.0 pmu_rngl_cdbus49 = 1'b0;
	end
	assign `SIG208 = pmu_rngl_cdbus49;

	initial begin
		#0 pmu_pct_ctlp_wr = 1'b0;
	end
	assign `SIG18 = pmu_pct_ctlp_wr;

	initial begin
		#0 pmu_pct_ctlpc_rd = 1'b0;
	end
	assign `SIG13 = pmu_pct_ctlpc_rd;

	initial begin
		#0 n9812dummy = 1'b0;
		#40000.0 n9812dummy = 1'bx;
	end
	assign `SIG246 = n9812dummy;

	initial begin
		#0 pmu_pct_ctlpcr0_read31 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read31 = 1'bx;
	end
	assign `SIG145 = pmu_pct_ctlpcr0_read31;

	initial begin
		 #70000.0 $finish;
	end

`endif
