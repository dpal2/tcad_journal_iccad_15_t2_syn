`ifdef SigSeT_1_32
	initial begin
		#0 pmu_pct_ctlpcr6_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read3 = 1'bx;
	end
	assign `SIG21 = pmu_pct_ctlpcr6_read3;

	initial begin
		#0 pmu_pct_ctlpcr6_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read1 = 1'bx;
	end
	assign `SIG30 = pmu_pct_ctlpcr6_read1;

	initial begin
		#0 pmu_pct_ctlatid1 = 1'b0;
		#40000.0 pmu_pct_ctlatid1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlatid1;

	initial begin
		#0 pmu_pct_ctlatid2 = 1'b0;
		#40000.0 pmu_pct_ctlatid2 = 1'b1;
		#13700.0 pmu_pct_ctlatid2 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctlatid2;

	initial begin
		#0 pmu_pct_ctlatid3 = 1'b0;
	end
	assign `SIG9 = pmu_pct_ctlatid3;

	initial begin
		#0 pmu_pct_ctlatid4 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlatid4;

	initial begin
		#0 pmu_pct_ctlatid5 = 1'b0;
	end
	assign `SIG6 = pmu_pct_ctlatid5;

	initial begin
		#0 pmu_pct_ctlatid6 = 1'b0;
	end
	assign `SIG2 = pmu_pct_ctlatid6;

	initial begin
		#0 pmu_pct_ctlatid7 = 1'b0;
	end
	assign `SIG3 = pmu_pct_ctlatid7;

	initial begin
		#0 n6614 = 1'b0;
		#40100.0 n6614 = 1'b1;
		#23600.0 n6614 = 1'b0;
	end
	assign `SIG7 = n6614;

	initial begin
		#0 pmu_pct_ctlpcr1_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read1 = 1'bx;
	end
	assign `SIG28 = pmu_pct_ctlpcr1_read1;

	initial begin
		#0 pmu_pct_ctlpcr1_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read3 = 1'bx;
	end
	assign `SIG26 = pmu_pct_ctlpcr1_read3;

	initial begin
		#0 pmu_pct_ctlpc_wr = 1'b0;
	end
	assign `SIG0 = pmu_pct_ctlpc_wr;

	initial begin
		#0 pmu_pct_ctlpcr5_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read1 = 1'bx;
	end
	assign `SIG31 = pmu_pct_ctlpcr5_read1;

	initial begin
		#0 pmu_pct_ctlpcr5_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read3 = 1'bx;
	end
	assign `SIG23 = pmu_pct_ctlpcr5_read3;

	initial begin
		#0 pmu_pct_ctlpcr3_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read3 = 1'bx;
	end
	assign `SIG25 = pmu_pct_ctlpcr3_read3;

	initial begin
		#0 pmu_pct_ctlpcr4_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read3 = 1'bx;
	end
	assign `SIG24 = pmu_pct_ctlpcr4_read3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr1 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr1 = 1'bx;
	end
	assign `SIG15 = pmu_pct_ctlsleep_cntr1;

	initial begin
		#0 pmu_pct_ctlsleep_cntr0 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr0 = 1'bx;
	end
	assign `SIG14 = pmu_pct_ctlsleep_cntr0;

	initial begin
		#0 pmu_pct_ctlsleep_cntr3 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr3 = 1'bx;
	end
	assign `SIG11 = pmu_pct_ctlsleep_cntr3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr2 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr2 = 1'bx;
	end
	assign `SIG12 = pmu_pct_ctlsleep_cntr2;

	initial begin
		#0 pmu_pct_ctlpcr7_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read3 = 1'bx;
	end
	assign `SIG20 = pmu_pct_ctlpcr7_read3;

	initial begin
		#0 pmu_pct_ctlpcr7_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read1 = 1'bx;
	end
	assign `SIG29 = pmu_pct_ctlpcr7_read1;

	initial begin
		#0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG1 = pmu_pct_ctlov_busy_clkgenc_0l1en;

	initial begin
		#0 n6158 = 1'b0;
		#63600.0 n6158 = 1'b1;
	end
	assign `SIG16 = n6158;

	initial begin
		#0 pmu_pct_ctlpcr0_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read3 = 1'bx;
	end
	assign `SIG27 = pmu_pct_ctlpcr0_read3;

	initial begin
		#0 n6144 = 1'b0;
		#40000.0 n6144 = 1'bx;
	end
	assign `SIG10 = n6144;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_86 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_15_86 = 1'b1;
	end
	assign `SIG19 = pdp_asi_ctlin_to_pctl_15_86;

	initial begin
		#0 pmu_pct_ctlpcr2_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read3 = 1'bx;
	end
	assign `SIG22 = pmu_pct_ctlpcr2_read3;

	initial begin
		#0 pmu_pdp_dppicpcr_muxpsel0 = 1'b0;
	end
	assign `SIG17 = pmu_pdp_dppicpcr_muxpsel0;

	initial begin
		#0 pmu_pct_ctlp_wr = 1'b0;
	end
	assign `SIG18 = pmu_pct_ctlp_wr;

	initial begin
		#0 pmu_pct_ctlpc_rd = 1'b0;
	end
	assign `SIG13 = pmu_pct_ctlpc_rd;

	initial begin
		 #70000.0 $finish;
	end

`endif
