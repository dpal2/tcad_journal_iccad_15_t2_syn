`ifdef SigSeT_1_64
	initial begin
		#0 n686dummy = 1'b0;
		#40000.0 n686dummy = 1'bx;
	end
	assign `SIG39 = n686dummy;

	initial begin
		#0 pmu_pct_ctlpcr6_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read3 = 1'bx;
	end
	assign `SIG21 = pmu_pct_ctlpcr6_read3;

	initial begin
		#0 pmu_pct_ctlpcr6_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read2 = 1'bx;
	end
	assign `SIG37 = pmu_pct_ctlpcr6_read2;

	initial begin
		#0 pmu_pct_ctlpcr6_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr6_read1 = 1'bx;
	end
	assign `SIG30 = pmu_pct_ctlpcr6_read1;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m3 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m3 = 1'b0;
	end
	assign `SIG45 = pmu_pct_ctlph_br_may_trap_m3;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m2 = 1'b0;
	end
	assign `SIG49 = pmu_pct_ctlph_br_may_trap_m2;

	initial begin
		#0 pmu_pct_ctlatid1 = 1'b0;
		#40000.0 pmu_pct_ctlatid1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlatid1;

	initial begin
		#0 pmu_pct_ctlatid2 = 1'b0;
		#40000.0 pmu_pct_ctlatid2 = 1'b1;
		#13700.0 pmu_pct_ctlatid2 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctlatid2;

	initial begin
		#0 pmu_pct_ctlatid3 = 1'b0;
	end
	assign `SIG9 = pmu_pct_ctlatid3;

	initial begin
		#0 pmu_pct_ctlatid4 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlatid4;

	initial begin
		#0 pmu_pct_ctlatid5 = 1'b0;
	end
	assign `SIG6 = pmu_pct_ctlatid5;

	initial begin
		#0 pmu_pct_ctlatid6 = 1'b0;
	end
	assign `SIG2 = pmu_pct_ctlatid6;

	initial begin
		#0 pmu_pct_ctlatid7 = 1'b0;
	end
	assign `SIG3 = pmu_pct_ctlatid7;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m2 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m2 = 1'bx;
	end
	assign `SIG58 = pmu_pct_ctll2dm_wrap_m2;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m7 = 1'b0;
	end
	assign `SIG51 = pmu_pct_ctlph_br_may_trap_m7;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m6 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m6 = 1'bx;
	end
	assign `SIG57 = pmu_pct_ctlph_br_may_trap_m6;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m7 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m7 = 1'bx;
	end
	assign `SIG61 = pmu_pct_ctll2dm_wrap_m7;

	initial begin
		#0 pmu_pct_ctltrap_m1 = 1'b0;
	end
	assign `SIG46 = pmu_pct_ctltrap_m1;

	initial begin
		#0 pmu_pct_ctltrap_m3 = 1'b0;
	end
	assign `SIG44 = pmu_pct_ctltrap_m3;

	initial begin
		#0 pmu_pct_ctltrap_m2 = 1'b0;
	end
	assign `SIG48 = pmu_pct_ctltrap_m2;

	initial begin
		#0 pmu_pct_ctltrap_m5 = 1'b0;
	end
	assign `SIG52 = pmu_pct_ctltrap_m5;

	initial begin
		#0 pmu_pct_ctltrap_m7 = 1'b0;
	end
	assign `SIG50 = pmu_pct_ctltrap_m7;

	initial begin
		#0 pmu_pct_ctltrap_m6 = 1'b0;
	end
	assign `SIG56 = pmu_pct_ctltrap_m6;

	initial begin
		#0 n6614 = 1'b0;
		#40100.0 n6614 = 1'b1;
		#23600.0 n6614 = 1'b0;
	end
	assign `SIG7 = n6614;

	initial begin
		#0 pmu_pct_ctlpcr1_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read1 = 1'bx;
	end
	assign `SIG28 = pmu_pct_ctlpcr1_read1;

	initial begin
		#0 pmu_pct_ctlpcr1_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr1_read3 = 1'bx;
	end
	assign `SIG26 = pmu_pct_ctlpcr1_read3;

	initial begin
		#0 pmu_pct_ctlpc_wr = 1'b0;
	end
	assign `SIG0 = pmu_pct_ctlpc_wr;

	initial begin
		#0 pmu_pct_ctlpcr5_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read1 = 1'bx;
	end
	assign `SIG31 = pmu_pct_ctlpcr5_read1;

	initial begin
		#0 pmu_pct_ctlpcr5_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read2 = 1'bx;
	end
	assign `SIG38 = pmu_pct_ctlpcr5_read2;

	initial begin
		#0 pmu_pct_ctlpcr5_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr5_read3 = 1'bx;
	end
	assign `SIG23 = pmu_pct_ctlpcr5_read3;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m3 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m3 = 1'bx;
	end
	assign `SIG63 = pmu_pct_ctll2dm_wrap_m3;

	initial begin
		#0 pmu_pct_ctlpcr3_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read2 = 1'bx;
	end
	assign `SIG43 = pmu_pct_ctlpcr3_read2;

	initial begin
		#0 pmu_pct_ctlpcr3_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read3 = 1'bx;
	end
	assign `SIG25 = pmu_pct_ctlpcr3_read3;

	initial begin
		#0 pmu_pct_ctlpcr3_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr3_read1 = 1'bx;
	end
	assign `SIG33 = pmu_pct_ctlpcr3_read1;

	initial begin
		#0 pmu_pct_ctlpcr4_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read1 = 1'bx;
	end
	assign `SIG32 = pmu_pct_ctlpcr4_read1;

	initial begin
		#0 pmu_pct_ctlpcr4_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr4_read3 = 1'bx;
	end
	assign `SIG24 = pmu_pct_ctlpcr4_read3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr1 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr1 = 1'bx;
	end
	assign `SIG15 = pmu_pct_ctlsleep_cntr1;

	initial begin
		#0 pmu_pct_ctlsleep_cntr0 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr0 = 1'bx;
	end
	assign `SIG14 = pmu_pct_ctlsleep_cntr0;

	initial begin
		#0 pmu_pct_ctlsleep_cntr3 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr3 = 1'bx;
	end
	assign `SIG11 = pmu_pct_ctlsleep_cntr3;

	initial begin
		#0 pmu_pct_ctlsleep_cntr2 = 1'b0;
		#40000.0 pmu_pct_ctlsleep_cntr2 = 1'bx;
	end
	assign `SIG12 = pmu_pct_ctlsleep_cntr2;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m1 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m1 = 1'bx;
	end
	assign `SIG60 = pmu_pct_ctll2dm_wrap_m1;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m1 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m1 = 1'bx;
	end
	assign `SIG47 = pmu_pct_ctlph_br_may_trap_m1;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m5 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m5 = 1'bx;
	end
	assign `SIG59 = pmu_pct_ctll2dm_wrap_m5;

	initial begin
		#0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
		#40000.0 pmu_pct_ctlph_br_may_trap_m5 = 1'bx;
		#13800.0 pmu_pct_ctlph_br_may_trap_m5 = 1'b0;
	end
	assign `SIG53 = pmu_pct_ctlph_br_may_trap_m5;

	initial begin
		#0 pmu_pct_ctll2dm_wrap_m6 = 1'b0;
		#40000.0 pmu_pct_ctll2dm_wrap_m6 = 1'bx;
	end
	assign `SIG62 = pmu_pct_ctll2dm_wrap_m6;

	initial begin
		#0 pmu_pct_ctlpcr7_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read2 = 1'bx;
	end
	assign `SIG36 = pmu_pct_ctlpcr7_read2;

	initial begin
		#0 pmu_pct_ctlpcr7_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read3 = 1'bx;
	end
	assign `SIG20 = pmu_pct_ctlpcr7_read3;

	initial begin
		#0 pmu_pct_ctlpcr7_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr7_read1 = 1'bx;
	end
	assign `SIG29 = pmu_pct_ctlpcr7_read1;

	initial begin
		#0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'b0;
		#40000.0 pmu_pct_ctlov_busy_clkgenc_0l1en = 1'bx;
	end
	assign `SIG1 = pmu_pct_ctlov_busy_clkgenc_0l1en;

	initial begin
		#0 n6158 = 1'b0;
		#63600.0 n6158 = 1'b1;
	end
	assign `SIG16 = n6158;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b1;
	end
	assign `SIG55 = pdp_asi_ctlin_to_pctl_4_00;

	initial begin
		#0 pmu_pct_ctlpcr0_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read1 = 1'bx;
	end
	assign `SIG35 = pmu_pct_ctlpcr0_read1;

	initial begin
		#0 pmu_pct_ctlpcr0_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read3 = 1'bx;
	end
	assign `SIG27 = pmu_pct_ctlpcr0_read3;

	initial begin
		#0 pmu_pct_ctlpcr0_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr0_read2 = 1'bx;
	end
	assign `SIG42 = pmu_pct_ctlpcr0_read2;

	initial begin
		#0 n6096 = 1'b0;
		#63600.0 n6096 = 1'b1;
	end
	assign `SIG54 = n6096;

	initial begin
		#0 n6627dummy = 1'b0;
		#40000.0 n6627dummy = 1'bx;
	end
	assign `SIG41 = n6627dummy;

	initial begin
		#0 n6144 = 1'b0;
		#40000.0 n6144 = 1'bx;
	end
	assign `SIG10 = n6144;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_86 = 1'b0;
		#53600.0 pdp_asi_ctlin_to_pctl_15_86 = 1'b1;
	end
	assign `SIG19 = pdp_asi_ctlin_to_pctl_15_86;

	initial begin
		#0 pmu_pct_ctlpcr2_read3 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read3 = 1'bx;
	end
	assign `SIG22 = pmu_pct_ctlpcr2_read3;

	initial begin
		#0 pmu_pct_ctlpcr2_read2 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read2 = 1'bx;
	end
	assign `SIG40 = pmu_pct_ctlpcr2_read2;

	initial begin
		#0 pmu_pct_ctlpcr2_read1 = 1'b0;
		#40000.0 pmu_pct_ctlpcr2_read1 = 1'bx;
	end
	assign `SIG34 = pmu_pct_ctlpcr2_read1;

	initial begin
		#0 pmu_pdp_dppicpcr_muxpsel0 = 1'b0;
	end
	assign `SIG17 = pmu_pdp_dppicpcr_muxpsel0;

	initial begin
		#0 pmu_pct_ctlp_wr = 1'b0;
	end
	assign `SIG18 = pmu_pct_ctlp_wr;

	initial begin
		#0 pmu_pct_ctlpc_rd = 1'b0;
	end
	assign `SIG13 = pmu_pct_ctlpc_rd;

	initial begin
		 #70000.0 $finish;
	end

`endif
