`ifdef PRankNetlist_64
	initial begin
		#0 pmu_pdp_dppicl016 = 1'b0;
		#59540.0 pmu_pdp_dppicl016 = 1'bx;
	end
	assign `SIG58 = pmu_pdp_dppicl016;

	initial begin
		#0 pmu_pdp_dppicl015 = 1'b0;
		#59540.0 pmu_pdp_dppicl015 = 1'bx;
	end
	assign `SIG55 = pmu_pdp_dppicl015;

	initial begin
		#0 pdp_asi_din_to_pctl31 = 1'b0;
	end
	assign `SIG13 = pdp_asi_din_to_pctl31;

	initial begin
		#0 pmu_pdp_dppicl716 = 1'b0;
		#59540.0 pmu_pdp_dppicl716 = 1'bx;
	end
	assign `SIG56 = pmu_pdp_dppicl716;

	initial begin
		#0 pmu_pdp_dppicl715 = 1'b0;
		#59540.0 pmu_pdp_dppicl715 = 1'bx;
	end
	assign `SIG54 = pmu_pdp_dppicl715;

	initial begin
		#0 pmu_pct_ctltlu_fl_w10 = 1'b0;
	end
	assign `SIG31 = pmu_pct_ctltlu_fl_w10;

	initial begin
		#0 pmu_pct_ctltlu_fl_w11 = 1'b0;
		#59540.0 pmu_pct_ctltlu_fl_w11 = 1'b1;
	end
	assign `SIG32 = pmu_pct_ctltlu_fl_w11;

	initial begin
		#0 pmu_pdp_dppdp_asi_din32 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din32 = 1'b1;
		#100.0 pmu_pdp_dppdp_asi_din32 = 1'b0;
		#24000.0 pmu_pdp_dppdp_asi_din32 = 1'b1;
	end
	assign `SIG45 = pmu_pdp_dppdp_asi_din32;

	initial begin
		#0 pmu_pdp_dppdp_asi_din33 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din33 = 1'b1;
		#24100.0 pmu_pdp_dppdp_asi_din33 = 1'b0;
	end
	assign `SIG37 = pmu_pdp_dppdp_asi_din33;

	initial begin
		#0 pmu_pdp_dppdp_asi_din34 = 1'b0;
	end
	assign `SIG22 = pmu_pdp_dppdp_asi_din34;

	initial begin
		#0 pmu_pdp_dppdp_asi_din35 = 1'b0;
	end
	assign `SIG38 = pmu_pdp_dppdp_asi_din35;

	initial begin
		#0 pmu_pdp_dppdp_asi_din36 = 1'b0;
		#59640.0 pmu_pdp_dppdp_asi_din36 = 1'b1;
		#24000.0 pmu_pdp_dppdp_asi_din36 = 1'b0;
	end
	assign `SIG20 = pmu_pdp_dppdp_asi_din36;

	initial begin
		#0 pmu_pdp_dppdp_asi_din37 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din37 = 1'b1;
		#100.0 pmu_pdp_dppdp_asi_din37 = 1'b0;
		#24000.0 pmu_pdp_dppdp_asi_din37 = 1'b1;
	end
	assign `SIG25 = pmu_pdp_dppdp_asi_din37;

	initial begin
		#0 pmu_pdp_dppdp_asi_din38 = 1'b0;
		#83640.0 pmu_pdp_dppdp_asi_din38 = 1'b1;
	end
	assign `SIG39 = pmu_pdp_dppdp_asi_din38;

	initial begin
		#0 pmu_pdp_dppdp_asi_din39 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din39 = 1'b1;
		#24100.0 pmu_pdp_dppdp_asi_din39 = 1'b0;
	end
	assign `SIG26 = pmu_pdp_dppdp_asi_din39;

	initial begin
		#0 pmu_pct_ctlatid7 = 1'b0;
	end
	assign `SIG50 = pmu_pct_ctlatid7;

	initial begin
		#0 pmu_pdp_dppdp_asi_din43 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din43 = 1'b1;
		#100.0 pmu_pdp_dppdp_asi_din43 = 1'b0;
		#24000.0 pmu_pdp_dppdp_asi_din43 = 1'b1;
	end
	assign `SIG40 = pmu_pdp_dppdp_asi_din43;

	initial begin
		#0 pmu_pct_ctlmem_typ_w0 = 1'b0;
		#59540.0 pmu_pct_ctlmem_typ_w0 = 1'b1;
	end
	assign `SIG7 = pmu_pct_ctlmem_typ_w0;

	initial begin
		#0 pmu_pct_ctlmem_typ_w1 = 1'b0;
		#59540.0 pmu_pct_ctlmem_typ_w1 = 1'b1;
	end
	assign `SIG8 = pmu_pct_ctlmem_typ_w1;

	initial begin
		#0 n5843 = 1'b0;
		#59540.0 n5843 = 1'b1;
		#24100.0 n5843 = 1'b0;
	end
	assign `SIG14 = n5843;

	initial begin
		#0 pmu_pdp_dppdp_asi_din_53 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din_53 = 1'b1;
		#24100.0 pmu_pdp_dppdp_asi_din_53 = 1'b0;
	end
	assign `SIG21 = pmu_pdp_dppdp_asi_din_53;

	initial begin
		#0 pmu_pdp_dppdp_asi_din_54 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din_54 = 1'b1;
	end
	assign `SIG33 = pmu_pdp_dppdp_asi_din_54;

	initial begin
		#0 n5832 = 1'b0;
		#83640.0 n5832 = 1'b1;
	end
	assign `SIG11 = n5832;

	initial begin
		#0 pmu_pdp_dppicl516 = 1'b0;
		#59540.0 pmu_pdp_dppicl516 = 1'bx;
	end
	assign `SIG57 = pmu_pdp_dppicl516;

	initial begin
		#0 pmu_pdp_dppicl027 = 1'b0;
		#59540.0 pmu_pdp_dppicl027 = 1'bx;
	end
	assign `SIG53 = pmu_pdp_dppicl027;

	initial begin
		#0 pmu_pdp_dppicl116 = 1'b0;
		#59540.0 pmu_pdp_dppicl116 = 1'bx;
	end
	assign `SIG59 = pmu_pdp_dppicl116;

	initial begin
		#0 pct_incr_pic_w16 = 1'b0;
		#59540.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
	end
	assign `SIG3 = pct_incr_pic_w16;

	initial begin
		#0 pct_incr_pic_w14 = 1'b0;
		#59540.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#100.0 pct_incr_pic_w14 = 1'b0;
		#100.0 pct_incr_pic_w14 = 1'b1;
		#22400.0 pct_incr_pic_w14 = 1'b0;
	end
	assign `SIG1 = pct_incr_pic_w14;

	initial begin
		#0 pct_incr_pic_w15 = 1'b0;
		#59540.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#100.0 pct_incr_pic_w15 = 1'b0;
		#100.0 pct_incr_pic_w15 = 1'b1;
		#22500.0 pct_incr_pic_w15 = 1'b0;
	end
	assign `SIG0 = pct_incr_pic_w15;

	initial begin
		#0 pct_incr_pic_w12 = 1'b0;
		#59640.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#100.0 pct_incr_pic_w12 = 1'b0;
		#100.0 pct_incr_pic_w12 = 1'b1;
		#22300.0 pct_incr_pic_w12 = 1'b0;
	end
	assign `SIG49 = pct_incr_pic_w12;

	initial begin
		#0 pct_incr_pic_w13 = 1'b0;
		#59640.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#100.0 pct_incr_pic_w13 = 1'b0;
		#100.0 pct_incr_pic_w13 = 1'b1;
		#22400.0 pct_incr_pic_w13 = 1'b0;
	end
	assign `SIG2 = pct_incr_pic_w13;

	initial begin
		#0 pmu_pdp_dppicl527 = 1'b0;
		#59540.0 pmu_pdp_dppicl527 = 1'bx;
	end
	assign `SIG51 = pmu_pdp_dppicl527;

	initial begin
		#0 pmu_pdp_dppicl028 = 1'b0;
		#59540.0 pmu_pdp_dppicl028 = 1'bx;
	end
	assign `SIG62 = pmu_pdp_dppicl028;

	initial begin
		#0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#59540.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b1;
		#100.0 pmu_pct_ctlpic_std_w17 = 1'b0;
	end
	assign `SIG5 = pmu_pct_ctlpic_std_w17;

	initial begin
		#0 pdp_asi_din_to_pctl17 = 1'b0;
		#59640.0 pdp_asi_din_to_pctl17 = 1'b1;
		#24000.0 pdp_asi_din_to_pctl17 = 1'b0;
	end
	assign `SIG48 = pdp_asi_din_to_pctl17;

	initial begin
		#0 pdp_asi_din_to_pctl14 = 1'b0;
	end
	assign `SIG6 = pdp_asi_din_to_pctl14;

	initial begin
		#0 pdp_asi_din_to_pctl15 = 1'b0;
		#59540.0 pdp_asi_din_to_pctl15 = 1'b1;
		#100.0 pdp_asi_din_to_pctl15 = 1'b0;
		#24000.0 pdp_asi_din_to_pctl15 = 1'b1;
	end
	assign `SIG10 = pdp_asi_din_to_pctl15;

	initial begin
		#0 pdp_asi_din_to_pctl18 = 1'b0;
	end
	assign `SIG19 = pdp_asi_din_to_pctl18;

	initial begin
		#0 n6158 = 1'b0;
	end
	assign `SIG12 = n6158;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#59540.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
		#100.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b0;
		#24000.0 pdp_asi_ctlin_to_pctl_4_02 = 1'b1;
	end
	assign `SIG15 = pdp_asi_ctlin_to_pctl_4_02;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_01 = 1'b0;
	end
	assign `SIG16 = pdp_asi_ctlin_to_pctl_4_01;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_00 = 1'b0;
	end
	assign `SIG17 = pdp_asi_ctlin_to_pctl_4_00;

	initial begin
		#0 pmu_pdp_dppicl528 = 1'b0;
		#59540.0 pmu_pdp_dppicl528 = 1'bx;
	end
	assign `SIG60 = pmu_pdp_dppicl528;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
		#59640.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b1;
		#24000.0 pdp_asi_ctlin_to_pctl_4_04 = 1'b0;
	end
	assign `SIG18 = pdp_asi_ctlin_to_pctl_4_04;

	initial begin
		#0 pmu_pct_ctlifu_fl_b0 = 1'b0;
		#73740.0 pmu_pct_ctlifu_fl_b0 = 1'b1;
	end
	assign `SIG23 = pmu_pct_ctlifu_fl_b0;

	initial begin
		#0 pmu_pct_ctlifu_fl_b1 = 1'b0;
	end
	assign `SIG24 = pmu_pct_ctlifu_fl_b1;

	initial begin
		#0 pmu_pdp_dppdp_asi_din41 = 1'b0;
	end
	assign `SIG35 = pmu_pdp_dppdp_asi_din41;

	initial begin
		#0 pmu_pdp_dppdp_asi_din40 = 1'b0;
	end
	assign `SIG36 = pmu_pdp_dppdp_asi_din40;

	initial begin
		#0 pmu_pct_ctldec_valid_m0 = 1'b0;
	end
	assign `SIG4 = pmu_pct_ctldec_valid_m0;

	initial begin
		#0 pmu_pdp_dppdp_asi_din42 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din42 = 1'b1;
	end
	assign `SIG41 = pmu_pdp_dppdp_asi_din42;

	initial begin
		#0 pmu_pdp_dppdp_asi_din45 = 1'b0;
		#83640.0 pmu_pdp_dppdp_asi_din45 = 1'b1;
	end
	assign `SIG42 = pmu_pdp_dppdp_asi_din45;

	initial begin
		#0 pmu_pdp_dppdp_asi_din44 = 1'b0;
		#83640.0 pmu_pdp_dppdp_asi_din44 = 1'b1;
	end
	assign `SIG34 = pmu_pdp_dppdp_asi_din44;

	initial begin
		#0 pmu_pdp_dppdp_asi_din47 = 1'b0;
		#83640.0 pmu_pdp_dppdp_asi_din47 = 1'b1;
	end
	assign `SIG43 = pmu_pdp_dppdp_asi_din47;

	initial begin
		#0 pmu_pdp_dppdp_asi_din46 = 1'b0;
		#59540.0 pmu_pdp_dppdp_asi_din46 = 1'b1;
	end
	assign `SIG44 = pmu_pdp_dppdp_asi_din46;

	initial begin
		#0 pdp_asi_din_to_pctl27 = 1'b0;
	end
	assign `SIG27 = pdp_asi_din_to_pctl27;

	initial begin
		#0 pdp_asi_din_to_pctl29 = 1'b0;
	end
	assign `SIG9 = pdp_asi_din_to_pctl29;

	initial begin
		#0 pmu_pdp_dppicl127 = 1'b0;
		#59540.0 pmu_pdp_dppicl127 = 1'bx;
	end
	assign `SIG52 = pmu_pdp_dppicl127;

	initial begin
		#0 pmu_pdp_dppicl128 = 1'b0;
		#59540.0 pmu_pdp_dppicl128 = 1'bx;
	end
	assign `SIG61 = pmu_pdp_dppicl128;

	initial begin
		#0 pmu_pdp_dppicl728 = 1'b0;
		#59540.0 pmu_pdp_dppicl728 = 1'bx;
	end
	assign `SIG63 = pmu_pdp_dppicl728;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_84 = 1'b0;
		#73640.0 pdp_asi_ctlin_to_pctl_15_84 = 1'b1;
	end
	assign `SIG28 = pdp_asi_ctlin_to_pctl_15_84;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_85 = 1'b0;
	end
	assign `SIG29 = pdp_asi_ctlin_to_pctl_15_85;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
		#59540.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b1;
		#14100.0 pdp_asi_ctlin_to_pctl_15_80 = 1'b0;
	end
	assign `SIG30 = pdp_asi_ctlin_to_pctl_15_80;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_81 = 1'b0;
		#83640.0 pdp_asi_ctlin_to_pctl_15_81 = 1'b1;
	end
	assign `SIG46 = pdp_asi_ctlin_to_pctl_15_81;

	initial begin
		#0 pdp_asi_ctlin_to_pctl_15_82 = 1'b0;
		#59540.0 pdp_asi_ctlin_to_pctl_15_82 = 1'b1;
	end
	assign `SIG47 = pdp_asi_ctlin_to_pctl_15_82;

	initial begin
		 #89540.0 $finish;
	end

`endif
