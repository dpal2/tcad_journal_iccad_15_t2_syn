`ifdef PRankNetlist_128
	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'bx;
	end
	assign `SIG82 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'bx;
	end
	assign `SIG83 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'bx;
	end
	assign `SIG84 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'bx;
	end
	assign `SIG85 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
	end
	assign `SIG4 = ncu_c2ifcd_ctlpcx_ncu_cputhr2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112 = 1'b0;
	end
	assign `SIG14 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
	end
	assign `SIG6 = ncu_c2ifcd_ctlpcx_ncu_cputhr0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110 = 1'b0;
	end
	assign `SIG16 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
	end
	assign `SIG8 = ncu_c2ifcd_ctlpcx_ncu_cputhr4;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlpcx_ncu_cputhr5;

	initial begin
		#0 intbuf_wr2i2c = 1'b0;
	end
	assign `SIG15 = intbuf_wr2i2c;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97 = 1'b0;
	end
	assign `SIG17 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96 = 1'b0;
	end
	assign `SIG18 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98 = 1'b0;
	end
	assign `SIG19 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10 = 1'b0;
	end
	assign `SIG119 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
	end
	assign `SIG3 = ncu_c2ifcd_ctlpcx_ncu_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'bx;
	end
	assign `SIG86 = ncu_c2ifcd_ctlpcx_ncu_addr8;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108 = 1'b0;
	end
	assign `SIG20 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109 = 1'b0;
	end
	assign `SIG21 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'b0;
	end
	assign `SIG127 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'bx;
	end
	assign `SIG43 = ncu_c2ifcd_ctlpcx_ncu_addr3;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
	end
	assign `SIG31 = ncu_c2ifcd_ctlpcx_ncu_addr2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'bx;
	end
	assign `SIG32 = ncu_c2ifcd_ctlpcx_ncu_addr1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'bx;
	end
	assign `SIG33 = ncu_c2ifcd_ctlpcx_ncu_addr0;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'bx;
	end
	assign `SIG44 = ncu_c2ifcd_ctlpcx_ncu_addr7;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'bx;
	end
	assign `SIG45 = ncu_c2ifcd_ctlpcx_ncu_addr6;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped21 = 1'b0;
	end
	assign `SIG27 = ncu_mb0_ctlrd_en_piped21;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped20 = 1'b0;
	end
	assign `SIG2 = ncu_mb0_ctlrd_en_piped20;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped22 = 1'b0;
	end
	assign `SIG28 = ncu_mb0_ctlrd_en_piped22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113 = 1'b1;
	end
	assign `SIG117 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
	end
	assign `SIG5 = ncu_c2ifcd_ctlpcx_ncu_cputhr3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111 = 1'b1;
	end
	assign `SIG42 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
	end
	assign `SIG7 = ncu_c2ifcd_ctlpcx_ncu_cputhr1;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out36 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out36 = 1'b1;
	end
	assign `SIG122 = ncu_mb0_ctldata_pipe_out36;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out35 = 1'b0;
	end
	assign `SIG25 = ncu_mb0_ctldata_pipe_out35;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out34 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out34 = 1'b1;
	end
	assign `SIG123 = ncu_mb0_ctldata_pipe_out34;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out33 = 1'b0;
	end
	assign `SIG26 = ncu_mb0_ctldata_pipe_out33;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out32 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out32 = 1'b1;
	end
	assign `SIG124 = ncu_mb0_ctldata_pipe_out32;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out31 = 1'b0;
	end
	assign `SIG125 = ncu_mb0_ctldata_pipe_out31;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'bx;
	end
	assign `SIG62 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'bx;
	end
	assign `SIG63 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'bx;
	end
	assign `SIG64 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'bx;
	end
	assign `SIG65 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'bx;
	end
	assign `SIG66 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'bx;
	end
	assign `SIG67 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100 = 1'b0;
	end
	assign `SIG23 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101 = 1'b0;
	end
	assign `SIG24 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'bx;
	end
	assign `SIG94 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'bx;
	end
	assign `SIG95 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
	end
	assign `SIG12 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
	end
	assign `SIG13 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
	end
	assign `SIG38 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'bx;
	end
	assign `SIG96 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'bx;
	end
	assign `SIG97 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'bx;
	end
	assign `SIG98 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'bx;
	end
	assign `SIG99 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'bx;
	end
	assign `SIG100 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'bx;
	end
	assign `SIG101 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'bx;
	end
	assign `SIG76 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'bx;
	end
	assign `SIG77 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'bx;
	end
	assign `SIG78 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114;

	initial begin
		#0 n9898 = 1'b0;
	end
	assign `SIG37 = n9898;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'bx;
	end
	assign `SIG102 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54;

	initial begin
		#0 n9890 = 1'b0;
	end
	assign `SIG11 = n9890;

	initial begin
		#0 n9897 = 1'b0;
	end
	assign `SIG40 = n9897;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'bx;
	end
	assign `SIG104 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'bx;
	end
	assign `SIG46 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'bx;
	end
	assign `SIG47 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'bx;
	end
	assign `SIG48 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'bx;
	end
	assign `SIG49 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'bx;
	end
	assign `SIG50 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'bx;
	end
	assign `SIG51 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'bx;
	end
	assign `SIG52 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'bx;
	end
	assign `SIG53 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'bx;
	end
	assign `SIG54 = ncu_c2ifcd_ctlpcx_ncu_addr19;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'bx;
	end
	assign `SIG55 = ncu_c2ifcd_ctlpcx_ncu_addr18;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'bx;
	end
	assign `SIG56 = ncu_c2ifcd_ctlpcx_ncu_addr17;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'bx;
	end
	assign `SIG57 = ncu_c2ifcd_ctlpcx_ncu_addr16;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'bx;
	end
	assign `SIG58 = ncu_c2ifcd_ctlpcx_ncu_addr15;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'bx;
	end
	assign `SIG59 = ncu_c2ifcd_ctlpcx_ncu_addr14;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'bx;
	end
	assign `SIG60 = ncu_c2ifcd_ctlpcx_ncu_addr13;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
	end
	assign `SIG61 = ncu_c2ifcd_ctlpcx_ncu_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
	end
	assign `SIG29 = ncu_c2ifcd_ctlpcx_ncu_addr10;

	initial begin
		#0 n9884 = 1'b0;
	end
	assign `SIG79 = n9884;

	initial begin
		#0 n9887 = 1'b0;
	end
	assign `SIG120 = n9887;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
	end
	assign `SIG115 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
	end
	assign `SIG116 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15;

	initial begin
		#0 ncu_mb0_ctluser_data_out0 = 1'b0;
	end
	assign `SIG106 = ncu_mb0_ctluser_data_out0;

	initial begin
		#0 ncu_mb0_ctluser_data_out1 = 1'b0;
	end
	assign `SIG107 = ncu_mb0_ctluser_data_out1;

	initial begin
		#0 ncu_mb0_ctluser_data_out2 = 1'b0;
	end
	assign `SIG108 = ncu_mb0_ctluser_data_out2;

	initial begin
		#0 ncu_mb0_ctluser_data_out3 = 1'b0;
	end
	assign `SIG109 = ncu_mb0_ctluser_data_out3;

	initial begin
		#0 ncu_mb0_ctluser_data_out4 = 1'b0;
	end
	assign `SIG110 = ncu_mb0_ctluser_data_out4;

	initial begin
		#0 ncu_mb0_ctluser_data_out5 = 1'b0;
	end
	assign `SIG111 = ncu_mb0_ctluser_data_out5;

	initial begin
		#0 ncu_mb0_ctluser_data_out6 = 1'b0;
	end
	assign `SIG112 = ncu_mb0_ctluser_data_out6;

	initial begin
		#0 ncu_mb0_ctluser_data_out7 = 1'b0;
	end
	assign `SIG113 = ncu_mb0_ctluser_data_out7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'bx;
	end
	assign `SIG68 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'bx;
	end
	assign `SIG69 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'bx;
	end
	assign `SIG70 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'bx;
	end
	assign `SIG71 = ncu_c2ifcd_ctlpcx_ncu_addr28;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out31 = 1'b0;
	end
	assign `SIG0 = ncu_mb0_ctlcmpsel_pipe_out31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out30 = 1'b0;
	end
	assign `SIG1 = ncu_mb0_ctlcmpsel_pipe_out30;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'bx;
	end
	assign `SIG72 = ncu_c2ifcd_ctlpcx_ncu_addr20;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'bx;
	end
	assign `SIG73 = ncu_c2ifcd_ctlpcx_ncu_addr21;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'bx;
	end
	assign `SIG74 = ncu_c2ifcd_ctlpcx_ncu_addr26;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'bx;
	end
	assign `SIG75 = ncu_c2ifcd_ctlpcx_ncu_addr27;

	initial begin
		#0 ncu_mb0_ctlconfig_out8 = 1'b0;
	end
	assign `SIG114 = ncu_mb0_ctlconfig_out8;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
	end
	assign `SIG30 = ncu_c2ifcd_ctlpcx_ncu_addr34;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'bx;
	end
	assign `SIG88 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'bx;
	end
	assign `SIG89 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'bx;
	end
	assign `SIG105 = ncu_c2ifcd_ctlpcx_ncu_req1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'bx;
	end
	assign `SIG80 = ncu_c2ifcd_ctlpcx_ncu_addr32;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG35 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'bx;
	end
	assign `SIG90 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3;

	initial begin
		#0 ncu_mb0_ctlconfig_out3 = 1'b0;
	end
	assign `SIG39 = ncu_mb0_ctlconfig_out3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'bx;
	end
	assign `SIG91 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'bx;
	end
	assign `SIG34 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'bx;
	end
	assign `SIG92 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'bx;
	end
	assign `SIG93 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'bx;
	end
	assign `SIG81 = ncu_c2ifcd_ctlpcx_ncu_addr39;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105 = 1'b1;
	end
	assign `SIG118 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106 = 1'b0;
	end
	assign `SIG22 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'bx;
	end
	assign `SIG87 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10;

	initial begin
		#0 ncu_mb0_ctlcontrol_out0 = 1'b0;
	end
	assign `SIG36 = ncu_mb0_ctlcontrol_out0;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d11 = 1'b0;
	end
	assign `SIG126 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d11;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d10 = 1'b0;
	end
	assign `SIG41 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d10;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d14 = 1'b0;
	end
	assign `SIG121 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'bx;
	end
	assign `SIG103 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50;

	initial begin
		#0 ncu_mb0_ctluser_bisi_rd_mode = 1'b0;
	end
	assign `SIG10 = ncu_mb0_ctluser_bisi_rd_mode;

	initial begin
		 #87345.0 $finish;
	end

`endif
