`ifdef HybrSel_64
	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'bx;
	end
	assign `SIG23 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21 = 1'bx;
	end
	assign `SIG33 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'bx;
	end
	assign `SIG31 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d131 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d131 = 1'bx;
	end
	assign `SIG63 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d131;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta99 = 1'b0;
	end
	assign `SIG6 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta99;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'b0;
		#222330.0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'bx;
	end
	assign `SIG17 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog;

	initial begin
		#0 iobuf_head_f0 = 1'b0;
		#222330.0 iobuf_head_f0 = 1'bx;
	end
	assign `SIG3 = iobuf_head_f0;

	initial begin
		#0 iobuf_head_f4 = 1'b0;
		#222330.0 iobuf_head_f4 = 1'bx;
	end
	assign `SIG18 = iobuf_head_f4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en = 1'b1;
	end
	assign `SIG14 = ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d121 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d121 = 1'bx;
	end
	assign `SIG57 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d121;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlmondo_data_wr_d1 = 1'b0;
	end
	assign `SIG41 = ncu_c2ifcd_ctlncu_c2ifc_ctlmondo_data_wr_d1;

	initial begin
		#0 ncu_cpx_data_ca145 = 1'b0;
	end
	assign `SIG13 = ncu_cpx_data_ca145;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d154 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d154 = 1'bx;
	end
	assign `SIG61 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d154;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d156 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d156 = 1'bx;
	end
	assign `SIG62 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d156;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_vld = 1'b0;
		#222330.0 ncu_i2cfcd_ctliobuf_vld = 1'bx;
	end
	assign `SIG11 = ncu_i2cfcd_ctliobuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
	end
	assign `SIG16 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
	end
	assign `SIG15 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244 = 1'bx;
	end
	assign `SIG40 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d244;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'bx;
	end
	assign `SIG27 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'bx;
	end
	assign `SIG26 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'bx;
	end
	assign `SIG28 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243 = 1'bx;
	end
	assign `SIG39 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d243;

	initial begin
		#0 n9884 = 1'b0;
		#222330.0 n9884 = 1'b1;
		#28250.0 n9884 = 1'b0;
	end
	assign `SIG45 = n9884;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr12 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr12 = 1'bx;
	end
	assign `SIG43 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d130 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d130 = 1'bx;
	end
	assign `SIG60 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d130;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq = 1'b0;
	end
	assign `SIG44 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153 = 1'bx;
	end
	assign `SIG35 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d153;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159 = 1'bx;
	end
	assign `SIG38 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d159;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d10 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d10 = 1'bx;
	end
	assign `SIG46 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d11 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d11 = 1'bx;
	end
	assign `SIG52 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d16 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d16 = 1'bx;
	end
	assign `SIG42 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d16;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d145 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d145 = 1'bx;
	end
	assign `SIG58 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d145;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147 = 1'bx;
	end
	assign `SIG37 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d147;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'bx;
	end
	assign `SIG25 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262 = 1'bx;
	end
	assign `SIG34 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d262;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'bx;
	end
	assign `SIG29 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260 = 1'bx;
	end
	assign `SIG36 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d260;

	initial begin
		#0 n71dummy = 1'b0;
	end
	assign `SIG8 = n71dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d112 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d112 = 1'bx;
	end
	assign `SIG59 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d112;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d111 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d111 = 1'bx;
	end
	assign `SIG55 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d111;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d117 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d117 = 1'bx;
	end
	assign `SIG49 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d117;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d114 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d114 = 1'bx;
	end
	assign `SIG48 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d114;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d115 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d115 = 1'bx;
	end
	assign `SIG56 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d115;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d137 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d137 = 1'bx;
	end
	assign `SIG54 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d137;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d135 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d135 = 1'bx;
	end
	assign `SIG53 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d135;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'bx;
	end
	assign `SIG21 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'bx;
	end
	assign `SIG22 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11 = 1'bx;
	end
	assign `SIG32 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'bx;
	end
	assign `SIG24 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'bx;
	end
	assign `SIG20 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'bx;
	end
	assign `SIG30 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b1;
		#46000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'bx;
	end
	assign `SIG19 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG1 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
	end
	assign `SIG0 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'b0;
		#222330.0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'bx;
	end
	assign `SIG12 = ncu_i2cfcd_ctliobuf_dout_d1_ue;

	initial begin
		#0 ncu_mb0_ctlconfig_out5 = 1'b0;
	end
	assign `SIG4 = ncu_mb0_ctlconfig_out5;

	initial begin
		#0 ncu_mb0_ctlconfig_out4 = 1'b0;
	end
	assign `SIG7 = ncu_mb0_ctlconfig_out4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d123 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d123 = 1'bx;
	end
	assign `SIG51 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d123;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d124 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d124 = 1'bx;
	end
	assign `SIG50 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data0_din_d124;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_vld = 1'b0;
	end
	assign `SIG10 = ncu_i2cfcd_ctlintbuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1 = 1'b0;
	end
	assign `SIG2 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d110 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d110 = 1'bx;
	end
	assign `SIG47 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_data1_din_d110;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100 = 1'b0;
	end
	assign `SIG5 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100;

	initial begin
		 #297330.0 $finish;
	end

`endif
