`ifdef SigSeT_1_256
	initial begin
		#0 ncu_c2ifcd_ctlmondo_addr_creg_mdata0_dec_d2 = 1'b0;
	end
	assign `SIG84 = ncu_c2ifcd_ctlmondo_addr_creg_mdata0_dec_d2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'bx;
	end
	assign `SIG184 = ncu_c2ifcd_ctlpcx_ncu_vld;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped22 = 1'b0;
	end
	assign `SIG15 = ncu_mb0_ctlrd_en_piped22;

	initial begin
		#0 n9932 = 1'b0;
	end
	assign `SIG2 = n9932;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv38 = 1'b0;
	end
	assign `SIG151 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv38;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
	end
	assign `SIG59 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata0_dec_d1;

	initial begin
		#0 n972dummy = 1'b0;
		#216030.0 n972dummy = 1'bx;
	end
	assign `SIG222 = n972dummy;

	initial begin
		#0 n968dummy = 1'b0;
		#216030.0 n968dummy = 1'bx;
	end
	assign `SIG216 = n968dummy;

	initial begin
		#0 n1019dummy = 1'b0;
		#216030.0 n1019dummy = 1'bx;
	end
	assign `SIG186 = n1019dummy;

	initial begin
		#0 n913dummy = 1'b0;
		#216030.0 n913dummy = 1'bx;
	end
	assign `SIG247 = n913dummy;

	initial begin
		#0 ncu_pcx_stall_pqttt = 1'b0;
		#216030.0 ncu_pcx_stall_pqttt = 1'bx;
	end
	assign `SIG234 = ncu_pcx_stall_pqttt;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt31 = 1'b0;
	end
	assign `SIG199 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt31;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv29 = 1'b0;
	end
	assign `SIG124 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv29;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv28 = 1'b0;
	end
	assign `SIG123 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv28;

	initial begin
		#0 ncu_mb0_ctlcontrol_out20 = 1'b0;
	end
	assign `SIG69 = ncu_mb0_ctlcontrol_out20;

	initial begin
		#0 ncu_mb0_ctlcontrol_out21 = 1'b0;
	end
	assign `SIG20 = ncu_mb0_ctlcontrol_out21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv25 = 1'b0;
	end
	assign `SIG121 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv25;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv24 = 1'b0;
	end
	assign `SIG120 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv24;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv27 = 1'b0;
	end
	assign `SIG111 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv27;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv26 = 1'b0;
	end
	assign `SIG122 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv26;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv21 = 1'b0;
	end
	assign `SIG155 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv21;

	initial begin
		#0 mb0_intbuf_rd_en = 1'b0;
	end
	assign `SIG53 = mb0_intbuf_rd_en;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv23 = 1'b0;
	end
	assign `SIG119 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv23;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv22 = 1'b0;
	end
	assign `SIG156 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv22;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d13 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d13 = 1'bx;
	end
	assign `SIG243 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d13;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt51 = 1'b0;
	end
	assign `SIG219 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt51;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d11 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d11 = 1'bx;
	end
	assign `SIG235 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d11;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10 = 1'bx;
	end
	assign `SIG233 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt01 = 1'b0;
	end
	assign `SIG213 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt01;

	initial begin
		#0 ncu_mb0_ctlres_read_data_piped21 = 1'b0;
		#216030.0 ncu_mb0_ctlres_read_data_piped21 = 1'bx;
	end
	assign `SIG252 = ncu_mb0_ctlres_read_data_piped21;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
	end
	assign `SIG75 = ncu_c2ifcd_ctlpcx_ncu_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'bx;
	end
	assign `SIG240 = ncu_c2ifcd_ctlpcx_ncu_addr8;

	initial begin
		#0 n894dummy = 1'b0;
		#216030.0 n894dummy = 1'bx;
	end
	assign `SIG50 = n894dummy;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlint_aov = 1'b0;
	end
	assign `SIG232 = ncu_i2cfcd_ctlncu_i2cfc_ctlint_aov;

	initial begin
		#0 ncu_mb0_ctlres_read_data_piped20 = 1'b0;
		#216030.0 ncu_mb0_ctlres_read_data_piped20 = 1'bx;
	end
	assign `SIG253 = ncu_mb0_ctlres_read_data_piped20;

	initial begin
		#0 n892dummy = 1'b0;
		#216030.0 n892dummy = 1'bx;
	end
	assign `SIG47 = n892dummy;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'bx;
	end
	assign `SIG239 = ncu_c2ifcd_ctlpcx_ncu_addr7;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'bx;
	end
	assign `SIG238 = ncu_c2ifcd_ctlpcx_ncu_addr6;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'bx;
	end
	assign `SIG179 = ncu_c2ifcd_ctlpcx_ncu_addr5;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'bx;
	end
	assign `SIG178 = ncu_c2ifcd_ctlpcx_ncu_addr4;

	initial begin
		#0 ncu_mb0_ctlres_read_data_piped22 = 1'b0;
		#216030.0 ncu_mb0_ctlres_read_data_piped22 = 1'bx;
	end
	assign `SIG251 = ncu_mb0_ctlres_read_data_piped22;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped21 = 1'b0;
	end
	assign `SIG16 = ncu_mb0_ctlrd_en_piped21;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped20 = 1'b0;
	end
	assign `SIG17 = ncu_mb0_ctlrd_en_piped20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata1_dec_d1 = 1'b0;
	end
	assign `SIG60 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata1_dec_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv58 = 1'b0;
	end
	assign `SIG134 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv58;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv59 = 1'b0;
	end
	assign `SIG113 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv59;

	initial begin
		#0 n912dummy = 1'b0;
		#216030.0 n912dummy = 1'bx;
	end
	assign `SIG248 = n912dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv12 = 1'b0;
	end
	assign `SIG159 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv50 = 1'b0;
	end
	assign `SIG144 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv50;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv51 = 1'b0;
	end
	assign `SIG116 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv51;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv52 = 1'b0;
	end
	assign `SIG145 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv52;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv53 = 1'b0;
	end
	assign `SIG172 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv53;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv54 = 1'b0;
	end
	assign `SIG173 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv54;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv55 = 1'b0;
	end
	assign `SIG129 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv55;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv56 = 1'b0;
	end
	assign `SIG132 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv56;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv57 = 1'b0;
	end
	assign `SIG133 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv57;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlint_aog = 1'b0;
	end
	assign `SIG225 = ncu_i2cfcd_ctlncu_i2cfc_ctlint_aog;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_pbv = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_pbv = 1'bx;
	end
	assign `SIG193 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_pbv;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10 = 1'bx;
	end
	assign `SIG41 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11 = 1'bx;
	end
	assign `SIG44 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'bx;
	end
	assign `SIG40 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13 = 1'bx;
	end
	assign `SIG42 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14 = 1'bx;
	end
	assign `SIG43 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15 = 1'bx;
	end
	assign `SIG38 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv1 = 1'b0;
	end
	assign `SIG166 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv1;

	initial begin
		#0 n10221 = 1'b0;
	end
	assign `SIG1 = n10221;

	initial begin
		#0 n895dummy = 1'b0;
		#216030.0 n895dummy = 1'bx;
	end
	assign `SIG48 = n895dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlpcx_ncu_data_rdy = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlpcx_ncu_data_rdy = 1'b1;
		#50750.0 ncu_c2ifcd_ctlncu_c2ifc_ctlpcx_ncu_data_rdy = 1'b0;
	end
	assign `SIG180 = ncu_c2ifcd_ctlncu_c2ifc_ctlpcx_ncu_data_rdy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv49 = 1'b0;
	end
	assign `SIG143 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv49;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv43 = 1'b0;
	end
	assign `SIG114 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv43;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv42 = 1'b0;
	end
	assign `SIG138 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv42;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv41 = 1'b0;
	end
	assign `SIG153 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv41;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv40 = 1'b0;
	end
	assign `SIG152 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv40;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv47 = 1'b0;
	end
	assign `SIG115 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv47;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv46 = 1'b0;
	end
	assign `SIG141 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv46;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv45 = 1'b0;
	end
	assign `SIG140 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv45;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv44 = 1'b0;
	end
	assign `SIG139 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv44;

	initial begin
		#0 ncu_mb0_ctlstart_transition_piped = 1'b0;
	end
	assign `SIG241 = ncu_mb0_ctlstart_transition_piped;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlint_pav = 1'b0;
	end
	assign `SIG191 = ncu_i2cfcd_ctlncu_i2cfc_ctlint_pav;

	initial begin
		#0 ncu_mb0_ctlres_read_data_piped10 = 1'b0;
		#216030.0 ncu_mb0_ctlres_read_data_piped10 = 1'bx;
	end
	assign `SIG250 = ncu_mb0_ctlres_read_data_piped10;

	initial begin
		#0 mb0_mondo_rd_en = 1'b0;
	end
	assign `SIG55 = mb0_mondo_rd_en;

	initial begin
		#0 n974dummy = 1'b0;
		#216030.0 n974dummy = 1'bx;
	end
	assign `SIG220 = n974dummy;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
	end
	assign `SIG91 = ncu_c2ifcd_ctlpcx_ncu_addr10;

	initial begin
		#0 n9929 = 1'b0;
		#216030.0 n9929 = 1'bx;
	end
	assign `SIG196 = n9929;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11 = 1'bx;
	end
	assign `SIG108 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr10 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr10 = 1'bx;
	end
	assign `SIG177 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr10;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d12 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d12 = 1'bx;
	end
	assign `SIG254 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2 = 1'bx;
	end
	assign `SIG39 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2;

	initial begin
		#0 n893dummy = 1'b0;
		#216030.0 n893dummy = 1'bx;
	end
	assign `SIG51 = n893dummy;

	initial begin
		#0 mb0_iobuf_wr_enttt = 1'b0;
	end
	assign `SIG58 = mb0_iobuf_wr_enttt;

	initial begin
		#0 mb0_failttt = 1'b0;
	end
	assign `SIG37 = mb0_failttt;

	initial begin
		#0 intbuf_wr2i2c = 1'b0;
	end
	assign `SIG185 = intbuf_wr2i2c;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_rd_d1 = 1'b0;
	end
	assign `SIG182 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_rd_d1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt41 = 1'b0;
	end
	assign `SIG218 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt41;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfd_ctliobuf_dout_d1134 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfd_ctliobuf_dout_d1134 = 1'bx;
	end
	assign `SIG244 = ncu_i2cfcd_ctlncu_i2cfd_ctliobuf_dout_d1134;

	initial begin
		#0 mb0_iobuf_rd_en = 1'b0;
	end
	assign `SIG52 = mb0_iobuf_rd_en;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'bx;
	end
	assign `SIG242 = ncu_c2ifcd_ctlpcx_ncu_addr3;

	initial begin
		#0 n969dummy = 1'b0;
		#216030.0 n969dummy = 1'bx;
	end
	assign `SIG215 = n969dummy;

	initial begin
		#0 n9890 = 1'b0;
	end
	assign `SIG106 = n9890;

	initial begin
		#0 n9897 = 1'b0;
	end
	assign `SIG76 = n9897;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_invld_d1 = 1'b0;
	end
	assign `SIG175 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_invld_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mbusy_dec_d1 = 1'b0;
	end
	assign `SIG62 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mbusy_dec_d1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt21 = 1'b0;
	end
	assign `SIG198 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt21;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req2 = 1'b0;
	end
	assign `SIG205 = ncu_i2cfcd_ctlintbuf_cpx_req2;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'bx;
	end
	assign `SIG223 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog;

	initial begin
		#0 n885dummy = 1'b0;
		#216030.0 n885dummy = 1'b1;
	end
	assign `SIG33 = n885dummy;

	initial begin
		#0 n9888 = 1'b0;
	end
	assign `SIG192 = n9888;

	initial begin
		#0 n970dummy = 1'b0;
		#216030.0 n970dummy = 1'bx;
	end
	assign `SIG201 = n970dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv20 = 1'b0;
	end
	assign `SIG154 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv20;

	initial begin
		#0 ncu_mb0_ctldone_counter_out2 = 1'b0;
	end
	assign `SIG25 = ncu_mb0_ctldone_counter_out2;

	initial begin
		#0 ncu_mb0_ctldone_counter_out0 = 1'b0;
	end
	assign `SIG22 = ncu_mb0_ctldone_counter_out0;

	initial begin
		#0 ncu_mb0_ctldone_counter_out1 = 1'b0;
	end
	assign `SIG24 = ncu_mb0_ctldone_counter_out1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
	end
	assign `SIG88 = ncu_c2ifcd_ctlpcx_ncu_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1 = 1'bx;
	end
	assign `SIG18 = ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req3 = 1'b0;
	end
	assign `SIG206 = ncu_i2cfcd_ctlintbuf_cpx_req3;

	initial begin
		#0 n9885 = 1'b0;
		#216030.0 n9885 = 1'b1;
		#34750.0 n9885 = 1'b0;
	end
	assign `SIG0 = n9885;

	initial begin
		#0 n9887 = 1'b0;
	end
	assign `SIG4 = n9887;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
	end
	assign `SIG255 = ncu_c2ifcd_ctlpcx_ncu_addr34;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_wr_d1 = 1'b0;
	end
	assign `SIG181 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_wr_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'bx;
	end
	assign `SIG79 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'bx;
	end
	assign `SIG82 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
	end
	assign `SIG78 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'bx;
	end
	assign `SIG80 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'bx;
	end
	assign `SIG81 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
	end
	assign `SIG77 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_dout_d1_ue = 1'b0;
	end
	assign `SIG194 = ncu_i2cfcd_ctlintbuf_dout_d1_ue;

	initial begin
		#0 n973dummy = 1'b0;
		#216030.0 n973dummy = 1'bx;
	end
	assign `SIG221 = n973dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1 = 1'b0;
		#267030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1 = 1'bx;
	end
	assign `SIG87 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9 = 1'bx;
	end
	assign `SIG54 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlmondo_data_bypass_d2 = 1'b0;
	end
	assign `SIG46 = ncu_c2ifcd_ctlmondo_data_bypass_d2;

	initial begin
		#0 mb0_mondo_wr_en = 1'b0;
	end
	assign `SIG57 = mb0_mondo_wr_en;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req0 = 1'b0;
	end
	assign `SIG224 = ncu_i2cfcd_ctlintbuf_cpx_req0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv32 = 1'b0;
	end
	assign `SIG146 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv32;

	initial begin
		#0 n975dummy = 1'b0;
		#216030.0 n975dummy = 1'bx;
	end
	assign `SIG214 = n975dummy;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_dout_d1134 = 1'b0;
	end
	assign `SIG245 = ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_dout_d1134;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv33 = 1'b0;
	end
	assign `SIG147 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv33;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_vld = 1'b0;
	end
	assign `SIG190 = ncu_i2cfcd_ctlintbuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv30 = 1'b0;
	end
	assign `SIG125 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv30;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv61 = 1'b0;
	end
	assign `SIG136 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv61;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv60 = 1'b0;
	end
	assign `SIG135 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv60;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv63 = 1'b0;
	end
	assign `SIG112 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv63;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv31 = 1'b0;
	end
	assign `SIG109 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out31 = 1'b0;
	end
	assign `SIG14 = ncu_mb0_ctlcmpsel_pipe_out31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out30 = 1'b0;
	end
	assign `SIG13 = ncu_mb0_ctlcmpsel_pipe_out30;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt71 = 1'b0;
	end
	assign `SIG211 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt71;

	initial begin
		#0 ncu_mb0_ctlcontrol_out17 = 1'b0;
	end
	assign `SIG237 = ncu_mb0_ctlcontrol_out17;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req1 = 1'b0;
	end
	assign `SIG226 = ncu_i2cfcd_ctlintbuf_cpx_req1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out16 = 1'b0;
	end
	assign `SIG176 = ncu_mb0_ctlcontrol_out16;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv14 = 1'b0;
	end
	assign `SIG161 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv15 = 1'b0;
	end
	assign `SIG127 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv16 = 1'b0;
	end
	assign `SIG162 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv16;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv17 = 1'b0;
	end
	assign `SIG163 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv17;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv10 = 1'b0;
	end
	assign `SIG158 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv11 = 1'b0;
	end
	assign `SIG126 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq_d1 = 1'b0;
	end
	assign `SIG89 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv13 = 1'b0;
	end
	assign `SIG160 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv35 = 1'b0;
	end
	assign `SIG117 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv35;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv18 = 1'b0;
	end
	assign `SIG164 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv18;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv19 = 1'b0;
	end
	assign `SIG128 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv19;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlint_pbv = 1'b0;
	end
	assign `SIG195 = ncu_i2cfcd_ctlncu_i2cfc_ctlint_pbv;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'bx;
	end
	assign `SIG83 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_pa112 = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_pa112 = 1'bx;
	end
	assign `SIG228 = ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_pa112;

	initial begin
		#0 n9971dummy = 1'b0;
		#216280.0 n9971dummy = 1'b1;
		#250.0 n9971dummy = 1'b0;
		#250.0 n9971dummy = 1'b1;
		#50250.0 n9971dummy = 1'bx;
	end
	assign `SIG110 = n9971dummy;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out0 = 1'b0;
	end
	assign `SIG34 = ncu_mb0_ctlfail_reg_out0;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out1 = 1'b0;
	end
	assign `SIG35 = ncu_mb0_ctlfail_reg_out1;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out2 = 1'b0;
	end
	assign `SIG36 = ncu_mb0_ctlfail_reg_out2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv39 = 1'b0;
	end
	assign `SIG118 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv39;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'bx;
	end
	assign `SIG189 = ncu_i2cfcd_ctliobuf_dout_d1_ue;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_pav = 1'b0;
		#216030.0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_pav = 1'bx;
	end
	assign `SIG188 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_pav;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_dout_d1136 = 1'b0;
	end
	assign `SIG236 = ncu_i2cfcd_ctlncu_i2cfd_ctlintbuf_dout_d1136;

	initial begin
		#0 mb0_intbuf_wr_en = 1'b0;
	end
	assign `SIG61 = mb0_intbuf_wr_en;

	initial begin
		#0 n897dummy = 1'b0;
		#216030.0 n897dummy = 1'bx;
	end
	assign `SIG45 = n897dummy;

	initial begin
		#0 ncu_c2ifcd_ctlmondo_addr_creg_mdata1_dec_d2 = 1'b0;
	end
	assign `SIG86 = ncu_c2ifcd_ctlmondo_addr_creg_mdata1_dec_d2;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt61 = 1'b0;
	end
	assign `SIG217 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt61;

	initial begin
		#0 mb0_addr4 = 1'b0;
	end
	assign `SIG94 = mb0_addr4;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_vld = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_vld = 1'bx;
	end
	assign `SIG187 = ncu_i2cfcd_ctliobuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlmondo_data_addr_p1_d15 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlmondo_data_addr_p1_d15 = 1'bx;
	end
	assign `SIG246 = ncu_c2ifcd_ctlncu_c2ifc_ctlmondo_data_addr_p1_d15;

	initial begin
		#0 mb0_addr5 = 1'b0;
	end
	assign `SIG101 = mb0_addr5;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG10 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_mb0_ctlconfig_out0 = 1'b0;
	end
	assign `SIG11 = ncu_mb0_ctlconfig_out0;

	initial begin
		#0 ncu_mb0_ctlconfig_out3 = 1'b0;
	end
	assign `SIG12 = ncu_mb0_ctlconfig_out3;

	initial begin
		#0 ncu_mb0_ctlconfig_out2 = 1'b0;
	end
	assign `SIG29 = ncu_mb0_ctlconfig_out2;

	initial begin
		#0 mb0_addr0 = 1'b0;
	end
	assign `SIG73 = mb0_addr0;

	initial begin
		#0 mb0_addr1 = 1'b0;
	end
	assign `SIG74 = mb0_addr1;

	initial begin
		#0 mb0_addr2 = 1'b0;
	end
	assign `SIG72 = mb0_addr2;

	initial begin
		#0 ncu_mb0_ctlconfig_out6 = 1'b0;
	end
	assign `SIG19 = ncu_mb0_ctlconfig_out6;

	initial begin
		#0 mb0_wdata1 = 1'b0;
	end
	assign `SIG92 = mb0_wdata1;

	initial begin
		#0 mb0_wdata0 = 1'b0;
		#216030.0 mb0_wdata0 = 1'b1;
	end
	assign `SIG96 = mb0_wdata0;

	initial begin
		#0 mb0_wdata3 = 1'b0;
	end
	assign `SIG70 = mb0_wdata3;

	initial begin
		#0 mb0_wdata2 = 1'b0;
		#216030.0 mb0_wdata2 = 1'b1;
	end
	assign `SIG97 = mb0_wdata2;

	initial begin
		#0 mb0_wdata5 = 1'b0;
	end
	assign `SIG93 = mb0_wdata5;

	initial begin
		#0 mb0_wdata4 = 1'b0;
		#216030.0 mb0_wdata4 = 1'b1;
	end
	assign `SIG98 = mb0_wdata4;

	initial begin
		#0 mb0_wdata7 = 1'b0;
	end
	assign `SIG71 = mb0_wdata7;

	initial begin
		#0 mb0_wdata6 = 1'b0;
		#216030.0 mb0_wdata6 = 1'b1;
	end
	assign `SIG99 = mb0_wdata6;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req6 = 1'b0;
	end
	assign `SIG229 = ncu_i2cfcd_ctlintbuf_cpx_req6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1 = 1'b0;
		#267030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1 = 1'bx;
	end
	assign `SIG85 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1;

	initial begin
		#0 n896dummy = 1'b0;
		#216030.0 n896dummy = 1'bx;
	end
	assign `SIG49 = n896dummy;

	initial begin
		#0 ncu_mb0_ctlres_read_data_piped11 = 1'b0;
		#216030.0 ncu_mb0_ctlres_read_data_piped11 = 1'bx;
	end
	assign `SIG249 = ncu_mb0_ctlres_read_data_piped11;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req7 = 1'b0;
	end
	assign `SIG227 = ncu_i2cfcd_ctlintbuf_cpx_req7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13 = 1'bx;
	end
	assign `SIG6 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12 = 1'bx;
	end
	assign `SIG3 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10 = 1'bx;
	end
	assign `SIG8 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req4 = 1'b0;
	end
	assign `SIG230 = ncu_i2cfcd_ctlintbuf_cpx_req4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15 = 1'bx;
	end
	assign `SIG5 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14 = 1'bx;
	end
	assign `SIG7 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14;

	initial begin
		#0 mb0_addr3 = 1'b0;
	end
	assign `SIG90 = mb0_addr3;

	initial begin
		#0 ncu_mb0_ctlcounter_out3 = 1'b0;
	end
	assign `SIG27 = ncu_mb0_ctlcounter_out3;

	initial begin
		#0 ncu_mb0_ctlcounter_out0 = 1'b0;
	end
	assign `SIG30 = ncu_mb0_ctlcounter_out0;

	initial begin
		#0 ncu_mb0_ctlcounter_out1 = 1'b0;
	end
	assign `SIG32 = ncu_mb0_ctlcounter_out1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out9 = 1'b0;
	end
	assign `SIG23 = ncu_mb0_ctlcontrol_out9;

	initial begin
		#0 ncu_mb0_ctlcontrol_out8 = 1'b0;
	end
	assign `SIG95 = ncu_mb0_ctlcontrol_out8;

	initial begin
		#0 ncu_mb0_ctlcontrol_out7 = 1'b0;
	end
	assign `SIG100 = ncu_mb0_ctlcontrol_out7;

	initial begin
		#0 ncu_mb0_ctlcontrol_out6 = 1'b0;
	end
	assign `SIG104 = ncu_mb0_ctlcontrol_out6;

	initial begin
		#0 ncu_mb0_ctlcontrol_out5 = 1'b0;
	end
	assign `SIG107 = ncu_mb0_ctlcontrol_out5;

	initial begin
		#0 ncu_mb0_ctlcontrol_out4 = 1'b0;
	end
	assign `SIG103 = ncu_mb0_ctlcontrol_out4;

	initial begin
		#0 ncu_mb0_ctlcontrol_out3 = 1'b0;
	end
	assign `SIG174 = ncu_mb0_ctlcontrol_out3;

	initial begin
		#0 ncu_mb0_ctlcontrol_out2 = 1'b0;
	end
	assign `SIG64 = ncu_mb0_ctlcontrol_out2;

	initial begin
		#0 ncu_mb0_ctlcontrol_out1 = 1'b0;
	end
	assign `SIG67 = ncu_mb0_ctlcontrol_out1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out0 = 1'b0;
	end
	assign `SIG31 = ncu_mb0_ctlcontrol_out0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1 = 1'b0;
	end
	assign `SIG105 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
	end
	assign `SIG102 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv48 = 1'b0;
	end
	assign `SIG142 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv48;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_cpx_req5 = 1'b0;
	end
	assign `SIG231 = ncu_i2cfcd_ctlintbuf_cpx_req5;

	initial begin
		#0 n971dummy = 1'b0;
		#216030.0 n971dummy = 1'bx;
	end
	assign `SIG200 = n971dummy;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req7 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req7 = 1'bx;
	end
	assign `SIG197 = ncu_i2cfcd_ctliobuf_cpx_req7;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req6 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req6 = 1'bx;
	end
	assign `SIG202 = ncu_i2cfcd_ctliobuf_cpx_req6;

	initial begin
		#0 n9898 = 1'b0;
	end
	assign `SIG183 = n9898;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt11 = 1'b0;
	end
	assign `SIG212 = ncu_i2cfcd_ctlncu_i2cfc_ctlcpx_cnt11;

	initial begin
		#0 ncu_mb0_ctlcontrol_out13 = 1'b0;
	end
	assign `SIG56 = ncu_mb0_ctlcontrol_out13;

	initial begin
		#0 ncu_mb0_ctlcontrol_out12 = 1'b0;
	end
	assign `SIG21 = ncu_mb0_ctlcontrol_out12;

	initial begin
		#0 ncu_mb0_ctlcontrol_out11 = 1'b0;
	end
	assign `SIG28 = ncu_mb0_ctlcontrol_out11;

	initial begin
		#0 ncu_mb0_ctlcontrol_out10 = 1'b0;
	end
	assign `SIG26 = ncu_mb0_ctlcontrol_out10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv36 = 1'b0;
	end
	assign `SIG149 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv36;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv37 = 1'b0;
	end
	assign `SIG150 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv37;

	initial begin
		#0 ncu_mb0_ctlcontrol_out15 = 1'b0;
	end
	assign `SIG66 = ncu_mb0_ctlcontrol_out15;

	initial begin
		#0 ncu_mb0_ctlcontrol_out14 = 1'b0;
	end
	assign `SIG68 = ncu_mb0_ctlcontrol_out14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv34 = 1'b0;
	end
	assign `SIG148 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv34;

	initial begin
		#0 ncu_mb0_ctlcontrol_out19 = 1'b0;
	end
	assign `SIG63 = ncu_mb0_ctlcontrol_out19;

	initial begin
		#0 ncu_mb0_ctlcontrol_out18 = 1'b0;
	end
	assign `SIG65 = ncu_mb0_ctlcontrol_out18;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv2 = 1'b0;
	end
	assign `SIG167 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv3 = 1'b0;
	end
	assign `SIG130 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv0 = 1'b0;
	end
	assign `SIG165 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv62 = 1'b0;
	end
	assign `SIG137 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv62;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv6 = 1'b0;
	end
	assign `SIG170 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv7 = 1'b0;
	end
	assign `SIG131 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv4 = 1'b0;
	end
	assign `SIG168 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv5 = 1'b0;
	end
	assign `SIG169 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv5;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req4 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req4 = 1'bx;
	end
	assign `SIG204 = ncu_i2cfcd_ctliobuf_cpx_req4;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req5 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req5 = 1'bx;
	end
	assign `SIG203 = ncu_i2cfcd_ctliobuf_cpx_req5;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv8 = 1'b0;
	end
	assign `SIG171 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv8;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv9 = 1'b0;
	end
	assign `SIG157 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv9;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req0 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req0 = 1'bx;
	end
	assign `SIG210 = ncu_i2cfcd_ctliobuf_cpx_req0;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req1 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req1 = 1'bx;
	end
	assign `SIG209 = ncu_i2cfcd_ctliobuf_cpx_req1;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req2 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req2 = 1'bx;
	end
	assign `SIG208 = ncu_i2cfcd_ctliobuf_cpx_req2;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_cpx_req3 = 1'b0;
		#216030.0 ncu_i2cfcd_ctliobuf_cpx_req3 = 1'bx;
	end
	assign `SIG207 = ncu_i2cfcd_ctliobuf_cpx_req3;

	initial begin
		 #291030.0 $finish;
	end

`endif
