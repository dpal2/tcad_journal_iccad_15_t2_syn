`ifdef HybrSel_32
	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20 = 1'bx;
	end
	assign `SIG23 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22 = 1'bx;
	end
	assign `SIG31 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_thr_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta99 = 1'b0;
	end
	assign `SIG6 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta99;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'b0;
		#222330.0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog = 1'bx;
	end
	assign `SIG17 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_aog;

	initial begin
		#0 iobuf_head_f0 = 1'b0;
		#222330.0 iobuf_head_f0 = 1'bx;
	end
	assign `SIG3 = iobuf_head_f0;

	initial begin
		#0 iobuf_head_f4 = 1'b0;
		#222330.0 iobuf_head_f4 = 1'bx;
	end
	assign `SIG18 = iobuf_head_f4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en = 1'b1;
	end
	assign `SIG14 = ncu_c2ifcd_ctlncu_c2ifd_ctlclkgenc_0l1en;

	initial begin
		#0 ncu_cpx_data_ca145 = 1'b0;
	end
	assign `SIG13 = ncu_cpx_data_ca145;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_vld = 1'b0;
		#222330.0 ncu_i2cfcd_ctliobuf_vld = 1'bx;
	end
	assign `SIG11 = ncu_i2cfcd_ctliobuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
	end
	assign `SIG16 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
	end
	assign `SIG15 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245 = 1'bx;
	end
	assign `SIG27 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d245;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246 = 1'bx;
	end
	assign `SIG26 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d246;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242 = 1'bx;
	end
	assign `SIG28 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d242;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141 = 1'bx;
	end
	assign `SIG25 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d141;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'b0;
		#45750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263 = 1'bx;
	end
	assign `SIG29 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d263;

	initial begin
		#0 n71dummy = 1'b0;
	end
	assign `SIG8 = n71dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15 = 1'bx;
	end
	assign `SIG21 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14 = 1'bx;
	end
	assign `SIG22 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10 = 1'bx;
	end
	assign `SIG24 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13 = 1'bx;
	end
	assign `SIG20 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12 = 1'bx;
	end
	assign `SIG30 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_addr10_3_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'b1;
		#46000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16 = 1'bx;
	end
	assign `SIG19 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_pkt_data_d16;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG1 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#222330.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#45500.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
	end
	assign `SIG0 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'b0;
		#222330.0 ncu_i2cfcd_ctliobuf_dout_d1_ue = 1'bx;
	end
	assign `SIG12 = ncu_i2cfcd_ctliobuf_dout_d1_ue;

	initial begin
		#0 ncu_mb0_ctlconfig_out5 = 1'b0;
	end
	assign `SIG4 = ncu_mb0_ctlconfig_out5;

	initial begin
		#0 ncu_mb0_ctlconfig_out4 = 1'b0;
	end
	assign `SIG7 = ncu_mb0_ctlconfig_out4;

	initial begin
		#0 ncu_i2cfcd_ctlintbuf_vld = 1'b0;
	end
	assign `SIG10 = ncu_i2cfcd_ctlintbuf_vld;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1 = 1'b0;
	end
	assign `SIG2 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100 = 1'b0;
	end
	assign `SIG5 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100;

	initial begin
		 #297330.0 $finish;
	end

`endif
