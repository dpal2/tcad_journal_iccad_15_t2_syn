`ifdef SigSeT_1_128
	initial begin
		#0 ncu_c2ifcd_ctlmondo_addr_creg_mdata0_dec_d2 = 1'b0;
	end
	assign `SIG84 = ncu_c2ifcd_ctlmondo_addr_creg_mdata0_dec_d2;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped22 = 1'b0;
	end
	assign `SIG15 = ncu_mb0_ctlrd_en_piped22;

	initial begin
		#0 n9932 = 1'b0;
	end
	assign `SIG2 = n9932;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
	end
	assign `SIG59 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata0_dec_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv29 = 1'b0;
	end
	assign `SIG124 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv29;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv28 = 1'b0;
	end
	assign `SIG123 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv28;

	initial begin
		#0 ncu_mb0_ctlcontrol_out20 = 1'b0;
	end
	assign `SIG69 = ncu_mb0_ctlcontrol_out20;

	initial begin
		#0 ncu_mb0_ctlcontrol_out21 = 1'b0;
	end
	assign `SIG20 = ncu_mb0_ctlcontrol_out21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv25 = 1'b0;
	end
	assign `SIG121 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv25;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv24 = 1'b0;
	end
	assign `SIG120 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv24;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv27 = 1'b0;
	end
	assign `SIG111 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv27;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv26 = 1'b0;
	end
	assign `SIG122 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv26;

	initial begin
		#0 mb0_intbuf_rd_en = 1'b0;
	end
	assign `SIG53 = mb0_intbuf_rd_en;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv23 = 1'b0;
	end
	assign `SIG119 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv23;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
	end
	assign `SIG75 = ncu_c2ifcd_ctlpcx_ncu_addr9;

	initial begin
		#0 n894dummy = 1'b0;
		#216030.0 n894dummy = 1'bx;
	end
	assign `SIG50 = n894dummy;

	initial begin
		#0 n892dummy = 1'b0;
		#216030.0 n892dummy = 1'bx;
	end
	assign `SIG47 = n892dummy;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped21 = 1'b0;
	end
	assign `SIG16 = ncu_mb0_ctlrd_en_piped21;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped20 = 1'b0;
	end
	assign `SIG17 = ncu_mb0_ctlrd_en_piped20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata1_dec_d1 = 1'b0;
	end
	assign `SIG60 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mdata1_dec_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv59 = 1'b0;
	end
	assign `SIG113 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv59;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv51 = 1'b0;
	end
	assign `SIG116 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv51;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10 = 1'bx;
	end
	assign `SIG41 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11 = 1'bx;
	end
	assign `SIG44 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'bx;
	end
	assign `SIG40 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13 = 1'bx;
	end
	assign `SIG42 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14 = 1'bx;
	end
	assign `SIG43 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15 = 1'bx;
	end
	assign `SIG38 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15;

	initial begin
		#0 n10221 = 1'b0;
	end
	assign `SIG1 = n10221;

	initial begin
		#0 n895dummy = 1'b0;
		#216030.0 n895dummy = 1'bx;
	end
	assign `SIG48 = n895dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv43 = 1'b0;
	end
	assign `SIG114 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv43;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv47 = 1'b0;
	end
	assign `SIG115 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv47;

	initial begin
		#0 mb0_mondo_rd_en = 1'b0;
	end
	assign `SIG55 = mb0_mondo_rd_en;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
	end
	assign `SIG91 = ncu_c2ifcd_ctlpcx_ncu_addr10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11 = 1'bx;
	end
	assign `SIG108 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2 = 1'bx;
	end
	assign `SIG39 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo_busy_dout_d2;

	initial begin
		#0 n893dummy = 1'b0;
		#216030.0 n893dummy = 1'bx;
	end
	assign `SIG51 = n893dummy;

	initial begin
		#0 mb0_iobuf_wr_enttt = 1'b0;
	end
	assign `SIG58 = mb0_iobuf_wr_enttt;

	initial begin
		#0 mb0_failttt = 1'b0;
	end
	assign `SIG37 = mb0_failttt;

	initial begin
		#0 mb0_iobuf_rd_en = 1'b0;
	end
	assign `SIG52 = mb0_iobuf_rd_en;

	initial begin
		#0 n9890 = 1'b0;
	end
	assign `SIG106 = n9890;

	initial begin
		#0 n9897 = 1'b0;
	end
	assign `SIG76 = n9897;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mbusy_dec_d1 = 1'b0;
	end
	assign `SIG62 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_addr_creg_mbusy_dec_d1;

	initial begin
		#0 n885dummy = 1'b0;
		#216030.0 n885dummy = 1'b1;
	end
	assign `SIG33 = n885dummy;

	initial begin
		#0 ncu_mb0_ctldone_counter_out2 = 1'b0;
	end
	assign `SIG25 = ncu_mb0_ctldone_counter_out2;

	initial begin
		#0 ncu_mb0_ctldone_counter_out0 = 1'b0;
	end
	assign `SIG22 = ncu_mb0_ctldone_counter_out0;

	initial begin
		#0 ncu_mb0_ctldone_counter_out1 = 1'b0;
	end
	assign `SIG24 = ncu_mb0_ctldone_counter_out1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
	end
	assign `SIG88 = ncu_c2ifcd_ctlpcx_ncu_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1 = 1'bx;
	end
	assign `SIG18 = ncu_c2ifcd_ctlncu_c2ifd_ctldin1_d1;

	initial begin
		#0 n9885 = 1'b0;
		#216030.0 n9885 = 1'b1;
		#34750.0 n9885 = 1'b0;
	end
	assign `SIG0 = n9885;

	initial begin
		#0 n9887 = 1'b0;
	end
	assign `SIG4 = n9887;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'bx;
	end
	assign `SIG79 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'bx;
	end
	assign `SIG82 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
	end
	assign `SIG78 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'bx;
	end
	assign `SIG80 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'bx;
	end
	assign `SIG81 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
	end
	assign `SIG77 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1 = 1'b0;
		#267030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1 = 1'bx;
	end
	assign `SIG87 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mbusy_dec_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9 = 1'bx;
	end
	assign `SIG54 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlmondo_data_bypass_d2 = 1'b0;
	end
	assign `SIG46 = ncu_c2ifcd_ctlmondo_data_bypass_d2;

	initial begin
		#0 mb0_mondo_wr_en = 1'b0;
	end
	assign `SIG57 = mb0_mondo_wr_en;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv30 = 1'b0;
	end
	assign `SIG125 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv30;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv63 = 1'b0;
	end
	assign `SIG112 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv63;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv31 = 1'b0;
	end
	assign `SIG109 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out31 = 1'b0;
	end
	assign `SIG14 = ncu_mb0_ctlcmpsel_pipe_out31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out30 = 1'b0;
	end
	assign `SIG13 = ncu_mb0_ctlcmpsel_pipe_out30;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv15 = 1'b0;
	end
	assign `SIG127 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv11 = 1'b0;
	end
	assign `SIG126 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq_d1 = 1'b0;
	end
	assign `SIG89 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_seq_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv35 = 1'b0;
	end
	assign `SIG117 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv35;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1 = 1'bx;
	end
	assign `SIG83 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata0_dec_d1;

	initial begin
		#0 n9971dummy = 1'b0;
		#216280.0 n9971dummy = 1'b1;
		#250.0 n9971dummy = 1'b0;
		#250.0 n9971dummy = 1'b1;
		#50250.0 n9971dummy = 1'bx;
	end
	assign `SIG110 = n9971dummy;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out0 = 1'b0;
	end
	assign `SIG34 = ncu_mb0_ctlfail_reg_out0;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out1 = 1'b0;
	end
	assign `SIG35 = ncu_mb0_ctlfail_reg_out1;

	initial begin
		#0 ncu_mb0_ctlfail_reg_out2 = 1'b0;
	end
	assign `SIG36 = ncu_mb0_ctlfail_reg_out2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv39 = 1'b0;
	end
	assign `SIG118 = ncu_c2ifcd_ctlncu_c2ifd_ctlvec_inv39;

	initial begin
		#0 mb0_intbuf_wr_en = 1'b0;
	end
	assign `SIG61 = mb0_intbuf_wr_en;

	initial begin
		#0 n897dummy = 1'b0;
		#216030.0 n897dummy = 1'bx;
	end
	assign `SIG45 = n897dummy;

	initial begin
		#0 ncu_c2ifcd_ctlmondo_addr_creg_mdata1_dec_d2 = 1'b0;
	end
	assign `SIG86 = ncu_c2ifcd_ctlmondo_addr_creg_mdata1_dec_d2;

	initial begin
		#0 mb0_addr4 = 1'b0;
	end
	assign `SIG94 = mb0_addr4;

	initial begin
		#0 mb0_addr5 = 1'b0;
	end
	assign `SIG101 = mb0_addr5;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG10 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_mb0_ctlconfig_out0 = 1'b0;
	end
	assign `SIG11 = ncu_mb0_ctlconfig_out0;

	initial begin
		#0 ncu_mb0_ctlconfig_out3 = 1'b0;
	end
	assign `SIG12 = ncu_mb0_ctlconfig_out3;

	initial begin
		#0 ncu_mb0_ctlconfig_out2 = 1'b0;
	end
	assign `SIG29 = ncu_mb0_ctlconfig_out2;

	initial begin
		#0 mb0_addr0 = 1'b0;
	end
	assign `SIG73 = mb0_addr0;

	initial begin
		#0 mb0_addr1 = 1'b0;
	end
	assign `SIG74 = mb0_addr1;

	initial begin
		#0 mb0_addr2 = 1'b0;
	end
	assign `SIG72 = mb0_addr2;

	initial begin
		#0 ncu_mb0_ctlconfig_out6 = 1'b0;
	end
	assign `SIG19 = ncu_mb0_ctlconfig_out6;

	initial begin
		#0 mb0_wdata1 = 1'b0;
	end
	assign `SIG92 = mb0_wdata1;

	initial begin
		#0 mb0_wdata0 = 1'b0;
		#216030.0 mb0_wdata0 = 1'b1;
	end
	assign `SIG96 = mb0_wdata0;

	initial begin
		#0 mb0_wdata3 = 1'b0;
	end
	assign `SIG70 = mb0_wdata3;

	initial begin
		#0 mb0_wdata2 = 1'b0;
		#216030.0 mb0_wdata2 = 1'b1;
	end
	assign `SIG97 = mb0_wdata2;

	initial begin
		#0 mb0_wdata5 = 1'b0;
	end
	assign `SIG93 = mb0_wdata5;

	initial begin
		#0 mb0_wdata4 = 1'b0;
		#216030.0 mb0_wdata4 = 1'b1;
	end
	assign `SIG98 = mb0_wdata4;

	initial begin
		#0 mb0_wdata7 = 1'b0;
	end
	assign `SIG71 = mb0_wdata7;

	initial begin
		#0 mb0_wdata6 = 1'b0;
		#216030.0 mb0_wdata6 = 1'b1;
	end
	assign `SIG99 = mb0_wdata6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1 = 1'b0;
		#267030.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1 = 1'bx;
	end
	assign `SIG85 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_creg_mdata1_dec_d1;

	initial begin
		#0 n896dummy = 1'b0;
		#216030.0 n896dummy = 1'bx;
	end
	assign `SIG49 = n896dummy;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13 = 1'bx;
	end
	assign `SIG6 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12 = 1'bx;
	end
	assign `SIG3 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10 = 1'bx;
	end
	assign `SIG8 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15 = 1'bx;
	end
	assign `SIG5 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14 = 1'b0;
		#216030.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14 = 1'bx;
	end
	assign `SIG7 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d14;

	initial begin
		#0 mb0_addr3 = 1'b0;
	end
	assign `SIG90 = mb0_addr3;

	initial begin
		#0 ncu_mb0_ctlcounter_out3 = 1'b0;
	end
	assign `SIG27 = ncu_mb0_ctlcounter_out3;

	initial begin
		#0 ncu_mb0_ctlcounter_out0 = 1'b0;
	end
	assign `SIG30 = ncu_mb0_ctlcounter_out0;

	initial begin
		#0 ncu_mb0_ctlcounter_out1 = 1'b0;
	end
	assign `SIG32 = ncu_mb0_ctlcounter_out1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out9 = 1'b0;
	end
	assign `SIG23 = ncu_mb0_ctlcontrol_out9;

	initial begin
		#0 ncu_mb0_ctlcontrol_out8 = 1'b0;
	end
	assign `SIG95 = ncu_mb0_ctlcontrol_out8;

	initial begin
		#0 ncu_mb0_ctlcontrol_out7 = 1'b0;
	end
	assign `SIG100 = ncu_mb0_ctlcontrol_out7;

	initial begin
		#0 ncu_mb0_ctlcontrol_out6 = 1'b0;
	end
	assign `SIG104 = ncu_mb0_ctlcontrol_out6;

	initial begin
		#0 ncu_mb0_ctlcontrol_out5 = 1'b0;
	end
	assign `SIG107 = ncu_mb0_ctlcontrol_out5;

	initial begin
		#0 ncu_mb0_ctlcontrol_out4 = 1'b0;
	end
	assign `SIG103 = ncu_mb0_ctlcontrol_out4;

	initial begin
		#0 ncu_mb0_ctlcontrol_out2 = 1'b0;
	end
	assign `SIG64 = ncu_mb0_ctlcontrol_out2;

	initial begin
		#0 ncu_mb0_ctlcontrol_out1 = 1'b0;
	end
	assign `SIG67 = ncu_mb0_ctlcontrol_out1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out0 = 1'b0;
	end
	assign `SIG31 = ncu_mb0_ctlcontrol_out0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1 = 1'b0;
	end
	assign `SIG105 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_rd_d1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#216280.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1 = 1'bx;
	end
	assign `SIG102 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpu_mondo_addr_invld_d1;

	initial begin
		#0 ncu_mb0_ctlcontrol_out13 = 1'b0;
	end
	assign `SIG56 = ncu_mb0_ctlcontrol_out13;

	initial begin
		#0 ncu_mb0_ctlcontrol_out12 = 1'b0;
	end
	assign `SIG21 = ncu_mb0_ctlcontrol_out12;

	initial begin
		#0 ncu_mb0_ctlcontrol_out11 = 1'b0;
	end
	assign `SIG28 = ncu_mb0_ctlcontrol_out11;

	initial begin
		#0 ncu_mb0_ctlcontrol_out10 = 1'b0;
	end
	assign `SIG26 = ncu_mb0_ctlcontrol_out10;

	initial begin
		#0 ncu_mb0_ctlcontrol_out15 = 1'b0;
	end
	assign `SIG66 = ncu_mb0_ctlcontrol_out15;

	initial begin
		#0 ncu_mb0_ctlcontrol_out14 = 1'b0;
	end
	assign `SIG68 = ncu_mb0_ctlcontrol_out14;

	initial begin
		#0 ncu_mb0_ctlcontrol_out19 = 1'b0;
	end
	assign `SIG63 = ncu_mb0_ctlcontrol_out19;

	initial begin
		#0 ncu_mb0_ctlcontrol_out18 = 1'b0;
	end
	assign `SIG65 = ncu_mb0_ctlcontrol_out18;

	initial begin
		 #291030.0 $finish;
	end

`endif
