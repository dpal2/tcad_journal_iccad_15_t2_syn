`ifdef PRankNetlist_32
	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
	end
	assign `SIG4 = ncu_c2ifcd_ctlpcx_ncu_cputhr2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112 = 1'b0;
	end
	assign `SIG14 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
	end
	assign `SIG6 = ncu_c2ifcd_ctlpcx_ncu_cputhr0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110 = 1'b0;
	end
	assign `SIG16 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
	end
	assign `SIG8 = ncu_c2ifcd_ctlpcx_ncu_cputhr4;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlpcx_ncu_cputhr5;

	initial begin
		#0 intbuf_wr2i2c = 1'b0;
	end
	assign `SIG15 = intbuf_wr2i2c;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97 = 1'b0;
	end
	assign `SIG17 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96 = 1'b0;
	end
	assign `SIG18 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98 = 1'b0;
	end
	assign `SIG19 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
	end
	assign `SIG3 = ncu_c2ifcd_ctlpcx_ncu_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108 = 1'b0;
	end
	assign `SIG20 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109 = 1'b0;
	end
	assign `SIG21 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
	end
	assign `SIG31 = ncu_c2ifcd_ctlpcx_ncu_addr2;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped21 = 1'b0;
	end
	assign `SIG27 = ncu_mb0_ctlrd_en_piped21;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped20 = 1'b0;
	end
	assign `SIG2 = ncu_mb0_ctlrd_en_piped20;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped22 = 1'b0;
	end
	assign `SIG28 = ncu_mb0_ctlrd_en_piped22;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
	end
	assign `SIG5 = ncu_c2ifcd_ctlpcx_ncu_cputhr3;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
	end
	assign `SIG7 = ncu_c2ifcd_ctlpcx_ncu_cputhr1;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out35 = 1'b0;
	end
	assign `SIG25 = ncu_mb0_ctldata_pipe_out35;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out33 = 1'b0;
	end
	assign `SIG26 = ncu_mb0_ctldata_pipe_out33;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100 = 1'b0;
	end
	assign `SIG23 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101 = 1'b0;
	end
	assign `SIG24 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
	end
	assign `SIG12 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
	end
	assign `SIG13 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21;

	initial begin
		#0 n9890 = 1'b0;
	end
	assign `SIG11 = n9890;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
	end
	assign `SIG29 = ncu_c2ifcd_ctlpcx_ncu_addr10;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out31 = 1'b0;
	end
	assign `SIG0 = ncu_mb0_ctlcmpsel_pipe_out31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out30 = 1'b0;
	end
	assign `SIG1 = ncu_mb0_ctlcmpsel_pipe_out30;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
	end
	assign `SIG30 = ncu_c2ifcd_ctlpcx_ncu_addr34;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106 = 1'b0;
	end
	assign `SIG22 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106;

	initial begin
		#0 ncu_mb0_ctluser_bisi_rd_mode = 1'b0;
	end
	assign `SIG10 = ncu_mb0_ctluser_bisi_rd_mode;

	initial begin
		 #87345.0 $finish;
	end

`endif
