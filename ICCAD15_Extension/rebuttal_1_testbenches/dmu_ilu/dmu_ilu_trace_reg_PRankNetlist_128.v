`ifdef PRankNetlist_128
	reg n13581;
	reg n15433;
	reg y2k_buf_addr2;
	reg n13582;
	reg eilrcd_is_cpl_reg;
	reg n14699;
	reg y2k_buf_addr5;
	reg y2k_buf_addr3;
	reg n13583;
	reg y2k_buf_addr4;
	reg y2k_buf_addr6;
	reg n13584;
	reg eilbufmgrerh_wptr4;
	reg eilbufmgrech_wptr4;
	reg eilbufmgrerh_rptr1;
	reg y2k_buf_addr1;
	reg y2k_buf_addr0;
	reg eilbufmgrech_wptr1;
	reg eilbufmgrech_wptr3;
	reg d2p_ihb_addr5;
	reg d2p_ihb_addr0;
	reg n15152;
	reg n15154;
	reg n15157;
	reg n15159;
	reg n15149;
	reg eilbufmgrecd_wptr_adv6;
	reg eilbufmgrech_wptr0;
	reg eilbufmgrech_wptr2;
	reg eilbufmgrecd_wptr6;
	reg d2p_ihb_addr3;
	reg d2p_ihb_addr1;
	reg eilbufmgrerd_wptr_adv6;
	reg eildatafsmnum_rds0;
	reg eilbufmgrn_d2p_erh_wptr5;
	reg n14693;
	reg eilrelgenrcd_is_pio_mwr_reg;
	reg n15418;
	reg eilbufmgrn_d2p_ech_wptr5;
	reg eilbufmgrerh_rptr4;
	reg iilihb_rptr6;
	reg eilbufmgrerd_wptr6;
	reg n14387;
	reg n12903;
	reg iilcrdtcntibc_nhc5;
	reg iilcrdtcntibc_nhc6;
	reg iilcrdtcntibc_nhc0;
	reg iilcrdtcntibc_nhc1;
	reg iilcrdtcntibc_nhc3;
	reg eildatafsmnum_wrs_adv4;
	reg eilbufmgrecd_wptr_adv3;
	reg eilbufmgrecd_wptr_adv7;
	reg eilbufmgrecd_wptr_adv5;
	reg eilbufmgrecd_wptr1;
	reg eilbufmgrecd_wptr0;
	reg eilbufmgrecd_wptr5;
	reg eilbufmgrecd_wptr7;
	reg eilbufmgrerd_wptr_adv1;
	reg d2p_ihb_addr4;
	reg d2p_ihb_addr2;
	reg eilbufmgrecd_wptr_adv1;
	reg eilbufmgrerd_wptr_adv5;
	reg eilbufmgrerd_wptr_adv7;
	reg eilbufmgrerd_wptr_adv3;
	reg eildatafsmnum_rds2;
	reg eildatafsmnum_rds3;
	reg eildatafsmnum_rds1;
	reg eildatafsmnum_rds4;
	reg eildatafsmnum_rds5;
	reg eilbufmgrerh_rptr3;
	reg eilbufmgrerh_rptr2;
	reg eilbufmgrerh_rptr0;
	reg eildatafsmnum_wrs_adv0;
	reg eildatafsmnum_wrs_adv5;
	reg eilbufmgrecd_wptr3;
	reg eilbufmgrerd_wptr3;
	reg eilbufmgrerd_wptr0;
	reg eilbufmgrerd_wptr1;
	reg eilbufmgrerd_wptr7;
	reg eilbufmgrerd_wptr5;
	reg eilbufmgrecd_wptr_adv0;
	reg n12895;
	reg eilbufmgrerd_wptr_adv0;
	reg iilcrdtcntibc_nhc4;
	reg iilcrdtcntibc_nhc7;
	reg iilcrdtcntibc_nhc2;
	reg d2p_cto_ackttt;
	reg eilbufmgrecd_wptr_adv2;
	reg eilbufmgrecd_wptr_adv4;
	reg iilcrdtcntibc_pdc10;
	reg iilcrdtcntibc_phc6;
	reg eilbufmgrerd_wptr_adv2;
	reg eilbufmgrecd_wptr4;
	reg eilbufmgrerd_wptr_adv4;
	reg n14734;
	reg n8914;
	reg y2k_rcd_deqttt;
	reg n15419;
	reg eilbufmgrecd_wptr2;
	reg n15420;
	reg eildatafsmnum_wrs_adv1;
	reg eildatafsmnum_wrs_adv3;
	reg eildatafsmnum_wrs_adv2;
	reg eilbufmgrerd_wptr2;
	reg eilbufmgrerd_wptr4;
	reg n14249;
	reg n14248;
	reg n14250;
	reg iilxfrfsmpio_tag_drain3;
	reg n13577;
	reg iilcrdtcntibc_pdc11;
	reg iilcrdtcntibc_phc7;
	reg iilcrdtcntibc_phc3;
	reg n15399;
	reg n15392;
	reg n15393;
	reg y2k_buf_addr7;
	reg n8757;
	reg n8756;
	reg n8755;
	reg n8754;
	reg n8753;
	reg n8752;
	reg n8751;
	reg n8750;
	reg n8759;
	reg n8758;
	reg n8744;
`endif
