`ifdef _COV_
`include "dmu_ilu_tick_defines_netlist_PRankNetlist_32.v"
`include "dmu_ilu_tick_defines_netlist_PRankNetlist_64.v"
`include "dmu_ilu_tick_defines_netlist_PRankNetlist_128.v"
`include "dmu_ilu_tick_defines_netlist_PRankNetlist_256.v"
/*
`include "dmu_ilu_tick_defines_netlist_HybrSel_32.v"
`include "dmu_ilu_tick_defines_netlist_HybrSel_64.v"
`include "dmu_ilu_tick_defines_netlist_HybrSel_128.v"
`include "dmu_ilu_tick_defines_netlist_HybrSel_256.v"
*/
`endif

module dmu_ilu_cov_bench;

reg [0:0] j2d_instance_id;
reg [7:0] k2y_buf_addr;
reg [127:0] k2y_buf_data;
reg [3:0] k2y_buf_dpar;
reg [31:0] k2y_csr_ring_out;
reg [5:0] k2y_dbg_sel_a;
reg [5:0] k2y_dbg_sel_b;
reg [4:0] k2y_dou_dptr;
reg [123:0] k2y_rcd;
reg [8:0] k2y_rel_rcd;
reg [95:0] p2d_csr_rcd;
reg [4:0] p2d_cto_tag;
reg [7:0] p2d_ecd_rptr;
reg [5:0] p2d_ech_rptr;
reg [7:0] p2d_erd_rptr;
reg [5:0] p2d_erh_rptr;
reg [127:0] p2d_idb_data;
reg [3:0] p2d_idb_dpar;
reg [127:0] p2d_ihb_data;
reg [3:0] p2d_ihb_dpar;
reg [6:0] p2d_ihb_wptr;
reg [2:0] p2d_mps;
reg [4:0] p2d_spare;
reg l1clk;
reg rst_wmr_;
reg j2d_rst_l;
reg j2d_por_l;
reg rst_dmu_async_por_;
reg k2y_buf_addr_vld_monitor;
reg k2y_dou_err;
reg k2y_dou_vld;
reg k2y_rcd_deq;
reg k2y_rcd_enq;
reg k2y_rel_enq;
reg p2d_ce_int;
reg p2d_csr_ack;
reg p2d_csr_req;
reg p2d_cto_req;
reg p2d_drain;
reg p2d_ibc_ack;
reg p2d_oe_int;
reg p2d_ue_int;
reg tcu_test_protect;


wire [95:0] d2p_csr_rcd;
wire [5:0] d2p_ech_wptr;
wire [127:0] d2p_edb_data;
wire [3:0] d2p_edb_dpar;
wire [7:0] d2p_edb_addr;
wire [127:0] d2p_ehb_data;
wire [3:0] d2p_ehb_dpar;
wire [5:0] d2p_ehb_addr;
wire [5:0] d2p_erh_wptr;
wire [7:0] d2p_ibc_nhc;
wire [11:0] d2p_ibc_pdc;
wire [7:0] d2p_ibc_phc;
wire [7:0] d2p_idb_addr;
wire [5:0] d2p_ihb_addr;
wire [4:0] d2p_spare;
wire [7:0] y2k_buf_addr;
wire [127:0] y2k_buf_data;
wire [3:0] y2k_buf_dpar;
wire [31:0] y2k_csr_ring_in;
wire [7:0] y2k_dbg_a;
wire [7:0] y2k_dbg_b;
wire [2:0] y2k_mps;
wire [115:0] y2k_rcd;
wire [8:0] y2k_rel_rcd;
wire [1:0] dmu_psr_rate_scale;
wire d2p_csr_ack;
wire d2p_csr_req;
wire d2p_cto_ack;
wire d2p_edb_we;
wire d2p_ehb_we;
wire d2p_ibc_req;
wire d2p_ihb_rd;
wire y2k_buf_addr_vld_monitor;
wire y2k_int_l;
wire y2k_rcd_deq;
wire y2k_rcd_enq;
wire y2k_rel_enq;
wire dmu_psr_pll_en_sds0;
wire dmu_psr_pll_en_sds1;
wire dmu_psr_rx_en_b0_sds0;
wire dmu_psr_rx_en_b1_sds0;
wire dmu_psr_rx_en_b2_sds0;
wire dmu_psr_rx_en_b3_sds0;
wire dmu_psr_rx_en_b0_sds1;
wire dmu_psr_rx_en_b1_sds1;
wire dmu_psr_rx_en_b2_sds1;
wire dmu_psr_rx_en_b3_sds1;
wire dmu_psr_tx_en_b0_sds0;
wire dmu_psr_tx_en_b1_sds0;
wire dmu_psr_tx_en_b2_sds0;
wire dmu_psr_tx_en_b3_sds0;
wire dmu_psr_tx_en_b0_sds1;
wire dmu_psr_tx_en_b1_sds1;
wire dmu_psr_tx_en_b2_sds1;
wire dmu_psr_tx_en_b3_sds1;
wire il2cl_gr_16;

`ifdef _COV_
`include "dmu_ilu_trace_reg_PRankNetlist_32.v"
`include "dmu_ilu_trace_reg_PRankNetlist_64.v"
`include "dmu_ilu_trace_reg_PRankNetlist_128.v"
`include "dmu_ilu_trace_reg_PRankNetlist_256.v"
/*
`include "dmu_ilu_trace_reg_HybrSel_32.v"
`include "dmu_ilu_trace_reg_HybrSel_64.v"
`include "dmu_ilu_trace_reg_HybrSel_128.v"
`include "dmu_ilu_trace_reg_HybrSel_256.v"
*/

`endif

dmu_ilu dmu_ilu_ (.*);

initial begin
    // Instance ID (From OpenSPARC Source Code)
    j2d_instance_id = 1'b0;
    // Clock and Rest Signals
    l1clk = 0;
    rst_wmr_ = 0;
    j2d_por_l = 0;
    j2d_rst_l = 0;
    rst_dmu_async_por_ = 0;
    // PCIE FC credits interface to TLU
    p2d_ibc_ack = 0;
    // CTO Interface - PIO Completion Time Out
    p2d_cto_req = 0;
    // Drain and Misc Interface
    p2d_drain = 0;
    p2d_ue_int = 0;
    p2d_ce_int = 0;
    p2d_oe_int = 0;
    // Data Path
    k2y_buf_addr_vld_monitor = 0;
    // Record interface to TMU
    k2y_rcd_deq = 0;
    k2y_rcd_enq = 0;
    // Release interface with TMU
    k2y_rel_enq = 0;
    // DOU DMA Rd CPL Buffer
    k2y_dou_err = 0;
    k2y_dou_vld = 0;
    // CSR Ring to TLU
    p2d_csr_ack = 0;
    p2d_csr_req = 0;
    // To PEU for PLL-Enable
    tcu_test_protect = 0;
    // Ptrs
	p2d_ecd_rptr = 0;
	p2d_ech_rptr = 0;
	p2d_erd_rptr = 0;
	p2d_erh_rptr = 0;

    p2d_ihb_wptr = 0;
end


initial begin
    // In 30 time units reset will be finished
    $display("Intializing design\n");
    #10;
    j2d_por_l = 1;
    #10;
    j2d_rst_l = 1;
    #10;
    rst_wmr_ = 1;
    #10;
    rst_dmu_async_por_ = 1;
    $display("Reset sequence ended: %d\n", $time);
end

always begin
    // 350 MHz clock
    #1.5 l1clk = ~l1clk;
end

`ifdef _COV_
`include "dmu_ilu_stim_netlist_PRankNetlist_32.v"
`include "dmu_ilu_stim_netlist_PRankNetlist_64.v"
`include "dmu_ilu_stim_netlist_PRankNetlist_128.v"
`include "dmu_ilu_stim_netlist_PRankNetlist_256.v"
/*
`include "dmu_ilu_stim_netlist_HybrSel_32.v"
`include "dmu_ilu_stim_netlist_HybrSel_64.v"
`include "dmu_ilu_stim_netlist_HybrSel_128.v"
`include "dmu_ilu_stim_netlist_HybrSel_256.v"
*/
`endif

endmodule
