`ifdef PRankNetlist_32
	initial begin
		#0 d2p_ihb_addr5 = 1'b0;
	end
	assign `SIG19 = d2p_ihb_addr5;

	initial begin
		#0 d2p_ihb_addr3 = 1'b0;
	end
	assign `SIG30 = d2p_ihb_addr3;

	initial begin
		#0 d2p_ihb_addr1 = 1'b0;
	end
	assign `SIG31 = d2p_ihb_addr1;

	initial begin
		#0 d2p_ihb_addr0 = 1'b0;
	end
	assign `SIG20 = d2p_ihb_addr0;

	initial begin
		#0 eilrcd_is_cpl_reg = 1'b0;
	end
	assign `SIG4 = eilrcd_is_cpl_reg;

	initial begin
		#0 eilbufmgrerh_wptr4 = 1'b0;
	end
	assign `SIG12 = eilbufmgrerh_wptr4;

	initial begin
		#0 n13584 = 1'b0;
		#42250.0 n13584 = 1'b1;
	end
	assign `SIG11 = n13584;

	initial begin
		#0 n13581 = 1'b0;
	end
	assign `SIG0 = n13581;

	initial begin
		#0 n13583 = 1'b0;
	end
	assign `SIG8 = n13583;

	initial begin
		#0 n13582 = 1'b0;
	end
	assign `SIG3 = n13582;

	initial begin
		#0 n15152 = 1'b0;
		#42250.0 n15152 = 1'bx;
	end
	assign `SIG21 = n15152;

	initial begin
		#0 n15154 = 1'b0;
		#42250.0 n15154 = 1'bx;
	end
	assign `SIG22 = n15154;

	initial begin
		#0 n15157 = 1'b0;
		#42250.0 n15157 = 1'bx;
	end
	assign `SIG23 = n15157;

	initial begin
		#0 n15159 = 1'b0;
		#42250.0 n15159 = 1'bx;
	end
	assign `SIG24 = n15159;

	initial begin
		#0 eilbufmgrecd_wptr_adv6 = 1'b0;
	end
	assign `SIG26 = eilbufmgrecd_wptr_adv6;

	initial begin
		#0 n15149 = 1'b0;
		#42250.0 n15149 = 1'bx;
	end
	assign `SIG25 = n15149;

	initial begin
		#0 eilbufmgrecd_wptr6 = 1'b0;
	end
	assign `SIG29 = eilbufmgrecd_wptr6;

	initial begin
		#0 n15433 = 1'b0;
	end
	assign `SIG1 = n15433;

	initial begin
		#0 eilbufmgrerh_rptr1 = 1'b0;
	end
	assign `SIG14 = eilbufmgrerh_rptr1;

	initial begin
		#0 y2k_buf_addr5 = 1'b0;
	end
	assign `SIG6 = y2k_buf_addr5;

	initial begin
		#0 y2k_buf_addr4 = 1'b0;
	end
	assign `SIG9 = y2k_buf_addr4;

	initial begin
		#0 y2k_buf_addr6 = 1'b0;
	end
	assign `SIG10 = y2k_buf_addr6;

	initial begin
		#0 y2k_buf_addr1 = 1'b0;
	end
	assign `SIG15 = y2k_buf_addr1;

	initial begin
		#0 y2k_buf_addr0 = 1'b0;
	end
	assign `SIG16 = y2k_buf_addr0;

	initial begin
		#0 y2k_buf_addr3 = 1'b0;
	end
	assign `SIG7 = y2k_buf_addr3;

	initial begin
		#0 y2k_buf_addr2 = 1'b0;
	end
	assign `SIG2 = y2k_buf_addr2;

	initial begin
		#0 n14699 = 1'b0;
	end
	assign `SIG5 = n14699;

	initial begin
		#0 eilbufmgrech_wptr4 = 1'b0;
	end
	assign `SIG13 = eilbufmgrech_wptr4;

	initial begin
		#0 eilbufmgrech_wptr1 = 1'b0;
	end
	assign `SIG17 = eilbufmgrech_wptr1;

	initial begin
		#0 eilbufmgrech_wptr0 = 1'b0;
	end
	assign `SIG27 = eilbufmgrech_wptr0;

	initial begin
		#0 eilbufmgrech_wptr3 = 1'b0;
	end
	assign `SIG18 = eilbufmgrech_wptr3;

	initial begin
		#0 eilbufmgrech_wptr2 = 1'b0;
	end
	assign `SIG28 = eilbufmgrech_wptr2;

	initial begin
		 #132250.0 $finish;
	end

`endif
