`ifdef SigSeT_1_32
	initial begin
		#0 n13360dummy = 1'b0;
		#93100.0 n13360dummy = 1'b1;
	end
	assign `SIG25 = n13360dummy;

	initial begin
		#0 n13358dummy = 1'b0;
		#93100.0 n13358dummy = 1'b1;
	end
	assign `SIG23 = n13358dummy;

	initial begin
		#0 d2j_data_vldttt = 1'b0;
	end
	assign `SIG4 = d2j_data_vldttt;

	initial begin
		#0 n11386dummy = 1'b0;
		#93100.0 n11386dummy = 1'bx;
	end
	assign `SIG28 = n11386dummy;

	initial begin
		#0 n13395dummy = 1'b0;
	end
	assign `SIG0 = n13395dummy;

	initial begin
		#0 d2j_ctag10 = 1'b0;
	end
	assign `SIG18 = d2j_ctag10;

	initial begin
		#0 n11354dummy = 1'b0;
		#93100.0 n11354dummy = 1'bx;
	end
	assign `SIG29 = n11354dummy;

	initial begin
		#0 cl2mm_tcr_ackttt = 1'b0;
		#93400.0 cl2mm_tcr_ackttt = 1'b1;
		#87600.0 cl2mm_tcr_ackttt = 1'b0;
	end
	assign `SIG15 = cl2mm_tcr_ackttt;

	initial begin
		#0 n12098dummy = 1'b0;
		#93100.0 n12098dummy = 1'b1;
	end
	assign `SIG26 = n12098dummy;

	initial begin
		#0 n18450 = 1'b0;
	end
	assign `SIG19 = n18450;

	initial begin
		#0 ctm2crm_rcd6 = 1'b0;
	end
	assign `SIG12 = ctm2crm_rcd6;

	initial begin
		#0 ctm2crm_rcd7 = 1'b0;
	end
	assign `SIG27 = ctm2crm_rcd7;

	initial begin
		#0 ctm2crm_rcd5 = 1'b0;
	end
	assign `SIG5 = ctm2crm_rcd5;

	initial begin
		#0 ctmicr_fifofifo_count2 = 1'b0;
	end
	assign `SIG1 = ctmicr_fifofifo_count2;

	initial begin
		#0 ctmicr_fifofifo_count0 = 1'b0;
		#160600.0 ctmicr_fifofifo_count0 = 1'b1;
	end
	assign `SIG2 = ctmicr_fifofifo_count0;

	initial begin
		#0 ctmicr_fifofifo_count4 = 1'b0;
	end
	assign `SIG3 = ctmicr_fifofifo_count4;

	initial begin
		#0 d2j_ctag11 = 1'b0;
		#93400.0 d2j_ctag11 = 1'b1;
		#87900.0 d2j_ctag11 = 1'b0;
	end
	assign `SIG20 = d2j_ctag11;

	initial begin
		#0 n11358dummy = 1'b0;
		#93100.0 n11358dummy = 1'bx;
	end
	assign `SIG24 = n11358dummy;

	initial begin
		#0 n11390dummy = 1'b0;
		#93100.0 n11390dummy = 1'bx;
	end
	assign `SIG22 = n11390dummy;

	initial begin
		#0 d2j_cmd2 = 1'b0;
	end
	assign `SIG14 = d2j_cmd2;

	initial begin
		#0 d2j_cmd1 = 1'b0;
		#93100.0 d2j_cmd1 = 1'b1;
		#300.0 d2j_cmd1 = 1'b0;
		#300.0 d2j_cmd1 = 1'b1;
		#87600.0 d2j_cmd1 = 1'b0;
	end
	assign `SIG31 = d2j_cmd1;

	initial begin
		#0 d2j_cmd0 = 1'b0;
		#93400.0 d2j_cmd0 = 1'b1;
		#87900.0 d2j_cmd0 = 1'b0;
	end
	assign `SIG30 = d2j_cmd0;

	initial begin
		#0 d2j_ctag9 = 1'b0;
	end
	assign `SIG17 = d2j_ctag9;

	initial begin
		#0 d2j_ctag8 = 1'b0;
	end
	assign `SIG16 = d2j_ctag8;

	initial begin
		#0 d2j_ctag3 = 1'b0;
		#93400.0 d2j_ctag3 = 1'b1;
		#300.0 d2j_ctag3 = 1'bx;
		#87600.0 d2j_ctag3 = 1'b0;
	end
	assign `SIG9 = d2j_ctag3;

	initial begin
		#0 d2j_ctag2 = 1'b0;
		#93400.0 d2j_ctag2 = 1'b1;
		#300.0 d2j_ctag2 = 1'bx;
		#87600.0 d2j_ctag2 = 1'b1;
	end
	assign `SIG8 = d2j_ctag2;

	initial begin
		#0 d2j_ctag1 = 1'b0;
		#93400.0 d2j_ctag1 = 1'b1;
		#300.0 d2j_ctag1 = 1'bx;
		#87600.0 d2j_ctag1 = 1'b0;
	end
	assign `SIG7 = d2j_ctag1;

	initial begin
		#0 d2j_ctag0 = 1'b0;
		#93400.0 d2j_ctag0 = 1'b1;
		#300.0 d2j_ctag0 = 1'bx;
		#87600.0 d2j_ctag0 = 1'b1;
	end
	assign `SIG11 = d2j_ctag0;

	initial begin
		#0 d2j_ctag7 = 1'b0;
	end
	assign `SIG13 = d2j_ctag7;

	initial begin
		#0 d2j_ctag6 = 1'b0;
	end
	assign `SIG21 = d2j_ctag6;

	initial begin
		#0 d2j_ctag5 = 1'b0;
		#93400.0 d2j_ctag5 = 1'bx;
		#87900.0 d2j_ctag5 = 1'b0;
	end
	assign `SIG6 = d2j_ctag5;

	initial begin
		#0 d2j_ctag4 = 1'b0;
		#93400.0 d2j_ctag4 = 1'b1;
		#300.0 d2j_ctag4 = 1'b0;
		#300.0 d2j_ctag4 = 1'bx;
		#87300.0 d2j_ctag4 = 1'b1;
	end
	assign `SIG10 = d2j_ctag4;

	initial begin
		 #183100.0 $finish;
	end

`endif
