`ifdef SigSeT_1_64
	`define SIG0 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.rd_ptr[0]
	`define SIG1 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_count [2]
	`define SIG2 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_count [0]
	`define SIG3 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_count [4]
	`define SIG4 dmu_clu_cov_bench.dmu_clu_.ctm.cmdctlfsm.d2j_dvld_s3
	`define SIG5 dmu_clu_cov_bench.dmu_clu_.ctm2crm_rcd[5]
	`define SIG6 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[5]
	`define SIG7 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[1]
	`define SIG8 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[2]
	`define SIG9 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[3]
	`define SIG10 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[4]
	`define SIG11 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[0]
	`define SIG12 dmu_clu_cov_bench.dmu_clu_.ctm2crm_rcd[6]
	`define SIG13 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[7]
	`define SIG14 dmu_clu_cov_bench.dmu_clu_.d2j_cmd[2]
	`define SIG15 dmu_clu_cov_bench.dmu_clu_.ctm.cmdctlfsm.cl2mm_tcr_ack
	`define SIG16 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[8]
	`define SIG17 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[9]
	`define SIG18 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[10]
	`define SIG19 dmu_clu_cov_bench.dmu_clu_.ctm.cmdctlfsm.proc_pio_err_d
	`define SIG20 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[11]
	`define SIG21 dmu_clu_cov_bench.dmu_clu_.d2j_ctag[6]
	`define SIG22 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[1][2]
	`define SIG23 dmu_clu_cov_bench.dmu_clu_.ctm.datactlfsm.cur_state[12]
	`define SIG24 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[0][2]
	`define SIG25 dmu_clu_cov_bench.dmu_clu_.ctm.datactlfsm.cur_state[20]
	`define SIG26 dmu_clu_cov_bench.dmu_clu_.ctm.datactlfsm.cur_state[4]
	`define SIG27 dmu_clu_cov_bench.dmu_clu_.ctm2crm_rcd[7]
	`define SIG28 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[1][1]
	`define SIG29 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[0][1]
	`define SIG30 dmu_clu_cov_bench.dmu_clu_.d2j_cmd[0]
	`define SIG31 dmu_clu_cov_bench.dmu_clu_.d2j_cmd[1]
	`define SIG32 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[0][3]
	`define SIG33 dmu_clu_cov_bench.dmu_clu_.ctm.datactlfsm.cur_state[8]
	`define SIG34 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[1][3]
	`define SIG35 dmu_clu_cov_bench.dmu_clu_.d2j_cmd[3]
	`define SIG36 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[0][5]
	`define SIG37 dmu_clu_cov_bench.dmu_clu_.ctm.dbg1_bus[1][5]
	`define SIG38 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[7][60]
	`define SIG39 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.wr_ptr[1]
	`define SIG40 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.wr_ptr[0]
	`define SIG41 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.wr_ptr[2]
	`define SIG42 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.wr_ptr[3]
	`define SIG43 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[8][60]
	`define SIG44 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[5][60]
	`define SIG45 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[6][60]
	`define SIG46 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[2][60]
	`define SIG47 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[3][60]
	`define SIG48 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[4][60]
	`define SIG49 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[1][60]
	`define SIG50 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[0][60]
	`define SIG51 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[9][60]
	`define SIG52 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[7][56]
	`define SIG53 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[5][56]
	`define SIG54 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[6][56]
	`define SIG55 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[3][56]
	`define SIG56 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[4][56]
	`define SIG57 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[2][56]
	`define SIG58 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[0][56]
	`define SIG59 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[1][56]
	`define SIG60 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[7][58]
	`define SIG61 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[8][56]
	`define SIG62 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[7][54]
	`define SIG63 dmu_clu_cov_bench.dmu_clu_.ctm.icr_fifo.fifo_ram[5][58]
`endif
