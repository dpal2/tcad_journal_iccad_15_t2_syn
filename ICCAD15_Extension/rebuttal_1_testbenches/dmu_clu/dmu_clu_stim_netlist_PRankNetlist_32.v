`ifdef PRankNetlist_32
	initial begin
		#0 n18412 = 1'b0;
		#47100.0 n18412 = 1'b1;
		#53100.0 n18412 = 1'b0;
	end
	assign `SIG18 = n18412;

	initial begin
		#0 n18413 = 1'b0;
		#133800.0 n18413 = 1'b1;
	end
	assign `SIG17 = n18413;

	initial begin
		#0 n18418 = 1'b0;
		#47100.0 n18418 = 1'b1;
		#34500.0 n18418 = 1'b0;
	end
	assign `SIG2 = n18418;

	initial begin
		#0 n16184 = 1'b0;
		#47100.0 n16184 = 1'b1;
	end
	assign `SIG8 = n16184;

	initial begin
		#0 n16198 = 1'b0;
		#47100.0 n16198 = 1'b1;
	end
	assign `SIG26 = n16198;

	initial begin
		#0 n16212 = 1'b0;
	end
	assign `SIG13 = n16212;

	initial begin
		#0 cl2tm_int_rptr2 = 1'b0;
	end
	assign `SIG7 = cl2tm_int_rptr2;

	initial begin
		#0 n16215 = 1'b0;
		#47100.0 n16215 = 1'b1;
	end
	assign `SIG25 = n16215;

	initial begin
		#0 cl2di_addr0 = 1'b0;
	end
	assign `SIG16 = cl2di_addr0;

	initial begin
		#0 n18436 = 1'b0;
		#47100.0 n18436 = 1'b1;
	end
	assign `SIG11 = n18436;

	initial begin
		#0 n16286 = 1'b0;
		#47100.0 n16286 = 1'b1;
	end
	assign `SIG20 = n16286;

	initial begin
		#0 n16288 = 1'b0;
		#47100.0 n16288 = 1'b1;
	end
	assign `SIG21 = n16288;

	initial begin
		#0 n16206 = 1'b0;
		#47100.0 n16206 = 1'b1;
	end
	assign `SIG27 = n16206;

	initial begin
		#0 n16222 = 1'b0;
		#47100.0 n16222 = 1'b1;
	end
	assign `SIG22 = n16222;

	initial begin
		#0 ctmbufmgrdou_sts_vctr20 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr20 = 1'b1;
	end
	assign `SIG24 = ctmbufmgrdou_sts_vctr20;

	initial begin
		#0 cl2tm_dma_rptr4 = 1'b0;
	end
	assign `SIG6 = cl2tm_dma_rptr4;

	initial begin
		#0 n18459 = 1'b0;
	end
	assign `SIG3 = n18459;

	initial begin
		#0 n18457 = 1'b0;
	end
	assign `SIG4 = n18457;

	initial begin
		#0 ctmbufmgrdou_sts_vctr27 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr27 = 1'b1;
	end
	assign `SIG28 = ctmbufmgrdou_sts_vctr27;

	initial begin
		#0 ctmbufmgrdou_sts_vctr26 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr26 = 1'b1;
	end
	assign `SIG29 = ctmbufmgrdou_sts_vctr26;

	initial begin
		#0 ctmbufmgrdou_sts_vctr25 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr25 = 1'b1;
	end
	assign `SIG30 = ctmbufmgrdou_sts_vctr25;

	initial begin
		#0 ctmbufmgrdou_sts_vctr24 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr24 = 1'b1;
	end
	assign `SIG31 = ctmbufmgrdou_sts_vctr24;

	initial begin
		#0 ctmbufmgrdou_sts_vctr21 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr21 = 1'b1;
	end
	assign `SIG23 = ctmbufmgrdou_sts_vctr21;

	initial begin
		#0 n18444 = 1'b0;
	end
	assign `SIG19 = n18444;

	initial begin
		#0 n18446 = 1'b0;
	end
	assign `SIG10 = n18446;

	initial begin
		#0 ctmbufmgrpio_blk_addr3 = 1'b0;
	end
	assign `SIG14 = ctmbufmgrpio_blk_addr3;

	initial begin
		#0 ctmbufmgrpio_blk_addr0 = 1'b0;
	end
	assign `SIG9 = ctmbufmgrpio_blk_addr0;

	initial begin
		#0 n18462 = 1'b0;
	end
	assign `SIG0 = n18462;

	initial begin
		#0 n18461 = 1'b0;
	end
	assign `SIG5 = n18461;

	initial begin
		#0 n18460 = 1'b0;
	end
	assign `SIG1 = n18460;

	initial begin
		#0 cl2mm_tdr_rcd135 = 1'b0;
		#99000.0 cl2mm_tdr_rcd135 = 1'bx;
	end
	assign `SIG12 = cl2mm_tdr_rcd135;

	initial begin
		#0 ctmbufmgrpio_blk_addr2 = 1'b0;
	end
	assign `SIG15 = ctmbufmgrpio_blk_addr2;

	initial begin
		 #137100.0 $finish;
	end

`endif
