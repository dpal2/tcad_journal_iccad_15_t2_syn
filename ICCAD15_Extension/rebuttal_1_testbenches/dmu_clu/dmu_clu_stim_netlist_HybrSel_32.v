`ifdef HybrSel_32
	initial begin
		#0 d2j_addr31 = 1'b0;
		#62900.0 d2j_addr31 = 1'b1;
		#300.0 d2j_addr31 = 1'bx;
		#27600.0 d2j_addr31 = 1'b0;
	end
	assign `SIG18 = d2j_addr31;

	initial begin
		#0 d2j_addr33 = 1'b0;
		#62900.0 d2j_addr33 = 1'b1;
		#300.0 d2j_addr33 = 1'bx;
		#27600.0 d2j_addr33 = 1'b0;
	end
	assign `SIG29 = d2j_addr33;

	initial begin
		#0 d2j_addr32 = 1'b0;
		#62900.0 d2j_addr32 = 1'b1;
		#300.0 d2j_addr32 = 1'bx;
		#27600.0 d2j_addr32 = 1'b0;
	end
	assign `SIG19 = d2j_addr32;

	initial begin
		#0 d2j_addr34 = 1'b0;
		#62900.0 d2j_addr34 = 1'b1;
		#300.0 d2j_addr34 = 1'bx;
		#27600.0 d2j_addr34 = 1'b0;
	end
	assign `SIG23 = d2j_addr34;

	initial begin
		#0 d2j_addr36 = 1'b0;
		#62900.0 d2j_addr36 = 1'b1;
		#300.0 d2j_addr36 = 1'bx;
		#27600.0 d2j_addr36 = 1'b0;
	end
	assign `SIG28 = d2j_addr36;

	initial begin
		#0 d2j_addr24 = 1'b0;
		#62900.0 d2j_addr24 = 1'bx;
		#27900.0 d2j_addr24 = 1'b0;
	end
	assign `SIG24 = d2j_addr24;

	initial begin
		#0 n11407dummy = 1'b0;
		#62900.0 n11407dummy = 1'bx;
	end
	assign `SIG1 = n11407dummy;

	initial begin
		#0 n13581dummy = 1'b0;
		#62900.0 n13581dummy = 1'b1;
	end
	assign `SIG3 = n13581dummy;

	initial begin
		#0 d2j_addr1 = 1'b0;
		#62900.0 d2j_addr1 = 1'b1;
		#300.0 d2j_addr1 = 1'b0;
		#300.0 d2j_addr1 = 1'bx;
		#27300.0 d2j_addr1 = 1'b0;
	end
	assign `SIG17 = d2j_addr1;

	initial begin
		#0 d2j_ctag10 = 1'b0;
	end
	assign `SIG9 = d2j_ctag10;

	initial begin
		#0 d2j_addr5 = 1'b0;
		#62900.0 d2j_addr5 = 1'b1;
		#300.0 d2j_addr5 = 1'b0;
		#300.0 d2j_addr5 = 1'bx;
		#27300.0 d2j_addr5 = 1'b0;
	end
	assign `SIG16 = d2j_addr5;

	initial begin
		#0 d2j_addr4 = 1'b0;
		#62900.0 d2j_addr4 = 1'b1;
		#300.0 d2j_addr4 = 1'b0;
		#300.0 d2j_addr4 = 1'b1;
		#300.0 d2j_addr4 = 1'bx;
		#27000.0 d2j_addr4 = 1'b0;
	end
	assign `SIG15 = d2j_addr4;

	initial begin
		#0 d2j_addr9 = 1'b0;
		#62900.0 d2j_addr9 = 1'b1;
		#300.0 d2j_addr9 = 1'b0;
		#300.0 d2j_addr9 = 1'b1;
		#300.0 d2j_addr9 = 1'bx;
		#27000.0 d2j_addr9 = 1'b0;
	end
	assign `SIG25 = d2j_addr9;

	initial begin
		#0 d2j_addr12 = 1'b0;
		#62900.0 d2j_addr12 = 1'bx;
		#27900.0 d2j_addr12 = 1'b0;
	end
	assign `SIG22 = d2j_addr12;

	initial begin
		#0 d2j_addr17 = 1'b0;
		#62900.0 d2j_addr17 = 1'b1;
		#300.0 d2j_addr17 = 1'bx;
		#27600.0 d2j_addr17 = 1'b0;
	end
	assign `SIG20 = d2j_addr17;

	initial begin
		#0 d2j_addr16 = 1'b0;
		#62900.0 d2j_addr16 = 1'b1;
		#300.0 d2j_addr16 = 1'bx;
		#27600.0 d2j_addr16 = 1'b0;
	end
	assign `SIG31 = d2j_addr16;

	initial begin
		#0 n18125 = 1'b0;
	end
	assign `SIG6 = n18125;

	initial begin
		#0 ctm2crm_rcd4 = 1'b0;
	end
	assign `SIG12 = ctm2crm_rcd4;

	initial begin
		#0 n13396dummy = 1'b0;
		#62900.0 n13396dummy = 1'b1;
	end
	assign `SIG2 = n13396dummy;

	initial begin
		#0 ctm2crm_rcd3 = 1'b0;
		#62900.0 ctm2crm_rcd3 = 1'b1;
		#18000.0 ctm2crm_rcd3 = 1'b0;
	end
	assign `SIG5 = ctm2crm_rcd3;

	initial begin
		#0 ctm2crm_rcd0 = 1'b0;
		#62900.0 ctm2crm_rcd0 = 1'b1;
		#18000.0 ctm2crm_rcd0 = 1'b0;
	end
	assign `SIG11 = ctm2crm_rcd0;

	initial begin
		#0 d2j_cmd3 = 1'b0;
	end
	assign `SIG8 = d2j_cmd3;

	initial begin
		#0 d2j_data_par3 = 1'b0;
		#62900.0 d2j_data_par3 = 1'bx;
	end
	assign `SIG0 = d2j_data_par3;

	initial begin
		#0 n18474 = 1'b0;
		#62900.0 n18474 = 1'b1;
	end
	assign `SIG4 = n18474;

	initial begin
		#0 d2j_addr22 = 1'b0;
		#62900.0 d2j_addr22 = 1'b1;
		#300.0 d2j_addr22 = 1'bx;
		#27600.0 d2j_addr22 = 1'b0;
	end
	assign `SIG26 = d2j_addr22;

	initial begin
		#0 d2j_addr23 = 1'b0;
		#62900.0 d2j_addr23 = 1'b1;
		#300.0 d2j_addr23 = 1'bx;
		#27600.0 d2j_addr23 = 1'b0;
	end
	assign `SIG27 = d2j_addr23;

	initial begin
		#0 d2j_addr27 = 1'b0;
		#62900.0 d2j_addr27 = 1'b1;
		#300.0 d2j_addr27 = 1'bx;
		#27600.0 d2j_addr27 = 1'b0;
	end
	assign `SIG30 = d2j_addr27;

	initial begin
		#0 d2j_addr29 = 1'b0;
		#62900.0 d2j_addr29 = 1'b1;
		#300.0 d2j_addr29 = 1'bx;
		#27600.0 d2j_addr29 = 1'b0;
	end
	assign `SIG21 = d2j_addr29;

	initial begin
		#0 d2j_cmd1 = 1'b0;
		#80900.0 d2j_cmd1 = 1'b1;
	end
	assign `SIG7 = d2j_cmd1;

	initial begin
		#0 d2j_ctag3 = 1'b0;
		#62900.0 d2j_ctag3 = 1'b1;
		#300.0 d2j_ctag3 = 1'b0;
		#300.0 d2j_ctag3 = 1'bx;
		#27300.0 d2j_ctag3 = 1'b0;
	end
	assign `SIG13 = d2j_ctag3;

	initial begin
		#0 d2j_ctag2 = 1'b0;
		#62900.0 d2j_ctag2 = 1'b1;
		#300.0 d2j_ctag2 = 1'bx;
		#27600.0 d2j_ctag2 = 1'b0;
	end
	assign `SIG10 = d2j_ctag2;

	initial begin
		#0 d2j_ctag0 = 1'b0;
		#62900.0 d2j_ctag0 = 1'b1;
		#300.0 d2j_ctag0 = 1'bx;
		#27600.0 d2j_ctag0 = 1'b0;
	end
	assign `SIG14 = d2j_ctag0;

	initial begin
		 #152900.0 $finish;
	end

`endif
