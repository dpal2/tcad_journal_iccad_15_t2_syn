`ifdef PRankNetlist_64
	initial begin
		#0 n18412 = 1'b0;
		#47100.0 n18412 = 1'b1;
		#53100.0 n18412 = 1'b0;
	end
	assign `SIG18 = n18412;

	initial begin
		#0 n18413 = 1'b0;
		#133800.0 n18413 = 1'b1;
	end
	assign `SIG17 = n18413;

	initial begin
		#0 n18418 = 1'b0;
		#47100.0 n18418 = 1'b1;
		#34500.0 n18418 = 1'b0;
	end
	assign `SIG2 = n18418;

	initial begin
		#0 n16184 = 1'b0;
		#47100.0 n16184 = 1'b1;
	end
	assign `SIG8 = n16184;

	initial begin
		#0 n16198 = 1'b0;
		#47100.0 n16198 = 1'b1;
	end
	assign `SIG26 = n16198;

	initial begin
		#0 n16212 = 1'b0;
	end
	assign `SIG13 = n16212;

	initial begin
		#0 cl2tm_int_rptr2 = 1'b0;
	end
	assign `SIG7 = cl2tm_int_rptr2;

	initial begin
		#0 n16215 = 1'b0;
		#47100.0 n16215 = 1'b1;
	end
	assign `SIG25 = n16215;

	initial begin
		#0 cl2di_addr1 = 1'b0;
	end
	assign `SIG62 = cl2di_addr1;

	initial begin
		#0 cl2di_addr0 = 1'b0;
	end
	assign `SIG16 = cl2di_addr0;

	initial begin
		#0 n18436 = 1'b0;
		#47100.0 n18436 = 1'b1;
	end
	assign `SIG11 = n18436;

	initial begin
		#0 ctmbufmgrdou_sts_vctr3 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr3 = 1'b1;
	end
	assign `SIG47 = ctmbufmgrdou_sts_vctr3;

	initial begin
		#0 n16286 = 1'b0;
		#47100.0 n16286 = 1'b1;
	end
	assign `SIG20 = n16286;

	initial begin
		#0 n16288 = 1'b0;
		#47100.0 n16288 = 1'b1;
	end
	assign `SIG21 = n16288;

	initial begin
		#0 n16206 = 1'b0;
		#47100.0 n16206 = 1'b1;
	end
	assign `SIG27 = n16206;

	initial begin
		#0 n16222 = 1'b0;
		#47100.0 n16222 = 1'b1;
	end
	assign `SIG22 = n16222;

	initial begin
		#0 n16225 = 1'b0;
		#47100.0 n16225 = 1'b1;
	end
	assign `SIG34 = n16225;

	initial begin
		#0 ctmbufmgrdou_sts_vctr20 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr20 = 1'b1;
	end
	assign `SIG24 = ctmbufmgrdou_sts_vctr20;

	initial begin
		#0 cl2tm_dma_rptr4 = 1'b0;
	end
	assign `SIG6 = cl2tm_dma_rptr4;

	initial begin
		#0 ctmbufmgrdou_sts_vctr4 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr4 = 1'b1;
	end
	assign `SIG40 = ctmbufmgrdou_sts_vctr4;

	initial begin
		#0 ctmbufmgrdou_sts_vctr5 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr5 = 1'b1;
	end
	assign `SIG41 = ctmbufmgrdou_sts_vctr5;

	initial begin
		#0 ctmbufmgrdou_sts_vctr6 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr6 = 1'b1;
	end
	assign `SIG42 = ctmbufmgrdou_sts_vctr6;

	initial begin
		#0 ctmbufmgrdou_sts_vctr7 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr7 = 1'b1;
	end
	assign `SIG43 = ctmbufmgrdou_sts_vctr7;

	initial begin
		#0 ctmbufmgrdou_sts_vctr0 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr0 = 1'b1;
	end
	assign `SIG44 = ctmbufmgrdou_sts_vctr0;

	initial begin
		#0 ctmbufmgrdou_sts_vctr1 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr1 = 1'b1;
	end
	assign `SIG45 = ctmbufmgrdou_sts_vctr1;

	initial begin
		#0 ctmbufmgrdou_sts_vctr2 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr2 = 1'b1;
	end
	assign `SIG46 = ctmbufmgrdou_sts_vctr2;

	initial begin
		#0 n18459 = 1'b0;
	end
	assign `SIG3 = n18459;

	initial begin
		#0 n18457 = 1'b0;
	end
	assign `SIG4 = n18457;

	initial begin
		#0 ctmbufmgrdou_sts_vctr8 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr8 = 1'b1;
	end
	assign `SIG48 = ctmbufmgrdou_sts_vctr8;

	initial begin
		#0 ctmbufmgrdou_sts_vctr9 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr9 = 1'b1;
	end
	assign `SIG49 = ctmbufmgrdou_sts_vctr9;

	initial begin
		#0 ctmbufmgrdou_sts_vctr27 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr27 = 1'b1;
	end
	assign `SIG28 = ctmbufmgrdou_sts_vctr27;

	initial begin
		#0 ctmbufmgrdou_sts_vctr26 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr26 = 1'b1;
	end
	assign `SIG29 = ctmbufmgrdou_sts_vctr26;

	initial begin
		#0 ctmbufmgrdou_sts_vctr25 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr25 = 1'b1;
	end
	assign `SIG30 = ctmbufmgrdou_sts_vctr25;

	initial begin
		#0 ctmbufmgrdou_sts_vctr24 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr24 = 1'b1;
	end
	assign `SIG31 = ctmbufmgrdou_sts_vctr24;

	initial begin
		#0 ctmbufmgrdou_sts_vctr23 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr23 = 1'b1;
	end
	assign `SIG32 = ctmbufmgrdou_sts_vctr23;

	initial begin
		#0 ctmbufmgrdou_sts_vctr22 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr22 = 1'b1;
	end
	assign `SIG33 = ctmbufmgrdou_sts_vctr22;

	initial begin
		#0 ctmbufmgrdou_sts_vctr21 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr21 = 1'b1;
	end
	assign `SIG23 = ctmbufmgrdou_sts_vctr21;

	initial begin
		#0 n16224 = 1'b0;
		#47100.0 n16224 = 1'b1;
	end
	assign `SIG35 = n16224;

	initial begin
		#0 ctmbufmgrdou_sts_vctr29 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr29 = 1'b1;
	end
	assign `SIG36 = ctmbufmgrdou_sts_vctr29;

	initial begin
		#0 ctmbufmgrdou_sts_vctr28 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr28 = 1'b1;
	end
	assign `SIG37 = ctmbufmgrdou_sts_vctr28;

	initial begin
		#0 n18444 = 1'b0;
	end
	assign `SIG19 = n18444;

	initial begin
		#0 n18446 = 1'b0;
	end
	assign `SIG10 = n18446;

	initial begin
		#0 ctmbufmgrdou_sts_vctr30 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr30 = 1'b1;
	end
	assign `SIG38 = ctmbufmgrdou_sts_vctr30;

	initial begin
		#0 ctmbufmgrdou_sts_vctr31 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr31 = 1'b1;
	end
	assign `SIG39 = ctmbufmgrdou_sts_vctr31;

	initial begin
		#0 ctmbufmgrpio_blk_addr3 = 1'b0;
	end
	assign `SIG14 = ctmbufmgrpio_blk_addr3;

	initial begin
		#0 ctmbufmgrdou_sts_vctr13 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr13 = 1'b1;
	end
	assign `SIG57 = ctmbufmgrdou_sts_vctr13;

	initial begin
		#0 ctmbufmgrpio_blk_addr0 = 1'b0;
	end
	assign `SIG9 = ctmbufmgrpio_blk_addr0;

	initial begin
		#0 n18462 = 1'b0;
	end
	assign `SIG0 = n18462;

	initial begin
		#0 n18461 = 1'b0;
	end
	assign `SIG5 = n18461;

	initial begin
		#0 n18460 = 1'b0;
	end
	assign `SIG1 = n18460;

	initial begin
		#0 cl2mm_tdr_rcd136 = 1'b0;
		#99000.0 cl2mm_tdr_rcd136 = 1'bx;
	end
	assign `SIG61 = cl2mm_tdr_rcd136;

	initial begin
		#0 cl2mm_tdr_rcd134 = 1'b0;
		#99000.0 cl2mm_tdr_rcd134 = 1'bx;
	end
	assign `SIG63 = cl2mm_tdr_rcd134;

	initial begin
		#0 cl2mm_tdr_rcd135 = 1'b0;
		#99000.0 cl2mm_tdr_rcd135 = 1'bx;
	end
	assign `SIG12 = cl2mm_tdr_rcd135;

	initial begin
		#0 cl2mm_tdr_rcd133 = 1'b0;
		#99000.0 cl2mm_tdr_rcd133 = 1'bx;
	end
	assign `SIG60 = cl2mm_tdr_rcd133;

	initial begin
		#0 ctmbufmgrdou_sts_vctr18 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr18 = 1'b1;
	end
	assign `SIG50 = ctmbufmgrdou_sts_vctr18;

	initial begin
		#0 ctmbufmgrdou_sts_vctr19 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr19 = 1'b1;
	end
	assign `SIG51 = ctmbufmgrdou_sts_vctr19;

	initial begin
		#0 ctmbufmgrdou_sts_vctr16 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr16 = 1'b1;
	end
	assign `SIG52 = ctmbufmgrdou_sts_vctr16;

	initial begin
		#0 ctmbufmgrdou_sts_vctr17 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr17 = 1'b1;
	end
	assign `SIG53 = ctmbufmgrdou_sts_vctr17;

	initial begin
		#0 ctmbufmgrdou_sts_vctr14 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr14 = 1'b1;
	end
	assign `SIG54 = ctmbufmgrdou_sts_vctr14;

	initial begin
		#0 ctmbufmgrdou_sts_vctr15 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr15 = 1'b1;
	end
	assign `SIG55 = ctmbufmgrdou_sts_vctr15;

	initial begin
		#0 ctmbufmgrdou_sts_vctr12 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr12 = 1'b1;
	end
	assign `SIG56 = ctmbufmgrdou_sts_vctr12;

	initial begin
		#0 ctmbufmgrpio_blk_addr2 = 1'b0;
	end
	assign `SIG15 = ctmbufmgrpio_blk_addr2;

	initial begin
		#0 ctmbufmgrdou_sts_vctr10 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr10 = 1'b1;
	end
	assign `SIG58 = ctmbufmgrdou_sts_vctr10;

	initial begin
		#0 ctmbufmgrdou_sts_vctr11 = 1'b0;
		#47100.0 ctmbufmgrdou_sts_vctr11 = 1'b1;
	end
	assign `SIG59 = ctmbufmgrdou_sts_vctr11;

	initial begin
		 #137100.0 $finish;
	end

`endif
