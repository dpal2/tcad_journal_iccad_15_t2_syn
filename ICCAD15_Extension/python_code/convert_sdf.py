import os, sys, fnmatch
from optparse import OptionParser
import re

def modify_sdf_ver(SDF_idx, lines):
    try:
        if SDF_idx[0] != -1:
            lines[SDF_idx[0]] = '(SDFVERSION \"OVI 3.0\")\n'
    except IndexError:
        pass
    return lines

def modify_dffsx2(DFFSX2_idx, lines):
    hold_dict = {}
    setup_dict = {}
    recovery_dict = {}
    try:
        if DFFSX2_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            RECOVERY_idx = [idx for idx, x in enumerate(lines) if 'RECOVERY' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]
            for ridx in RECOVERY_idx:
                RECOVERY_pat = re.findall(r'\(([^)]*?)\)', lines[ridx].lstrip().lstrip('(').rstrip('\n')[:-1])
                recovery_dict[RECOVERY_pat[0] + '::' + RECOVERY_pat[1]] = RECOVERY_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffsx2: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffsx2: SETUP Key Lengths do not match"
            assert len(RECOVERY_idx) == len(recovery_dict.keys()), "modify_dffsx2: RECOVERY Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()) + len(recovery_dict.keys()), "modify_dffsx2: HOLD and SETUP-RECOVERY do not have same lengths"

            index = 0
            r_index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                try:
                    lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                except KeyError:
                    lines[HOLD_idx[index]] = '\t(HOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + hold_dict[hold_key] + '))\n'
                    lines[RECOVERY_idx[r_index]] = '\t(RECOVERY (' +  hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + recovery_dict[hold_key] + '))\n'
                    r_index = r_index + 1

                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_dffsx1(DFFSX1_idx, lines):
    hold_dict = {}
    setup_dict = {}
    recovery_dict = {}
    try:
        if DFFSX1_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            RECOVERY_idx = [idx for idx, x in enumerate(lines) if 'RECOVERY' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]
            for ridx in RECOVERY_idx:
                RECOVERY_pat = re.findall(r'\(([^)]*?)\)', lines[ridx].lstrip().lstrip('(').rstrip('\n')[:-1])
                recovery_dict[RECOVERY_pat[0] + '::' + RECOVERY_pat[1]] = RECOVERY_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffsx1: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffsx1: SETUP Key Lengths do not match"
            assert len(RECOVERY_idx) == len(recovery_dict.keys()), "modify_dffsx1: RECOVERY Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()) + len(recovery_dict.keys()), "modify_dffsx1: HOLD and SETUP-RECOVERY do not have same lengths"

            index = 0
            r_index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                try:
                    lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                except KeyError:
                    lines[HOLD_idx[index]] = '\t(HOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + hold_dict[hold_key] + '))\n'
                    lines[RECOVERY_idx[r_index]] = '\t(RECOVERY (' +  hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + recovery_dict[hold_key] + '))\n'
                    r_index = r_index + 1

                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_dffrx2(DFFRX2_idx, lines):
    hold_dict = {}
    setup_dict = {}
    recovery_dict = {}
    try:
        if DFFRX2_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            RECOVERY_idx = [idx for idx, x in enumerate(lines) if 'RECOVERY' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]
            for ridx in RECOVERY_idx:
                RECOVERY_pat = re.findall(r'\(([^)]*?)\)', lines[ridx].lstrip().lstrip('(').rstrip('\n')[:-1])
                recovery_dict[RECOVERY_pat[0] + '::' + RECOVERY_pat[1]] = RECOVERY_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffrx2: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffrx2: SETUP Key Lengths do not match"
            assert len(RECOVERY_idx) == len(recovery_dict.keys()), "modify_dffrx2: RECOVERY Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()) + len(recovery_dict.keys()), "modify_dffrx2: HOLD and SETUP-RECOVERY do not have same lengths"

            index = 0
            r_index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                try:
                    lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                except KeyError:
                    lines[HOLD_idx[index]] = '\t(HOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + hold_dict[hold_key] + '))\n'
                    lines[RECOVERY_idx[r_index]] = '\t(RECOVERY (' +  hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + recovery_dict[hold_key] + '))\n'
                    r_index = r_index + 1

                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_dffrx1(DFFRX1_idx, lines):
    hold_dict = {}
    setup_dict = {}
    recovery_dict = {}
    try:
        if DFFRX1_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            RECOVERY_idx = [idx for idx, x in enumerate(lines) if 'RECOVERY' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]
            for ridx in RECOVERY_idx:
                RECOVERY_pat = re.findall(r'\(([^)]*?)\)', lines[ridx].lstrip().lstrip('(').rstrip('\n')[:-1])
                recovery_dict[RECOVERY_pat[0] + '::' + RECOVERY_pat[1]] = RECOVERY_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffrx1: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffrx1: SETUP Key Lengths do not match"
            assert len(RECOVERY_idx) == len(recovery_dict.keys()), "modify_dffrx1: RECOVERY Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()) + len(recovery_dict.keys()), "modify_dffrx1: HOLD and SETUP-RECOVERY do not have same lengths"

            index = 0
            r_index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                try:
                    lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                except KeyError:
                    lines[HOLD_idx[index]] = '\t(HOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + hold_dict[hold_key] + '))\n'
                    lines[RECOVERY_idx[r_index]] = '\t(RECOVERY (' +  hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + recovery_dict[hold_key] + '))\n'
                    r_index = r_index + 1

                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_sdffrx2(SDFFRX2_idx, lines):
    hold_dict = {}
    setup_dict = {}
    recovery_dict = {}
    try:
        if SDFFRX2_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            RECOVERY_idx = [idx for idx, x in enumerate(lines) if 'RECOVERY' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]
            for ridx in RECOVERY_idx:
                RECOVERY_pat = re.findall(r'\(([^)]*?)\)', lines[ridx].lstrip().lstrip('(').rstrip('\n')[:-1])
                recovery_dict[RECOVERY_pat[0] + '::' + RECOVERY_pat[1]] = RECOVERY_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_sdffrx2: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_sdffrx2: SETUP Key Lengths do not match"
            assert len(RECOVERY_idx) == len(recovery_dict.keys()), "modify_sdffrx2: RECOVERY Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()) + len(recovery_dict.keys()), "modify_sdffrx2: HOLD and SETUP-RECOVERY do not have same lengths"

            index = 0
            r_index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                try:
                    lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                except KeyError:
                    lines[HOLD_idx[index]] = '\t(HOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + hold_dict[hold_key] + '))\n'
                    lines[RECOVERY_idx[r_index]] = '\t(RECOVERY (' +  hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + recovery_dict[hold_key] + '))\n'
                    r_index = r_index + 1

                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_sdffx2(SDFFX2_idx, lines):
    hold_dict = {}
    setup_dict = {}
    try:
        if SDFFX2_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_sdffx2: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_sdffx2: SETUP Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()), "modify_sdffx2: HOLD and SETUP do not have same lengths"

            index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines


def modify_sdffx1(SDFFX1_idx, lines):
    hold_dict = {}
    setup_dict = {}
    try:
        if SDFFX1_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                hold_dict[HOLD_pat[0] + '::' + HOLD_pat[1]] = HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1])
                setup_dict[SETUP_pat[0] + '::' + SETUP_pat[1]] = SETUP_pat[2]

            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_sdffx1: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_sdffx1: SETUP Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()), "modify_sdffx1: HOLD and SETUP do not have same lengths"

            index = 0
            for hold_key in hold_dict.keys():
                #lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :] + ')) (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key[:hold_key.find('::')] + ') (' + hold_key[hold_key.find('::') + 2 :][hold_key[hold_key.find('::') + 2 :].find('(') + 1:] + ') (' + setup_dict[hold_key] + ') (' + hold_dict[hold_key] + '))\n'
                index = index + 1

            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)]

    except IndexError:
        pass
    return lines

def modify_dffx2(DFFX2_idx, lines):
    hold_dict = {}
    setup_dict = {}
    try:
        if DFFX2_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1]) 
                hold_dict[HOLD_pat[0]] = HOLD_pat[1] + '::' + HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1]) 
                setup_dict[SETUP_pat[0]] = SETUP_pat[1] + '::' + SETUP_pat[2]
        
            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffx2: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffx2: SETUP Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()), "modify_dffx2: HOLD and SETUP do not have same lengths"
                
            index = 0
            for hold_key in hold_dict.keys():
                lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key + ') (' + hold_dict[hold_key][:hold_dict[hold_key].find('::')] + ') (' + setup_dict[hold_key][setup_dict[hold_key].find('::') + 2:] + ') (' + hold_dict[hold_key][hold_dict[hold_key].find('::') + 2:] + '))\n'
                index = index + 1
            #print HOLD_idx, SETUP_idx   
            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)] 
    except IndexError:
        pass
    return lines

def modify_dffx1(DFFX1_idx, lines):
    hold_dict = {}
    setup_dict = {}
    try:
        if DFFX1_idx[0] != -1:
            HOLD_idx = [idx for idx, x in enumerate(lines) if 'HOLD' in x]
            SETUP_idx = [idx for idx, x in enumerate(lines) if 'SETUP' in x]
            
            for hidx in HOLD_idx:
                HOLD_pat = re.findall(r'\(([^)]*?)\)', lines[hidx].lstrip().lstrip('(').rstrip('\n')[:-1]) 
                hold_dict[HOLD_pat[0]] = HOLD_pat[1] + '::' + HOLD_pat[2]
            for sidx in SETUP_idx:
                SETUP_pat = re.findall(r'\(([^)]*?)\)', lines[sidx].lstrip().lstrip('(').rstrip('\n')[:-1]) 
                setup_dict[SETUP_pat[0]] = SETUP_pat[1] + '::' + SETUP_pat[2]
        
            assert len(HOLD_idx) == len(hold_dict.keys()), "modify_dffx1: HOLD Key Lengths do not match"
            assert len(SETUP_idx) == len(setup_dict.keys()), "modify_dffx1: SETUP Key Lengths do not match"
            assert len(hold_dict.keys()) == len(setup_dict.keys()), "modify_dffx1: HOLD and SETUP do not have same lengths"
                
            index = 0
            for hold_key in hold_dict.keys():
                lines[HOLD_idx[index]] = '\t(SETUPHOLD (' + hold_key + ') (' + hold_dict[hold_key][:hold_dict[hold_key].find('::')] + ') (' + setup_dict[hold_key][setup_dict[hold_key].find('::') + 2:] + ') (' + hold_dict[hold_key][hold_dict[hold_key].find('::') + 2:] + '))\n'
                index = index + 1
            #print HOLD_idx, SETUP_idx   
            lines = [val for (i, val) in enumerate(lines) if i not in set(SETUP_idx)] 
    except IndexError:
        pass
    return lines

def print_lines(rdlinearr, start_index, stop_index, sdf_file):
    lines = rdlinearr[start_index:stop_index]

    celltype = ''

    SDF_idx = [idx for idx, x in enumerate(lines) if 'SDFVERSION' in x]
    DFFX1_idx = [idx for idx, x in enumerate(lines) if '\"DFF_X1\"' in x]
    DFFX2_idx = [idx for idx, x in enumerate(lines) if '\"DFF_X2\"' in x]
    SDFFX1_idx = [idx for idx, x in enumerate(lines) if '\"SDFF_X1\"' in x]
    SDFFX2_idx = [idx for idx, x in enumerate(lines) if '\"SDFF_X2\"' in x]
    SDFFRX2_idx = [idx for idx, x in enumerate(lines) if '\"SDFFR_X2\"' in x]
    DFFRX1_idx = [idx for idx, x in enumerate(lines) if '\"DFFR_X1\"' in x]
    DFFRX2_idx = [idx for idx, x in enumerate(lines) if '\"DFFR_X2\"' in x]
    DFFSX1_idx = [idx for idx, x in enumerate(lines) if '\"DFFS_X1\"' in x]
    DFFSX2_idx = [idx for idx, x in enumerate(lines) if '\"DFFS_X2\"' in x]
    
    lines = modify_sdf_ver(SDF_idx, lines)
    lines = modify_dffx1(DFFX1_idx, lines)
    lines = modify_dffx2(DFFX2_idx, lines)
    lines = modify_sdffx1(SDFFX1_idx, lines)
    lines = modify_sdffx2(SDFFX2_idx, lines)
    lines = modify_sdffrx2(SDFFRX2_idx, lines)
    
    lines = modify_dffrx1(DFFRX1_idx, lines)
    lines = modify_dffrx2(DFFRX2_idx, lines)
    lines = modify_dffsx1(DFFSX1_idx, lines)
    lines = modify_dffsx2(DFFSX2_idx, lines)

    for line_ in lines:
        if 'CELLTYPE' in line_:
            celltype = line_[line_.find('\"') + 1 : line_.rfind('\"')]
        sdf_file.write(line_)

    return celltype

def get_file_names(root_dir):
    name_of_files = []
    for root, dirNames, fileNames in os.walk(root_dir):
        if "DONT_SELECT" in fileNames:
            continue
        for fileName in fnmatch.filter(fileNames, '*_base_gate.sdf'):
            name_of_files.append(os.path.join(root, fileName))
    
    return name_of_files

def modify_sdf_file(name_of_files, modis):
   
    for file_name in name_of_files:
        celltypes_found = []
        sdf_file_name = file_name[file_name.rfind('/') + 1:]
        design_name = sdf_file_name[:sdf_file_name.find('_base_gate')]
        modi_sdf_file = open(modis + '/' + design_name + '_modified_gate.sdf', 'w')
        celltype_file = open(modis + '/' + design_name + '.cell', 'w')

        forig = open(file_name, 'r')
        flines = forig.readlines()

        #celltype_idx = filter(lambda x: 'CELLTYPE' in x, flines)
        celltype_idx = [idx for idx, x in enumerate(flines) if x.rstrip('\n') == '(CELL']
        #print celltype_idx
        tot_cell_cons = 0
        prev_index = 0
        for idx in range(len(celltype_idx)):
            #print "Cell Type: " + flines[celltype_idx[idx]].rstrip('\n')
            tot_cell_cons = tot_cell_cons + 1
            #print "Current Index of Celltype found: " + str(idx)
            celltype_ = print_lines(flines, prev_index, celltype_idx[idx], modi_sdf_file)
            celltypes_found.append(celltype_)
            prev_index = celltype_idx[idx]
            #print "New Prev Index: " + str(prev_index) + '\n'
            if tot_cell_cons == len(celltype_idx):
                celltype_ = print_lines(flines, prev_index, len(flines), modi_sdf_file)
                celltypes_found.append(celltype_)
        
        celltypes_found = sorted(list(set(celltypes_found)))
        celltype_file.write('Cell Types contained in ' + design_name + ':\n' + '\n'.join(cell for cell in celltypes_found))
        forig.close()
        modi_sdf_file.close()
        celltype_file.close()


    return


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-o", "--orig_sdf_file", dest="orig_sdf_file", help="Original SDF File without modification")
    parser.add_option("-m", "--modi_sdf_file", dest="modi_sdf_file", help="Modified SDF File")

    (options, args) = parser.parse_args()

    name_of_files = get_file_names(options.orig_sdf_file)
    modify_sdf_file(name_of_files, options.modi_sdf_file)
