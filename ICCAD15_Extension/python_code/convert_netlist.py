import os, sys
from optparse import OptionParser
import regex as re

def get_line_checked(line, wire_name_with_space):
    #if "assign" in line:
        #print line
    for name_ in wire_name_with_space:
        if name_ in line:
            #print "#" + name_ + "#" + line + "\n"
            line = line.replace(name_, name_.lstrip().rstrip())
            #line = re.sub(name_+r'\s+', name_.rstrip(), line)
            #print line
    #if "assign" in line:
        #print line
    return line


def rewrite_netlist(file_name):
    new_outputs = []
    output_string = ''
    new_outputs_vector = {}
    output_string_vector = []
    gtech_string_vector = []
    wire_name_with_space = []
    f = open(file_name, 'r')
    for line in f:
        # We need to modify the lines of GTECH_FD which has .QN but no .Q, If it has either .Q or .Q and .QN 
        # both, we don't need to do anything
        if "GTECH_FD1" in line or "GTECH_FD2" in line or "GTECH_FD4" in line:
            if ".QN(" in line and ".Q(" not in line:
                #print line
                line_before_qn = line[:line.find(".QN")]
                line_after_qn = line[line.find(".QN") :]
                qn_arg = line_after_qn[line_after_qn.find('(') + 1: line_after_qn.find(')')].lstrip().rstrip()
                q_arg = "q_" + qn_arg
                q_string = ".Q(" + q_arg +"), "
                if "[" not in q_arg:
                    new_outputs.append(q_arg)
                else:
                    signal_name = q_arg[q_arg.find('[')]
                    signal_current_bit = q_arg[q_arg.find('[') + 1 : q_arg.find(']')].rstrip().lstrip()
                    if signal_name in new_outputs_vector.keys():
                        signal_widths = new_outputs_vector[signal_name]
                        if signal_current_bit > int(signal_widths[0]):
                            new_outputs_vector[signal_name] = tuple([str(signal_current_bit), 0])
                        else:
                            print "Width of signal : " + signal_name + " unchanged\n"
                    else:
                        new_outputs_vector[signal_name] = tuple([str(signal_current_bit), 0])
                        
                complete_gtech_fd1_string = line_before_qn + q_string + line_after_qn
                #print complete_gtech_fd1_string
                gtech_string_vector.append(complete_gtech_fd1_string)
                #print "\n"
            else:
                gtech_string_vector.append(line)
    f.close()

    if new_outputs:
        output_string = "output " + ", ".join(ele for ele in new_outputs) + ";\n"
        #print output_string
    if new_outputs_vector:
        for signal_name in new_outputs_vector.keys():
            signal_widths = new_outputs_vector[signal_name] 
            op_string = "output [" + signal_widths[0] + ":" + str(signal_widths[1]) + "] " + signal_name + ";"
            output_string_vector.append(op_string)

   
    modified_file_name = file_name[:file_name.rfind('_')] + "_bertacco.v"
    rd_f = open(file_name, 'r')
    wr_f = open(modified_file_name, 'w')
    for line in rd_f:
        if "wire" in line:
            wire_vect = re.split('\s+', line)
            #print wire_vect
            if len(wire_vect) == 5:
                wr_f.write(wire_vect[0] + ' ' + wire_vect[1] + ' ' + wire_vect[2] + ';\n')
                wire_name_with_space.append(wire_vect[2] + ' ')
                continue
        elif "output" in line:
            if output_string:
                wr_f.write(output_string)
                output_string = ''
            if output_string_vector:
                for ele in output_string_vector:
                    wr_f.write(ele)
                output_string_vector = []
        elif "GTECH_FD1" in line or "GTECH_FD2" in line or "GTECH_FD4" in line:
            continue
        elif "endmodule" in line:
            if gtech_string_vector:
                for ele in gtech_string_vector:
                    wr_f.write(get_line_checked(ele, wire_name_with_space))
        wr_f.write(get_line_checked(line, wire_name_with_space))

    rd_f.close()
    wr_f.close()
    
    #print wire_name_with_space


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-f", "--file_name", dest="file_name")
    (options, args) = parser.parse_args()

    rewrite_netlist(options.file_name)

# python convert_netlist.py -f ../synthesized_netlist/sii_inc_ctl/sii_inc_ctl_flat_gtech_preprocessed.v
