import os, sys
from datetime import datetime as dt
from optparse import OptionParser
import regex as re
import pprint as pp

# Timezones for the different VCD files
TIMEZONE = {
        'dmu_ilu': {'HybrSel' : 30075, 'PRankNetlist': 42250},
        'dmu_clu': {'SigSeT_1': 93100, 'HybrSel': 62900, 'PRankNetlist': 47100},
        'dmu_dsn': {'SigSeT_1': 42700, 'HybrSel': 17700, 'PRankNetlist': 56844},
        'mcu_rdpctl_ctl': {'SigSeT_1': 60550, 'HybrSel': 60000, 'PRankNetlist': 60000},
        'pmu': {'SigSeT_1': 40000, 'HybrSel': 39340, 'PRankNetlist': 59540},
        'ncu_fcd_ctl': {'SigSeT_1': 216030 , 'HybrSel': 222330, 'PRankNetlist': 12345}
        }

CLK_PERIOD = {
        'dmu_ilu': 3.0,
        'dmu_clu': 3.0,
        'dmu_dsn': 3.0,
        'mcu_rdpctl_ctl': 2.5,
        'pmu': 1.0,
        'ncu_fcd_ctl': 2.5
        }

CYCLELENGTH = 300

SCOPE = '$scope'
UPSCOPE = '$upscope'
DUMPVARS = '$dumpvars'
END = '$end'

#PRankNetlist
designs = ['dmu_dsn', 'dmu_ilu', 'dmu_clu', 'pmu', 'ncu_fcd_ctl', 'mcu_rdpctl_ctl']
#HybrSel
#designs = ['pmu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'ncu_fcd_ctl', 'dmu_clu']
#SigSeT_1
#designs = ['pmu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'ncu_fcd_ctl', 'dmu_clu']




#designs = ['dmu_ilu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'ncu_fcd_ctl', 'pmu']
#designs = ['mcu_rdpctl_ctl', 'dmu_dsn', 'ncu_fcd_ctl', 'pmu']
#designs = ['dmu_clu']

def write_tick_define(design_name, netlist_dir, map_file_loc, sel_sig_loc, tool_names, buff_size, tick_define_loc):
    '''
    buff_size = 256 (always)
    '''
    tool_sig_map_table = {}

    map_ = map_file_loc + '/' + design_name + '_flat_preprocessed.map'
    map_file = open(map_, 'r')
    map_data = map_file.readlines()
    tool_considered = []

    #tick_file_name = tick_define_loc + '/' + design_name + '/' + design_name + '_tick_defines_netlist.v'
    #tick_define_file = open(tick_file_name, 'w')

    #trace_reg_file_name = tick_define_loc + '/' + design_name + '/' + design_name + '_trace_reg.v'
    #trace_reg_file = open(trace_reg_file_name, 'w')

    for tool in tool_names:
    
        tick_file_name = tick_define_loc + '/' + design_name + '/' + design_name + '_tick_defines_netlist_' + tool + '_' + str(buff_size) + '.v'
        tick_define_file = open(tick_file_name, 'w')

        trace_reg_file_name = tick_define_loc + '/' + design_name + '/' + design_name + '_trace_reg_' + tool + '_' + str(buff_size) + '.v'
        trace_reg_file = open(trace_reg_file_name, 'w')

        sig_map_tab = {}
        index = 0
        tool_considered.append(tool)
        tick_define_file.write('`ifdef ' + tool + '_' + str(buff_size) + '_O\n')
        trace_reg_file.write('`ifdef ' + tool + '_' + str(buff_size) + '_O\n')
        #sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_' + str(buff_size) + '_selected_signal.txt'
        sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_256_selected_signal.txt'
        #print sel_
        try:
            sel_file = open(sel_, 'r')
        except IOError:
            # If selected signal file does not exist
            if len(tool_considered) < len(tool_names):
                tick_define_file.write('`else\n')
                trace_reg_file.write('`else\n')
            else:
                for i in range(len(tool_names)):
                    tick_define_file.write('`endif\n')
                    trace_reg_file.write('`endif\n')
            continue
        sel_data_complete = sel_file.readlines()
        sel_data = sel_data_complete[0:buff_size]
        for ele in sel_data:
            ele_ = ele.rstrip().lstrip()
            trace_reg_file.write('\treg ' + ele_ + ';\n')
            #print ele_
            mapping_name = filter(lambda x: ele_ == x[:x.find('::')], map_data)
            if mapping_name:
                lhs = mapping_name[0][:mapping_name[0].find('::')]
                rhs = mapping_name[0][mapping_name[0].find('::') + 2 :].rstrip('\n')
                
                rhs = rhs.replace('\\', '')
                rhs = rhs.replace('/', '.')
                
                tick_define_file.write('\t`define SIG' + str(index) + ' ' + design_name + '_cov_bench.' + design_name + '_.' + rhs  + '\n')
                sig_map_tab[ele_] = 'SIG' + str(index)
                #sig_map_tab['SIG' + str(index)] = ele_
            else:
                reg_name = ''
                if design_name == 'dmu_clu':
                    if 'dummy' in ele_[-5:]:
                        reg_name = get_reg_corr_node(ele_[:ele_.rfind('dummy')], netlist_dir + '/dmu_dmc/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                    else:
                        reg_name = get_reg_corr_node(ele_, netlist_dir + '/dmu_dmc/' + design_name + '/' + design_name + '_flat_preprocessed.v')

                else:
                    if 'dummy' in ele_[-5:]:
                        reg_name = get_reg_corr_node(ele_[:ele_.rfind('dummy')], netlist_dir + '/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                    else:
                        reg_name = get_reg_corr_node(ele_, netlist_dir + '/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                
                #print "Node: " + ele_ + " Corr reg: " + reg_name

                #if 'dummy' in ele_[-5:]:
                #trace_file.write('\t\tassign ' + ele_ + ' = ~' + design_name + '_.' + ele_[:ele_.find('dummy')] + ';\n')
                tick_define_file.write('\t`define SIG' + str(index) + ' ' + design_name + '_cov_bench.' + design_name + '_.' + reg_name + '\n')
                sig_map_tab[ele_] = 'SIG' + str(index)
                #sig_map_tab['SIG' + str(index)] = ele_
                #else:
                #trace_file.write('\t\tassign ' + ele_ + ' = ' + design_name + '_.' + reg_name + ';\n')
            index = index + 1

        if len(tool_considered) < len(tool_names):
            tick_define_file.write('`else\n')
            trace_reg_file.write('`else\n')
        else:
            for i in range(len(tool_names)):
                tick_define_file.write('`endif\n')
                trace_reg_file.write('`endif\n')

        tool_sig_map_table[tool] = sig_map_tab
    
        tick_define_file.close()
        trace_reg_file.close()

    return tool_sig_map_table

def get_reg_corr_node(sig_name, netlist_name):
    #print "get_reg_corr_node Signal name received: " + sig_name
    reg_name = ''
    netlist = open(netlist_name, 'r')
    netlist_con = netlist.readlines()
    pattQ = re.compile(r'.Q\(\s*.*\b('+sig_name+')\b*\)')
    pattQN = re.compile(r'.QN\(\s*.*\b('+sig_name+')\b*\)')
    #sig_occurences = filter(lambda x: '.Q(' + sig_name in x or '.QN(' + sig_name in x, netlist_con)
    sig_occurences = filter(lambda x: re.search(pattQ, x) or re.search(pattQN, x), netlist_con)
    if sig_occurences:
        elements = sig_occurences[0].split(' ')
        reg_name =  elements[1]
        reg_name = reg_name.replace('\\', '')
        reg_name = reg_name.replace('/', '.')
        reg_name = reg_name[:reg_name.rfind('_reg')] + reg_name[reg_name.rfind('_reg') + 4:]
        #print reg_name
    #print reg_name + '\n'
    return reg_name


def get_signal_val(design_name, vcd_file_loc, stim_location, signal_table, tool_names, buff_size):
    '''
    signal_table = dict of dict. First label dict key is the name of the tool and the 
    second label dict key is the name of the selected signals and the key value is SIG[0-9]+
    '''
    tool_considered = []
    #stim_file_name = stim_location + '/' + design_name + '/' + design_name + '_stim_netlist.v'
    #stim_file = open(stim_file_name, 'w')

    clk_period = CLK_PERIOD[design_name] * 100

    for tool in tool_names:
        
        stim_file_name = stim_location + '/' + design_name + '/' + design_name + '_stim_netlist_' + tool + '_' + str(buff_size) + '.v'
        stim_file = open(stim_file_name, 'w')

        try:
            sig_map_tab = signal_table[tool]
        except KeyError:
            print "Tool name is: " + tool
            exit(0)
        #pp.pprint(sig_map_tab)
        val_map_tab = {}
        trimmed_signal_val = {}

        tool_considered.append(tool)
    
        vcd_file_name = vcd_file_loc + '/' + design_name + '_' + tool + '.vcd'
        #print "VCD File Name: " + vcd_file
        try:
            vcd_file = open(vcd_file_name, 'r')
        except IOError:
            if len(tool_considered) < len(tool_name):
                stim_file.write('`else\n')
            else:
                for i in range(len(tool_names)):
                    stim_file.write('`endif\n')
            continue

        vcd_file_contents = vcd_file.readlines()
        vcd_file.close()

        stim_file.write('`ifdef ' + tool + '_' + str(buff_size) + '_O\n')

        sig_tab = {}
        
        STARTTIME = TIMEZONE[design_name][tool]

        ENDTIME = STARTTIME + int(clk_period * CYCLELENGTH)

        scope_index = [idx for idx, x in enumerate(vcd_file_contents) if SCOPE in x]
        upscope_index = [idx for idx, x in enumerate(vcd_file_contents) if UPSCOPE in x]
        for idx in range(scope_index[0] + 1, upscope_index[0]):
            line = vcd_file_contents[idx]
            line_content = line.split(' ')
            signal_name = line_content[4]
            signal_smbl = line_content[3]
            sig_tab[signal_smbl] = signal_name
            try:
                #val_map_tab[sig_map_tab[signal_name]] = []
                val_map_tab[signal_name] = []
                trimmed_signal_val[signal_name] = []
            except KeyError:
                print "For design: " + design_name + " Signal: " + signal_name + " is not traced."

        pattern = re.compile(r'^#{1}[0-9]+')
        time_index = [idx for idx, x in enumerate(vcd_file_contents) if re.search(pattern, x)]
        for i in range(len(time_index) - 1):
            if int(vcd_file_contents[time_index[i]][1:]) > ENDTIME:
                break
            for j in range(time_index[i] + 1, time_index[i + 1]):
                if DUMPVARS in vcd_file_contents[j] or END in vcd_file_contents[j]:
                    continue
                else:
                    time = vcd_file_contents[time_index[i]][1:].lstrip().rstrip()
                    value = vcd_file_contents[j][0]
                    smbl = vcd_file_contents[j][1:].lstrip().rstrip()
                    signal_name = sig_tab[smbl]
                    try:
                        #val_map_tab[sig_map_tab[signal_name]].append(value + ':' + time)
                        val_map_tab[signal_name].append(value + ':' + time)
                    except KeyError:
                        print "For design: " + design_name + " Signal: " + signal_name + " is not traced."

                
        selected_signals = val_map_tab.keys()
        #print selected_signals

        for signal in selected_signals:
            #print "Trimming signal: " + signal
            sig_val_list = val_map_tab[signal]
            time = [int(data[data.index(':') + 1:]) for data in sig_val_list]
            print(time)
            closestSTARTTIME = min(time, key=lambda x:abs(x - STARTTIME))
            closestENDTIME = min(time, key=lambda x:abs(x - ENDTIME))

            if closestSTARTTIME == closestENDTIME:
                ele = sig_val_list[time.index(closestSTARTTIME)]
                val = ele[:ele.find(':')]
                if val == 'x':
                    #trimmed_signal_val[signal] = [str(STARTTIME + clk_period * i) + ':x' for i in range(CYCLELENGTH)]
                    trimmed_signal_val[signal] = [str(STARTTIME) + ':x']
                    #signal = [-10] * CYCLELENGTH
                else:
                    #trimmed_signal_val[signal] = [str(STARTTIME + clk_period * i) + ':' + val for i in range(CYCLELENGTH)]
                    trimmed_signal_val[signal] = [str(STARTTIME) + ':' + val]
                    #signal = [int(val)] * CYCLELENGTH
            elif closestSTARTTIME < closestENDTIME:
                curr_ele_ptr = 1
                extract_ele = sig_val_list[time.index(closestSTARTTIME) : time.index(closestENDTIME) + 1]
                extract_time = time[time.index(closestSTARTTIME) : time.index(closestENDTIME) + 1]
                for cycle in range(CYCLELENGTH):
                    curr_time = STARTTIME + cycle * clk_period
                    if curr_time < extract_time[curr_ele_ptr]:
                        val = extract_ele[curr_ele_ptr - 1][:extract_ele[curr_ele_ptr - 1].find(':')]
                        if val == 'x':
                            #signal[cycle] = -10
                            #trimmed_signal_val[signal] = str(curr_time) + ':x'
                            trimmed_signal_val[signal].append(str(curr_time) + ':x')
                        else:
                            #signal[cycle] = int(val)
                            #trimmed_signal_val[signal] = str(curr_time) + ':' + val
                            trimmed_signal_val[signal].append(str(curr_time) + ':' + val)
                    elif curr_time >= extract_time[curr_ele_ptr]:
                        val = extract_ele[curr_ele_ptr][:extract_ele[curr_ele_ptr].find(':')]
                        if val == 'x':
                            #signal[cycle] = -10
                            #trimmed_signal_val[signal] = str(curr_time) + ':x'
                            trimmed_signal_val[signal].append(str(curr_time) + ':x')
                        else:
                            #signal[cycle] = int(val)
                            #trimmed_signal_val[signal] = str(curr_time) + ':' + val
                            trimmed_signal_val[signal].append(str(curr_time) + ':' + val)

                    if curr_ele_ptr < len(extract_time) - 1:
                        curr_ele_ptr = curr_ele_ptr + 1
        
        for signal in selected_signals:
            print signal
            prev_time = 0
            prev_val = 'z'
            accumulate_time = 0.0
            
            sig_list = trimmed_signal_val[signal]
            try:
                sig_alias = sig_map_tab[signal]
                stim_file.write('\tinitial begin' + '\n')
            except KeyError:
                print "Signal Alias for: " + signal + " not found. Continuing... "
                continue
            #stim_file.write('\t\t#0 `' + sig_alias + ' = 1\'b0;\n')
            stim_file.write('\t\t#0 ' + signal + ' = 1\'b0;\n')
            prev_val = '0'
            for i in range(len(sig_list)):
                curr_tuple = sig_list[i]
                #print '#' + curr_tuple + '#'
                try:
                    curr_time = float(curr_tuple[:curr_tuple.find(':')])
                except ValueError:
                    print "Next time value received: " + curr_tuple[:curr_tuple.find(':')]
                    print sig_list
                    exit(0)
                
                time_diff = curr_time - prev_time
                curr_val = curr_tuple[curr_tuple.find(':') +1 :]
                
                if prev_val == curr_val:
                    accumulate_time = accumulate_time + time_diff
                else:
                    accumulate_time = accumulate_time + time_diff
                    #stim_file.write('\t\t#' + str(curr_time - prev_time) + ' `' + sig_alias + ' = 1\'b' + curr_tuple[curr_tuple.find(':') +1 :] + ';\n')
                    #stim_file.write('\t\t#' + str(accumulate_time) + ' `' + sig_alias + ' = 1\'b' + curr_val + ';\n')
                    stim_file.write('\t\t#' + str(accumulate_time) + ' ' + signal + ' = 1\'b' + curr_val + ';\n')
                    accumulate_time = 0.0
                
                prev_val = curr_val
                prev_time = curr_time

            stim_file.write('\tend' + '\n')
            stim_file.write('\tassign `' + sig_alias + ' = ' + signal + ';\n\n')
        
        stim_file.write('\tinitial begin\n')
        stim_file.write('\t\t #' + str(STARTTIME + clk_period * CYCLELENGTH) + ' $finish;\n')
        stim_file.write('\tend\n\n')

        if len(tool_considered) < len(tool_names):
            stim_file.write('`else\n')
        else:
            for i in range(len(tool_names)):
                stim_file.write('`endif\n')

        stim_file.close()
    return


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-r", "--root_dir", dest="root_dir")
    parser.add_option("-m", "--map_file_loc", dest="map_file_loc")
    #parser.add_option("-l", "--stimulus_location", dest="stimulus_location")
    parser.add_option("-d", "--tick_define_stim_location", dest="tick_define_stim_location")
    parser.add_option("-s", "--selected_signals_loc", dest="selected_signals_loc")
    parser.add_option("-v", "--vcd_file_loc", dest="vcd_file_loc")
    parser.add_option("-t", "--tool", dest="tool_names")

    (options, args) = parser.parse_args()

    tool_names = []
    for tool_name in options.tool_names.split(','):
        tool_names.append(tool_name)

    #buff_sizes = [256, 128, 64, 32]
    buff_sizes = [256]
    
    for design_name in designs:
        for buff_size in buff_sizes:
            print "Working on the design: " + design_name
            tool_sig_map_table = write_tick_define(design_name, options.root_dir, options.map_file_loc, options.selected_signals_loc, tool_names, buff_size, options.tick_define_stim_location)
            get_signal_val(design_name, options.vcd_file_loc, options.tick_define_stim_location, tool_sig_map_table, tool_names, buff_size)


# python write_stimulas.py -r /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist -m /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/map_files -d /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/testbenches -s /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/python_code -t PRankNetlist -v /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_RTL/trace_files
