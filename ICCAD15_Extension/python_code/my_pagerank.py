import networkx as nx
import pprint as pp
import operator as op

def my_pagerank(G, OUTPUT, DFF):
     
    #print "Entering in the modified PageRank routine.."
    
    out_degree = {}
    in_degree = {}

    for Node in G:
        #print "Node: " + Node
        try:
            if G.node[Node]['type'] == 'dff':
                out_degree[Node] = G.out_degree(Node)
                in_degree[Node] = G.in_degree(Node)
        except KeyError:
            pass
    out_degree_sorted = sorted(out_degree.items(), key=op.itemgetter(1), reverse=True) 
    in_degree_sorted = sorted(in_degree.items(), key=op.itemgetter(1), reverse=True) 
    
    '''
    print "Node\t\t\tOut Degree\tIn Degree"
    for tuple_ in out_degree_sorted:
        in_val = -1
        for tuple__ in in_degree_sorted:
            if tuple_[0] == tuple__[0]:
                in_val = tuple__[1]
        print tuple_[0] + '\t\t\t' + str(tuple_[1]) + '\t' + str(in_val)
    '''
    #dangling_nodes = [n for n in G if G.out_degree(n) == 0.0]
    
    # Calculating PageRank based on the standard algorithm. It has default valeu
    prank = nx.pagerank(G)
   
    # Modifying the Rank of the nodes based on its out degree and calculated Pagerank value
    # Logically we want which has higher PageRank value and has high connectivity (because high
    # connectivity means a flop can propagate value in many different design paths and hence
    # can restore more signals.
    # Calculated Modified Rank = outdegree(node) / prank(node)

    for Node in prank.keys():
        node_prank = -1.0
        out_degree_val = -1.0
        try:
            if G.node[Node]['type'] == 'dff':
                node_prank = prank[Node]
                out_degree_val = out_degree[Node]
                node_prank_modified = out_degree_val / node_prank
                prank[Node] = node_prank_modified
        except KeyError:
            pass

    return prank

def my_norm_out_pagerank(G, OUTPUT, DFF):
     
    #print "Entering in the modified PageRank routine.."
    tot_number_edges = G.number_of_edges()

    out_degree = {}
    in_degree = {}

    for Node in G:
        #print "Node: " + Node
        try:
            if G.node[Node]['type'] == 'dff':
                out_degree[Node] = G.out_degree(Node)
                in_degree[Node] = G.in_degree(Node)
        except KeyError:
            pass
    out_degree_sorted = sorted(out_degree.items(), key=op.itemgetter(1), reverse=True) 
    in_degree_sorted = sorted(in_degree.items(), key=op.itemgetter(1), reverse=True) 
    
    '''
    print "Node\t\t\tOut Degree\tIn Degree"
    for tuple_ in out_degree_sorted:
        in_val = -1
        for tuple__ in in_degree_sorted:
            if tuple_[0] == tuple__[0]:
                in_val = tuple__[1]
        print tuple_[0] + '\t\t\t' + str(tuple_[1]) + '\t' + str(in_val)
    '''
    #dangling_nodes = [n for n in G if G.out_degree(n) == 0.0]
    
    # Calculating PageRank based on the standard algorithm. It has default valeu
    prank = nx.pagerank(G)
   
    # Modifying the Rank of the nodes based on its out degree and calculated Pagerank value
    # Logically we want which has higher PageRank value and has high connectivity (because high
    # connectivity means a flop can propagate value in many different design paths and hence
    # can restore more signals.
    # Calculated Modified Rank = outdegree(node) / prank(node)

    for Node in prank.keys():
        node_prank = -1.0
        out_degree_val = -1.0
        try:
            if G.node[Node]['type'] == 'dff':
                node_prank = prank[Node]
                out_degree_val = out_degree[Node]
                out_degree_norm = out_degree_val * 1.0 / tot_number_edges
                node_prank_modified = out_degree_norm / node_prank
                prank[Node] = node_prank_modified
        except KeyError:
            pass

    return prank
