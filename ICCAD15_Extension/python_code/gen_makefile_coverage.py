import os, sys
from datetime import datetime as dt

tools = ['PRankNetlist', 'HybrSel', 'SigSeT_1']
designs = ['pmu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'dmu_ilu', 'ncu_fcd_ctl', 'dmu_clu']
buff_sizes = [256, 128, 64, 32]

make_loc = '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_coverage'

make_file_name = make_loc + '/Makefile'
mfile = open(make_file_name, 'w')

mfile.write('WORKHOME = .' + '\n\n')
CAUTION_STRING = '# auto-generated makefile at: ' + dt.now().strftime('%d-%b-%Y %I:%M:%S %p') + ' #'
mfile.write(len(CAUTION_STRING) * '#' + '\n')
mfile.write(CAUTION_STRING.upper() + '\n')
mfile.write(len(CAUTION_STRING) * '#' + '\n\n\n')

for design in designs:
    OBJ1 = []
    tool_considered = []
    DES_STRING = 5 * '#' + ' Coverage simulation for design: ' + design.upper() + ' ' + 5 * '#'
    mfile.write(len(DES_STRING) * '#' + '\n')
    mfile.write(DES_STRING + '\n')
    mfile.write(len(DES_STRING) * '#' + '\n\n')

    for tool in tools:
        OBJ2 = []
        if design == 'dmu_ilu' and tool == 'SigSeT_1' or design == 'dmu_ilu' and tool == 'HybrSel':
            continue
        tool_considered.append(tool)
        mfile.write('#' + 5 * '-->' + ' Tool used: ' + tool.upper() + ' ' + 5 * '<--' + '#' + '\n\n')
        for buff_size in buff_sizes:
            mfile.write('#' + 5 * '>>' + ' Coverage for buffer size: ' + str(buff_size) + ' ' + \
                    5 * '<<' + '#' + '\n')
            RM_STRING = '\\rm -rfv $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + '_' + \
                    str(buff_size) + '/exec $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + \
                    '_' + str(buff_size) +  '/coverage_db_' + design + \
                    '_rtl.vdb $(WORKHOME)/build_run/cov_' + \
                    design + '_' + tool.lower() + '_' + str(buff_size) + '/urgReport\n'

            MKDIR_STRING = 'mkdir -pv $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + '_' + \
                    str(buff_size) + '/exec\n'
            
            COVERAGE = '-cm branch+fsm+cond+line+tgl+path -cm_hier ' + \
                    '../scripts/hier_file/cm_hier_' + design + '_file -cm_line contassign -lca ' + \
                    '-cm_dir $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + '_' + \
                    str(buff_size) + '/coverage_db_' + design + '_rtl'
            VCS_STRING = 'vcs -R -full64 -o $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + \
                    '_' + str(buff_size) + '/exec/cov_' + design + '_' + tool.lower() + '_' + \
                    str(buff_size) + ' -Mdir=$(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + \
                    '_' + str(buff_size) + '/csrc -f ../scripts/rebuttal_file_lists/flist_rtl_' + \
                    design + ' -l $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + '_' + \
                    str(buff_size) + '/comp.log +v2k -top ' + design + '_cov_bench -sverilog ' + \
                    '+define+_COV_ +define+' + tool + '_' + str(buff_size) + \
                    ' +incdir+$(WORKHOME)/../rebuttal_1_testbenches/' + design + ' -timescale=1ns/10ps ' + \
                    '+define+_SVERILOG_ +incdir+$(WORKHOME)/../rebuttal_1_rtl/uncore/' + design + \
                    ' +error+512 ' + COVERAGE + '\n'

            URG_STRING = 'urg -full64 -dir $(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + \
                    '_' + str(buff_size) + '/coverage_db_' + design + '_rtl.vdb -report ' + \
                    '$(WORKHOME)/build_run/cov_' + design + '_' + tool.lower() + '_' + str(buff_size) + \
                    '/urgReport -lca\n'
            
            OBJ_STRING = 'cov_' + design + '_' + tool.lower() + '_' + str(buff_size)
            OBJ2.append(OBJ_STRING)

            mfile.write(OBJ_STRING + ':\n')
            mfile.write('\t' + RM_STRING)
            mfile.write('\t' + MKDIR_STRING)
            mfile.write('\t' + VCS_STRING)
            mfile.write('\t' + URG_STRING)
            mfile.write('\n')
        mfile.write(design + '_' + tool.lower() + ': ' + ' '.join(x for x in OBJ2))
        mfile.write('\n')
        OBJ1.extend(OBJ2)
        del OBJ2
        mfile.write('\n')
    #mfile.write(design + ': ' + ' '.join(x for x in OBJ1))
    mfile.write(design + ': ' + ' '.join(design + '_' + x.lower() for x in tool_considered))
    del OBJ1
    del tool_considered
    mfile.write('\n\n')


mfile.write('\n\n')
mfile.write('all: ' + ' '.join(x for x in designs) + '\n')
mfile.write('clean:' + '\n')
mfile.write('\t' + '\\rm -rfv ./build_run/*')

mfile.close()
