import os, sys
from optparse import OptionParser
import regex as re

designs = ['dmu_ilu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'dmu_clu', 'ncu_fcd_ctl', 'pmu']
#designs = ['dmu_clu']

def get_reg_corr_node(sig_name, netlist_name):
    #print "get_reg_corr_node Signal name received: " + sig_name
    reg_name = ''
    netlist = open(netlist_name, 'r')
    netlist_con = netlist.readlines()
    pattQ = re.compile(r'.Q\(\s*.*\b('+sig_name+')\b*\)')
    pattQN = re.compile(r'.QN\(\s*.*\b('+sig_name+')\b*\)')
    #sig_occurences = filter(lambda x: '.Q(' + sig_name in x or '.QN(' + sig_name in x, netlist_con)
    sig_occurences = filter(lambda x: re.search(pattQ, x) or re.search(pattQN, x), netlist_con)
    if sig_occurences:
        elements = sig_occurences[0].split(' ')
        reg_name =  elements[1]
        reg_name = reg_name.replace('\\', '')
        reg_name = reg_name.replace('/', '.')
        reg_name = reg_name[:reg_name.rfind('_reg')] + reg_name[reg_name.rfind('_reg') + 4:]
        #print reg_name
    #print reg_name + '\n'
    return reg_name

def write_trace_assign(design_name, netlist_dir, map_file_loc, sel_sig_loc, tool_names, buff_size, trace_file):
    map_ = map_file_loc + '/' + design_name + '_flat_preprocessed.map'
    map_file = open(map_, 'r')
    map_data = map_file.readlines()
    tool_considered = []
    for tool in tool_names:
        tool_considered.append(tool)
        trace_file.write('`ifdef ' + tool + '\n')
        sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_' + str(buff_size) + '_selected_signal.txt'
        try:
            sel_file = open(sel_, 'r')
        except IOError:
            # If selected signal file does not exist
            if len(tool_considered) < len(tool_names):
                trace_file.write('`else\n')
            else:
                for i in range(len(tool_names)):
                        trace_file.write('`endif\n')
            continue
        sel_data = sel_file.readlines()
        for ele in sel_data:
            ele_ = ele.rstrip().lstrip()
            #print ele_
            mapping_name = filter(lambda x: ele_ == x[:x.find('::')], map_data)
            if mapping_name:
                lhs = mapping_name[0][:mapping_name[0].find('::')]
                rhs = mapping_name[0][mapping_name[0].find('::') + 2 :].rstrip('\n')
                
                rhs = rhs.replace('\\', '')
                rhs = rhs.replace('/', '.')
                
                trace_file.write('\t\tassign ' + lhs + ' = ' + design_name + '_.' + rhs + ';\n')
            else:
                reg_name = ''
                if design_name == 'dmu_clu':
                    if 'dummy' in ele_[-5:]:
                        reg_name = get_reg_corr_node(ele_[:ele_.rfind('dummy')], netlist_dir + '/dmu_dmc/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                    else:
                        reg_name = get_reg_corr_node(ele_, netlist_dir + '/dmu_dmc/' + design_name + '/' + design_name + '_flat_preprocessed.v')

                else:
                    if 'dummy' in ele_[-5:]:
                        reg_name = get_reg_corr_node(ele_[:ele_.rfind('dummy')], netlist_dir + '/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                    else:
                        reg_name = get_reg_corr_node(ele_, netlist_dir + '/' + design_name + '/' + design_name + '_flat_preprocessed.v')
                
                #print "Node: " + ele_ + " Corr reg: " + reg_name

                if 'dummy' in ele_[-5:]:
                    #trace_file.write('\t\tassign ' + ele_ + ' = ~' + design_name + '_.' + ele_[:ele_.find('dummy')] + ';\n')
                    trace_file.write('\t\tassign ' + ele_ + ' = ~' + design_name + '_.' + reg_name + ';\n')
                else:
                    trace_file.write('\t\tassign ' + ele_ + ' = ' + design_name + '_.' + reg_name + ';\n')

        if len(tool_considered) < len(tool_names):
            trace_file.write('`else\n')
        else:
            for i in range(len(tool_names)):
                    trace_file.write('`endif\n')

    return

def write_trace_vcd(design_name, sel_sig_loc, tool_names, buff_size, trace_file):
    tool_considered = []
    trace_file.write('initial begin\n')
    
    for tool in tool_names:
        tool_considered.append(tool)
        trace_file.write('`ifdef ' + tool + '\n')
        sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_' + str(buff_size) + '_selected_signal.txt'
        try:
            sel_file = open(sel_, 'r')
        except IOError:
            continue
        sel_data = sel_file.readlines()
        trace_file.write('\t\t$dumpfile(\"' + design_name + '_' + tool + '.vcd\"' + ');\n')
        for ele in sel_data:
            ele_ = ele.rstrip().lstrip()
            trace_file.write('\t\t$dumpvars(1, ' + ele_ + ');\n')

        if len(tool_considered) < len(tool_names):
            trace_file.write('`else\n')
        else:
            for i in range(len(tool_names)):\
                    trace_file.write('`endif\n')
    
    trace_file.write('end\n')

    return

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-r", "--root_dir", dest="root_dir")
    parser.add_option("-m", "--map_file_loc", dest="map_file_loc")
    parser.add_option("-l", "--testbench_location", dest="testbench_location")
    parser.add_option("-s", "--selected_signals_loc", dest="selected_signals_loc")
    #parser.add_option("-k", "--vcs_makefile_loc", dest="vcs_mk_loc")
    #parser.add_option("-b", "--buffer_sizes", default="32,64", dest="buffer_sizes")
    parser.add_option("-t", "--tool", default="SigSeT_1", dest="tool_names", help="Name of the tool executable")
    #parser.add_option("-c", "--control_input_file", default="ctl.cfg", dest="ctl_inp_file", help="Control input file containing clock, reset and special control input signal names")

    (options, args) = parser.parse_args()

    #buffer_sizes = []
    #for buf_size in options.buffer_sizes.split(','):
    #    buffer_sizes.append(buf_size)

    tool_names = []
    for tool_name in options.tool_names.split(','):
        tool_names.append(tool_name)

    for design_name in designs:
        print "Working on design netlist: " + design_name
        trace_name = options.testbench_location + '/' + design_name + '/' + design_name + '_rtl_trace.v'
        trace_file = open(trace_name, 'w')
        
        write_trace_assign(design_name, options.root_dir, options.map_file_loc, options.selected_signals_loc, tool_names, 256, trace_file)

        trace_file.write('\n\n')

        write_trace_vcd(design_name, options.selected_signals_loc, tool_names, 256, trace_file) 

        trace_file.close()


###############################################################
################  COMMAND TO RUN ##############################
###############################################################

#python write_rtl_trace_dump.py -m /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/map_files -l /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/testbenches -s /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection -t SigSeT_1,HybrSel,PRankNetlist -r /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist

