import os, sys
from datetime import datetime as dt

tools = ['HybrSel', 'SigSeT_1', 'PRankNetlist']
designs = ['pmu', 'mcu_rdpctl_ctl', 'dmu_dsn', 'dmu_ilu', 'ncu_fcd_ctl', 'dmu_clu']
buff_sizes = [256, 128, 64, 32]
#cycles = 512
cycles = 300

CLK_PERIOD = {
        'dmu_ilu': 3.0,
        'dmu_clu': 3.0,
        'dmu_dsn': 3.0,
        'mcu_rdpctl_ctl': 2.5,
        'pmu': 1.0,
        'ncu_fcd_ctl': 2.5
        }

srr_loc = '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/rebuttal_1_srr'
srr_file_name = srr_loc + '/run_srr_by_design.sh'
sfile = open(srr_file_name, 'w')

sfile.write('#!/bin/sh\n\n\n')

WORKDIR = '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension'

CAUTION_STRING = '# auto-generated SRR script at: ' + dt.now().strftime('%d-%b-%Y %I:%M:%S %p') + ' #'
sfile.write(len(CAUTION_STRING) * '#' + '\n')
sfile.write(CAUTION_STRING.upper() + '\n')
sfile.write(len(CAUTION_STRING) * '#' + '\n\n\n')


for design in designs:

    DES_STRING = 5 * '#' + ' State restoration ratio calculation for design: ' + design.upper() + ' ' + 5 * '#'
    sfile.write(len(DES_STRING) * '#' + '\n')
    sfile.write(DES_STRING + '\n')
    sfile.write(len(DES_STRING) * '#' + '\n\n')

    for tool in tools:

        if design == 'dmu_ilu' and tool == 'SigSeT_1':
            continue

        sfile.write('#' + 5 * '-->' + ' Tool used: ' + tool.upper() + ' ' + 5 * '<--' + '#' + '\n\n')

        for buff_size in buff_sizes:

            DIR_NAME = srr_loc + '/' + str(buff_size) + '_' + str(cycles)
            
            sfile.write('#' + 5 * '>>' + ' Coverage for buffer size: ' + str(buff_size) + ' ' + \
                    5 * '<<' + '#' + '\n')

            if tool == 'PRankNetlist':
                RR_STRING = 'python ./RR.py ' + WORKDIR + '/synthesized_netlist/' + design + '/' + \
                        design + '_flat_preprocessed.bench ' + WORKDIR + '/python_code/' + design + '/' + \
                        tool + '/' + design + '_flat_preprocessed_' + str(buff_size) + \
                        '_selected_signal.txt ' + WORKDIR + '/Simulation_RTL/trace_files/' + design + \
                        '_' + tool + '.vcd ' + design + ' ' + \
                        str(CLK_PERIOD[design]) + ' ' + tool + ' ' + str(buff_size) + ' ' + str(cycles) + ' 1' \
                        + ' || true'

            else:
                RR_STRING = 'python ./RR.py ' + WORKDIR + '/synthesized_netlist/' + design + '/' + \
                        design + '_flat_preprocessed.bench ' + WORKDIR + '/Signal_Selection/' + design + '/' + \
                        tool + '/' + design + '_flat_preprocessed_' + str(buff_size) + \
                        '_selected_signal.txt ' + WORKDIR + '/Simulation_RTL/trace_files/' + design + \
                        '_' + tool + '.vcd ' + design + ' ' +\
                        str(CLK_PERIOD[design]) + ' ' + tool + ' ' + str(buff_size) + ' ' + str(cycles) + ' 1' \
                        + ' || true'

            
            MV_STYRING = 'mv ' + design + '_' + tool + '_' + str(buff_size) + '.txt ' + DIR_NAME
            
            sfile.write('if [ -d \"' + DIR_NAME + '\" ]; then\n')
            sfile.write('\t echo \"' + DIR_NAME + ' exists\"\n')
            sfile.write('else\n')
            sfile.write('\t mkdir -pv \"' + DIR_NAME + '\"\n')
            sfile.write('fi\n\n')

            sfile.write(RR_STRING + '\n')
            sfile.write('\n')
            sfile.write(MV_STYRING + '\n')
        sfile.write('\n\n')
    sfile.write('\n\n')


sfile.close()
