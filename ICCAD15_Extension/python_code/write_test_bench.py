import os, sys, fnmatch
from optparse import OptionParser
from subprocess import Popen, PIPE
from datetime import datetime as dt
import ConfigParser as CP
import ast

def get_file_names(root_dir, Tool):
    name_of_files = []
    #buffer_size = options.buffer_size
    if Tool == "AASR":
        for root, dirNames, fileNames in os.walk(root_dir):
            if "DONT_SELECT" in fileNames:
                continue
            for fileName in fnmatch.filter(fileNames, '*_getch.v'):
                name_of_files.append(os.path.join(root, fileName))
    else:
        for root, dirNames, fileNames in os.walk(root_dir):
            if "DONT_SELECT" in fileNames:
                continue
            for fileName in fnmatch.filter(fileNames, '*_flat_preprocessed.v'):
                name_of_files.append(os.path.join(root, fileName))
    return name_of_files

def write_reg_wire(file_name, bench_file):
    inputs = []
    outputs = []
    netlist = open(file_name, 'r')
    netlist_con = netlist.readlines()
    #print netlist_con
    inputs_ = filter(lambda x: 'input' in x[:5], netlist_con)
    #print inputs_
    outputs_ = filter(lambda x: 'output' in x[:6], netlist_con)
    for ele in inputs_:
        if '[' in ele:
            input_words = ele.split(' ')
            inputs.append(input_words[1] + '::' + input_words[2])
        else:
            list_of_inputs = ele[6:][:-1].lstrip().rstrip()
            input_list = list_of_inputs.split(',')
            inputs.extend(input_list)

    for ele in outputs_:
        if '[' in ele:
            output_words = ele.split(' ')
            outputs.append(output_words[1] + '::' + output_words[2])
        else:
            list_of_outputs = ele[6:][:-1].lstrip().rstrip()
            output_list = list_of_outputs.split(',')
            outputs.extend(output_list)

    #print inputs
    for inp in inputs:
        if '::' in inp:
            bench_file.write('reg ' + inp[:inp.find('::')].lstrip().rstrip() + ' ' + inp[inp.find('::') + 2 :].replace('\n','').replace(';','').lstrip().rstrip() + ';\n')
        else:
            bench_file.write('reg ' + inp.replace(';','').lstrip().rstrip() + ';\n')
    
    bench_file.write("\n\n");

    for outp in outputs:
        if '::' in outp:
            bench_file.write('wire ' + outp[:outp.find('::')].lstrip().rstrip() + ' ' + outp[outp.find('::') + 2 :][:-2].lstrip().rstrip() + ';\n')
        else:
            bench_file.write('wire ' + outp.lstrip().rstrip() + ';\n')

    bench_file.write("\n\n");
    
    return inputs

def write_assign(design_name, map_file_loc, sel_sig_loc, buffer_sizes, tool_names, bench_file):
    map_ = map_file_loc + '/' + design_name + '_flat_preprocessed.map'
    map_file = open(map_, 'r')
    map_data = map_file.readlines()
    tool_considered = []
    for tool in tool_names:
        buff_considered = []
        tool_considered.append(tool)
        bench_file.write('`ifdef ' + tool + '\n')
        for buff_size in buffer_sizes:
            buff_considered.append(buff_size)
            sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_' + str(buff_size) + '_selected_signal.txt'
            try:
                sel_file = open(sel_, 'r')
            except IOError:
                continue
            sel_data = sel_file.readlines()
            bench_file.write('\t`ifdef _' + buff_size + '_\n')
            for ele in sel_data:
                ele_ = ele.rstrip().lstrip()
                #print ele_
                mapping_name = filter(lambda x: ele_ == x[:x.find('::')], map_data)
                if mapping_name:
                    lhs = mapping_name[0][:mapping_name[0].find('::')]
                    rhs = mapping_name[0][mapping_name[0].find('::') + 2 :].rstrip('\n')
                    #if '\\' in rhs and '[' not in rhs:
                    if '\\' in rhs:
                        rhs = rhs + ' '
                    bench_file.write('\t\tassign ' + lhs + ' = ' + design_name + '_.' + rhs + ';\n')
                else:
                    if 'dummy' in ele_:
                        bench_file.write('\t\tassign ' + ele_ + ' = ~' + design_name + '_.' + ele_[:ele_.find('dummy')] + ';\n')
                    else:
                        bench_file.write('\t\tassign ' + ele_ + ' = ' + design_name + '_.' + ele_ + ';\n')

            if len(buff_considered) < len(buffer_sizes):
                bench_file.write('\t`else\n')
            else:
                for i in range(len(buffer_sizes)):
                    bench_file.write('\t`endif\n')
        if len(tool_considered) < len(tool_names):
            bench_file.write('`else\n')
        else:
            for i in range(len(tool_names)):
                    bench_file.write('`endif\n')

    return

def write_trace_vcd(design_name, sel_sig_loc, buffer_sizes, tool_names, bench_file):
    tool_considered = []
    bench_file.write('initial begin\n')
    
    for tool in tool_names:
        buff_considered = []
        tool_considered.append(tool)
        bench_file.write('`ifdef ' + tool + '\n')
        for buff_size in buffer_sizes:
            buff_considered.append(buff_size)
            sel_ = sel_sig_loc + '/' + design_name + '/' + tool + '/' + design_name + '_flat_preprocessed_' + str(buff_size) + '_selected_signal.txt'
            try:
                sel_file = open(sel_, 'r')
            except IOError:
                continue
            sel_data = sel_file.readlines()
            bench_file.write('\t`ifdef _' + buff_size + '_\n')
            bench_file.write('\t\t$dumpfile(\"' + design_name + '_' + tool + '_' + str(buff_size) + '.vcd\"' + ');\n')
            for ele in sel_data:
                ele_ = ele.rstrip().lstrip()
                bench_file.write('\t\t$dumpvars(1, ' + ele_ + ');\n')

            if len(buff_considered) < len(buffer_sizes):
                bench_file.write('\t`else\n')
            else:
                for i in range(len(buffer_sizes)):
                    bench_file.write('\t`endif\n')

        if len(tool_considered) < len(tool_names):
            bench_file.write('`else\n')
        else:
            for i in range(len(tool_names)):
                    bench_file.write('`endif\n')
    
    bench_file.write('end\n')

    return

def write_stimulas(inputs, spl_inps, max_clk_period, bench_file):
    bench_file.write('always begin\n')
    bench_file.write('\t#' + str(10.0 * max_clk_period) + ';\n')
    for inp in inputs:
        if '::' in inp:
            lhs = inp[inp.find('::') + 2 :].replace(';','').replace('\n','').lstrip().rstrip()
            if lhs not in spl_inps:
                bench_file.write('\t' + lhs + ' = $random;\n')
        else:
            lhs = inp.replace(';','').lstrip().rstrip()
            if lhs not in spl_inps:
                bench_file.write('\t' + lhs + ' = $random;\n')

    bench_file.write('\t#' + str(20.0 * max_clk_period) + ';\n')
    bench_file.write('end\n')
    return

def write_control_input(Clock, Reset, Ctl_Inp_List, Sim_Time, netlist_path, design_name, bench_file):
    clk_periods = {}
    clks = Clock.keys()
    rsts = Reset.keys()
    ctls = Ctl_Inp_List.keys()

    spl_inps = []

    
    bench_file.write('initial begin\n')
    bench_file.write('\t$sdf_annotate(\"' + netlist_path + '/' + design_name + '_modified_gate.sdf\", ' + design_name + '_' + ');\n') 
    if clks:
        for clk_ in clks:
            if clk_:
                spl_inps.append(clk_)
                clk_info = Clock[clk_]
                clk_init_val = clk_info[:clk_info.find('::')]
                clk_period = clk_info[clk_info.find('::') + 2:]
                clk_periods[clk_] = clk_period
                bench_file.write('\t' + clk_ + ' = ' + clk_init_val + ';\n')
    
    if rsts:
        for rst_ in rsts:
            if rst_:
                spl_inps.append(rst_)
                rst_info = Reset[rst_]
                bench_file.write('\t' + rst_ + ' = ' + rst_info + ';\n')
    
    if ctls:
        for ctl_ in ctls:
            if ctl_:
                spl_inps.append(ctl_)
                ctl_info = Ctl_Inp_List[ctl_]
                bench_file.write('\t' + ctl_ + ' = ' + ctl_info + ';\n')

    bench_file.write('\t#' + str(Sim_Time) + ' $finish;\n')
    bench_file.write('end\n')
    
    return clk_periods, spl_inps

def find_max_period(clk_periods):
    clks = clk_periods.keys()
    max_clk_period = -1
    for clk_ in clks:
        try:
            max_clk_period = max(max_clk_period, int(clk_periods[clk_]))
        except ValueError:
            max_clk_period = max(max_clk_period, float(clk_periods[clk_]))

    return max_clk_period

def write_clock(clk_periods, bench_file):
    clks = clk_periods.keys()
    bench_file.write('always begin\n')
    for clk_ in clks:
        try:
            bench_file.write('\t#' + str(int(clk_periods[clk_]) / 2.0) + ' ' + clk_ + ' = ~' + clk_ + ';\n')
        except ValueError:
            bench_file.write('\t#' + str(float(clk_periods[clk_]) / 2.0) + ' ' + clk_ + ' = ~' + clk_ + ';\n')
    bench_file.write('end\n')

    return

def write_force_release(design_name, map_file_loc, max_clk_period, bench_file):
    flop_ = map_file_loc + '/' + design_name + '_flat_preprocessed.inst'
    flop_file = open(flop_, 'r')
    flop_data = flop_file.readlines()

    bench_file.write('initial begin\n')
    
    for flop in flop_data:
        if '\\' in flop:
            bench_file.write('\tforce ' + design_name + '_.' + flop.rstrip().lstrip() + ' .IQ = 1\'b0;\n')
        else:
            bench_file.write('\tforce ' + design_name + '_.' + flop.rstrip().lstrip() + '.IQ = 1\'b0;\n')

    bench_file.write('\n')

    bench_file.write('\t#' + str(10.0 * max_clk_period) + ';\n')

    bench_file.write('\n')

    for flop in flop_data:
        if '\\' in flop:
            bench_file.write('\trelease ' + design_name + '_.' + flop.rstrip().lstrip() + ' .IQ ;\n')
        else:
            bench_file.write('\trelease ' + design_name + '_.' + flop.rstrip().lstrip() + '.IQ ;\n')
    
    bench_file.write('end\n')
    return

def generate_vcs_command(design_name, tool_names, buffer_sizes, vcs_makefile):
    
    for tool in tool_names:
        vcs_makefile.write(design_name + '_' + tool + ":\n")
        vcd_file_name = ''
        for buff_size in buffer_sizes:
            rm_command_to_execute = "\\rm -rfv $(WORKHOME)/" + design_name + "/exec"
            mkdir_command_to_execute = "mkdir -pv $(WORKHOME)/" + design_name + "/exec"
            vcs_command_to_execute = "vcs -R -full64 -o $(WORKHOME)/" + design_name + "/exec/" + design_name + " -sverilog -Mdir=$(WORKHOME)/" + design_name + "/csrc -f $(FILEHOME)/flist_" + design_name + " -l $(WORKHOME)/" + design_name + "/comp_" + design_name + ".log -top " + design_name + "_bench -v $(LIBHOME)/flops/DFF_X1/DFF_X1.v -v $(LIBHOME)/flops/DFF_X2/DFF_X2.v -v $(LIBHOME)/flops/SDFF_X2/SDFF_X2.v -v $(LIBHOME)/flops/SDFF_X1/SDFF_X1.v -v $(LIBHOME)/flops/SDFFR_X2/SDFFR_X2.v -v $(LIBHOME)/gates/AND/AND.v -v $(LIBHOME)/gates/OR/OR.v -v $(LIBHOME)/gates/NAND/NAND.v -v $(LIBHOME)/gates/NOR/NOR.v -v $(LIBHOME)/gates/INV/INV.v -timescale=1ns/10ps +v2k +libext+.v +liborder +define+" + tool + "+_" + buff_size + "_ +compsdf +sdfverbose +neg_tchk +error+10000"
            vcd_file_name = design_name + '_' + tool + '_' + buff_size + '.vcd ' + vcd_file_name
            vcs_makefile.write('\t' + rm_command_to_execute + '\n')
            vcs_makefile.write('\t' + mkdir_command_to_execute + '\n')
            vcs_makefile.write('\t' + vcs_command_to_execute + '\n')

        vcs_makefile.write('\t-mv ' + vcd_file_name + './' + design_name + '\n\n')
    vcs_makefile.write(design_name + ': ' + ' '.join(design_name + '_' + tool for tool in tool_names))
    vcs_makefile.write('\n\n')
    return

def generate_test_bench(name_of_files, map_file_loc, testbench_loc, sel_sig_loc, ctl_inp_file, buffer_sizes, vcs_mk_loc, tool_names):
    
    # Even before writing the Verilog testbench file for each of the design
    # reading the special control input file for each of the relevant design
    # primarily to get the clock, reset and any special input that needs to have
    # special value and cannot be randomized as in stimulas generation

    Clock_List = []
    Reset_List = []
    Ctl_Inp_List = []
    Sim_Time_List = []

    config_control = CP.RawConfigParser()
    config_control.read(ctl_inp_file)
    Designs = ast.literal_eval(config_control.get('Configuration', 'Designs'))

    vcs_makefile = open(vcs_mk_loc + '/Makefile', 'w')

    WORKHOME = ast.literal_eval(config_control.get('Configuration', 'WORKHOME'))
    LIBHOME = ast.literal_eval(config_control.get('Configuration', 'LIBHOME'))
    FILEHOME = ast.literal_eval(config_control.get('Configuration', 'FILEHOME'))

    vcs_makefile.write('WORKHOME=' + WORKHOME + '\n')
    vcs_makefile.write('LIBHOME=' + LIBHOME + '\n')
    vcs_makefile.write('FILEHOME=' + FILEHOME + '\n')
    vcs_makefile.write('\n') 

    for i in range(len(Designs)):
        #print "Reading Configuration for Design: " + Designs[i] + "\n"
        Clock_List.append(ast.literal_eval(config_control.get(Designs[i], 'clock')))
        Reset_List.append(ast.literal_eval(config_control.get(Designs[i], 'reset')))
        Ctl_Inp_List.append(ast.literal_eval(config_control.get(Designs[i], 'control_input')))
        Sim_Time_List.append(ast.literal_eval(config_control.get(Designs[i], 'sim_dur')))

    for file_name in name_of_files:
        #path = file_name[:file_name.rfind('/')]
        netlist_path = file_name[:file_name.rfind('/')]
        netlist_name = file_name[file_name.rfind('/') + 1 :]
        design_name = netlist_name[:netlist_name.find('_flat')]
        print design_name
        if not os.path.isdir(testbench_loc + "/" + design_name):
            mkdir_cmd = "mkdir -pv " + testbench_loc + "/" + design_name
            Popen(mkdir_cmd, shell=True, stdout=PIPE, stderr=PIPE)
        bench_name = testbench_loc + "/" + design_name + "/" + design_name + "_bench.v"
        benhc_file_header = "/* Testbench file for design " + design_name + " generated on " + dt.now().__str__() + " */\n"
        bench_file = open(bench_name, 'w')

        bench_file.write(benhc_file_header)

        bench_file.write("module " + design_name + "_bench();\n\n")

        inputs = write_reg_wire(file_name, bench_file)

        bench_file.write(design_name + ' ' + design_name + '_ ' + '(.*);\n\n')

        write_assign(design_name, map_file_loc, sel_sig_loc, buffer_sizes, tool_names, bench_file)

        bench_file.write('\n\n')

        write_trace_vcd(design_name, sel_sig_loc, buffer_sizes, tool_names, bench_file)

        bench_file.write('\n\n')

        clk_periods, spl_inps = write_control_input(Clock_List[Designs.index(design_name)], Reset_List[Designs.index(design_name)], Ctl_Inp_List[Designs.index(design_name)], Sim_Time_List[Designs.index(design_name)], netlist_path, design_name, bench_file)
       
        max_clk_period = find_max_period(clk_periods)

        bench_file.write('\n\n')

        write_force_release(design_name, map_file_loc, max_clk_period, bench_file)

        bench_file.write('\n\n')

        write_clock(clk_periods, bench_file)

        bench_file.write('\n\n')

        write_stimulas(inputs, spl_inps, max_clk_period, bench_file)

        bench_file.write('\n\n')

        bench_file.write("endmodule")

        bench_file.close()

        generate_vcs_command(design_name, tool_names, buffer_sizes, vcs_makefile)
    
    vcs_makefile.write('all: ' + ' '.join(design_name for design_name in Designs))
    vcs_makefile.close()

    return


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-r", "--root_dir", dest="root_dir")
    parser.add_option("-m", "--map_file_loc", dest="map_file_loc")
    parser.add_option("-l", "--testbench_location", dest="testbench_location")
    parser.add_option("-s", "--selected_signals_loc", dest="selected_signals_loc")
    parser.add_option("-k", "--vcs_makefile_loc", dest="vcs_mk_loc")
    parser.add_option("-b", "--buffer_sizes", default="32,64", dest="buffer_sizes")
    parser.add_option("-t", "--tool", default="SigSeT_1", dest="tool_names", help="Name of the tool executable")
    parser.add_option("-c", "--control_input_file", default="ctl.cfg", dest="ctl_inp_file", help="Control input file containing clock, reset and special control input signal names")

    (options, args) = parser.parse_args()

    buffer_sizes = []
    for buf_size in options.buffer_sizes.split(','):
        buffer_sizes.append(buf_size)

    tool_names = []
    for tool_name in options.tool_names.split(','):
        tool_names.append(tool_name)

    #name_of_files = get_file_names(options.root_dir, 'SigSeT_1')
    #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dec/dec_flat_preprocessed.v']
    #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/fgu_fac_ctl/fgu_fac_ctl_flat_preprocessed.v']
    name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dmc/dmu_clu/dmu_clu_flat_preprocessed.v']
    generate_test_bench(name_of_files, options.map_file_loc, options.testbench_location, options.selected_signals_loc, options.ctl_inp_file, buffer_sizes, options.vcs_mk_loc, tool_names)

###############################################
#####       OLD COMMAND #######################
###############################################

# python write_test_bench.py -r /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist -m /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/map_files -l /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/testbenches -s /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection -b 64,128,256 -t SigSeT_1

###############################################
#####       NEW COMMAND #######################
###############################################

#python write_test_bench.py -r /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist -m /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/map_files -l /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/testbenches -s /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Signal_Selection -b 64,128,256 -t SigSeT_1,HybrSel,PRankNetlist -c ./design.cfg -k /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/Simulation_Netlist
