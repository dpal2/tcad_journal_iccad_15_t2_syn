import sys
import os, fnmatch 
from optparse import OptionParser
from subprocess import Popen, PIPE
from datetime import datetime
import signal
from threading import Thread, Event
from Queue import Queue
from time import sleep
from decimal import Decimal

def get_memory_usage(VmKey, PID):
    _proc_status = '/proc/' + str(PID) + '/status'
    _scale = {'kB': 1024.0, 'mB': 1024.0 * 1024.0,
              'KB': 1024.0, 'MB': 1024.0 * 1024.0}
    try:
        t = open(_proc_status)
        v = t.readlines()
        t.close()
    except:
        return 0.0
    VmKeyLine = filter(lambda x: VmKey in x, v)
    try:
        VmKeys = VmKeyLine[0].split(None, 3)
    except IndexError:
        return 0.0

    if len(VmKeys) < 3:
        return 0.0
    return float(VmKeys[1]) * _scale[VmKeys[2]]

def memory_usage(PID):
    return get_memory_usage('VmSize:', PID)


def poll_memory(proc, q):
    memory = 0.0
    while proc.poll() is None:
        memory = max(memory, memory_usage(proc.pid))
        sleep(1)
        #print str(memory)
    q.put(memory)

def kill_on_timeout(done, timeout, proc, err):
    if not done.wait(timeout):
        err.write("Timeout: Signal Selection Failed.\n")
        proc.kill()

def exec_command(command, timeout, out_file_name, err_file_name):
    done = Event()
    q = Queue()
    if out_file_name == 'pipe' or out_file_name == 'PIPE':
        out = PIPE
    else:
        out = open(out_file_name, 'w')

    if err_file_name == 'pipe' or err_file_name == 'PIPE':
        err = PIPE
    else:
        err = open(err_file_name, 'w')

    proc = Popen(command, shell=True, stdout=out, stderr=err)

    watcher = Thread(target=kill_on_timeout, args=(done, timeout, proc, err))
    watcher.daemon = True
    watcher.start()

    memory = 0.0 
    memwatch = Thread(target=poll_memory, args=(proc, q))
    memwatch.daemon = True
    memwatch.start()

    data, stderr = proc.communicate()
    done.set()
    #print "Max memory usage of PID : " + str(proc.pid) + " " + str(q.get() / 1048576) + " MB"
   
    if not out_file_name == 'PIPE' and not out_file_name == 'pipe':
        out.close()

    if not err_file_name == 'PIPE' and not err_file_name == 'pipe':
        err.close()

    return data, stderr, proc.returncode, q.get()

def get_file_names(root_dir, Tool):
    name_of_files = []
    #buffer_size = options.buffer_size
    if Tool == "HybrSel":
        for root, dirNames, fileNames in os.walk(root_dir):
            if "DONT_SELECT" in fileNames:
                continue
            for fileName in fnmatch.filter(fileNames, '*.bench_ad'):
                name_of_files.append(os.path.join(root, fileName))
    elif Tool == "AASR":
        for root, dirNames, fileNames in os.walk(root_dir):
            if "DONT_SELECT" in fileNames:
                continue
            for fileName in fnmatch.filter(fileNames, '*_bertacco.v'):
                name_of_files.append(os.path.join(root, fileName))
    else:
        for root, dirNames, fileNames in os.walk(root_dir):
            if "DONT_SELECT" in fileNames:
                continue
            for fileName in fnmatch.filter(fileNames, '*.bench'):
                name_of_files.append(os.path.join(root, fileName))
    return name_of_files

def get_flipflop_name(selected_signal_file_name):

    selected_flop_file_name = selected_signal_file_name[:selected_signal_file_name.rfind('_')] + "_flop.txt"

    ff_select = open(selected_signal_file_name, 'r')
    graph_file = open("graph.txt", 'r')
    mapping_file = open("mapping.csv", 'r')
    ff_name = open(selected_flop_file_name, 'w')

    sel_ff_ = ff_select.readlines()
    sel_ff = sel_ff_[0].split(',')
    graph_data = graph_file.readlines()
    map_data = mapping_file.readlines()
    ff_name_ = []
    for ff in sel_ff:
        ff_ = ff.lstrip().rstrip()
        if ff_:
            #print ff_
            nxt_st_conn = filter(lambda x: 'FF ' + ff_ in x, graph_data)
            if nxt_st_conn:
                #print  nxt_st_conn[0]
                connectivity_ = nxt_st_conn[0].split(':')[1].lstrip().rstrip().split(' ')
                #print connectivity_[0], connectivity_[2]
                ff_sig_to_trace = filter(lambda x: connectivity_[0] in x.split(',')[0].lstrip().rstrip(), map_data)
                if ff_sig_to_trace:
                    for ele in ff_sig_to_trace:
                        if connectivity_[0] == ele.split(',')[0].lstrip().rstrip():
                            ff_name_.append(ele.split(',')[1].lstrip().rstrip())
    if ff_name_:
        ff_name.write("\n".join(ele for ele in ff_name_))

    ff_select.close()
    graph_file.close()
    mapping_file.close()
    ff_name.close()
    return selected_flop_file_name, selected_signal_file_name

#name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/sii_inc_ctl/sii_inc_ctl_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/sii_ilc_ctl/sii_ilc_ctl_flat_preprocessed.bench']

def run_srr_tool(name_of_files, buffer_size, Tool, mode, depth):
    Master_File_Name = "MasterLogFile_" + Tool + "_" + str(buffer_size) + ".txt"
    MasterFile = open(Master_File_Name, 'w')
    for complete_filename in name_of_files:
        file_name = complete_filename[complete_filename.rfind('/') + 1 : complete_filename.rfind('.')]
        dir_name = file_name[:file_name.index('_flat')]

        report_file_name = file_name + "_" + str(buffer_size) +  "_report.txt"
        
        if Tool == 'HybrSel':
            selected_signal_file_name = file_name + ".out"
        elif Tool == 'PRankNetlist':
            selected_signal_file_name = file_name + "_pagerank.txt"
        else:
            selected_signal_file_name = file_name + "_"  + str(buffer_size) + "_selected_signal.txt"
        
        out_file_name = file_name + "_" + str(buffer_size) + "_out.txt"
        err_file_name = file_name + "_" + str(buffer_size) + "_err.txt"

        print "Selecting signal for design: " + dir_name + "\n"
    
        report_file = open(report_file_name, 'w')

        #report_file.write(20 * '#' + "\n\n")
    
        # Commands to execute different SRR tools on the different Bench files from OpenSPARC design
        if Tool == "AASR":
            mkdir_cmd_to_exe = "mkdir -pv " + dir_name + "/" + Tool
            aasr_cmd_to_exe = "AASR " + complete_filename + " " + buffer_size + " " + depth + " " + selected_signal_file_name
            
            if not os.path.isdir(dir_name + "/" + Tool):
                exec_command(mkdir_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
                print mkdir_cmd_to_exe + "\n"
                report_file.write("Command Executed: " + mkdir_cmd_to_exe + "\n\n")
            else:
                report_file.write("Directory: " + dir_name + "/" + Tool + " exists. \'mkdir\' not executed\n\n")

            print aasr_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + aasr_cmd_to_exe + "\n\n")

            start_aasr = datetime.now()
            # Allowing 120 minutes for each signal selection at most
            data, err, retcode, mem_usage = exec_command(aasr_cmd_to_exe, 120*60, out_file_name, err_file_name)
            end_aasr = datetime.now()

            report_file.write("Return code for the file for AASR command: " + file_name + " : " + str(retcode) + "\n\n")
            selected_signal_file_name_old = ''
            if retcode == 0:
                selected_signal_file_name, selected_signal_file_name_old = get_flipflop_name(selected_signal_file_name)
            else:
                report_file.write(Tool + " encountered error while selecting signals. See: " + err_file_name + " and " + out_file_name + "\n\n")

            report_file.write("Total run-time for signal selection for : " + file_name + " : " + str(end_aasr - start_aasr) + "\n\n")
            report_file.write("Peak memory usage for signal selection for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 

            mv_cmd_to_exe = "mv " + selected_signal_file_name + " " + selected_signal_file_name_old + " mapping.csv graph.txt " + dir_name + "/" + Tool
            rm_aasr_files = "rm -rfv " + "drivers.txt levels.txt"

            print mv_cmd_to_exe
            report_file.write("Command Executed: " + mv_cmd_to_exe)
            exec_command(mv_cmd_to_exe, 1*60, 'PIPE', 'PIPE')

            print rm_aasr_files
            report_file.write("Command Executed: " + rm_aasr_files)
            exec_command(rm_aasr_files, 1*60, 'PIPE', 'PIPE')

        elif Tool == 'SigSeT_1':
            mkdir_cmd_to_exe = "mkdir -pv " + dir_name + "/" + Tool
            sigset_1_cmd_to_exe = "SigSeT_1 -i " + complete_filename + " -o ./" + dir_name + "/" + Tool + "/" + selected_signal_file_name + " -n " + buffer_size + " -w 2.0 -p EG"
            if not os.path.isdir(dir_name + "/" + Tool):
                exec_command(mkdir_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
                print mkdir_cmd_to_exe + "\n"
                report_file.write("Command Executed: " + mkdir_cmd_to_exe + "\n\n")
            else:
                report_file.write("Directory: " + dir_name + "/" + Tool + " exists. \'mkdir\' not executed\n\n")

            print sigset_1_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + sigset_1_cmd_to_exe + "\n\n")
        
            start_sigset_1 = datetime.now()
            # Allowing 45 minutes for each signal selection at most
            data, err, retcode, mem_usage = exec_command(sigset_1_cmd_to_exe, 30*60, out_file_name, err_file_name)
            end_sigset_1 = datetime.now()
        
            report_file.write("Return code for the file for SigSeT_1 command: " + file_name + " : " +  str(retcode) + "\n\n")
            if retcode == 1:
                report_file.write("See: " + selected_signal_file_name + "\n\n")
            else:
                report_file.write(Tool + " encountered error while selecting signals. See: " + err_file_name + " and " + out_file_name + "\n\n")

            report_file.write("Total run-time for signal selection for : " + file_name + " : " + str(end_sigset_1 - start_sigset_1) + "\n\n")
            report_file.write("Peak memory usage for signal selection for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 

        elif Tool == 'SigSeT_2':
            ilp_problem_file_name = file_name + "_" + buffer_size + "_ilp_problem.txt" 
            lp_result_file_name =  file_name + "_" + buffer_size + "_lp_result.txt"
            mkdir_cmd_to_exe = "mkdir -pv " + dir_name + "/" + Tool
            sigset_2_cmd_to_exe = "SigSeT_2 -benchmark " + complete_filename + " -bw " + buffer_size + " -save ./" + dir_name + "/" + Tool + "/" + ilp_problem_file_name 
            lpsolve_cmd_to_exe = "LPSOLVE ./" + dir_name + "/" + Tool + "/" + ilp_problem_file_name
            parse_lpsolve_cmd_to_exe = "PARSELP ./" + dir_name + "/" + Tool + "/" + lp_result_file_name
            mv_cmd_to_exe_1 = "mv " + lp_result_file_name + " " + dir_name + "/" + Tool
            mv_cmd_to_exe_2 = "mv " + selected_signal_file_name + " " + dir_name + "/" + Tool

            if not os.path.isdir(dir_name + "/" + Tool):
                exec_command(mkdir_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
                print mkdir_cmd_to_exe + "\n"
                report_file.write("Command Executed: " + mkdir_cmd_to_exe + "\n\n")
            else:
                report_file.write("Directory: " + dir_name + "/" + Tool + " exists. \'mkdir\' not executed\n\n")

            print sigset_2_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + sigset_2_cmd_to_exe + "\n\n")

            # Allowing 45 minutes for each signal selection at most
            start_sigset_2 =  datetime.now()
            data, err, retcode, mem_usage = exec_command(sigset_2_cmd_to_exe, 30*60, out_file_name, err_file_name)
            end_sigset_2 = datetime.now()

            report_file.write("Return code for the file for SigSeT 2 command: " + file_name + " : " + str(retcode) + "\n\n")
            report_file.write("Total run-time for signal selection for : " + file_name + " : " + str(end_sigset_2 - start_sigset_2) + "\n\n")
            report_file.write("Peak memory usage for signal selection for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 

            if retcode != 0:
                next

            print lpsolve_cmd_to_exe + "\n"
            start_lpsolve = datetime.now()
            data, err, retcode, mem_usage = exec_command(lpsolve_cmd_to_exe, 10*60, lp_result_file_name, 'PIPE')
            end_lpsolve = datetime.now()
            exec_command(mv_cmd_to_exe_1, 1*60, 'PIPE', 'PIPE')

            report_file.write("Return code for the file for LPSOLVE command: " + file_name + " : " + str(retcode) + "\n\n")
            report_file.write("Total run-time of LPSOLVE for : " + file_name + " : " + str(end_lpsolve - start_lpsolve) + "\n\n")
            report_file.write("Peak memory usage for LPSOLVE for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 


            print parse_lpsolve_cmd_to_exe + "\n"
            start_parse_lp = datetime.now()
            data, err, retcode, mem_usage = exec_command(parse_lpsolve_cmd_to_exe, 10*60, selected_signal_file_name, 'PIPE')
            end_parse_lp = datetime.now()

            report_file.write("Return code for the file for LPSolve Output parsing command: " + file_name + " : " + str(retcode) + "\n\n")
            report_file.write("Total run-time of LPSolve Output parsing command for : " + file_name + " : " + str(end_lpsolve - start_lpsolve) + "\n\n")
            report_file.write("Peak memory usage for LPSolve Output parsing command for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 
            
            exec_command(mv_cmd_to_exe_2, 1*60, 'PIPE', 'PIPE')

        elif Tool == 'HybrSel':
            assign_file_name = complete_filename[:complete_filename.rfind('.')] + ".assign"

            mkdir_cmd_to_exe = "mkdir -pv " + dir_name + "/" + Tool
            cp_hybrsel_files = "cp -v " + complete_filename + " " + assign_file_name + " ."
            hybrsel_cmd_to_exe = "HybrSel " + complete_filename[complete_filename.rfind('/') + 1:] + " " + buffer_size + " " + mode
            rm_hybrsel_files = "rm -rfv " + file_name + ".bench_ad " + file_name + ".assign " + file_name + ".init_dff_val"
            sed_hybrsel_out_file = "sed -i -n -e \'2,$p\' " + file_name + ".out"
            mv_hybrsel_out_file = "mv " + file_name + ".out " + dir_name + "/" + Tool + "/" + file_name + "_" + buffer_size + "_selected_signal.txt"

            if not os.path.isdir(dir_name + "/" + Tool):
                exec_command(mkdir_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
                print mkdir_cmd_to_exe + "\n"
                report_file.write("Command Executed: " + mkdir_cmd_to_exe + "\n\n")
            else:
                report_file.write("Directory: " + dir_name + "/" + Tool + " exists. \'mkdir\' not executed\n\n")

            print cp_hybrsel_files + "\n"
            report_file.write("Command Executed: " + cp_hybrsel_files + "\n\n")
            exec_command(cp_hybrsel_files, 1*60, 'PIPE', 'PIPE')

            # Allowing 45 minutes for each signal selection at most
            print hybrsel_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + hybrsel_cmd_to_exe + "\n\n")
            start_hybrsel = datetime.now()
            data, err, retcode, mem_usage = exec_command(hybrsel_cmd_to_exe, 30*60, selected_signal_file_name, err_file_name) 
            end_hybrsel = datetime.now()

            report_file.write("Return code for the file for HybrSel command: " + file_name + " : " + str(retcode) + "\n\n")
            
            report_file.write("Total run-time for signal selection for : " + file_name + " : " + str(end_hybrsel - start_hybrsel) + "\n\n")
            report_file.write("Peak memory usage for signal selection for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 
            
            print rm_hybrsel_files
            report_file.write("Command Executed: " + rm_hybrsel_files + "\n\n")
            exec_command(rm_hybrsel_files, 1*60, 'PIPE', 'PIPE')

            print sed_hybrsel_out_file
            report_file.write("Command Executed: " + sed_hybrsel_out_file + "\n\n")
            exec_command(sed_hybrsel_out_file, 1*60, 'PIPE', 'PIPE')

            print mv_hybrsel_out_file
            report_file.write("Command Executed: " + mv_hybrsel_out_file + "\n\n")
            exec_command(mv_hybrsel_out_file, 1*60, 'PIPE', 'PIPE')

        elif Tool == 'PRankNetlist':
            temp_file_name = selected_signal_file_name + ".tmp"

            mkdir_cmd_to_exe = "mkdir -pv " + dir_name + "/" + Tool
            cd_in_cmd_to_exe = "cd " + dir_name + "/PRankNetlist"
            cd_out_cmd_to_exe = "cd ../../"
            cp_cmd_to_exe = "cp -v " + complete_filename + " ."
            rm_cmd_to_exe = "rm -rfv " + complete_filename[complete_filename.rfind('/') + 1:]
            sed_cmd_to_exe = "sed -n 1," + buffer_size + "p " + selected_signal_file_name + " | awk -F\'[(|,]\' \'{print $2}\' | tr -d \"\'\" "
            mv_cmd_to_exe = "mv " + temp_file_name + " " + file_name + "_" + buffer_size + "_selected_signal.txt"
            prnet_cmd_to_exe = "python /home/debjit/Software/PRank_NetList/iscas89_pagerank_ana.py " + complete_filename[complete_filename.rfind('/') + 1:]

            if not os.path.isdir(dir_name + "/" + Tool):
                exec_command(mkdir_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
                print mkdir_cmd_to_exe + "\n"
                report_file.write("Command Executed: " + mkdir_cmd_to_exe + "\n\n")
            else:
                report_file.write("Directory: " + dir_name + "/" + Tool + " exists. \'mkdir\' not executed\n\n")

            prevdir = os.getcwd()
            # exec_command(cd_in_cmd_to_exe, 1*60, PIPE, PIPE)
            # print cd_in_cmd_to_exe + "\n"
            os.chdir(dir_name + "/PRankNetlist")
            print "Current working directory is: " + os.getcwd() + "\n"
            #report_file.write("Command Executed: " + cd_in_cmd_to_exe + "\n\n")
            
            exec_command(cp_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
            print cp_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + cp_cmd_to_exe + "\n\n")
           
            start_prnet = datetime.now()
            data, err, retcode, mem_usage = exec_command(prnet_cmd_to_exe, 30*60, out_file_name, err_file_name)
            end_prnet = datetime.now()
            print prnet_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + prnet_cmd_to_exe + "\n\n")
            report_file.write("Return code for the file for PRankNetlist command: " + file_name + " : " + str(retcode) + "\n\n")
            report_file.write("Total run-time for signal selection for : " + file_name + " : " + str(end_prnet - start_prnet) + "\n\n")
            report_file.write("Peak memory usage for signal selection for : " + file_name + " : " + str(round(Decimal(mem_usage / 1048576), 2)) + " MB\n\n") 

            exec_command(rm_cmd_to_exe, 10*60, 'PIPE', 'PIPE')
            print rm_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + rm_cmd_to_exe + "\n\n")

            exec_command(sed_cmd_to_exe, 1*60, temp_file_name, 'PIPE')
            print sed_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + sed_cmd_to_exe + "\n\n")

            exec_command(mv_cmd_to_exe, 1*60, 'PIPE', 'PIPE')
            print mv_cmd_to_exe + "\n"
            report_file.write("Command Executed: " + mv_cmd_to_exe + "\n\n")

            os.chdir(prevdir)

            #exec_command(cd_out_cmd_to_exe, 1*60, PIPE, PIPE)
            #print cd_out_cmd_to_exe + "\n"
            #report_file.write("Command Executed: " + cd_out_cmd_to_exe + "\n\n")

        #### START FROM THIS
        #elif Tool == 'AASR':

        else:
            print "Unknown SRR based tool: " + Tool


        #report_file.write(20 * '#' + "\n\n")
    
        # To move the report files in the respective directories
        mv_cmd_to_exe = "mv " + report_file_name + " " + out_file_name + " " + err_file_name + " ./" + dir_name + "/" + Tool
        print mv_cmd_to_exe
        report_file.write("Command Executed: " + mv_cmd_to_exe + "\n")
        report_file.close()
        m_pipe = Popen(mv_cmd_to_exe, shell=True, stdout=PIPE, stderr=PIPE)
        m_out, m_err = m_pipe.communicate()
    
        print "\n\n"

        MasterFile.write("*" * 100 + "\n")
        MasterFile.write("Detail for the design module: " + dir_name + "\n\n")
        MasterFile.write("$$ Report:\n")
        MasterFile.flush()
        c1 = Popen("cat " + dir_name + "/" + Tool + "/" + report_file_name, shell=True, stderr=PIPE, stdout=MasterFile)
        c1_ret_code = c1.wait()
        MasterFile.flush()
        MasterFile.write("\n\n")
        MasterFile.write("$$ Error:\n")
        MasterFile.flush()
        c2 = Popen("cat " + dir_name + "/" + Tool + "/" + err_file_name, shell=True, stderr=PIPE, stdout=MasterFile)
        c2_ret_code = c2.wait()
        MasterFile.flush()
        MasterFile.write("\n\n")
        try:
            if os.stat(dir_name + "/" + Tool + "/" + selected_signal_file_name).st_size > 0:
                MasterFile.write("$$ Selected Signals:\n")
                MasterFile.flush()
                c3 = Popen("cat " + dir_name + "/" + Tool + "/" + selected_signal_file_name, shell=True, stderr=PIPE, stdout=MasterFile)
                c3_ret_code = c3.wait()
                MasterFile.flush()
                MasterFile.write("\n\n")
            else:
                MasterFile.write("No signals selected\n\n")
                MasterFile.flush()
        except OSError:
            MasterFile.write("No selected signals file found.\n\n")
        MasterFile.write("*" * 100 + "\n\n")
        MasterFile.flush()
    MasterFile.close()


if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-r", "--root_dir", dest="root_dir")
    parser.add_option("-b", "--buffer_sizes", default="32,64", dest="buffer_sizes")
    parser.add_option("-m", "--mode", default="-1", dest="mode")
    parser.add_option("-t", "--tool", default="SigSeT", dest="tool_names", help="Name of the Tool executable")
    parser.add_option("-d", "--depth", default="-1", dest="depth", help="depth of trace buffer in cycles")
    (options, args) = parser.parse_args()
    
    buffer_sizes = []
    for buf_size in options.buffer_sizes.split(','):
        buffer_sizes.append(buf_size)

    tool_names = []
    for tool_name in options.tool_names.split(','):
        tool_names.append(tool_name)

    if "AASR" in tool_names:
        if options.depth == -1:
            print "With AASR depth is mandatory. Specify it in -d <depth_of_trace_buffer_format>"
            sys.exit(-1)

    for Tool in tool_names:
        #name_of_files = get_file_names(options.root_dir, Tool)
        #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dec/dec_flat_preprocessed.bench']
        #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/l2t_wbuf_ctl/l2t_wbuf_ctl_flat_preprocessed.bench_ad']
        #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/s5378/s5378_flat_gtech_bertacco.v']
        #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench']
        #name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench']
        name_of_files = ['/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dmc/dmu_clu/dmu_clu_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_dsn/dmu_dsn_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/dmu_ilu/dmu_ilu_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/mcu_rdpctl_ctl/mcu_rdpctl_ctl_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/pmu/pmu_flat_preprocessed.bench', '/home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist/ncu_fcd_ctl/ncu_fcd_ctl_flat_preprocessed.bench']
        print "Working on Tool: " + Tool
        for buffer_size in buffer_sizes:
            print "Working on trace buffer size: " + str(buffer_size)
            run_srr_tool(name_of_files, buffer_size, Tool, options.mode, options.depth)
        print "\n\n"

# python run_all_netlist_sigset_1.py -r /home/debjit/Work/Post_Silicon_Journal/OpenSPARC_T2_Source_Code_Journal/ICCAD15_Extension/synthesized_netlist -b 32,64,128,256 -t SigSet_1 -m -1 -d 4096
