module dmu_ilu_global_bench();

reg [0:0] j2d_instance_id;
reg [7:0] k2y_buf_addr;
reg [127:0] k2y_buf_data;
reg [3:0] k2y_buf_dpar;
reg [31:0] k2y_csr_ring_out;
reg [5:0] k2y_dbg_sel_a;
reg [5:0] k2y_dbg_sel_b;
reg [4:0] k2y_dou_dptr;
reg [123:0] k2y_rcd;
reg [8:0] k2y_rel_rcd;
reg [95:0] p2d_csr_rcd;
reg [4:0] p2d_cto_tag;
reg [7:0] p2d_ecd_rptr;
reg [5:0] p2d_ech_rptr;
reg [7:0] p2d_erd_rptr;
reg [5:0] p2d_erh_rptr;
reg [127:0] p2d_idb_data;
reg [3:0] p2d_idb_dpar;
reg [127:0] p2d_ihb_data;
reg [3:0] p2d_ihb_dpar;
reg [6:0] p2d_ihb_wptr;
reg [2:0] p2d_mps;
reg [4:0] p2d_spare;
reg l1clk;
reg rst_wmr_;
reg j2d_rst_l;
reg j2d_por_l;
reg rst_dmu_async_por_;
reg k2y_buf_addr_vld_monitor;
reg k2y_dou_err;
reg k2y_dou_vld;
reg k2y_rcd_deq;
reg k2y_rcd_enq;
reg k2y_rel_enq;
reg p2d_ce_int;
reg p2d_csr_ack;
reg p2d_csr_req;
reg p2d_cto_req;
reg p2d_drain;
reg p2d_ibc_ack;
reg p2d_oe_int;
reg p2d_ue_int;
reg tcu_test_protect;


wire [95:0] d2p_csr_rcd;
wire [5:0] d2p_ech_wptr;
wire [127:0] d2p_edb_data;
wire [3:0] d2p_edb_dpar;
wire [7:0] d2p_edb_addr;
wire [127:0] d2p_ehb_data;
wire [3:0] d2p_ehb_dpar;
wire [5:0] d2p_ehb_addr;
wire [5:0] d2p_erh_wptr;
wire [7:0] d2p_ibc_nhc;
wire [11:0] d2p_ibc_pdc;
wire [7:0] d2p_ibc_phc;
wire [7:0] d2p_idb_addr;
wire [5:0] d2p_ihb_addr;
wire [4:0] d2p_spare;
wire [7:0] y2k_buf_addr;
wire [127:0] y2k_buf_data;
wire [3:0] y2k_buf_dpar;
wire [31:0] y2k_csr_ring_in;
wire [7:0] y2k_dbg_a;
wire [7:0] y2k_dbg_b;
wire [2:0] y2k_mps;
wire [115:0] y2k_rcd;
wire [8:0] y2k_rel_rcd;
wire [1:0] dmu_psr_rate_scale;
wire d2p_csr_ack;
wire d2p_csr_req;
wire d2p_cto_ack;
wire d2p_edb_we;
wire d2p_ehb_we;
wire d2p_ibc_req;
wire d2p_ihb_rd;
wire y2k_buf_addr_vld_monitor;
wire y2k_int_l;
wire y2k_rcd_deq;
wire y2k_rcd_enq;
wire y2k_rel_enq;
wire dmu_psr_pll_en_sds0;
wire dmu_psr_pll_en_sds1;
wire dmu_psr_rx_en_b0_sds0;
wire dmu_psr_rx_en_b1_sds0;
wire dmu_psr_rx_en_b2_sds0;
wire dmu_psr_rx_en_b3_sds0;
wire dmu_psr_rx_en_b0_sds1;
wire dmu_psr_rx_en_b1_sds1;
wire dmu_psr_rx_en_b2_sds1;
wire dmu_psr_rx_en_b3_sds1;
wire dmu_psr_tx_en_b0_sds0;
wire dmu_psr_tx_en_b1_sds0;
wire dmu_psr_tx_en_b2_sds0;
wire dmu_psr_tx_en_b3_sds0;
wire dmu_psr_tx_en_b0_sds1;
wire dmu_psr_tx_en_b1_sds1;
wire dmu_psr_tx_en_b2_sds1;
wire dmu_psr_tx_en_b3_sds1;
wire il2cl_gr_16;;

integer seed;

// Local pointer generation variable
reg [7:0] _p2d_ecd_rptr;
reg [5:0] _p2d_ech_rptr;
reg [7:0] _p2d_erd_rptr;
reg [5:0] _p2d_erh_rptr;
reg [6:0] _p2d_ihb_wptr;

reg [4:0] _k2y_dou_dptr;

initial begin
    $random_seed_gen(seed);
    seed = 444815127;
end

dmu_ilu dmu_ilu_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("dmu_ilu_rtl.vcd");
        $dumpvars(0, dmu_ilu_global_bench);
    end
`endif
*/

initial begin
    // Instance ID (From OpenSPARC Source Code)
    j2d_instance_id = 1'b0;
    // Clock and Rest Signals
    l1clk = 0;
    rst_wmr_ = 0;
    j2d_por_l = 0;
    j2d_rst_l = 0;
    rst_dmu_async_por_ = 0;
    // PCIE FC credits interface to TLU
    p2d_ibc_ack = 0;
    // CTO Interface - PIO Completion Time Out
    p2d_cto_req = 0;
    // Drain and Misc Interface
    p2d_drain = 0;
    p2d_ue_int = 0;
    p2d_ce_int = 0;
    p2d_oe_int = 0;
    // Data Path
    k2y_buf_addr_vld_monitor = 0;
    // Record interface to TMU
    k2y_rcd_deq = 0;
    k2y_rcd_enq = 0;
    // Release interface with TMU
    k2y_rel_enq = 0;
    // DOU DMA Rd CPL Buffer
    k2y_dou_err = 0;
    k2y_dou_vld = 0;
    // CSR Ring to TLU
    p2d_csr_ack = 0;
    p2d_csr_req = 0;
    // To PEU for PLL-Enable
    tcu_test_protect = 0;
    // Ptrs
	p2d_ecd_rptr = 0;
	p2d_ech_rptr = 0;
	p2d_erd_rptr = 0;
	p2d_erh_rptr = 0;

    p2d_ihb_wptr = 0;
    // Local pointer generation variable
    _p2d_ecd_rptr = 0;
	_p2d_ech_rptr = 0;
	_p2d_erd_rptr = 0;
	_p2d_erh_rptr = 0;

    _p2d_ihb_wptr = 0;
    
    k2y_dou_dptr = 0;
    _k2y_dou_dptr = 0;

end

task reset_task;
    begin
        // In 30 time units reset will be finished
        $display("Intializing design\n");
        #10;
        j2d_por_l = 1;
        #10;
        j2d_rst_l = 1;
        #10;
        rst_wmr_ = 1;
        #10;
        rst_dmu_async_por_ = 1;
        $display("Reset sequence ended: %d\n", $time);
    end
endtask

class p2d_cto;
    
    rand logic cto_req;
    rand logic [2:0] num_of_cyls;

    constraint num_of_cyls_range {num_of_cyls inside {[3'b100:3'b111]};};

    function new (int seed);
        this.srandom(seed);
        p2d_cto_cover = new();
    endfunction

    covergroup p2d_cto_cover;
        coverpoint cto_req;
        coverpoint num_of_cyls;
    endgroup

endclass

task p2d_cto_task(p2d_cto cto);
    @(posedge l1clk)
    if(cto.randomize() == 1)
    begin
        p2d_cto_req = cto.cto_req;
    end
    else
    begin
        $display("Randomization at cto failed.\n");
    end
endtask

initial begin
    p2d_cto cto;
    #5;
    cto = new(seed);
    #500;
    repeat (500) begin
        p2d_cto_task(cto);
        if(p2d_cto_req)
        begin
            p2d_cto_tag = $urandom;
            @(posedge l1clk);
            p2d_cto_req = 1'b0;
            p2d_cto_tag = {5{1'bX}};
        end
    #300;
    end
end

class p2d_error;

    rand logic _p2d_ue_int;
    rand logic _p2d_ce_int;
    rand logic _p2d_oe_int;
    rand logic _p2d_drain;
   
    constraint error_con_1 { !(_p2d_ue_int & _p2d_ce_int);};
    constraint error_con_2 { !(_p2d_ce_int & _p2d_oe_int);};
    constraint error_con_3 { !(_p2d_ue_int & _p2d_oe_int);};

    //constraint drain_state { (_p2d_ue_int | _p2d_ce_int | _p2d_oe_int) -> {_p2d_drain};}; 

    constraint ue_dist { _p2d_ue_int dist {0:=9, 1:=1};};
    constraint ce_dist { _p2d_ce_int dist {0:=9, 1:=1};};
    constraint oe_dist { _p2d_oe_int dist {0:=9, 1:=1};};
    constraint drain_dist { _p2d_drain dist {0:=9, 1:=1};};

    function new (int seed);
        this.srandom(seed);
        p2d_error_cover = new();
    endfunction

    covergroup p2d_error_cover;
        coverpoint _p2d_ue_int;
        coverpoint _p2d_ce_int;
        coverpoint _p2d_oe_int;
        coverpoint _p2d_drain;
    endgroup
endclass

task p2d_err_task(p2d_error err);
    @(posedge l1clk)
    if(err.randomize() == 1)
    begin
        p2d_ue_int = err._p2d_ue_int;
        p2d_ce_int = err._p2d_ce_int;
        p2d_oe_int = err._p2d_oe_int;
        //p2d_drain = err._p2d_drain;
        err.p2d_error_cover.sample();
    end
    else
    begin
        $display("Randomization at err failed.\n");
    end
endtask

initial begin
    p2d_error error;
    #5;
    error = new (seed);
    #500;
    repeat (400) begin
        p2d_err_task(error);
        if(!p2d_drain)
            p2d_mps = $urandom;
        else
            p2d_mps = {3{1'bX}};
        @(posedge l1clk);
        p2d_ue_int = 1'b0;
        p2d_ce_int = 1'b0;
        p2d_oe_int = 1'b0;
        p2d_drain = 1'b0;
        #200;
    end
end

class k2y_enq;

    rand logic _k2y_rcd_enq;
    rand logic [2:0] rcd_range;

    constraint rcd_enq_dist { _k2y_rcd_enq dist {0:=4, 1:=6};};
    
    logic [2:0] rcd_vals[] = '{3'b001, 3'b010, 3'b100};
    constraint rcd_range_range {
        rcd_range inside rcd_vals;
    } 

    function new (int seed);
        this.srandom(seed);
        k2y_enq_cover = new();
    endfunction

    covergroup k2y_enq_cover;
        coverpoint _k2y_rcd_enq;
    endgroup
endclass

task k2y_enq_task(k2y_enq enq);
    @(posedge l1clk)
    if(enq.randomize() == 1)
    begin
        k2y_rcd_enq = enq._k2y_rcd_enq;
        enq.k2y_enq_cover.sample();
    end
    else
    begin
        $display("Randomization at enq failed.\n");
    end
endtask

integer _range;

initial begin
    k2y_enq enq;
    #5;
    enq = new(seed);
    #500;
    repeat (600) begin
        _range = 0;
        k2y_enq_task(enq);
        if(k2y_rcd_enq)
        begin
            k2y_dou_vld = 1'b1;
            k2y_dou_dptr = _k2y_dou_dptr + 1;
            _k2y_dou_dptr = k2y_dou_dptr;
            k2y_dou_err = $urandom;
            k2y_rcd = $urandom;
            repeat (enq.rcd_range) @(posedge l1clk) begin
                if(_range == 0)
                begin
                    k2y_dou_vld = 1'b0;
                    k2y_dou_err = 1'b0;
                    k2y_dou_dptr = {5{1'bX}};
                    k2y_rcd = $urandom;
                    _range = _range + 1;
                end
                else
                begin
                    k2y_rcd = $urandom;
                end
            end
            k2y_dou_vld = 1'b0;
            k2y_dou_err = 1'b0;
            k2y_dou_dptr = {5{1'bX}};
            k2y_rcd_enq = 1'b0;
            k2y_rcd = {124{1'bX}};
        end
    #200;
    end
    /*
    repeat (600) begin
        k2y_enq_task(enq);
        if(k2y_rcd_enq)
        begin
            k2y_dou_vld = 1'b1;
            k2y_dou_dptr = _k2y_dou_dptr + 1;
            _k2y_dou_dptr = k2y_dou_dptr;
            k2y_dou_err = $urandom;
            k2y_rcd = $urandom;
            @(posedge l1clk);
            k2y_dou_vld = 1'b0;
            k2y_dou_err = 1'b0;
            k2y_dou_dptr = {5{1'bX}};
            repeat (enq.rcd_range) @(posedge l1clk) begin
                k2y_rcd =$urandom;
            end
            k2y_rcd_enq = 1'b0;
            k2y_rcd = {124{1'bX}};
        end
    #200;
    end
    */
end

class k2y_buf;
    
    rand logic _k2y_buf_addr_vld_monitor;

    function new (int seed);
        this.srandom(seed);
        k2y_buf_cover = new();
    endfunction 

    covergroup k2y_buf_cover;
        coverpoint _k2y_buf_addr_vld_monitor;
    endgroup
endclass

task k2y_buf_task (k2y_buf _k2y);
    @(posedge l1clk)
    if (_k2y.randomize() == 1)
    begin
       k2y_buf_addr_vld_monitor = _k2y._k2y_buf_addr_vld_monitor;
       _k2y.k2y_buf_cover.sample();
    end
    else
    begin
        $display("Randomization at _k2y failed.\n");
    end
endtask


// k2y_rel_enq and k2y_rel_rcd need to be connected
integer range;

initial begin
    k2y_buf _k2y;
    #5;
    _k2y = new(seed);
    #10;
    reset_task;
    #600;
    repeat (500) begin
        range = $urandom_range(6, 8);
        k2y_buf_task(_k2y);
        if(_k2y._k2y_buf_addr_vld_monitor)
        begin
            k2y_buf_addr = $random;
            repeat (range) @(posedge l1clk) begin
                k2y_buf_addr_vld_monitor = 1'b1;
                k2y_buf_addr = $random;
                k2y_buf_data = $random;
                k2y_buf_dpar[3] = ^k2y_buf_data[127:96];
                k2y_buf_dpar[2] = ^k2y_buf_data[95:64];
                k2y_buf_dpar[1] = ^k2y_buf_data[63:32];
                k2y_buf_dpar[0] = ^k2y_buf_data[31:0];
            end
            @(posedge l1clk);
            k2y_buf_addr_vld_monitor = 1'b0;
            k2y_buf_addr = {8{1'bX}};
            k2y_buf_data = $urandom;
            k2y_buf_dpar[3] = ^k2y_buf_data[127:96];
            k2y_buf_dpar[2] = ^k2y_buf_data[95:64];
            k2y_buf_dpar[1] = ^k2y_buf_data[63:32];
            k2y_buf_dpar[0] = ^k2y_buf_data[31:0];
            @(posedge l1clk);
            k2y_buf_data = {128{1'bX}};
            k2y_buf_dpar = {4{1'bX}};
        end
    #100;
    end
end

// p2d_csr_req and p2d_csr_rcd need to be connected. p2d_csr_ack goes with p2d_csr_req

class p2d_csr;

    rand logic csr_ack;
    rand logic csr_req;

    constraint ac_req_con { !(csr_ack & csr_req);};
    constraint csr_ack_dist { csr_ack dist {0:=7, 1:=3};};
    constraint csr_req_dist { csr_req dist {0:=4, 1:=6};};


    function new (int seed);
        this.srandom(seed);
        p2d_csr_cover = new();
    endfunction

    covergroup p2d_csr_cover;
        coverpoint csr_ack;
        coverpoint csr_req;
    endgroup
endclass

task p2d_csr_task(p2d_csr csr);
    @(posedge l1clk)
    if(csr.randomize() == 1)
    begin
        p2d_csr_ack = csr.csr_ack;
        p2d_csr_req = csr.csr_req;
        /*
        if (p2d_csr_req)
        begin
            p2d_csr_rcd = $urandom;
        end
        */
        csr.p2d_csr_cover.sample();
    end
    else
    begin
        $display("Randomization at csr failed.\n");
    end
endtask

initial begin
    p2d_csr csr;
    #5;
    csr = new(seed);
    #600;
    repeat (400) begin
        p2d_csr_task(csr);
        if(p2d_csr_req)
        begin
            p2d_csr_rcd = $urandom;
            @(posedge l1clk);
            p2d_csr_req = 1'b0;
            p2d_csr_rcd = {96{1'bX}};
        end
        if(p2d_csr_ack)
        begin
            @(posedge l1clk);
            p2d_csr_ack = 1'b0;
        end
    #200;
    end
end


            
always begin
    #2000;
    p2d_ibc_ack =1'b1;
    #3;
    p2d_ibc_ack = 1'b0;
end

/*
always begin
    #2500;
    p2d_cto_req = 1'b1;
    #3;
    p2d_cto_req = 1'b0;
end
*/

always begin
    #2000;
    k2y_rcd_deq = 1'b1;
    #12;
    k2y_rcd_deq =- 1'b0;
end

always begin
    // 350 MHz clock
    #1.5 l1clk = ~l1clk;
end

initial begin
    #2000;
    repeat(100) @(posedge l1clk)begin
        k2y_csr_ring_out = $urandom;
        #500;
    end
    $finish;
end

always begin
    #1000;
    p2d_ibc_ack = 1'b1;
    #3; 
    p2d_ibc_ack = 1'b0;
end

function bit [7:0] bin2gray(input bit [7:0] bincode);
    bit [7:0] graycode;
    graycode[7] = bincode[7];
    graycode[6] = bincode[7] ^ bincode[6];
    graycode[5] = bincode[6] ^ bincode[5];
    graycode[4] = bincode[5] ^ bincode[4];
    graycode[3] = bincode[4] ^ bincode[3];
    graycode[2] = bincode[3] ^ bincode[2];
    graycode[1] = bincode[2] ^ bincode[1];
    graycode[0] = bincode[1] ^ bincode[0];
    //$display("Received Binary Code: %b\n", bincode);
    //$display("Returned Gray Code: %b\n", graycode);
    return graycode;
endfunction

always begin
    #2000;
    p2d_ech_rptr = bin2gray({2'b00, _p2d_ech_rptr});
    p2d_ecd_rptr = bin2gray(_p2d_ecd_rptr);
    _p2d_ech_rptr = _p2d_ech_rptr + 1;
    _p2d_ecd_rptr = _p2d_ecd_rptr + 1;
end

always begin
    #3000;
    p2d_erh_rptr = bin2gray({2'b00, _p2d_erh_rptr});
    p2d_erd_rptr = bin2gray(_p2d_erd_rptr);
    _p2d_erh_rptr = _p2d_erh_rptr + 1;
    _p2d_erd_rptr = _p2d_erd_rptr + 1;
end

always begin
    #2500;
    p2d_ihb_wptr = bin2gray({2'b00, _p2d_ihb_wptr});
    _p2d_ihb_wptr = _p2d_ihb_wptr + 1;
    p2d_ihb_data = $urandom;
    p2d_ihb_dpar[3] = ^p2d_ihb_data[127:96];
    p2d_ihb_dpar[2] = ^p2d_ihb_data[95:64];
    p2d_ihb_dpar[1] = ^p2d_ihb_data[63:32];
    p2d_ihb_dpar[0] = ^p2d_ihb_data[31:0];
end

always begin
    #2600;
    p2d_idb_data = $urandom;
    p2d_idb_dpar[3] = ^p2d_idb_data[127:96];
    p2d_idb_dpar[2] = ^p2d_idb_data[95:64];
    p2d_idb_dpar[1] = ^p2d_idb_data[63:32];
    p2d_idb_dpar[0] = ^p2d_idb_data[31:0];
end

always begin
    #2500;
    k2y_rel_enq = $urandom;
    if(k2y_rel_enq)
    begin
        k2y_rel_rcd = $urandom;
    end
    else
    begin
        k2y_rel_rcd = {9{1'bX}};
    end
    @(posedge l1clk);
    k2y_rel_enq = 1'b0;
end

`include "dmu_ilu_rtl_trace.v"

endmodule
