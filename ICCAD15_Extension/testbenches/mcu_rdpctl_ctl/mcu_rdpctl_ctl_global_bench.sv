/* Testbench file for design pmu generated on 2017-07-24 23:55:54.221250 */


module mcu_rdpctl_ctl_global_bench();

logic [19:0] drif_send_info;
logic [1:0] readdp_ecc_single_err;
logic [1:0] readdp_ecc_multi_err;
logic [15:0] readdp0_syndrome;
logic [15:0] readdp1_syndrome;
logic [35:0] readdp0_ecc_loc;
logic [35:0] readdp1_ecc_loc;
logic [31:0] drif_scrub_addr;
logic [1:0] mcu_id;
logic [2:0] l2if0_rd_dummy_req_id;
logic [2:0] l2if1_rd_dummy_req_id;
logic [39:0] drif_ucb_data_39to0;
logic [63:54] drif_ucb_data_63to54;
logic fbdic_serdes_dtm;
logic fbdic_rddata_vld;
logic fbdic_crc_error;
logic fbdic_chnl_reset_error;
logic drif_err_state_crc_fr;
logic fbdic_chnl_reset_error_mode;
logic fbdic_err_unrecov;
logic fbdic_err_recov;
logic fbdic_cfgrd_crc_error;
logic drif_send_info_val;
logic drif_single_channel_mode;
logic l2if0_rd_dummy_req;
logic l2if0_rd_dummy_req_addr5;
logic l2if0_rd_dummy_addr_err;
logic l2if1_rd_dummy_req;
logic l2if1_rd_dummy_req_addr5;
logic l2if1_rd_dummy_addr_err;
logic drif_err_sts_reg_ld;
logic drif_err_addr_reg_ld;
logic drif_err_cnt_reg_ld;
logic drif_err_loc_reg_ld;
logic drif_err_retry_reg_ld;
logic drif_dbg_trig_reg_ld;
logic rdata_err_ecci;
logic rdata_pm_1mcu;
logic rdata_pm_2mcu;
logic drl2clk;
logic scan_in;
logic wmr_scan_in;
logic tcu_pce_ov;
logic tcu_aclk;
logic tcu_bclk;
logic aclk_wmr;
logic tcu_scan_en;
logic wmr_protect;


wire [35:0] rdpctl_err_addr_reg;
wire [25:0] rdpctl_err_sts_reg;
wire [35:0] rdpctl_err_loc;
wire [15:0] rdpctl_err_cnt;
wire [36:0] rdpctl_err_retry_reg;
wire [7:0] rdpctl_drq0_clear_ent;
wire [7:0] rdpctl_drq1_clear_ent;
wire [14:0] rdpctl_err_fifo_data;
wire [1:0] rdpctl_fbd_unrecov_err;
wire [2:0] rdpctl_rd_req_id;
wire [2:0] rdpctl_rddata_en;
wire [1:0] rdpctl_dtm_mask_chnl;
wire [1:0] rdpctl_dtm_chnl_enable;
wire rdpctl_scrub_addrinc_en;
wire rdpctl_dbg_trig_enable;
wire rdpctl_err_fifo_enq;
wire rdpctl_fifo_empty;
wire rdpctl_fifo_full;
wire rdpctl_no_crc_err;
wire rdpctl_crc_err;
wire rdpctl_fbd0_recov_err;
wire rdpctl_fbd1_recov_err;
wire rdpctl_crc_recov_err;
wire rdpctl_crc_unrecov_err;
wire rdpctl_scrub_read_done;
wire rdpctl_scrb0_err_valid;
wire rdpctl_scrb1_err_valid;
wire rdpctl_l2t0_data_valid;
wire rdpctl_l2t1_data_valid;
wire rdpctl_qword_id;
wire rdpctl_pa_err;
wire rdpctl_radr_parity;
wire rdpctl_inj_ecc_err;
wire rdpctl0_dummy_data_valid;
wire rdpctl1_dummy_data_valid;
wire rdpctl_secc_cnt_intr;
wire rdpctl_scrub_wren;
wire rdpctl_mask_err;
wire rdpctl_dtm_atspeed;
wire scan_out;
wire wmr_scan_out;

integer seed;

initial begin
    $random_seed_gen(seed);
end

mcu_rdpctl_ctl mcu_rdpctl_ctl_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("mcu_rdpctl_ctl_rtl.vcd");
        $dumpvars(0, mcu_rdpctl_ctl_);
    end
`endif
*/

initial begin
    // module clock
    drl2clk = 0;
    // scan signals
    tcu_aclk = 0;
    tcu_bclk = 0;
    tcu_pce_ov = 0;
    tcu_scan_en = 0;
    
    scan_in = 0;
    
    wmr_scan_in = 0;
    wmr_protect = 0;
    aclk_wmr = 0;

    // Control signals like Error, Valid
    fbdic_serdes_dtm = 0;
    fbdic_rddata_vld = 0;
    fbdic_crc_error = 0;
    fbdic_chnl_reset_error = 0;
    drif_err_state_crc_fr = 0;
    fbdic_chnl_reset_error_mode = 0;
    fbdic_err_recov = 0;
    fbdic_err_unrecov = 0;
    fbdic_cfgrd_crc_error = 0;

    // Information returning read data
    drif_send_info_val = 0;
    readdp_ecc_single_err = 0;
	readdp_ecc_multi_err = 0;
	readdp0_syndrome = 0;
	readdp1_syndrome = 0;
	readdp0_ecc_loc = 0;
	readdp1_ecc_loc = 0;
	
	drif_scrub_addr = 0;
	mcu_id = 0;

    drif_single_channel_mode = 0;

    l2if0_rd_dummy_req = 0;
    l2if0_rd_dummy_req_addr5 = 0;
    l2if0_rd_dummy_req_id = 0;
	l2if0_rd_dummy_addr_err = 0;

    l2if1_rd_dummy_req = 0;
	l2if1_rd_dummy_req_addr5 = 0;
    l2if1_rd_dummy_req_id = 0;
    l2if1_rd_dummy_addr_err = 0;

    drif_err_sts_reg_ld = 0;
	drif_err_addr_reg_ld = 0;
	drif_err_cnt_reg_ld = 0;
	drif_err_loc_reg_ld = 0;
	drif_err_retry_reg_ld = 0;
	drif_dbg_trig_reg_ld = 0;

	rdata_err_ecci = 0;

end

always begin
    // 400 MHz Clock
	#1.25 drl2clk = ~drl2clk;
end

class _mcu_rdpctl_ctl;

	rand logic [19:0]	_drif_send_info;		// info for CMD A slot (rd or wr)
    
    rand logic [39:0] _drif_ucb_data_39to0;
    rand logic [63:54] _drif_ucb_data_63to54;

	rand logic _rdata_pm_1mcu;
	rand logic _rdata_pm_2mcu;

    constraint drif_send_info_range { _drif_send_info inside {[0:20'hF_FFFF]};};
    constraint drif_ucb_data_39to0_range { _drif_ucb_data_39to0 inside {[0:40'hFF_FFFF_FFFF]};};
    constraint drif_ucb_data_63to54_range { _drif_ucb_data_63to54 inside {[0:10'h3FF]};};

    constraint rdata_pm_1mcu_range { _rdata_pm_1mcu dist {1:=7, 0:=3};};
    constraint rdata_pm_2mcu_range { _rdata_pm_2mcu dist {1:=2, 0:=8};};

    function new (int seed);
        this.srandom(seed);
        mcu_rdpctl_ctl_phy_data_cover = new();
    endfunction

    covergroup mcu_rdpctl_ctl_phy_data_cover;
        coverpoint _drif_send_info;
        coverpoint _drif_ucb_data_39to0;
        coverpoint _drif_ucb_data_63to54;
    endgroup
endclass

task reset_task;
    begin
        $display("Initializing design\n");
        #100;
        fbdic_rddata_vld = 1;
        drif_send_info_val = 1;
        #10;
        $display("Reset sequence ended: %d\n", $time);
    end
endtask

task mcu_rdpctl_ctl_phy_data(_mcu_rdpctl_ctl mcu_rdpctl_ctl_data);
    @(posedge drl2clk)
    if (mcu_rdpctl_ctl_data.randomize() == 1)
    begin
        drif_send_info = mcu_rdpctl_ctl_data._drif_send_info;
        drif_ucb_data_39to0 = mcu_rdpctl_ctl_data._drif_ucb_data_39to0;
        drif_ucb_data_63to54 = mcu_rdpctl_ctl_data._drif_ucb_data_63to54;

        rdata_pm_1mcu = mcu_rdpctl_ctl_data._rdata_pm_1mcu;
        rdata_pm_2mcu = mcu_rdpctl_ctl_data._rdata_pm_2mcu;

        mcu_rdpctl_ctl_data.mcu_rdpctl_ctl_phy_data_cover.sample();
    end
    else
    begin
        $display("Randomization at mcu_rdpctl_ctl_phy_data failed.\n");
    end
endtask

// Randomizing some of the data / control / error detection signal

always begin
    #10000 fbdic_crc_error = 1;
    #10 fbdic_crc_error = 0;
end

always begin
    #20000 fbdic_chnl_reset_error = 1;
    fbdic_chnl_reset_error_mode = 1;
    #10 fbdic_chnl_reset_error = 0;
    fbdic_chnl_reset_error_mode = 0;
end


initial begin
    _mcu_rdpctl_ctl mcu_rdpctl_ctl_data;
    #5;
    $display("Seed is: %d \n", seed);
    mcu_rdpctl_ctl_data = new(seed);
    #10;
    reset_task;
    #10;
    repeat (1024) begin
        mcu_rdpctl_ctl_phy_data(mcu_rdpctl_ctl_data);
        #100;
    end
    $finish;
end

`include "mcu_rdpctl_ctl_rtl_trace.v"


endmodule
