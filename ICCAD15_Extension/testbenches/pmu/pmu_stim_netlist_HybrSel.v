`ifdef HybrSel
	initial begin
		#0 pmu_pdp_dppicl014 = 1'b0;
		#39340.0 pmu_pdp_dppicl014 = 1'bx;
	end
	assign `SIG19 = pmu_pdp_dppicl014;

	initial begin
		#0 pmu_pdp_dppicl015 = 1'b0;
		#39340.0 pmu_pdp_dppicl015 = 1'bx;
	end
	assign `SIG29 = pmu_pdp_dppicl015;

	initial begin
		#0 pmu_pdp_dppicl016 = 1'b0;
		#39340.0 pmu_pdp_dppicl016 = 1'bx;
	end
	assign `SIG34 = pmu_pdp_dppicl016;

	initial begin
		#0 n11801 = 1'b0;
		#39340.0 n11801 = 1'bx;
	end
	assign `SIG32 = n11801;

	initial begin
		#0 pmu_pct_ctlpcr3_read17 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read17 = 1'bx;
	end
	assign `SIG233 = pmu_pct_ctlpcr3_read17;

	initial begin
		#0 pmu_pct_ctlfl_w1 = 1'b0;
		#39340.0 pmu_pct_ctlfl_w1 = 1'b1;
	end
	assign `SIG108 = pmu_pct_ctlfl_w1;

	initial begin
		#0 pmu_pct_ctlpcr6_read0 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read0 = 1'bx;
	end
	assign `SIG209 = pmu_pct_ctlpcr6_read0;

	initial begin
		#0 pmu_pct_ctltg0_e10 = 1'b0;
		#63740.0 pmu_pct_ctltg0_e10 = 1'b1;
	end
	assign `SIG104 = pmu_pct_ctltg0_e10;

	initial begin
		#0 pmu_pct_ctlpcr6_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read9 = 1'bx;
	end
	assign `SIG122 = pmu_pct_ctlpcr6_read9;

	initial begin
		#0 pmu_pct_ctltg0_e12 = 1'b0;
		#39340.0 pmu_pct_ctltg0_e12 = 1'b1;
		#24400.0 pmu_pct_ctltg0_e12 = 1'b0;
	end
	assign `SIG105 = pmu_pct_ctltg0_e12;

	initial begin
		#0 n6340dummy = 1'b0;
		#39340.0 n6340dummy = 1'b1;
	end
	assign `SIG203 = n6340dummy;

	initial begin
		#0 pmu_pct_ctlpcr4_read8 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read8 = 1'bx;
	end
	assign `SIG241 = pmu_pct_ctlpcr4_read8;

	initial begin
		#0 pmu_pct_ctltlu_fl_w10 = 1'b0;
	end
	assign `SIG146 = pmu_pct_ctltlu_fl_w10;

	initial begin
		#0 pmu_pct_ctltlu_fl_w11 = 1'b0;
		#39340.0 pmu_pct_ctltlu_fl_w11 = 1'b1;
		#14400.0 pmu_pct_ctltlu_fl_w11 = 1'b0;
	end
	assign `SIG189 = pmu_pct_ctltlu_fl_w11;

	initial begin
		#0 pmu_pdp_dppich630 = 1'b0;
		#39340.0 pmu_pdp_dppich630 = 1'bx;
	end
	assign `SIG65 = pmu_pdp_dppich630;

	initial begin
		#0 pmu_pct_ctlskip_wakeup = 1'b0;
		#39340.0 pmu_pct_ctlskip_wakeup = 1'bx;
	end
	assign `SIG163 = pmu_pct_ctlskip_wakeup;

	initial begin
		#0 pmu_pct_ctlhash = 1'b0;
	end
	assign `SIG97 = pmu_pct_ctlhash;

	initial begin
		#0 n6311dummy = 1'b0;
		#39340.0 n6311dummy = 1'b1;
	end
	assign `SIG252 = n6311dummy;

	initial begin
		#0 pmu_pct_ctlxbar1 = 1'b0;
		#39440.0 pmu_pct_ctlxbar1 = 1'b1;
		#24300.0 pmu_pct_ctlxbar1 = 1'b0;
	end
	assign `SIG242 = pmu_pct_ctlxbar1;

	initial begin
		#0 pmu_pct_ctlpcr1_read8 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read8 = 1'bx;
	end
	assign `SIG253 = pmu_pct_ctlpcr1_read8;

	initial begin
		#0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#39440.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'bx;
		#100.0 pmu_pdp_dppic4c0_0l1en = 1'b0;
	end
	assign `SIG7 = pmu_pdp_dppic4c0_0l1en;

	initial begin
		#0 pmu_pct_ctlpcr6_read26 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read26 = 1'bx;
	end
	assign `SIG176 = pmu_pct_ctlpcr6_read26;

	initial begin
		#0 pmu_rngl_cdbus55 = 1'b0;
	end
	assign `SIG99 = pmu_rngl_cdbus55;

	initial begin
		#0 n11038dummy = 1'b0;
		#39340.0 n11038dummy = 1'bx;
	end
	assign `SIG66 = n11038dummy;

	initial begin
		#0 pmu_pct_ctlpcr1_read5 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read5 = 1'bx;
	end
	assign `SIG125 = pmu_pct_ctlpcr1_read5;

	initial begin
		#0 pmu_rngl_cdbus62 = 1'b0;
		#39440.0 pmu_rngl_cdbus62 = 1'b1;
		#24300.0 pmu_rngl_cdbus62 = 1'b0;
	end
	assign `SIG98 = pmu_rngl_cdbus62;

	initial begin
		#0 pct_incr_pic_w16 = 1'b0;
		#39440.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#100.0 pct_incr_pic_w16 = 1'b0;
		#100.0 pct_incr_pic_w16 = 1'b1;
		#22100.0 pct_incr_pic_w16 = 1'b0;
	end
	assign `SIG1 = pct_incr_pic_w16;

	initial begin
		#0 pmu_pct_ctlpcr3_read11 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read11 = 1'bx;
	end
	assign `SIG254 = pmu_pct_ctlpcr3_read11;

	initial begin
		#0 pmu_rngl_cdbus64 = 1'b0;
		#39340.0 pmu_rngl_cdbus64 = 1'b1;
	end
	assign `SIG92 = pmu_rngl_cdbus64;

	initial begin
		#0 pmu_pct_ctlpcr5_read17 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read17 = 1'bx;
	end
	assign `SIG166 = pmu_pct_ctlpcr5_read17;

	initial begin
		#0 pmu_pct_ctlpcr2_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read27 = 1'bx;
	end
	assign `SIG153 = pmu_pct_ctlpcr2_read27;

	initial begin
		#0 n6334dummy = 1'b0;
	end
	assign `SIG137 = n6334dummy;

	initial begin
		#0 pmu_pct_ctlpcr3_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read30 = 1'bx;
	end
	assign `SIG235 = pmu_pct_ctlpcr3_read30;

	initial begin
		#0 n11614 = 1'b0;
		#39340.0 n11614 = 1'bx;
	end
	assign `SIG24 = n11614;

	initial begin
		#0 n6323dummy = 1'b0;
		#39340.0 n6323dummy = 1'b1;
	end
	assign `SIG156 = n6323dummy;

	initial begin
		#0 pmu_pct_ctlpcr6_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read7 = 1'bx;
	end
	assign `SIG190 = pmu_pct_ctlpcr6_read7;

	initial begin
		#0 pmu_pct_ctlpcr6_read6 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read6 = 1'bx;
	end
	assign `SIG224 = pmu_pct_ctlpcr6_read6;

	initial begin
		#0 pmu_pct_ctlpcr0_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read28 = 1'bx;
	end
	assign `SIG142 = pmu_pct_ctlpcr0_read28;

	initial begin
		#0 pmu_pct_ctlx6_10 = 1'b0;
		#39340.0 pmu_pct_ctlx6_10 = 1'bx;
	end
	assign `SIG76 = pmu_pct_ctlx6_10;

	initial begin
		#0 pmu_pct_ctlx6_11 = 1'b0;
		#39340.0 pmu_pct_ctlx6_11 = 1'bx;
	end
	assign `SIG90 = pmu_pct_ctlx6_11;

	initial begin
		#0 pmu_pct_ctlpcr6_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read27 = 1'bx;
	end
	assign `SIG141 = pmu_pct_ctlpcr6_read27;

	initial begin
		#0 pmu_pct_ctlpcr3_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read28 = 1'bx;
	end
	assign `SIG157 = pmu_pct_ctlpcr3_read28;

	initial begin
		#0 pmu_pct_ctlpriv3 = 1'b0;
		#39340.0 pmu_pct_ctlpriv3 = 1'b1;
		#24400.0 pmu_pct_ctlpriv3 = 1'b0;
	end
	assign `SIG240 = pmu_pct_ctlpriv3;

	initial begin
		#0 pmu_pct_ctlpriv2 = 1'b0;
		#53740.0 pmu_pct_ctlpriv2 = 1'b1;
	end
	assign `SIG175 = pmu_pct_ctlpriv2;

	initial begin
		#0 pmu_pct_ctlpcr7_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read15 = 1'bx;
	end
	assign `SIG229 = pmu_pct_ctlpcr7_read15;

	initial begin
		#0 pmu_pct_ctlpriv6 = 1'b0;
	end
	assign `SIG255 = pmu_pct_ctlpriv6;

	initial begin
		#0 pmu_pct_ctlpcr5_read16 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read16 = 1'bx;
	end
	assign `SIG221 = pmu_pct_ctlpcr5_read16;

	initial begin
		#0 pmu_pdp_dppich723 = 1'b0;
		#39340.0 pmu_pdp_dppich723 = 1'bx;
	end
	assign `SIG64 = pmu_pdp_dppich723;

	initial begin
		#0 pmu_pct_ctlpcr5_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read14 = 1'bx;
	end
	assign `SIG167 = pmu_pct_ctlpcr5_read14;

	initial begin
		#0 pmu_pct_ctlpcr5_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read15 = 1'bx;
	end
	assign `SIG218 = pmu_pct_ctlpcr5_read15;

	initial begin
		#0 pmu_pct_ctllsu_tid_b2 = 1'b0;
		#39340.0 pmu_pct_ctllsu_tid_b2 = 1'b1;
		#100.0 pmu_pct_ctllsu_tid_b2 = 1'b0;
		#24500.0 pmu_pct_ctllsu_tid_b2 = 1'b1;
	end
	assign `SIG69 = pmu_pct_ctllsu_tid_b2;

	initial begin
		#0 pmu_pct_ctlpcr5_read10 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read10 = 1'bx;
	end
	assign `SIG168 = pmu_pct_ctlpcr5_read10;

	initial begin
		#0 pmu_pct_ctlpcr5_read11 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read11 = 1'bx;
	end
	assign `SIG251 = pmu_pct_ctlpcr5_read11;

	initial begin
		#0 scan_out_ = 1'b0;
	end
	assign `SIG119 = scan_out_;

	initial begin
		#0 pmu_pct_ctlrc4 = 1'b0;
		#39340.0 pmu_pct_ctlrc4 = 1'b1;
		#24400.0 pmu_pct_ctlrc4 = 1'b0;
	end
	assign `SIG91 = pmu_pct_ctlrc4;

	initial begin
		#0 n11726 = 1'b0;
		#39340.0 n11726 = 1'bx;
	end
	assign `SIG27 = n11726;

	initial begin
		#0 pmu_pct_ctlx0_10 = 1'b0;
		#39340.0 pmu_pct_ctlx0_10 = 1'bx;
	end
	assign `SIG83 = pmu_pct_ctlx0_10;

	initial begin
		#0 pmu_pct_ctlpcr2_read10 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read10 = 1'bx;
	end
	assign `SIG199 = pmu_pct_ctlpcr2_read10;

	initial begin
		#0 pmu_pct_ctltlu_pmu_trap_mask_m0 = 1'b0;
		#63740.0 pmu_pct_ctltlu_pmu_trap_mask_m0 = 1'b1;
	end
	assign `SIG103 = pmu_pct_ctltlu_pmu_trap_mask_m0;

	initial begin
		#0 pmu_pct_ctltlu_pmu_trap_mask_m1 = 1'b0;
		#39340.0 pmu_pct_ctltlu_pmu_trap_mask_m1 = 1'b1;
		#24400.0 pmu_pct_ctltlu_pmu_trap_mask_m1 = 1'b0;
	end
	assign `SIG107 = pmu_pct_ctltlu_pmu_trap_mask_m1;

	initial begin
		#0 pmu_pct_ctlpcr3_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read29 = 1'bx;
	end
	assign `SIG245 = pmu_pct_ctlpcr3_read29;

	initial begin
		#0 n8464 = 1'b0;
		#39340.0 n8464 = 1'bx;
	end
	assign `SIG33 = n8464;

	initial begin
		#0 pmu_pct_ctlpcr4_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read30 = 1'bx;
	end
	assign `SIG234 = pmu_pct_ctlpcr4_read30;

	initial begin
		#0 n11741 = 1'b0;
		#39340.0 n11741 = 1'bx;
	end
	assign `SIG49 = n11741;

	initial begin
		#0 n5842 = 1'b0;
		#39340.0 n5842 = 1'b1;
		#100.0 n5842 = 1'b0;
		#24300.0 n5842 = 1'b1;
	end
	assign `SIG237 = n5842;

	initial begin
		#0 pmu_pct_ctlpcr3_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read20 = 1'bx;
	end
	assign `SIG160 = pmu_pct_ctlpcr3_read20;

	initial begin
		#0 pmu_pct_ctlpcr3_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read23 = 1'bx;
	end
	assign `SIG180 = pmu_pct_ctlpcr3_read23;

	initial begin
		#0 pmu_pct_ctlpcr3_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read22 = 1'bx;
	end
	assign `SIG116 = pmu_pct_ctlpcr3_read22;

	initial begin
		#0 n11599 = 1'b0;
		#39340.0 n11599 = 1'bx;
	end
	assign `SIG13 = n11599;

	initial begin
		#0 pmu_pct_ctlpcr3_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read27 = 1'bx;
	end
	assign `SIG169 = pmu_pct_ctlpcr3_read27;

	initial begin
		#0 pmu_pct_ctlpcr5_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read27 = 1'bx;
	end
	assign `SIG214 = pmu_pct_ctlpcr5_read27;

	initial begin
		#0 pmu_pct_ctlx5_11 = 1'b0;
		#39340.0 pmu_pct_ctlx5_11 = 1'bx;
	end
	assign `SIG88 = pmu_pct_ctlx5_11;

	initial begin
		#0 pmu_pct_ctlx5_10 = 1'b0;
		#39340.0 pmu_pct_ctlx5_10 = 1'bx;
	end
	assign `SIG79 = pmu_pct_ctlx5_10;

	initial begin
		#0 pmu_pct_ctlpcr7_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read30 = 1'bx;
	end
	assign `SIG206 = pmu_pct_ctlpcr7_read30;

	initial begin
		#0 pmu_pct_ctlpcr6_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read22 = 1'bx;
	end
	assign `SIG110 = pmu_pct_ctlpcr6_read22;

	initial begin
		#0 pmu_pct_ctlx2_11 = 1'b0;
		#39340.0 pmu_pct_ctlx2_11 = 1'bx;
	end
	assign `SIG81 = pmu_pct_ctlx2_11;

	initial begin
		#0 pmu_pct_ctlpcr0_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read15 = 1'bx;
	end
	assign `SIG132 = pmu_pct_ctlpcr0_read15;

	initial begin
		#0 n8490 = 1'b0;
		#39340.0 n8490 = 1'bx;
	end
	assign `SIG42 = n8490;

	initial begin
		#0 pmu_pct_ctlpcr0_read17 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read17 = 1'bx;
	end
	assign `SIG147 = pmu_pct_ctlpcr0_read17;

	initial begin
		#0 n6332dummy = 1'b0;
		#39340.0 n6332dummy = 1'b1;
	end
	assign `SIG194 = n6332dummy;

	initial begin
		#0 pmu_pct_ctlpcr1_read0 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read0 = 1'bx;
	end
	assign `SIG226 = pmu_pct_ctlpcr1_read0;

	initial begin
		#0 pmu_pct_ctlpcr0_read16 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read16 = 1'bx;
	end
	assign `SIG216 = pmu_pct_ctlpcr0_read16;

	initial begin
		#0 n8392 = 1'b0;
		#39340.0 n8392 = 1'bx;
	end
	assign `SIG11 = n8392;

	initial begin
		#0 pmu_pct_ctlfl_w0 = 1'b0;
		#63740.0 pmu_pct_ctlfl_w0 = 1'b1;
	end
	assign `SIG101 = pmu_pct_ctlfl_w0;

	initial begin
		#0 pmu_pct_ctlpcr1_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read7 = 1'bx;
	end
	assign `SIG183 = pmu_pct_ctlpcr1_read7;

	initial begin
		#0 n10733 = 1'b0;
		#39340.0 n10733 = 1'bx;
	end
	assign `SIG17 = n10733;

	initial begin
		#0 pmu_pct_ctlpcr1_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read9 = 1'bx;
	end
	assign `SIG126 = pmu_pct_ctlpcr1_read9;

	initial begin
		#0 pmu_pct_ctlpcr2_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read15 = 1'bx;
	end
	assign `SIG123 = pmu_pct_ctlpcr2_read15;

	initial begin
		#0 pmu_rngl_cdbus13 = 1'b0;
		#39340.0 pmu_rngl_cdbus13 = 1'b1;
		#24400.0 pmu_rngl_cdbus13 = 1'b0;
	end
	assign `SIG102 = pmu_rngl_cdbus13;

	initial begin
		#0 n11583 = 1'b0;
		#39340.0 n11583 = 1'bx;
	end
	assign `SIG23 = n11583;

	initial begin
		#0 n8477 = 1'b0;
		#39340.0 n8477 = 1'bx;
	end
	assign `SIG25 = n8477;

	initial begin
		#0 n6330dummy = 1'b0;
		#39340.0 n6330dummy = 1'b1;
	end
	assign `SIG197 = n6330dummy;

	initial begin
		#0 pmu_pct_ctlpcr0_read13 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read13 = 1'bx;
	end
	assign `SIG223 = pmu_pct_ctlpcr0_read13;

	initial begin
		#0 n5837 = 1'b0;
	end
	assign `SIG100 = n5837;

	initial begin
		#0 pmu_pct_ctlpcr1_read12 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read12 = 1'bx;
	end
	assign `SIG238 = pmu_pct_ctlpcr1_read12;

	initial begin
		#0 pmu_pct_ctlpcr1_read16 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read16 = 1'bx;
	end
	assign `SIG227 = pmu_pct_ctlpcr1_read16;

	initial begin
		#0 pmu_pct_ctlpcr1_read17 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read17 = 1'bx;
	end
	assign `SIG148 = pmu_pct_ctlpcr1_read17;

	initial begin
		#0 pmu_pct_ctlpcr1_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read14 = 1'bx;
	end
	assign `SIG174 = pmu_pct_ctlpcr1_read14;

	initial begin
		#0 pmu_pct_ctlpcr1_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read15 = 1'bx;
	end
	assign `SIG225 = pmu_pct_ctlpcr1_read15;

	initial begin
		#0 pmu_pct_ctlpcr5_read4 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read4 = 1'bx;
	end
	assign `SIG243 = pmu_pct_ctlpcr5_read4;

	initial begin
		#0 pmu_pct_ctlpcr5_read5 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read5 = 1'bx;
	end
	assign `SIG131 = pmu_pct_ctlpcr5_read5;

	initial begin
		#0 pmu_pct_ctlpcr5_read6 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read6 = 1'bx;
	end
	assign `SIG152 = pmu_pct_ctlpcr5_read6;

	initial begin
		#0 pmu_pct_ctlpcr5_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read7 = 1'bx;
	end
	assign `SIG188 = pmu_pct_ctlpcr5_read7;

	initial begin
		#0 pmu_pct_ctlpcr5_read0 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read0 = 1'bx;
	end
	assign `SIG177 = pmu_pct_ctlpcr5_read0;

	initial begin
		#0 pmu_pct_ctlpcr7_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read20 = 1'bx;
	end
	assign `SIG192 = pmu_pct_ctlpcr7_read20;

	initial begin
		#0 pmu_pct_ctlpcr7_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read23 = 1'bx;
	end
	assign `SIG185 = pmu_pct_ctlpcr7_read23;

	initial begin
		#0 pmu_pct_ctlpcr7_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read22 = 1'bx;
	end
	assign `SIG112 = pmu_pct_ctlpcr7_read22;

	initial begin
		#0 pmu_pct_ctlpcr7_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read29 = 1'bx;
	end
	assign `SIG198 = pmu_pct_ctlpcr7_read29;

	initial begin
		#0 pmu_pct_ctlpcr5_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read9 = 1'bx;
	end
	assign `SIG120 = pmu_pct_ctlpcr5_read9;

	initial begin
		#0 pmu_pct_ctlpcr5_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read30 = 1'bx;
	end
	assign `SIG159 = pmu_pct_ctlpcr5_read30;

	initial begin
		#0 pmu_pct_ctlpcr7_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read28 = 1'bx;
	end
	assign `SIG222 = pmu_pct_ctlpcr7_read28;

	initial begin
		#0 pmu_pct_ctlpcr4_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read15 = 1'bx;
	end
	assign `SIG215 = pmu_pct_ctlpcr4_read15;

	initial begin
		#0 pmu_pct_ctlpcr4_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read14 = 1'bx;
	end
	assign `SIG173 = pmu_pct_ctlpcr4_read14;

	initial begin
		#0 pmu_pct_ctlpcr7_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read24 = 1'bx;
	end
	assign `SIG193 = pmu_pct_ctlpcr7_read24;

	initial begin
		#0 n6336dummy = 1'b0;
		#39340.0 n6336dummy = 1'b1;
	end
	assign `SIG205 = n6336dummy;

	initial begin
		#0 pmu_pct_ctlpcr6_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read28 = 1'bx;
	end
	assign `SIG232 = pmu_pct_ctlpcr6_read28;

	initial begin
		#0 n8363 = 1'b0;
		#39340.0 n8363 = 1'bx;
	end
	assign `SIG56 = n8363;

	initial begin
		#0 pmu_pct_ctlx3_11 = 1'b0;
		#39340.0 pmu_pct_ctlx3_11 = 1'bx;
	end
	assign `SIG87 = pmu_pct_ctlx3_11;

	initial begin
		#0 pmu_pct_ctlpcr7_read26 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read26 = 1'bx;
	end
	assign `SIG200 = pmu_pct_ctlpcr7_read26;

	initial begin
		#0 pmu_pct_ctlx2_10 = 1'b0;
		#39340.0 pmu_pct_ctlx2_10 = 1'bx;
	end
	assign `SIG86 = pmu_pct_ctlx2_10;

	initial begin
		#0 pmu_pct_ctlx4_11 = 1'b0;
		#39340.0 pmu_pct_ctlx4_11 = 1'bx;
	end
	assign `SIG89 = pmu_pct_ctlx4_11;

	initial begin
		#0 n8260 = 1'b0;
		#39340.0 n8260 = 1'bx;
	end
	assign `SIG47 = n8260;

	initial begin
		#0 pmu_pct_ctlx1_11 = 1'b0;
		#39340.0 pmu_pct_ctlx1_11 = 1'bx;
	end
	assign `SIG78 = pmu_pct_ctlx1_11;

	initial begin
		#0 pmu_pct_ctlx1_10 = 1'b0;
		#39340.0 pmu_pct_ctlx1_10 = 1'bx;
	end
	assign `SIG85 = pmu_pct_ctlx1_10;

	initial begin
		#0 n8289 = 1'b0;
		#39340.0 n8289 = 1'bx;
	end
	assign `SIG28 = n8289;

	initial begin
		#0 pmu_pct_ctlpcr4_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read27 = 1'bx;
	end
	assign `SIG210 = pmu_pct_ctlpcr4_read27;

	initial begin
		#0 pmu_asi_clken = 1'b0;
		#39340.0 pmu_asi_clken = 1'b1;
	end
	assign `SIG74 = pmu_asi_clken;

	initial begin
		#0 n10760 = 1'b0;
		#39340.0 n10760 = 1'bx;
	end
	assign `SIG15 = n10760;

	initial begin
		#0 pmu_pct_ctlpcr5_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read23 = 1'bx;
	end
	assign `SIG170 = pmu_pct_ctlpcr5_read23;

	initial begin
		#0 pmu_pct_ctlpcr7_read10 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read10 = 1'bx;
	end
	assign `SIG140 = pmu_pct_ctlpcr7_read10;

	initial begin
		#0 pmu_pct_ctlpcr7_read11 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read11 = 1'bx;
	end
	assign `SIG135 = pmu_pct_ctlpcr7_read11;

	initial begin
		#0 pmu_pct_ctlth_m0 = 1'b0;
		#39340.0 pmu_pct_ctlth_m0 = 1'b1;
	end
	assign `SIG71 = pmu_pct_ctlth_m0;

	initial begin
		#0 pmu_pct_ctlx0_11 = 1'b0;
		#39340.0 pmu_pct_ctlx0_11 = 1'bx;
	end
	assign `SIG77 = pmu_pct_ctlx0_11;

	initial begin
		#0 pmu_pct_ctlpcr7_read16 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read16 = 1'bx;
	end
	assign `SIG150 = pmu_pct_ctlpcr7_read16;

	initial begin
		#0 pmu_pct_ctlpcr4_read25 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read25 = 1'bx;
	end
	assign `SIG248 = pmu_pct_ctlpcr4_read25;

	initial begin
		#0 n8275 = 1'b0;
		#39340.0 n8275 = 1'bx;
	end
	assign `SIG18 = n8275;

	initial begin
		#0 pmu_pct_ctlpcr4_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read28 = 1'bx;
	end
	assign `SIG207 = pmu_pct_ctlpcr4_read28;

	initial begin
		#0 pmu_pct_ctlpcr4_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read29 = 1'bx;
	end
	assign `SIG250 = pmu_pct_ctlpcr4_read29;

	initial begin
		#0 pmu_pct_ctlpcr5_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read29 = 1'bx;
	end
	assign `SIG172 = pmu_pct_ctlpcr5_read29;

	initial begin
		#0 pmu_pct_ctlpcr5_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read28 = 1'bx;
	end
	assign `SIG219 = pmu_pct_ctlpcr5_read28;

	initial begin
		#0 pmu_pct_ctlx4_10 = 1'b0;
		#39340.0 pmu_pct_ctlx4_10 = 1'bx;
	end
	assign `SIG84 = pmu_pct_ctlx4_10;

	initial begin
		#0 pmu_pct_ctlx3_10 = 1'b0;
		#39340.0 pmu_pct_ctlx3_10 = 1'bx;
	end
	assign `SIG80 = pmu_pct_ctlx3_10;

	initial begin
		#0 pmu_pct_ctlpcr5_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read24 = 1'bx;
	end
	assign `SIG191 = pmu_pct_ctlpcr5_read24;

	initial begin
		#0 pmu_pct_ctlpcr4_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read20 = 1'bx;
	end
	assign `SIG164 = pmu_pct_ctlpcr4_read20;

	initial begin
		#0 pmu_pct_ctlpcr5_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read22 = 1'bx;
	end
	assign `SIG109 = pmu_pct_ctlpcr5_read22;

	initial begin
		#0 pmu_pct_ctlpcr4_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read22 = 1'bx;
	end
	assign `SIG118 = pmu_pct_ctlpcr4_read22;

	initial begin
		#0 pmu_pct_ctlpcr5_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr5_read20 = 1'bx;
	end
	assign `SIG186 = pmu_pct_ctlpcr5_read20;

	initial begin
		#0 pmu_pdp_dppich021 = 1'b0;
		#39340.0 pmu_pdp_dppich021 = 1'bx;
	end
	assign `SIG45 = pmu_pdp_dppich021;

	initial begin
		#0 n6214dummy = 1'b0;
	end
	assign `SIG95 = n6214dummy;

	initial begin
		#0 pmu_pdp_dppich022 = 1'b0;
		#39340.0 pmu_pdp_dppich022 = 1'bx;
	end
	assign `SIG60 = pmu_pdp_dppich022;

	initial begin
		#0 pmu_pdp_dppich025 = 1'b0;
		#39340.0 pmu_pdp_dppich025 = 1'bx;
	end
	assign `SIG5 = pmu_pdp_dppich025;

	initial begin
		#0 n8378 = 1'b0;
		#39340.0 n8378 = 1'bx;
	end
	assign `SIG43 = n8378;

	initial begin
		#0 pmu_pdp_dppich027 = 1'b0;
		#39340.0 pmu_pdp_dppich027 = 1'bx;
	end
	assign `SIG53 = pmu_pdp_dppich027;

	initial begin
		#0 pmu_pdp_dppich026 = 1'b0;
		#39340.0 pmu_pdp_dppich026 = 1'bx;
	end
	assign `SIG2 = pmu_pdp_dppich026;

	initial begin
		#0 pmu_pdp_dppich029 = 1'b0;
		#39340.0 pmu_pdp_dppich029 = 1'bx;
	end
	assign `SIG3 = pmu_pdp_dppich029;

	initial begin
		#0 pmu_pdp_dppich028 = 1'b0;
		#39340.0 pmu_pdp_dppich028 = 1'bx;
	end
	assign `SIG9 = pmu_pdp_dppich028;

	initial begin
		#0 pmu_pct_ctlpcr3_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read9 = 1'bx;
	end
	assign `SIG128 = pmu_pct_ctlpcr3_read9;

	initial begin
		#0 pmu_pct_ctlpcr0_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read14 = 1'bx;
	end
	assign `SIG144 = pmu_pct_ctlpcr0_read14;

	initial begin
		#0 pmu_pct_ctlpcr4_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read7 = 1'bx;
	end
	assign `SIG184 = pmu_pct_ctlpcr4_read7;

	initial begin
		#0 pmu_pdp_dppich024 = 1'b0;
		#39340.0 pmu_pdp_dppich024 = 1'bx;
	end
	assign `SIG4 = pmu_pdp_dppich024;

	initial begin
		#0 pmu_pct_ctlpcr4_read11 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read11 = 1'bx;
	end
	assign `SIG247 = pmu_pct_ctlpcr4_read11;

	initial begin
		#0 n10676 = 1'b0;
		#39340.0 n10676 = 1'bx;
	end
	assign `SIG57 = n10676;

	initial begin
		#0 pmu_pct_ctlpcr0_read0 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read0 = 1'bx;
	end
	assign `SIG231 = pmu_pct_ctlpcr0_read0;

	initial begin
		#0 n11567 = 1'b0;
		#39340.0 n11567 = 1'bx;
	end
	assign `SIG59 = n11567;

	initial begin
		#0 n11711 = 1'b0;
		#39340.0 n11711 = 1'bx;
	end
	assign `SIG20 = n11711;

	initial begin
		#0 pmu_pct_ctlpcr3_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read24 = 1'bx;
	end
	assign `SIG204 = pmu_pct_ctlpcr3_read24;

	initial begin
		#0 pmu_pct_ctlpcr1_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read30 = 1'bx;
	end
	assign `SIG145 = pmu_pct_ctlpcr1_read30;

	initial begin
		#0 n6656dummy = 1'b0;
		#39340.0 n6656dummy = 1'b1;
	end
	assign `SIG106 = n6656dummy;

	initial begin
		#0 pmu_pct_ctlx7_11 = 1'b0;
		#39340.0 pmu_pct_ctlx7_11 = 1'bx;
	end
	assign `SIG82 = pmu_pct_ctlx7_11;

	initial begin
		#0 pmu_pct_ctlx7_10 = 1'b0;
		#39340.0 pmu_pct_ctlx7_10 = 1'bx;
	end
	assign `SIG75 = pmu_pct_ctlx7_10;

	initial begin
		#0 pmu_pdp_dppicl06 = 1'b0;
		#39340.0 pmu_pdp_dppicl06 = 1'bx;
	end
	assign `SIG46 = pmu_pdp_dppicl06;

	initial begin
		#0 pmu_pdp_dppicl07 = 1'b0;
		#39340.0 pmu_pdp_dppicl07 = 1'bx;
	end
	assign `SIG58 = pmu_pdp_dppicl07;

	initial begin
		#0 pmu_pdp_dppicl04 = 1'b0;
		#39340.0 pmu_pdp_dppicl04 = 1'bx;
	end
	assign `SIG61 = pmu_pdp_dppicl04;

	initial begin
		#0 pmu_pdp_dppicl05 = 1'b0;
		#39340.0 pmu_pdp_dppicl05 = 1'bx;
	end
	assign `SIG38 = pmu_pdp_dppicl05;

	initial begin
		#0 pmu_pct_ctlpcr7_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read14 = 1'bx;
	end
	assign `SIG208 = pmu_pct_ctlpcr7_read14;

	initial begin
		#0 pmu_pct_ctlaes_d1 = 1'b0;
		#39340.0 pmu_pct_ctlaes_d1 = 1'b1;
		#14500.0 pmu_pct_ctlaes_d1 = 1'b0;
	end
	assign `SIG96 = pmu_pct_ctlaes_d1;

	initial begin
		#0 pmu_pct_ctlpcr0_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read24 = 1'bx;
	end
	assign `SIG195 = pmu_pct_ctlpcr0_read24;

	initial begin
		#0 pmu_pdp_dppicl08 = 1'b0;
		#39340.0 pmu_pdp_dppicl08 = 1'bx;
	end
	assign `SIG37 = pmu_pdp_dppicl08;

	initial begin
		#0 pmu_pdp_dppicl09 = 1'b0;
		#39340.0 pmu_pdp_dppicl09 = 1'bx;
	end
	assign `SIG54 = pmu_pdp_dppicl09;

	initial begin
		#0 n11644 = 1'b0;
		#39340.0 n11644 = 1'bx;
	end
	assign `SIG30 = n11644;

	initial begin
		#0 pmu_pct_ctlpcr6_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read23 = 1'bx;
	end
	assign `SIG113 = pmu_pct_ctlpcr6_read23;

	initial begin
		#0 pmu_pct_ctlpcr6_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read20 = 1'bx;
	end
	assign `SIG133 = pmu_pct_ctlpcr6_read20;

	initial begin
		#0 pmu_pdp_dppich031 = 1'b0;
		#39340.0 pmu_pdp_dppich031 = 1'bx;
	end
	assign `SIG6 = pmu_pdp_dppich031;

	initial begin
		#0 pmu_pdp_dppicl029 = 1'b0;
		#39340.0 pmu_pdp_dppicl029 = 1'bx;
	end
	assign `SIG39 = pmu_pdp_dppicl029;

	initial begin
		#0 pmu_pdp_dppicl028 = 1'b0;
		#39340.0 pmu_pdp_dppicl028 = 1'bx;
	end
	assign `SIG48 = pmu_pdp_dppicl028;

	initial begin
		#0 pmu_pct_ctlpcr6_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read24 = 1'bx;
	end
	assign `SIG187 = pmu_pct_ctlpcr6_read24;

	initial begin
		#0 pmu_pct_ctlpcr0_read12 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read12 = 1'bx;
	end
	assign `SIG230 = pmu_pct_ctlpcr0_read12;

	initial begin
		#0 pmu_pdp_dppicl027 = 1'b0;
		#39340.0 pmu_pdp_dppicl027 = 1'bx;
	end
	assign `SIG51 = pmu_pdp_dppicl027;

	initial begin
		#0 pmu_pct_ctlpcr6_read14 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read14 = 1'bx;
	end
	assign `SIG212 = pmu_pct_ctlpcr6_read14;

	initial begin
		#0 pmu_pdp_dppicl021 = 1'b0;
		#39340.0 pmu_pdp_dppicl021 = 1'bx;
	end
	assign `SIG31 = pmu_pdp_dppicl021;

	initial begin
		#0 pmu_pdp_dppicl020 = 1'b0;
		#39340.0 pmu_pdp_dppicl020 = 1'bx;
	end
	assign `SIG40 = pmu_pdp_dppicl020;

	initial begin
		#0 pmu_pdp_dppicl023 = 1'b0;
		#39340.0 pmu_pdp_dppicl023 = 1'bx;
	end
	assign `SIG52 = pmu_pdp_dppicl023;

	initial begin
		#0 pmu_pdp_dppicl022 = 1'b0;
		#39340.0 pmu_pdp_dppicl022 = 1'bx;
	end
	assign `SIG55 = pmu_pdp_dppicl022;

	initial begin
		#0 pmu_pct_ctlpcr7_read6 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read6 = 1'bx;
	end
	assign `SIG134 = pmu_pct_ctlpcr7_read6;

	initial begin
		#0 pmu_pct_ctlpcr7_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read7 = 1'bx;
	end
	assign `SIG121 = pmu_pct_ctlpcr7_read7;

	initial begin
		#0 pmu_pct_ctlpcr7_read5 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read5 = 1'bx;
	end
	assign `SIG165 = pmu_pct_ctlpcr7_read5;

	initial begin
		#0 n5924dummy = 1'b0;
		#39340.0 n5924dummy = 1'b1;
	end
	assign `SIG93 = n5924dummy;

	initial begin
		#0 n11551 = 1'b0;
		#39340.0 n11551 = 1'bx;
	end
	assign `SIG22 = n11551;

	initial begin
		#0 pmu_pct_ctldes = 1'b0;
	end
	assign `SIG94 = pmu_pct_ctldes;

	initial begin
		#0 n11629 = 1'b0;
		#39340.0 n11629 = 1'bx;
	end
	assign `SIG21 = n11629;

	initial begin
		#0 pmu_pct_ctlpcr1_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read23 = 1'bx;
	end
	assign `SIG178 = pmu_pct_ctlpcr1_read23;

	initial begin
		#0 pmu_pct_ctlpcr1_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read22 = 1'bx;
	end
	assign `SIG114 = pmu_pct_ctlpcr1_read22;

	initial begin
		#0 pmu_pct_ctlpcr1_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read20 = 1'bx;
	end
	assign `SIG181 = pmu_pct_ctlpcr1_read20;

	initial begin
		#0 pmu_pct_ctlpcr1_read27 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read27 = 1'bx;
	end
	assign `SIG171 = pmu_pct_ctlpcr1_read27;

	initial begin
		#0 pmu_pdp_dppicl730 = 1'b0;
		#39340.0 pmu_pdp_dppicl730 = 1'bx;
	end
	assign `SIG63 = pmu_pdp_dppicl730;

	initial begin
		#0 pmu_pct_ctlpcr1_read24 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read24 = 1'bx;
	end
	assign `SIG201 = pmu_pct_ctlpcr1_read24;

	initial begin
		#0 pmu_pct_ctlpcr0_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read29 = 1'bx;
	end
	assign `SIG217 = pmu_pct_ctlpcr0_read29;

	initial begin
		#0 pmu_pct_ctlpcr1_read29 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read29 = 1'bx;
	end
	assign `SIG139 = pmu_pct_ctlpcr1_read29;

	initial begin
		#0 pmu_pct_ctlpcr1_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr1_read28 = 1'bx;
	end
	assign `SIG138 = pmu_pct_ctlpcr1_read28;

	initial begin
		#0 n6327dummy = 1'b0;
		#39340.0 n6327dummy = 1'b1;
	end
	assign `SIG158 = n6327dummy;

	initial begin
		#0 pmu_pct_ctlpcr0_read4 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read4 = 1'bx;
	end
	assign `SIG249 = pmu_pct_ctlpcr0_read4;

	initial begin
		#0 pmu_pct_ctlpcr0_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read7 = 1'bx;
	end
	assign `SIG117 = pmu_pct_ctlpcr0_read7;

	initial begin
		#0 pmu_pct_ctlifu_fl_b0 = 1'b0;
	end
	assign `SIG202 = pmu_pct_ctlifu_fl_b0;

	initial begin
		#0 pmu_pdp_dppich07 = 1'b0;
		#39340.0 pmu_pdp_dppich07 = 1'bx;
	end
	assign `SIG14 = pmu_pdp_dppich07;

	initial begin
		#0 pmu_pct_ctlpcr3_read6 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read6 = 1'bx;
	end
	assign `SIG151 = pmu_pct_ctlpcr3_read6;

	initial begin
		#0 pmu_pdp_dppich08 = 1'b0;
		#39340.0 pmu_pdp_dppich08 = 1'bx;
	end
	assign `SIG36 = pmu_pdp_dppich08;

	initial begin
		#0 pmu_pdp_dppich09 = 1'b0;
		#39340.0 pmu_pdp_dppich09 = 1'bx;
	end
	assign `SIG50 = pmu_pdp_dppich09;

	initial begin
		#0 pmu_pct_ctlpcr0_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read9 = 1'bx;
	end
	assign `SIG124 = pmu_pct_ctlpcr0_read9;

	initial begin
		#0 pmu_pct_ctlpcr0_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read20 = 1'bx;
	end
	assign `SIG179 = pmu_pct_ctlpcr0_read20;

	initial begin
		#0 pmu_pct_ctlpcr6_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr6_read30 = 1'bx;
	end
	assign `SIG143 = pmu_pct_ctlpcr6_read30;

	initial begin
		#0 pmu_pct_ctlpcr0_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read22 = 1'bx;
	end
	assign `SIG111 = pmu_pct_ctlpcr0_read22;

	initial begin
		#0 pmu_pct_ctlpcr0_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read23 = 1'bx;
	end
	assign `SIG246 = pmu_pct_ctlpcr0_read23;

	initial begin
		#0 pmu_pct_ctlpcr7_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr7_read9 = 1'bx;
	end
	assign `SIG136 = pmu_pct_ctlpcr7_read9;

	initial begin
		#0 pmu_pct_ctlpcr0_read25 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read25 = 1'bx;
	end
	assign `SIG211 = pmu_pct_ctlpcr0_read25;

	initial begin
		#0 pmu_pct_ctlpcr0_read26 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read26 = 1'bx;
	end
	assign `SIG213 = pmu_pct_ctlpcr0_read26;

	initial begin
		#0 n11815 = 1'b0;
		#39340.0 n11815 = 1'bx;
	end
	assign `SIG10 = n11815;

	initial begin
		#0 pmu_pct_ctlpcr2_read22 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read22 = 1'bx;
	end
	assign `SIG115 = pmu_pct_ctlpcr2_read22;

	initial begin
		#0 pmu_pct_ctldec_valid_m0 = 1'b0;
		#39340.0 pmu_pct_ctldec_valid_m0 = 1'b1;
	end
	assign `SIG73 = pmu_pct_ctldec_valid_m0;

	initial begin
		#0 pmu_pdp_dppicl031 = 1'b0;
		#39340.0 pmu_pdp_dppicl031 = 1'bx;
	end
	assign `SIG16 = pmu_pdp_dppicl031;

	initial begin
		#0 n6144 = 1'b0;
		#39340.0 n6144 = 1'bx;
	end
	assign `SIG0 = n6144;

	initial begin
		#0 pmu_pct_ctlpcr3_read7 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read7 = 1'bx;
	end
	assign `SIG162 = pmu_pct_ctlpcr3_read7;

	initial begin
		#0 pmu_pct_ctlpcr3_read15 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read15 = 1'bx;
	end
	assign `SIG220 = pmu_pct_ctlpcr3_read15;

	initial begin
		#0 pmu_pct_ctltg1_e11 = 1'b0;
		#39340.0 pmu_pct_ctltg1_e11 = 1'b1;
		#100.0 pmu_pct_ctltg1_e11 = 1'b0;
		#24300.0 pmu_pct_ctltg1_e11 = 1'b1;
	end
	assign `SIG70 = pmu_pct_ctltg1_e11;

	initial begin
		#0 pmu_pct_ctltg1_e10 = 1'b0;
		#63740.0 pmu_pct_ctltg1_e10 = 1'b1;
	end
	assign `SIG72 = pmu_pct_ctltg1_e10;

	initial begin
		#0 pmu_pct_ctltg1_e12 = 1'b0;
		#53740.0 pmu_pct_ctltg1_e12 = 1'b1;
	end
	assign `SIG68 = pmu_pct_ctltg1_e12;

	initial begin
		#0 pmu_pct_ctlpmu_pmen = 1'b0;
		#39340.0 pmu_pct_ctlpmu_pmen = 1'b1;
	end
	assign `SIG67 = pmu_pct_ctlpmu_pmen;

	initial begin
		#0 pmu_pct_ctlpcr2_read23 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read23 = 1'bx;
	end
	assign `SIG182 = pmu_pct_ctlpcr2_read23;

	initial begin
		#0 pmu_pct_ctlpcr4_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr4_read9 = 1'bx;
	end
	assign `SIG130 = pmu_pct_ctlpcr4_read9;

	initial begin
		#0 pmu_pct_ctlifu_fl_b1 = 1'b0;
	end
	assign `SIG196 = pmu_pct_ctlifu_fl_b1;

	initial begin
		#0 pmu_pct_ctlpcr2_read5 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read5 = 1'bx;
	end
	assign `SIG129 = pmu_pct_ctlpcr2_read5;

	initial begin
		#0 pmu_pct_ctlpcr2_read4 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read4 = 1'bx;
	end
	assign `SIG244 = pmu_pct_ctlpcr2_read4;

	initial begin
		#0 pmu_pct_ctlpcr2_read0 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read0 = 1'bx;
	end
	assign `SIG228 = pmu_pct_ctlpcr2_read0;

	initial begin
		#0 pmu_pct_ctlpcr3_read16 = 1'b0;
		#39340.0 pmu_pct_ctlpcr3_read16 = 1'bx;
	end
	assign `SIG236 = pmu_pct_ctlpcr3_read16;

	initial begin
		#0 n6309dummy = 1'b0;
		#39340.0 n6309dummy = 1'b1;
	end
	assign `SIG239 = n6309dummy;

	initial begin
		#0 pmu_pct_ctlpcr2_read9 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read9 = 1'bx;
	end
	assign `SIG127 = pmu_pct_ctlpcr2_read9;

	initial begin
		#0 n11829 = 1'b0;
		#39340.0 n11829 = 1'bx;
	end
	assign `SIG8 = n11829;

	initial begin
		#0 n10706 = 1'b0;
		#39340.0 n10706 = 1'bx;
	end
	assign `SIG62 = n10706;

	initial begin
		#0 pmu_pct_ctlpcr2_read28 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read28 = 1'bx;
	end
	assign `SIG155 = pmu_pct_ctlpcr2_read28;

	initial begin
		#0 pmu_pct_ctlpcr2_read20 = 1'b0;
		#39340.0 pmu_pct_ctlpcr2_read20 = 1'bx;
	end
	assign `SIG161 = pmu_pct_ctlpcr2_read20;

	initial begin
		#0 pmu_pdp_dppich010 = 1'b0;
		#39340.0 pmu_pdp_dppich010 = 1'bx;
	end
	assign `SIG12 = pmu_pdp_dppich010;

	initial begin
		#0 n6325dummy = 1'b0;
		#39340.0 n6325dummy = 1'b1;
	end
	assign `SIG154 = n6325dummy;

	initial begin
		#0 pmu_pct_ctlpcr0_read30 = 1'b0;
		#39340.0 pmu_pct_ctlpcr0_read30 = 1'bx;
	end
	assign `SIG149 = pmu_pct_ctlpcr0_read30;

	initial begin
		#0 pmu_pdp_dppich014 = 1'b0;
		#39340.0 pmu_pdp_dppich014 = 1'bx;
	end
	assign `SIG41 = pmu_pdp_dppich014;

	initial begin
		#0 pmu_pdp_dppich015 = 1'b0;
		#39340.0 pmu_pdp_dppich015 = 1'bx;
	end
	assign `SIG26 = pmu_pdp_dppich015;

	initial begin
		#0 pmu_pdp_dppich016 = 1'b0;
		#39340.0 pmu_pdp_dppich016 = 1'bx;
	end
	assign `SIG44 = pmu_pdp_dppich016;

	initial begin
		#0 pmu_pdp_dppich017 = 1'b0;
		#39340.0 pmu_pdp_dppich017 = 1'bx;
	end
	assign `SIG35 = pmu_pdp_dppich017;

	initial begin
		 #69340.0 $finish;
	end

`endif
