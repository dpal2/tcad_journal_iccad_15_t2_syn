`ifdef _COV_
`include "pmu_tick_defines_netlist_PRankNetlist.v"
`include "pmu_tick_defines_netlist_SigSeT_1.v"
`include "pmu_tick_defines_netlist_HybrSel.v"
`endif

module pmu_cov_bench;

logic [64:0] in_rngl_cdbus;
logic [12:0] dec_pmu_instr0_type_d;
logic [12:0] dec_pmu_instr1_type_d;
logic [5:0] lsu_pmu_mem_type_b;
logic [1:0] dec_br_taken_e;
logic [2:0] mmu_pmu_tid;
logic [6:0] l15_pmu_xbar_optype;
logic [4:0] spu_pmu_cwq_busy;
logic [2:0] spu_pmu_cwq_tid;
logic [3:0] spu_pmu_ma_busy;
logic [7:0] tlu_pmu_pstate_priv;
logic [7:0] tlu_pmu_hpstate_hpriv;
logic [7:0] tlu_pmu_trap_taken;
logic [1:0] tlu_pmu_trap_mask_e;
logic [1:0] dec_valid_e;
logic [1:0] dec_flush_m;
logic [1:0] dec_flush_b;
logic [1:0] tlu_flush_pmu_b;
logic [1:0] tlu_flush_pmu_w;
logic l2clk;
logic scan_in;
logic tcu_pce_ov;
logic spc_bclk;
logic spc_aclk;
logic tcu_scan_en;
logic lsu_pmu_pmen;
logic lsu_asi_clken;
logic dec_lsu_sel0_d;
logic mmu_pmu_l2ret;
logic mmu_pmu_l2miss;
logic mmu_pmu_dtlb;


wire [64:0] pmu_rngl_cdbus;
wire [7:0] pmu_tlu_trap_m;
wire [7:0] pmu_tlu_debug_event;
wire [7:0] pmu_lsu_l2dmiss_trap_m;
wire scan_out;
wire pmu_lsu_dcmiss_trap_m;
wire pmu_lsu_dtmiss_trap_m;

`ifdef _COV_
`include "pmu_trace_reg_PRankNetlist.v"
`include "pmu_trace_reg_SigSeT_1.v"
`include "pmu_trace_reg_HybrSel.v"
`endif


/*
integer seed;

initial begin
    $random_seed_gen(seed);
end
*/

pmu pmu_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("pmu_rtl.vcd");
        $dumpvars(0, pmu_);
    end
`endif
*/

initial begin
    // module clock
    l2clk = 0;
    // scan signals
    scan_in = 0;
    tcu_pce_ov = 0;
    spc_bclk = 0;
    spc_aclk = 0;
    tcu_scan_en = 0;
end

initial begin
    $display("Initializing design\n");
    lsu_pmu_pmen = 0;
    lsu_asi_clken = 0;
    #100;
    lsu_pmu_pmen = 1'b1;
    #10;
    lsu_asi_clken = 1'b1;
    $display("Reset sequence ended: %d\n", $time);
end


always begin
    // 1.4 GHz Clock
	#0.5 l2clk = ~l2clk;
end

`ifdef _COV_
`include "pmu_stim_netlist_PRankNetlist.v"
`include "pmu_stim_netlist_SigSeT_1.v"
`include "pmu_stim_netlist_HybrSel.v"
`endif

endmodule
