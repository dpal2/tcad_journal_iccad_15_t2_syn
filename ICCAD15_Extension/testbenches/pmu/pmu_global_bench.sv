/* Testbench file for design pmu generated on 2017-07-24 23:55:54.221250 */

/*
RTL VCS Command:

vcs -R -full64 -o ./pmu_rtl/exec/pmu -sverilog -Mdir=./pmu_rtl/csrc -f ../scripts/file_lists/flist_rtl_pmu -l ./pmu_rtl/comp.log -top pmu_global_bench -timescale=1ns/10ps +v2k +define+_RTL_

*/


module pmu_global_bench();

logic [64:0] in_rngl_cdbus;
logic [12:0] dec_pmu_instr0_type_d;
logic [12:0] dec_pmu_instr1_type_d;
logic [5:0] lsu_pmu_mem_type_b;
logic [1:0] dec_br_taken_e;
logic [2:0] mmu_pmu_tid;
logic [6:0] l15_pmu_xbar_optype;
logic [4:0] spu_pmu_cwq_busy;
logic [2:0] spu_pmu_cwq_tid;
logic [3:0] spu_pmu_ma_busy;
logic [7:0] tlu_pmu_pstate_priv;
logic [7:0] tlu_pmu_hpstate_hpriv;
logic [7:0] tlu_pmu_trap_taken;
logic [1:0] tlu_pmu_trap_mask_e;
logic [1:0] dec_valid_e;
logic [1:0] dec_flush_m;
logic [1:0] dec_flush_b;
logic [1:0] tlu_flush_pmu_b;
logic [1:0] tlu_flush_pmu_w;
logic l2clk;
logic scan_in;
logic tcu_pce_ov;
logic spc_bclk;
logic spc_aclk;
logic tcu_scan_en;
logic lsu_pmu_pmen;
logic lsu_asi_clken;
logic dec_lsu_sel0_d;
logic mmu_pmu_l2ret;
logic mmu_pmu_l2miss;
logic mmu_pmu_dtlb;


wire [64:0] pmu_rngl_cdbus;
wire [7:0] pmu_tlu_trap_m;
wire [7:0] pmu_tlu_debug_event;
wire [7:0] pmu_lsu_l2dmiss_trap_m;
wire scan_out;
wire pmu_lsu_dcmiss_trap_m;
wire pmu_lsu_dtmiss_trap_m;

integer seed;

initial begin
    $random_seed_gen(seed);
end

pmu pmu_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("pmu_rtl.vcd");
        $dumpvars(0, pmu_);
    end
`endif
*/


initial begin
    // module clock
    l2clk = 0;
    // scan signals
    scan_in = 0;
    tcu_pce_ov = 0;
    spc_bclk = 0;
    spc_aclk = 0;
    tcu_scan_en = 0;
end

always begin
    // 1.4 GHz Clock
	#0.5 l2clk = ~l2clk;
end

class _pmu;
	rand logic [64:0] _in_rngl_cdbus;
    constraint in_rngl_cdbus_range { _in_rngl_cdbus inside {[0:65'h1FFFFFFFFFFFFFFFF]};}
	
    rand logic [1:0] _dec_br_taken_e;
    constraint dec_br_taken_e_range { _dec_br_taken_e dist {0:=8, 1:=2};}

    rand logic _dec_lsu_sel0_d;
    constraint dec_lsu_sel0_d_range { _dec_lsu_sel0_d dist {0:=6, 1:=4};}

	rand logic _mmu_pmu_l2ret;
    constraint mmu_pmu_l2ret_range { _mmu_pmu_l2ret dist {1:=7, 0:=3};}

	rand logic _mmu_pmu_l2miss;
    constraint mmu_pmu_l2miss_range { _mmu_pmu_l2miss dist {0:=7, 1:=3};}

	rand logic _mmu_pmu_dtlb;
    constraint mmu_pmu_dtlb_range { _mmu_pmu_dtlb dist {0:=8, 1:=2};}

	rand logic [2:0] _mmu_pmu_tid;
    constraint mmu_pmu_tid_range { _mmu_pmu_tid inside {[0:3'b111]};}

    logic [1:0] flush_vals[] = '{00, 01, 10};

	rand logic [1:0] _dec_valid_e;
	rand logic [1:0] _dec_flush_m;
	rand logic [1:0] _dec_flush_b;
	rand logic [1:0] _tlu_flush_pmu_b;
	rand logic [1:0] _tlu_flush_pmu_w;

    constraint dec_valid_e_range {
        _dec_valid_e inside flush_vals;
    }

    constraint dec_flush_m_range {
        _dec_flush_m inside flush_vals;
    }

    constraint dec_flush_b_range {
        _dec_flush_b inside flush_vals;
    }

    constraint tlu_flush_pmu_b_range {
        _tlu_flush_pmu_b inside flush_vals;
    }

    constraint tlu_flush_pmu_w_range {
        _tlu_flush_pmu_w inside flush_vals;
    }

    logic instr_type_vals[] = '{10'b10_0000_0000, 10'b01_0000_0000, 10'b00_1000_0000, 10'b00_0100_0000, 10'b00_0010_0000, 10'b00_0001_0000, 10'b00_0000_1000, 10'b00_0000_0100, 10'b00_0000_0010, 10'b00_0000_0001};
    logic [1:0] instr_tid_vals[] = '{2'b00, 2'b01, 2'b10, 2'b11};

	rand logic dec_pmu_instr0_vld;
    rand logic [1:0] dec_pmu_instr0_tid;
    rand logic [9:0] dec_pmu_instr0_type;
    constraint dec_pmu_instr0_vld_range { dec_pmu_instr0_vld dist {1:=7, 0:=3};}
    constraint dec_pmu_instr0_tid_range {
        dec_pmu_instr0_tid inside instr_tid_vals;
    }
    constraint dec_pmu_instr0_type_range {
        dec_pmu_instr0_type inside instr_type_vals;
    }

	rand logic dec_pmu_instr1_vld;
    rand logic [1:0] dec_pmu_instr1_tid;
    rand logic [9:0] dec_pmu_instr1_type;
    constraint dec_pmu_instr1_vld_range { dec_pmu_instr1_vld dist {1:=6, 0:=4};}
    constraint dec_pmu_instr1_tid_range {
        dec_pmu_instr1_tid inside instr_tid_vals;
    }
    constraint dec_pmu_instr1_type_range {
        dec_pmu_instr1_type inside instr_type_vals;
    }

    logic [2:0] l2miss_type_vals[] = '{3'b100, 3'b010, 3'b001}; 
    rand logic [2:0] lsu_pmu_mem_type_b_tid;
    rand logic [2:0] lsu_pmu_mem_miss_type;
    constraint lsu_pmu_mem_type_b_tid_range { lsu_pmu_mem_type_b_tid inside {[0:3'b111]};};
    constraint lsu_pmu_mem_miss_type_range {
        lsu_pmu_mem_miss_type inside lsu_pmu_mem_miss_type;
    }
    
    rand logic l15_pmu_xbar_optype_vld;
    rand logic [2:0] l15_pmu_xbar_optype_tid;
    rand logic [2:0] l15_pmu_xbar_optype_type;
    constraint l15_pmu_xbar_optype_vld_range { l15_pmu_xbar_optype_vld dist {1:=7, 0:=3};};
    constraint l15_pmu_xbar_optype_tid_range { l15_pmu_xbar_optype_tid dist {[0:3'b111]};};
    constraint l15_pmu_xbar_optype_type_range { l15_pmu_xbar_optype_type inside {[0:3'b111]};};

    logic [4:0] spu_busy_vals[] = '{5'b1_0000, 5'b0_1000, 5'b0_0100, 5'b0_0010, 5'b0_0001};
	rand logic [4:0] _spu_pmu_cwq_busy;
    constraint spu_pmu_cwq_busy_range {
        _spu_pmu_cwq_busy inside spu_busy_vals;
    }

	rand logic [2:0] _spu_pmu_cwq_tid;
    constraint spu_pmu_cwq_tid_range { _spu_pmu_cwq_tid inside {[0:3'b111]};};

	rand logic spu_pmu_ma_busy_idle;
	rand logic spu_pmu_ma_tid;
    constraint spu_pmu_ma_busy_idle_range { spu_pmu_ma_busy_idle dist {1:=4, 0:=6};}
    constraint spu_pmu_ma_tid_range { spu_pmu_ma_tid inside {[0:3'b111]};};

	rand logic tlu_pmu_pstate_priv_7; constraint tlu_pmu_pstate_priv_7_range {tlu_pmu_pstate_priv_7 dist {0:=9, 1:=1};};
	rand logic tlu_pmu_pstate_priv_6; constraint tlu_pmu_pstate_priv_6_range {tlu_pmu_pstate_priv_6 dist {0:=8, 1:=2};}; 
	rand logic tlu_pmu_pstate_priv_5; constraint tlu_pmu_pstate_priv_5_range {tlu_pmu_pstate_priv_5 dist {0:=7, 1:=3};}; 
	rand logic tlu_pmu_pstate_priv_4; constraint tlu_pmu_pstate_priv_4_range {tlu_pmu_pstate_priv_4 dist {0:=6, 1:=4};}; 
	rand logic tlu_pmu_pstate_priv_3; constraint tlu_pmu_pstate_priv_3_range {tlu_pmu_pstate_priv_3 dist {0:=5, 1:=5};}; 
	rand logic tlu_pmu_pstate_priv_2; constraint tlu_pmu_pstate_priv_2_range {tlu_pmu_pstate_priv_2 dist {0:=4, 1:=6};};
	rand logic tlu_pmu_pstate_priv_1; constraint tlu_pmu_pstate_priv_1_range {tlu_pmu_pstate_priv_1 dist {0:=3, 1:=7};};
	rand logic tlu_pmu_pstate_priv_0; constraint tlu_pmu_pstate_priv_0_range {tlu_pmu_pstate_priv_0 dist {0:=2, 1:=8};};

	rand logic tlu_pmu_hpstate_hpriv_7; constraint tlu_pmu_hpstate_hpriv_7_range {tlu_pmu_hpstate_hpriv_7 dist {0:=9, 1:=1};};
	rand logic tlu_pmu_hpstate_hpriv_6; constraint tlu_pmu_hpstate_hpriv_6_range {tlu_pmu_hpstate_hpriv_6 dist {0:=8, 1:=2};}; 
	rand logic tlu_pmu_hpstate_hpriv_5; constraint tlu_pmu_hpstate_hpriv_5_range {tlu_pmu_hpstate_hpriv_5 dist {0:=7, 1:=3};}; 
	rand logic tlu_pmu_hpstate_hpriv_4; constraint tlu_pmu_hpstate_hpriv_4_range {tlu_pmu_hpstate_hpriv_4 dist {0:=6, 1:=4};}; 
	rand logic tlu_pmu_hpstate_hpriv_3; constraint tlu_pmu_hpstate_hpriv_3_range {tlu_pmu_hpstate_hpriv_3 dist {0:=5, 1:=5};}; 
	rand logic tlu_pmu_hpstate_hpriv_2; constraint tlu_pmu_hpstate_hpriv_2_range {tlu_pmu_hpstate_hpriv_2 dist {0:=4, 1:=6};};
	rand logic tlu_pmu_hpstate_hpriv_1; constraint tlu_pmu_hpstate_hpriv_1_range {tlu_pmu_hpstate_hpriv_1 dist {0:=3, 1:=7};};
	rand logic tlu_pmu_hpstate_hpriv_0; constraint tlu_pmu_hpstate_hpriv_0_range {tlu_pmu_hpstate_hpriv_0 dist {0:=2, 1:=8};};

    logic [7:0] tlu_pmu_trap_vals[] = '{8'b1000_0000, 8'b0100_0000, 8'b0010_0000, 8'b0001_0000, 8'b0000_1000, 8'b0000_0100, 8'b0000_0010, 8'b0000_0001};	
    rand logic [7:0] _tlu_pmu_trap_taken; 
    constraint tlu_pmu_trap_taken_range { 
        _tlu_pmu_trap_taken inside tlu_pmu_trap_vals;
    }
      
    logic [1:0] trap_mask_vals[] = '{2'b10, 2'b01};
	rand logic [1:0] _tlu_pmu_trap_mask_e;
    constraint _tlu_pmu_trap_mask_e_range {
        _tlu_pmu_trap_mask_e inside trap_mask_vals;
    }

    function new (int seed);
        this.srandom(seed);
        pmu_phy_data_cover = new();
    endfunction

    covergroup pmu_phy_data_cover;
        coverpoint _in_rngl_cdbus;
        coverpoint _dec_br_taken_e;
        coverpoint _dec_lsu_sel0_d;
        coverpoint _mmu_pmu_l2ret;
        coverpoint _mmu_pmu_l2miss;
        coverpoint _mmu_pmu_dtlb;
        coverpoint _mmu_pmu_tid;
        coverpoint _dec_valid_e;
        coverpoint _dec_flush_m;
        coverpoint _dec_flush_b;
        coverpoint _tlu_flush_pmu_b;
        coverpoint _tlu_flush_pmu_w;
        coverpoint dec_pmu_instr0_vld;
        coverpoint dec_pmu_instr0_tid;
        coverpoint dec_pmu_instr0_type;
        coverpoint dec_pmu_instr1_vld;
        coverpoint dec_pmu_instr1_tid;
        coverpoint dec_pmu_instr1_type;
        coverpoint lsu_pmu_mem_type_b_tid;
        coverpoint lsu_pmu_mem_miss_type;
        coverpoint l15_pmu_xbar_optype_vld;
        coverpoint l15_pmu_xbar_optype_tid;
        coverpoint l15_pmu_xbar_optype_type;
        coverpoint _spu_pmu_cwq_busy;
        coverpoint _spu_pmu_cwq_tid;
        coverpoint spu_pmu_ma_busy_idle;
        coverpoint spu_pmu_ma_tid;
		coverpoint  tlu_pmu_pstate_priv_7;
		coverpoint  tlu_pmu_pstate_priv_6;
		coverpoint  tlu_pmu_pstate_priv_5;
		coverpoint  tlu_pmu_pstate_priv_4;
		coverpoint  tlu_pmu_pstate_priv_3;
		coverpoint  tlu_pmu_pstate_priv_2;
		coverpoint  tlu_pmu_pstate_priv_1;    
		coverpoint  tlu_pmu_pstate_priv_0;
		coverpoint  tlu_pmu_hpstate_hpriv_7;
		coverpoint  tlu_pmu_hpstate_hpriv_6;
		coverpoint  tlu_pmu_hpstate_hpriv_5;
		coverpoint  tlu_pmu_hpstate_hpriv_4;
		coverpoint  tlu_pmu_hpstate_hpriv_3;
		coverpoint  tlu_pmu_hpstate_hpriv_2;
		coverpoint  tlu_pmu_hpstate_hpriv_1;    
		coverpoint  tlu_pmu_hpstate_hpriv_0;
		coverpoint  _tlu_pmu_trap_taken;
    endgroup
endclass

task reset_task;
    begin
        $display("Initializing design\n");
        lsu_pmu_pmen = 0;
        lsu_asi_clken = 0;
        #100;
        lsu_pmu_pmen = 1'b1;
        #10;
        lsu_asi_clken = 1'b1;
        $display("Reset sequence ended: %d\n", $time);
    end
endtask

task pmu_phy_data(_pmu pmu_data);
    @(posedge l2clk)
    if (pmu_data.randomize() == 1)
    begin
        in_rngl_cdbus = pmu_data._in_rngl_cdbus;
        dec_pmu_instr0_type_d = {pmu_data.dec_pmu_instr0_vld, pmu_data.dec_pmu_instr0_tid, pmu_data.dec_pmu_instr0_type};
        dec_pmu_instr1_type_d = {pmu_data.dec_pmu_instr1_vld, pmu_data.dec_pmu_instr1_tid, pmu_data.dec_pmu_instr1_type};
        lsu_pmu_mem_type_b = {pmu_data.lsu_pmu_mem_type_b_tid, pmu_data.lsu_pmu_mem_miss_type};
        dec_br_taken_e = pmu_data._dec_br_taken_e;
        dec_lsu_sel0_d = pmu_data._dec_lsu_sel0_d;
        mmu_pmu_l2ret = pmu_data._mmu_pmu_l2ret;
        mmu_pmu_l2miss = pmu_data._mmu_pmu_l2miss;
        mmu_pmu_dtlb = pmu_data._mmu_pmu_dtlb;
        mmu_pmu_tid = pmu_data._mmu_pmu_tid;
        l15_pmu_xbar_optype = {pmu_data.l15_pmu_xbar_optype_vld, pmu_data.l15_pmu_xbar_optype_tid, pmu_data.l15_pmu_xbar_optype_type};
        spu_pmu_cwq_busy = pmu_data._spu_pmu_cwq_busy;
        spu_pmu_cwq_tid = pmu_data._spu_pmu_cwq_tid;
        spu_pmu_ma_busy = {pmu_data.spu_pmu_ma_busy_idle, pmu_data.spu_pmu_ma_tid};
        tlu_pmu_pstate_priv = {
                                pmu_data.tlu_pmu_pstate_priv_7,  
                                pmu_data.tlu_pmu_pstate_priv_6,  
                                pmu_data.tlu_pmu_pstate_priv_4,  
                                pmu_data.tlu_pmu_pstate_priv_3,  
                                pmu_data.tlu_pmu_pstate_priv_2,  
                                pmu_data.tlu_pmu_pstate_priv_1,  
                                pmu_data.tlu_pmu_pstate_priv_0  
                            };
        tlu_pmu_hpstate_hpriv = {
                                pmu_data.tlu_pmu_hpstate_hpriv_7,
                                pmu_data.tlu_pmu_hpstate_hpriv_6,
                                pmu_data.tlu_pmu_hpstate_hpriv_5,
                                pmu_data.tlu_pmu_hpstate_hpriv_4,
                                pmu_data.tlu_pmu_hpstate_hpriv_3,
                                pmu_data.tlu_pmu_hpstate_hpriv_2,
                                pmu_data.tlu_pmu_hpstate_hpriv_1,
                                pmu_data.tlu_pmu_hpstate_hpriv_0
                            };
        tlu_pmu_trap_taken = pmu_data._tlu_pmu_trap_taken;
        tlu_pmu_trap_mask_e = pmu_data._tlu_pmu_trap_mask_e;
        dec_valid_e = pmu_data._dec_valid_e;
        dec_flush_m = pmu_data._dec_flush_m;
        dec_flush_b = pmu_data._dec_flush_b;
        tlu_flush_pmu_b = pmu_data._tlu_flush_pmu_b;
        tlu_flush_pmu_w = pmu_data._tlu_flush_pmu_w;
        pmu_data.pmu_phy_data_cover.sample();
    end
    else
    begin
        $display("Randomization at pmu_phy_data failed.\n");
    end
endtask

initial begin
    _pmu pmu_data;
    #5;
    $display("Seed is: %d \n", seed);
    pmu_data = new(seed);
    #10;
    reset_task;
    #10;
    repeat (1024) begin
        pmu_phy_data(pmu_data);
        #100;
    end
    $finish;
end


`include "pmu_rtl_trace.v"

endmodule
