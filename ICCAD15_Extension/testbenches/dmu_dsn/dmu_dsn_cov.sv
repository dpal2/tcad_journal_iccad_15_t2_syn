`ifdef _COV_
`include "dmu_dsn_tick_defines_netlist_PRankNetlist.v"
`include "dmu_dsn_tick_defines_netlist_SigSeT_1.v"
`include "dmu_dsn_tick_defines_netlist_HybrSel.v"
`endif

module dmu_dsn_cov_bench();

reg [3:0] sii_dmu_wrack_tag;
reg [127:0] sio_dmu_data;
reg [7:0] sio_dmu_parity;
reg [31:0] ncu_dmu_data; // DONE
reg [31:0] d2j_csr_ring_in; // DONE
reg [63:0] ncu_dmu_pio_data; // DONE
reg [5:0] ncu_dmu_mondo_id;
reg [3:0] d2j_cmd;
reg [36:0] d2j_addr;
reg [15:0] d2j_ctag;
reg [127:0] d2j_data;
reg [15:0] d2j_bmsk;
reg [4:0] d2j_data_par;
reg [3:0] d2j_p_wrack_tag;
reg [5:0] cr2ds_dbg_sel_a;
reg [5:0] cr2ds_dbg_sel_b;
reg l1clk;  // DONE
reg rst_l;  // DONE
reg sio_dmu_hdr_vld; // DONE
reg sii_dmu_wrack_par; // DONE
reg sii_dmu_wrack_vld; // DONE
reg ncu_dmu_vld; // DONE
reg ncu_dmu_stall; // DONE
reg ncu_dmu_pio_hdr_vld; // DONE
reg ncu_dmu_mmu_addr_vld; // DONE
reg ncu_dmu_d_pei; // DONE
reg ncu_dmu_siicr_pei; // DONE
reg ncu_dmu_ctag_uei; // DONE
reg ncu_dmu_ctag_cei; // DONE
reg ncu_dmu_ncucr_pei; // DONE
reg ncu_dmu_iei; // DONE
reg ncu_dmu_mondo_ack; // DONE
reg ncu_dmu_mondo_nack; // DONE
reg ncu_dmu_mondo_id_par; // DONE
reg d2j_cmd_vld; // DONE
reg d2j_data_vld; // DONE
reg d2j_p_wrack_vld; // DONE
reg dbg1_dmu_stall; // DONE
reg dbg1_dmu_resume; // DONE


wire [127:0] dmu_sii_data;
wire [7:0] dmu_sii_parity;
wire [15:0] dmu_sii_be;
wire [31:0] dmu_ncu_data;
wire [31:0] j2d_csr_ring_out;
wire [3:0] dmu_ncu_wrack_tag;
wire [3:0] j2d_d_wrack_tag;
wire [1:0] j2d_di_cmd;
wire [15:0] j2d_di_ctag;
wire [3:0] j2d_p_cmd;
wire [35:0] j2d_p_addr;
wire [15:0] j2d_p_bmsk;
wire [10:0] j2d_p_ctag;
wire [127:0] j2d_d_data;
wire [3:0] j2d_d_data_par;
wire [127:0] j2d_p_data;
wire [3:0] j2d_p_data_par;
wire [42:6] j2d_mmu_addr;
wire [7:0] ds2cr_dbg_a;
wire [7:0] ds2cr_dbg_b;
wire dmu_sii_hdr_vld;
wire dmu_sii_reqbypass;
wire dmu_sii_datareq;
wire dmu_sii_datareq16;
wire dmu_sii_be_parity;
wire dmu_ncu_stall;
wire dmu_ncu_vld;
wire dmu_ncu_wrack_vld;
wire dmu_ncu_wrack_par;
wire dmu_ncu_d_pe;
wire dmu_ncu_siicr_pe;
wire dmu_ncu_ctag_ue;
wire dmu_ncu_ctag_ce;
wire dmu_ncu_ncucr_pe;
wire dmu_ncu_ie;
wire j2d_d_wrack_vld;
wire j2d_di_cmd_vld;
wire j2d_p_cmd_vld;
wire j2d_d_data_err;
wire j2d_d_data_vld;
wire j2d_p_data_vld;
wire j2d_mmu_addr_vld;
wire dsn_dmc_iei;
wire dmu_dbg1_stall_ack;
wire ds2cl_stall;

`ifdef _COV_
`include "dmu_dsn_trace_reg_PRankNetlist.v"
`include "dmu_dsn_trace_reg_SigSeT_1.v"
`include "dmu_dsn_trace_reg_HybrSel.v"
`endif

dmu_dsn dmu_dsn_ (.*);

initial begin
    // module clock
    l1clk = 0;
    // module reset
    rst_l = 0;
    // input valid signals
    sio_dmu_hdr_vld = 0;
    sii_dmu_wrack_par = 0;
    sii_dmu_wrack_vld = 0;
    sii_dmu_wrack_tag = {4{1'bX}};
    sio_dmu_data = {128{1'bX}};
    sio_dmu_parity = {8{1'bX}};
    ncu_dmu_vld = 0;
    ncu_dmu_stall = 0;
    ncu_dmu_pio_hdr_vld = 0;
    ncu_dmu_mmu_addr_vld = 0;
    // ei signals
	ncu_dmu_d_pei = 0;
	ncu_dmu_siicr_pei = 0;
	ncu_dmu_ctag_uei = 0;
	ncu_dmu_ctag_cei = 0;
	ncu_dmu_ncucr_pei = 0;
	ncu_dmu_iei = 0;
    //
    ncu_dmu_mondo_ack = 0;
    ncu_dmu_mondo_nack = 0;
    ncu_dmu_mondo_id_par = 0;
    //
    d2j_cmd_vld = 0;
    d2j_data_vld = 0;
    d2j_p_wrack_vld = 0;
    //
    dbg1_dmu_stall = 0;
    dbg1_dmu_resume = 0;
    //
    cr2ds_dbg_sel_a = 0;
    cr2ds_dbg_sel_b = 0;
     
end

always begin
    // 350 Mhz Clock
    #1.5 l1clk = ~l1clk;
end

initial begin
    $display("Initializing design\n");
    #100;
    rst_l = 1;
    $display("Reset sequence ended: %d\n", $time);
end

`ifdef _COV_
`include "dmu_dsn_stim_netlist_PRankNetlist.v"
`include "dmu_dsn_stim_netlist_SigSeT_1.v"
`include "dmu_dsn_stim_netlist_HybrSel.v"
`endif

endmodule
