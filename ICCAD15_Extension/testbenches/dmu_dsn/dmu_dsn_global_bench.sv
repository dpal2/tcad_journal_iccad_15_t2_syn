module dmu_dsn_global_bench();

reg [3:0] sii_dmu_wrack_tag;
reg [127:0] sio_dmu_data;
reg [7:0] sio_dmu_parity;
reg [31:0] ncu_dmu_data; // DONE
reg [31:0] d2j_csr_ring_in; // DONE
reg [63:0] ncu_dmu_pio_data; // DONE
reg [5:0] ncu_dmu_mondo_id;
reg [3:0] d2j_cmd;
reg [36:0] d2j_addr;
reg [15:0] d2j_ctag;
reg [127:0] d2j_data;
reg [15:0] d2j_bmsk;
reg [4:0] d2j_data_par;
reg [3:0] d2j_p_wrack_tag;
reg [5:0] cr2ds_dbg_sel_a;
reg [5:0] cr2ds_dbg_sel_b;
reg l1clk;  // DONE
reg rst_l;  // DONE
reg sio_dmu_hdr_vld; // DONE
reg sii_dmu_wrack_par; // DONE
reg sii_dmu_wrack_vld; // DONE
reg ncu_dmu_vld; // DONE
reg ncu_dmu_stall; // DONE
reg ncu_dmu_pio_hdr_vld; // DONE
reg ncu_dmu_mmu_addr_vld; // DONE
reg ncu_dmu_d_pei; // DONE
reg ncu_dmu_siicr_pei; // DONE
reg ncu_dmu_ctag_uei; // DONE
reg ncu_dmu_ctag_cei; // DONE
reg ncu_dmu_ncucr_pei; // DONE
reg ncu_dmu_iei; // DONE
reg ncu_dmu_mondo_ack; // DONE
reg ncu_dmu_mondo_nack; // DONE
reg ncu_dmu_mondo_id_par; // DONE
reg d2j_cmd_vld; // DONE
reg d2j_data_vld; // DONE
reg d2j_p_wrack_vld; // DONE
reg dbg1_dmu_stall; // DONE
reg dbg1_dmu_resume; // DONE


wire [127:0] dmu_sii_data;
wire [7:0] dmu_sii_parity;
wire [15:0] dmu_sii_be;
wire [31:0] dmu_ncu_data;
wire [31:0] j2d_csr_ring_out;
wire [3:0] dmu_ncu_wrack_tag;
wire [3:0] j2d_d_wrack_tag;
wire [1:0] j2d_di_cmd;
wire [15:0] j2d_di_ctag;
wire [3:0] j2d_p_cmd;
wire [35:0] j2d_p_addr;
wire [15:0] j2d_p_bmsk;
wire [10:0] j2d_p_ctag;
wire [127:0] j2d_d_data;
wire [3:0] j2d_d_data_par;
wire [127:0] j2d_p_data;
wire [3:0] j2d_p_data_par;
wire [42:6] j2d_mmu_addr;
wire [7:0] ds2cr_dbg_a;
wire [7:0] ds2cr_dbg_b;
wire dmu_sii_hdr_vld;
wire dmu_sii_reqbypass;
wire dmu_sii_datareq;
wire dmu_sii_datareq16;
wire dmu_sii_be_parity;
wire dmu_ncu_stall;
wire dmu_ncu_vld;
wire dmu_ncu_wrack_vld;
wire dmu_ncu_wrack_par;
wire dmu_ncu_d_pe;
wire dmu_ncu_siicr_pe;
wire dmu_ncu_ctag_ue;
wire dmu_ncu_ctag_ce;
wire dmu_ncu_ncucr_pe;
wire dmu_ncu_ie;
wire j2d_d_wrack_vld;
wire j2d_di_cmd_vld;
wire j2d_p_cmd_vld;
wire j2d_d_data_err;
wire j2d_d_data_vld;
wire j2d_p_data_vld;
wire j2d_mmu_addr_vld;
wire dsn_dmc_iei;
wire dmu_dbg1_stall_ack;
wire ds2cl_stall;

integer seed;

initial begin
    $random_seed_gen(seed);
end

dmu_dsn dmu_dsn_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("dmu_dsn_rtl.vcd");
        $dumpvars(0, dmu_dsn_);
    end
`endif
*/


initial begin
    // module clock
    l1clk = 0;
    // module reset
    rst_l = 0;
    // input valid signals
    sio_dmu_hdr_vld = 0;
    sii_dmu_wrack_par = 0;
    sii_dmu_wrack_vld = 0;
    sii_dmu_wrack_tag = {4{1'bX}};
    sio_dmu_data = {128{1'bX}};
    sio_dmu_parity = {8{1'bX}};
    ncu_dmu_vld = 0;
    ncu_dmu_stall = 0;
    ncu_dmu_pio_hdr_vld = 0;
    ncu_dmu_mmu_addr_vld = 0;
    // ei signals
	ncu_dmu_d_pei = 0;
	ncu_dmu_siicr_pei = 0;
	ncu_dmu_ctag_uei = 0;
	ncu_dmu_ctag_cei = 0;
	ncu_dmu_ncucr_pei = 0;
	ncu_dmu_iei = 0;
    //
    ncu_dmu_mondo_ack = 0;
    ncu_dmu_mondo_nack = 0;
    ncu_dmu_mondo_id_par = 0;
    //
    d2j_cmd_vld = 0;
    d2j_data_vld = 0;
    d2j_p_wrack_vld = 0;
    //
    dbg1_dmu_stall = 0;
    dbg1_dmu_resume = 0;
    //
    cr2ds_dbg_sel_a = 0;
    cr2ds_dbg_sel_b = 0;
     
end

always begin
    // 350 Mhz Clock
    #1.5 l1clk = ~l1clk;
end

class d2j;
    rand logic [3:0] d2jcmd;
    rand logic _d2j_cmd_vld;
    rand logic _d2j_p_wrack_vld;

    logic [3:0] cmds[] = '{4'b1010, 4'b0010, 4'b0000, 4'b0100, 4'b0101};

    constraint d2jcmd_range {
        d2jcmd inside cmds;
    }
    
    rand logic TimeOutError;
    rand logic UnmappedAddressError;
    rand logic UncorrectableError;
    rand logic [15:0] dmc_tag;
    rand logic [39:0] bus_address;

    constraint TimeOutError_range { TimeOutError dist {0:=9, 1:=1};};
    constraint UnmappedAddressError_range { UnmappedAddressError dist {0:=7, 1:=3};};
    constraint UncorrectableError_range { UncorrectableError dist {0:=9, 1:=1};};
    
    logic [15:0] tag_vals[] = '{16'h8000, 16'h4000, 16'h2000, 16'h1000, 16'h0800, 16'h0400, 16'h0200, 16'h0100, 16'h0080, 16'h0040, 16'h0020, 16'h0010, 16'h0008, 16'h0004, 16'h0002, 16'h0001};
    constraint dmc_tag_range {
        dmc_tag inside tag_vals;
    }
    // Table 6-11 Vol 1 435 - 526 
    constraint bus_address_range { bus_address dist {[0:40'hFF_FFFF_FFC0]};};

    function new (int seed);
        this.srandom(seed);
        d2j_cover = new();
    endfunction

    covergroup d2j_cover;
        coverpoint d2jcmd;
        coverpoint _d2j_cmd_vld;
        coverpoint _d2j_p_wrack_vld;
        coverpoint TimeOutError;
        coverpoint UnmappedAddressError;
        coverpoint UncorrectableError;
        coverpoint dmc_tag;
        coverpoint bus_address;
    endgroup
endclass

task d2j_task(d2j _d2j);
    reg [5:0] Ctag; 
    @(posedge l1clk)
    if(_d2j.randomize() == 1)
    begin
        d2j_cmd_vld = _d2j._d2j_cmd_vld;
        if (d2j_cmd_vld)
        begin
            d2j_cmd = _d2j.d2jcmd;
            if (d2j_cmd == 4'b0010)
            begin
                Ctag[5:4] = 2'b00;
                Ctag[3] = ^_d2j.dmc_tag[15:12];
                Ctag[2] = ^_d2j.dmc_tag[11:8];
                Ctag[1] = ^_d2j.dmc_tag[7:4];
                Ctag[0] = ^_d2j.dmc_tag[3:0]; 
                // Table 6-11 Vol 1 434 - 526
                // No Payload
                d2j_data = {{d2j_cmd, 2'b10}, 1'b0, _d2j.bus_address[0] == 1 ? 2'b10 : 2'b01, _d2j.TimeOutError, _d2j.UnmappedAddressError, _d2j.UncorrectableError, _d2j.dmc_tag, 1'b0, ^{d2j_cmd, 2'b10}, Ctag, 1'b0, _d2j.bus_address}; 
                d2j_addr = _d2j.bus_address;
                d2j_ctag = $random;
            end
            else if (d2j_cmd == 4'b0100 || d2j_cmd == 4'b0101)
            begin
                Ctag[5:4] = 2'b00;
                Ctag[3] = ^_d2j.dmc_tag[15:12];
                Ctag[2] = ^_d2j.dmc_tag[11:8];
                Ctag[1] = ^_d2j.dmc_tag[7:4];
                Ctag[0] = ^_d2j.dmc_tag[3:0];
                // Table 6-13 Vol 1 436 - 526
                // 4 Clock Cycles of PayLoad
                d2j_data = {{d2j_cmd, 2'b10}, 1'b0, 1'b0, _d2j.TimeOutError, _d2j.UnmappedAddressError, _d2j.UncorrectableError, _d2j.dmc_tag, 1'b0, ^{d2j_cmd, 2'b10}, Ctag, 1'b0, _d2j.bus_address};
                d2j_addr = _d2j.bus_address;
                d2j_bmsk = $random;
                d2j_ctag = $random;
            end
            else if (d2j_cmd == 4'b0000)
            begin
                // Table 6-15 Vol 1 437 - 526
                // One cycle of PayLoad
                d2j_data = {{d2j_cmd, 2'b01}, 1'b0, 1'b0, _d2j.TimeOutError, _d2j.UnmappedAddressError, _d2j.UncorrectableError, _d2j.dmc_tag, {24{1'b0}}, {40{1'bX}}};
                d2j_addr = _d2j.bus_address;
                d2j_bmsk = $random;
                d2j_ctag = $random;
            end
            else if (d2j_cmd == 4'b1010)
            begin
                // Table 6-17 Vol 1 438 - 526
                // One cyle of PayLoad
                d2j_cmd = {{d2j_cmd, 2'b01}, 1'b0, 1'b0, _d2j.TimeOutError, _d2j.UnmappedAddressError, _d2j.UncorrectableError, _d2j.dmc_tag, ^{d2j_cmd, 2'b01}, {24{1'b0}}, {40{1'bX}}};
                d2j_ctag = $random;
            end
        end
        _d2j.d2j_cover.sample();
    end
    else
    begin
        $display("Randomization at _d2j failed.\n");
    end
endtask

class  ack_nack_gen;
    rand logic _ncu_dmu_mondo_ack;
    rand logic _ncu_dmu_mondo_nack;
    // Needed to generate ncu_dmu_mondo_id
    rand logic [3:0] cl_tag;
    rand logic [1:0] mdo_tag;

    constraint ack_nack_con { !(_ncu_dmu_mondo_ack & _ncu_dmu_mondo_nack); };
    constraint cl_tag_range { cl_tag inside {[0:4'b1111]};};
    constraint mdo_tag_range { mdo_tag inside {[0:2'b11]};};

    function new (int seed);
        this.srandom(seed);
        ack_nack_gen_cover = new();
    endfunction

    covergroup ack_nack_gen_cover;
        coverpoint _ncu_dmu_mondo_ack;
        coverpoint _ncu_dmu_mondo_nack;
        coverpoint cl_tag;
        coverpoint mdo_tag;
    endgroup
endclass

task ack_nack_gen_task(ack_nack_gen _ack_nack_gen);
    @(posedge l1clk)
    if(_ack_nack_gen.randomize() == 1)
    begin
        ncu_dmu_mondo_ack = _ack_nack_gen._ncu_dmu_mondo_ack;
        ncu_dmu_mondo_nack = _ack_nack_gen._ncu_dmu_mondo_nack;
        if (ncu_dmu_mondo_ack || ncu_dmu_mondo_nack)
        begin
            ncu_dmu_mondo_id = {_ack_nack_gen.cl_tag, _ack_nack_gen.mdo_tag};
            ncu_dmu_mondo_id_par = ^ncu_dmu_mondo_id;
        end
        else
        begin
            ncu_dmu_mondo_id = 6'bXX_XXXX;
        end
        _ack_nack_gen.ack_nack_gen_cover.sample();
    end
    else
    begin
        $display("Randomization at _ack_nack_gen failed.\n");
    end
endtask

class sio_dmu_dat;

    // Needed to construct sio_dmu_data
    rand logic _sio_dmu_hdr_vld;

    rand logic TimeOutError;
    rand logic UnmappedAddressError;
    rand logic UncorrectableError;
    rand logic [15:0] dmc_tag;
    rand logic [39:0] bus_address;


    constraint TimeOutError_range { TimeOutError dist {0:=9, 1:=1};};
    constraint UnmappedAddressError_range { UnmappedAddressError dist {0:=7, 1:=3};};
    constraint UncorrectableError_range { UncorrectableError dist {0:=9, 1:=1};};
    
    logic [15:0] tag_vals[] = '{16'h8000, 16'h4000, 16'h2000, 16'h1000, 16'h0800, 16'h0400, 16'h0200, 16'h0100, 16'h0080, 16'h0040, 16'h0020, 16'h0010, 16'h0008, 16'h0004, 16'h0002, 16'h0001};
    constraint dmc_tag_range {
        dmc_tag inside tag_vals;
    }

    constraint bus_address_range { bus_address dist {[0:40'hFF_FFFF_FFFF]};};

    function new (int seed);
        this.srandom(seed);
        sio_dmu_data_cover = new();
    endfunction

    covergroup sio_dmu_data_cover;
        coverpoint _sio_dmu_hdr_vld;
        coverpoint TimeOutError;
        coverpoint UnmappedAddressError;
        coverpoint UncorrectableError;
        coverpoint dmc_tag;
        coverpoint bus_address;
    endgroup

endclass

task sio_dmu_data_task(sio_dmu_dat _sio_dmu_data);
    reg [5:0] Ctag;
    @(posedge l1clk)
    if(_sio_dmu_data.randomize() == 1)
    begin
        sio_dmu_hdr_vld = _sio_dmu_data._sio_dmu_hdr_vld;
        if(sio_dmu_hdr_vld)
        begin
            Ctag[5:4] = 2'b00;
            Ctag[3] = ^_sio_dmu_data.dmc_tag[15:12];
            Ctag[2] = ^_sio_dmu_data.dmc_tag[11:8];
            Ctag[1] = ^_sio_dmu_data.dmc_tag[7:4];
            Ctag[0] = ^_sio_dmu_data.dmc_tag[3:0];
            sio_dmu_data = {6'b1010_10, 1'b0, 1'b0, _sio_dmu_data.TimeOutError, _sio_dmu_data.UnmappedAddressError, _sio_dmu_data.UncorrectableError, _sio_dmu_data.dmc_tag, 1'b0, Ctag, 1'b0, _sio_dmu_data.bus_address};
        end
    end
    else
    begin
        $display("Randomization at _sio_dmu_data failed\n");
    end
endtask

class dmu_dsn_data;
    rand logic [31:0] _ncu_dmu_data;
    rand logic [63:0] _ncu_dmu_pio_data;
    rand logic [31:0] _d2j_csr_ring_in;

    constraint ncu_dmu_data_range { _ncu_dmu_data inside {[0:32'hFFFF_FFFF]};};
    constraint ncu_dmu_pio_data_range { _ncu_dmu_pio_data inside {[0:64'hFFFF_FFFF_FFFF_FFFF]};};
    constraint d2j_csr_ring_in_range { _d2j_csr_ring_in inside {[0:32'hFFFF_FFFF]};};



    function new (int seed);
        this.srandom(seed);
        dmu_dsn_data_cover = new();
    endfunction

    covergroup dmu_dsn_data_cover;
        coverpoint _ncu_dmu_data;
        coverpoint _ncu_dmu_pio_data;
        coverpoint _d2j_csr_ring_in;
    endgroup
endclass

task dmu_dsn_data_task(dmu_dsn_data _dmu_dsn_data);
    @(posedge l1clk)
    if (_dmu_dsn_data.randomize() == 1)
    begin
        ncu_dmu_data = _dmu_dsn_data._ncu_dmu_data;
        ncu_dmu_pio_data = _dmu_dsn_data._ncu_dmu_pio_data;
        d2j_csr_ring_in = _dmu_dsn_data._d2j_csr_ring_in;
        _dmu_dsn_data.dmu_dsn_data_cover.sample();
    end
    else
    begin
        $display("Randomization at _dmu_dsn_data failed.\n");
    end
endtask

task reset_task;
    begin
        $display("Initializing design\n");
        #100;
        rst_l = 1;
        $display("Reset sequence ended: %d\n", $time);
    end
endtask

always begin
    #5000 ncu_dmu_stall = 1;
    #48 ncu_dmu_stall = 0;
end

always begin
    #500 ncu_dmu_vld = 1'b1;
    #3 ncu_dmu_vld = 1'b0;
end

always begin
    #500 ncu_dmu_pio_hdr_vld = 1'b1;
    #3 ncu_dmu_pio_hdr_vld = 1'b0;
end

always begin
    #1000 sii_dmu_wrack_vld = 1'b1;
    sii_dmu_wrack_tag = $random;
    sii_dmu_wrack_par = ^sii_dmu_wrack_tag;
    @(posedge l1clk);
    sii_dmu_wrack_vld = 1'b0;
    sii_dmu_wrack_tag = {4{1'bX}};
    sii_dmu_wrack_par = 1'bX;
end

always @(posedge l1clk)
begin
    if (d2j_cmd == 4'b1010)
    begin
        d2j_p_wrack_vld = 1'b1;
        d2j_p_wrack_tag = $random;
    end
    else
    begin
        d2j_p_wrack_vld = 1'bX;
        d2j_p_wrack_tag = {4{1'bX}};
    end
end


initial begin
    d2j _d2j;
    #5;
    _d2j = new(seed);
    #600;
    repeat(500) begin
        d2j_task(_d2j);
        @(posedge l1clk);
        d2j_cmd_vld = 0;
        d2j_cmd = {4{1'bX}};
        d2j_addr = {36{1'bX}};
        // d2j data depends on d2j_cmd. See Tables
        if(_d2j._d2j_cmd_vld)
        begin
            if (_d2j.d2jcmd == 4'b0010)
            begin
                d2j_data_vld = 0;
                d2j_data = {128{1'bX}};
                d2j_ctag = {16{1'bX}};
            end
            else if (_d2j.d2jcmd == 4'b0100 || _d2j.d2jcmd == 4'b0101)
            begin
                d2j_bmsk = {16{1'bX}};
                d2j_ctag = {16{1'bX}};
                d2j_data_vld = 1'b1;
                d2j_data = $random;
                d2j_data_par[0] = d2j_data[29:0];
                d2j_data_par[1] = d2j_data[59:30];
                d2j_data_par[2] = d2j_data[89:60];
                d2j_data_par[3] = d2j_data[119:90];
                d2j_data_par[4] = d2j_data[127:120];
                repeat (4) @(posedge l1clk) begin
                    d2j_data = $random;
                    d2j_data_par[0] = d2j_data[29:0];
                    d2j_data_par[1] = d2j_data[59:30];
                    d2j_data_par[2] = d2j_data[89:60];
                    d2j_data_par[3] = d2j_data[119:90];
                    d2j_data_par[4] = d2j_data[127:120];
                end
                d2j_data_vld = 1'b0;
                d2j_data = {128{1'bX}};
                d2j_data_par = {5{1'bX}};
            end
            else if (_d2j.d2jcmd == 4'b0000)
            begin
                d2j_bmsk = {16{1'bX}};
                d2j_ctag = {16{1'bX}};
                d2j_data_vld = 1'b1;
                d2j_data = $random;
                d2j_data_par[0] = d2j_data[29:0];
                d2j_data_par[1] = d2j_data[59:30];
                d2j_data_par[2] = d2j_data[89:60];
                d2j_data_par[3] = d2j_data[119:90];
                d2j_data_par[4] = d2j_data[127:120];
                @(posedge l1clk);
                d2j_data_vld = 1'b0;
                d2j_data = {128{1'bX}};
                d2j_data_par = {5{1'bX}};
            end
            else if (_d2j.d2jcmd == 4'b1010)
            begin
                d2j_ctag = {16{1'bX}};
                d2j_data_vld = 1'b1;
                d2j_data = $random;
                d2j_data_par[0] = d2j_data[29:0];
                d2j_data_par[1] = d2j_data[59:30];
                d2j_data_par[2] = d2j_data[89:60];
                d2j_data_par[3] = d2j_data[119:90];
                d2j_data_par[4] = d2j_data[127:120];
                @(posedge l1clk);
                d2j_data_vld = 1'b0;
                d2j_data = {128{1'bX}};
                d2j_data_par = {5{1'bX}};
            end
                    end
        d2j_data_vld = 0;
        d2j_data = {128{1'bX}};
    #500;
    end
end

initial begin
    sio_dmu_dat _sio_dmu_data;
    #5;
    _sio_dmu_data = new(seed);
    #400;
    repeat (500 ) begin
        sio_dmu_data_task(_sio_dmu_data);
        @(posedge l1clk);
        sio_dmu_hdr_vld = 0;
        sio_dmu_data = $random;
        sio_dmu_parity[7] = ^sio_dmu_data[127:112];
        sio_dmu_parity[6] = ^sio_dmu_data[111:96];
        sio_dmu_parity[5] = ^sio_dmu_data[95:80];
        sio_dmu_parity[4] = ^sio_dmu_data[79:64];
        sio_dmu_parity[3] = ^sio_dmu_data[63:48];
        sio_dmu_parity[2] = ^sio_dmu_data[47:32];
        sio_dmu_parity[1] = ^sio_dmu_data[31:16];
        sio_dmu_parity[0] = ^sio_dmu_data[15:0];
        if(_sio_dmu_data._sio_dmu_hdr_vld)
        begin
            repeat (4) @(posedge l1clk) begin
                sio_dmu_data = $random;
                sio_dmu_parity[7] = ^sio_dmu_data[127:112];
                sio_dmu_parity[6] = ^sio_dmu_data[111:96];
                sio_dmu_parity[5] = ^sio_dmu_data[95:80];
                sio_dmu_parity[4] = ^sio_dmu_data[79:64];
                sio_dmu_parity[3] = ^sio_dmu_data[63:48];
                sio_dmu_parity[2] = ^sio_dmu_data[47:32];
                sio_dmu_parity[1] = ^sio_dmu_data[31:16];
                sio_dmu_parity[0] = ^sio_dmu_data[15:0];
            end
        end
        sio_dmu_data = {128{1'bX}};
        sio_dmu_parity = {8{1'bX}};
        #500;
    end
end

initial begin
    ack_nack_gen _ack_nack_gen;
    #5;
    _ack_nack_gen = new(seed);
    #300;
    repeat(100) begin
        ack_nack_gen_task(_ack_nack_gen);
        @(posedge l1clk);
        ncu_dmu_mondo_ack = 0;
        ncu_dmu_mondo_nack = 0;
        ncu_dmu_mondo_id = 6'bXX_XXXX;
        #1000;
    end
end

initial begin
    dmu_dsn_data _dmu_dsn_data;
    #5;
    $display("Seed is: %d \n", seed);
    _dmu_dsn_data = new(seed);
    #10;
    reset_task;
    #10;
    repeat (1024) begin
        dmu_dsn_data_task(_dmu_dsn_data);
        #100;
    end
    $finish;
end

`include "dmu_dsn_rtl_trace.v"

endmodule

/**

d2j_p_wrack_tag needs modification and udnerstanding. Moving to a new bench now.


**/
