`ifdef PRankNetlist
	initial begin
		#0 ncu_mb0_ctluser_incr_addr2 = 1'b0;
	end
	assign `SIG210 = ncu_mb0_ctluser_incr_addr2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61 = 1'bx;
	end
	assign `SIG82 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data61;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60 = 1'bx;
	end
	assign `SIG83 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data60;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63 = 1'bx;
	end
	assign `SIG84 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data63;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62 = 1'bx;
	end
	assign `SIG85 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data62;

	initial begin
		#0 n9932 = 1'b0;
	end
	assign `SIG128 = n9932;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr2 = 1'bx;
	end
	assign `SIG4 = ncu_c2ifcd_ctlpcx_ncu_cputhr2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112 = 1'b0;
	end
	assign `SIG14 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_112;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr0 = 1'bx;
	end
	assign `SIG6 = ncu_c2ifcd_ctlpcx_ncu_cputhr0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110 = 1'b0;
	end
	assign `SIG16 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_110;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr4 = 1'bx;
	end
	assign `SIG8 = ncu_c2ifcd_ctlpcx_ncu_cputhr4;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr5 = 1'bx;
	end
	assign `SIG9 = ncu_c2ifcd_ctlpcx_ncu_cputhr5;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail1 = 1'b0;
		#12595.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail1 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail1 = 1'b0;
	end
	assign `SIG242 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail1;

	initial begin
		#0 intbuf_wr2i2c = 1'b0;
	end
	assign `SIG15 = intbuf_wr2i2c;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97 = 1'b0;
	end
	assign `SIG17 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta97;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96 = 1'b0;
	end
	assign `SIG18 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta96;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98 = 1'b0;
	end
	assign `SIG19 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta98;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d13 = 1'b0;
	end
	assign `SIG175 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d13;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d12 = 1'b0;
	end
	assign `SIG225 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d12;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d11 = 1'b0;
	end
	assign `SIG172 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d11;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10 = 1'b0;
	end
	assign `SIG119 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d10;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d14 = 1'b0;
	end
	assign `SIG177 = ncu_i2cfcd_ctlncu_i2cfc_ctliobuf_head_d14;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr9 = 1'bx;
	end
	assign `SIG3 = ncu_c2ifcd_ctlpcx_ncu_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr8 = 1'bx;
	end
	assign `SIG86 = ncu_c2ifcd_ctlpcx_ncu_addr8;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108 = 1'b0;
	end
	assign `SIG20 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_108;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109 = 1'b0;
	end
	assign `SIG21 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_109;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12 = 1'b0;
	end
	assign `SIG127 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr3 = 1'bx;
	end
	assign `SIG43 = ncu_c2ifcd_ctlpcx_ncu_addr3;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr2 = 1'bx;
	end
	assign `SIG31 = ncu_c2ifcd_ctlpcx_ncu_addr2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr1 = 1'bx;
	end
	assign `SIG32 = ncu_c2ifcd_ctlpcx_ncu_addr1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr0 = 1'bx;
	end
	assign `SIG33 = ncu_c2ifcd_ctlpcx_ncu_addr0;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr7 = 1'bx;
	end
	assign `SIG44 = ncu_c2ifcd_ctlpcx_ncu_addr7;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr6 = 1'bx;
	end
	assign `SIG45 = ncu_c2ifcd_ctlpcx_ncu_addr6;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr5 = 1'bx;
	end
	assign `SIG155 = ncu_c2ifcd_ctlpcx_ncu_addr5;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr4 = 1'bx;
	end
	assign `SIG132 = ncu_c2ifcd_ctlpcx_ncu_addr4;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped21 = 1'b0;
	end
	assign `SIG27 = ncu_mb0_ctlrd_en_piped21;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped20 = 1'b0;
	end
	assign `SIG2 = ncu_mb0_ctlrd_en_piped20;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped22 = 1'b0;
	end
	assign `SIG28 = ncu_mb0_ctlrd_en_piped22;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req3 = 1'bx;
	end
	assign `SIG241 = ncu_c2ifcd_ctlpcx_ncu_req3;

	initial begin
		#0 ncu_mb0_ctluser_bisi_wr_mode = 1'b0;
	end
	assign `SIG218 = ncu_mb0_ctluser_bisi_wr_mode;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113 = 1'b1;
	end
	assign `SIG117 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_113;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13 = 1'b0;
	end
	assign `SIG250 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr3 = 1'bx;
	end
	assign `SIG5 = ncu_c2ifcd_ctlpcx_ncu_cputhr3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103 = 1'bx;
	end
	assign `SIG181 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_103;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111 = 1'b1;
	end
	assign `SIG42 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_111;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2 = 1'bx;
	end
	assign `SIG157 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_cputhr1 = 1'bx;
	end
	assign `SIG7 = ncu_c2ifcd_ctlpcx_ncu_cputhr1;

	initial begin
		#0 ncu_mb0_ctlrd_en_piped32 = 1'b0;
	end
	assign `SIG191 = ncu_mb0_ctlrd_en_piped32;

	initial begin
		#0 ncu_mb0_ctluser_start_addr3 = 1'b0;
	end
	assign `SIG202 = ncu_mb0_ctluser_start_addr3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57 = 1'bx;
	end
	assign `SIG166 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data57;

	initial begin
		#0 ncu_mb0_ctluser_start_addr1 = 1'b0;
	end
	assign `SIG204 = ncu_mb0_ctluser_start_addr1;

	initial begin
		#0 ncu_mb0_ctluser_start_addr0 = 1'b0;
	end
	assign `SIG205 = ncu_mb0_ctluser_start_addr0;

	initial begin
		#0 ncu_mb0_ctluser_start_addr5 = 1'b0;
	end
	assign `SIG206 = ncu_mb0_ctluser_start_addr5;

	initial begin
		#0 ncu_mb0_ctluser_start_addr4 = 1'b0;
	end
	assign `SIG207 = ncu_mb0_ctluser_start_addr4;

	initial begin
		#0 ncu_mb0_ctlconfig_out0 = 1'b0;
	end
	assign `SIG156 = ncu_mb0_ctlconfig_out0;

	initial begin
		#0 ncu_mb0_ctluser_cmpsel0 = 1'b0;
	end
	assign `SIG199 = ncu_mb0_ctluser_cmpsel0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14 = 1'b0;
	end
	assign `SIG254 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10 = 1'b0;
	end
	assign `SIG253 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d10;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out36 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out36 = 1'b1;
	end
	assign `SIG122 = ncu_mb0_ctldata_pipe_out36;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out35 = 1'b0;
	end
	assign `SIG25 = ncu_mb0_ctldata_pipe_out35;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out34 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out34 = 1'b1;
	end
	assign `SIG123 = ncu_mb0_ctldata_pipe_out34;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out33 = 1'b0;
	end
	assign `SIG26 = ncu_mb0_ctldata_pipe_out33;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out32 = 1'b0;
		#12345.0 ncu_mb0_ctldata_pipe_out32 = 1'b1;
	end
	assign `SIG124 = ncu_mb0_ctldata_pipe_out32;

	initial begin
		#0 ncu_mb0_ctldata_pipe_out31 = 1'b0;
	end
	assign `SIG125 = ncu_mb0_ctldata_pipe_out31;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr29 = 1'bx;
	end
	assign `SIG231 = ncu_c2ifcd_ctlpcx_ncu_addr29;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109 = 1'bx;
	end
	assign `SIG62 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_109;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108 = 1'bx;
	end
	assign `SIG63 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_108;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105 = 1'bx;
	end
	assign `SIG64 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_105;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104 = 1'bx;
	end
	assign `SIG65 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_104;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107 = 1'bx;
	end
	assign `SIG66 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_107;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106 = 1'bx;
	end
	assign `SIG67 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_106;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr36 = 1'bx;
	end
	assign `SIG236 = ncu_c2ifcd_ctlpcx_ncu_addr36;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60 = 1'b0;
		#12595.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60 = 1'b0;
	end
	assign `SIG195 = ncu_c2ifcd_ctlncu_c2ifc_ctlcpubuf_tail_plus60;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15 = 1'b0;
	end
	assign `SIG176 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100 = 1'b0;
	end
	assign `SIG23 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta100;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101 = 1'b0;
	end
	assign `SIG24 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta101;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0 = 1'bx;
	end
	assign `SIG158 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data0;

	initial begin
		#0 ncu_i2cfcd_ctliobuf_vld = 1'b0;
	end
	assign `SIG196 = ncu_i2cfcd_ctliobuf_vld;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlint_pav = 1'b0;
	end
	assign `SIG194 = ncu_i2cfcd_ctlncu_i2cfc_ctlint_pav;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49 = 1'bx;
	end
	assign `SIG94 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data49;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48 = 1'bx;
	end
	assign `SIG95 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data48;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20 = 1'bx;
	end
	assign `SIG12 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21 = 1'bx;
	end
	assign `SIG13 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22 = 1'bx;
	end
	assign `SIG38 = ncu_c2ifcd_ctlncu_c2ifd_ctlcpu_mondo_cpu_id_d22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43 = 1'bx;
	end
	assign `SIG96 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data43;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42 = 1'bx;
	end
	assign `SIG97 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data42;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41 = 1'bx;
	end
	assign `SIG160 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data41;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40 = 1'bx;
	end
	assign `SIG161 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data40;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47 = 1'bx;
	end
	assign `SIG98 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data47;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46 = 1'bx;
	end
	assign `SIG99 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data46;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45 = 1'bx;
	end
	assign `SIG100 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data45;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44 = 1'bx;
	end
	assign `SIG101 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data44;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60 = 1'bx;
	end
	assign `SIG248 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta60;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62 = 1'bx;
	end
	assign `SIG243 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta62;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63 = 1'bx;
	end
	assign `SIG249 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta63;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110 = 1'bx;
	end
	assign `SIG76 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_110;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111 = 1'bx;
	end
	assign `SIG77 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_111;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114 = 1'bx;
	end
	assign `SIG78 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data_114;

	initial begin
		#0 ncu_mb0_ctluser_start_addr2 = 1'b0;
	end
	assign `SIG203 = ncu_mb0_ctluser_start_addr2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr30 = 1'bx;
	end
	assign `SIG237 = ncu_c2ifcd_ctlpcx_ncu_addr30;

	initial begin
		#0 ncu_mb0_ctlconfig_out5 = 1'b0;
	end
	assign `SIG214 = ncu_mb0_ctlconfig_out5;

	initial begin
		#0 n9898 = 1'b0;
	end
	assign `SIG37 = n9898;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58 = 1'bx;
	end
	assign `SIG162 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data58;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59 = 1'bx;
	end
	assign `SIG163 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data59;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54 = 1'bx;
	end
	assign `SIG102 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data54;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55 = 1'bx;
	end
	assign `SIG164 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data55;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56 = 1'bx;
	end
	assign `SIG165 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data56;

	initial begin
		#0 n9890 = 1'b0;
	end
	assign `SIG11 = n9890;

	initial begin
		#0 n9897 = 1'b0;
	end
	assign `SIG40 = n9897;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51 = 1'bx;
	end
	assign `SIG104 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data51;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52 = 1'bx;
	end
	assign `SIG167 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data52;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53 = 1'bx;
	end
	assign `SIG240 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data53;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29 = 1'bx;
	end
	assign `SIG46 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data29;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'b1;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28 = 1'bx;
	end
	assign `SIG47 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data28;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59 = 1'bx;
	end
	assign `SIG255 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta59;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21 = 1'bx;
	end
	assign `SIG142 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data21;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'b0;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20 = 1'bx;
	end
	assign `SIG143 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data20;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23 = 1'bx;
	end
	assign `SIG48 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data23;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22 = 1'bx;
	end
	assign `SIG49 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data22;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25 = 1'bx;
	end
	assign `SIG50 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data25;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24 = 1'bx;
	end
	assign `SIG51 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data24;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27 = 1'bx;
	end
	assign `SIG52 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data27;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26 = 1'bx;
	end
	assign `SIG53 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data26;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr19 = 1'bx;
	end
	assign `SIG54 = ncu_c2ifcd_ctlpcx_ncu_addr19;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr18 = 1'bx;
	end
	assign `SIG55 = ncu_c2ifcd_ctlpcx_ncu_addr18;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr17 = 1'bx;
	end
	assign `SIG56 = ncu_c2ifcd_ctlpcx_ncu_addr17;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr16 = 1'bx;
	end
	assign `SIG57 = ncu_c2ifcd_ctlpcx_ncu_addr16;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr15 = 1'bx;
	end
	assign `SIG58 = ncu_c2ifcd_ctlpcx_ncu_addr15;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr14 = 1'bx;
	end
	assign `SIG59 = ncu_c2ifcd_ctlpcx_ncu_addr14;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr13 = 1'bx;
	end
	assign `SIG60 = ncu_c2ifcd_ctlpcx_ncu_addr13;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr12 = 1'bx;
	end
	assign `SIG133 = ncu_c2ifcd_ctlpcx_ncu_addr12;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr11 = 1'bx;
	end
	assign `SIG61 = ncu_c2ifcd_ctlpcx_ncu_addr11;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr10 = 1'bx;
	end
	assign `SIG29 = ncu_c2ifcd_ctlpcx_ncu_addr10;

	initial begin
		#0 n9884 = 1'b0;
	end
	assign `SIG79 = n9884;

	initial begin
		#0 n9885 = 1'b0;
	end
	assign `SIG234 = n9885;

	initial begin
		#0 n9887 = 1'b0;
	end
	assign `SIG120 = n9887;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10 = 1'bx;
	end
	assign `SIG192 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11 = 1'bx;
	end
	assign `SIG189 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12 = 1'bx;
	end
	assign `SIG115 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13 = 1'bx;
	end
	assign `SIG188 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14 = 1'bx;
	end
	assign `SIG227 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15 = 1'bx;
	end
	assign `SIG116 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr2_a_d15;

	initial begin
		#0 ncu_mb0_ctluser_incr_addr0 = 1'b0;
	end
	assign `SIG208 = ncu_mb0_ctluser_incr_addr0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11 = 1'b0;
	end
	assign `SIG252 = ncu_c2ifcd_ctlncu_c2ifd_ctlrd_a_d11;

	initial begin
		#0 ncu_mb0_ctluser_data_out0 = 1'b0;
	end
	assign `SIG106 = ncu_mb0_ctluser_data_out0;

	initial begin
		#0 ncu_mb0_ctluser_data_out1 = 1'b0;
	end
	assign `SIG107 = ncu_mb0_ctluser_data_out1;

	initial begin
		#0 ncu_mb0_ctluser_data_out2 = 1'b0;
	end
	assign `SIG108 = ncu_mb0_ctluser_data_out2;

	initial begin
		#0 ncu_mb0_ctluser_data_out3 = 1'b0;
	end
	assign `SIG109 = ncu_mb0_ctluser_data_out3;

	initial begin
		#0 ncu_mb0_ctluser_data_out4 = 1'b0;
	end
	assign `SIG110 = ncu_mb0_ctluser_data_out4;

	initial begin
		#0 ncu_mb0_ctluser_data_out5 = 1'b0;
	end
	assign `SIG111 = ncu_mb0_ctluser_data_out5;

	initial begin
		#0 ncu_mb0_ctluser_data_out6 = 1'b0;
	end
	assign `SIG112 = ncu_mb0_ctluser_data_out6;

	initial begin
		#0 ncu_mb0_ctluser_data_out7 = 1'b0;
	end
	assign `SIG113 = ncu_mb0_ctluser_data_out7;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlio_pav = 1'b0;
	end
	assign `SIG173 = ncu_i2cfcd_ctlncu_i2cfc_ctlio_pav;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38 = 1'bx;
	end
	assign `SIG134 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data38;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39 = 1'bx;
	end
	assign `SIG135 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data39;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46 = 1'bx;
	end
	assign `SIG244 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta46;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47 = 1'bx;
	end
	assign `SIG245 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta47;

	initial begin
		#0 ncu_mb0_ctluser_array_sel0 = 1'b0;
	end
	assign `SIG200 = ncu_mb0_ctluser_array_sel0;

	initial begin
		#0 ncu_mb0_ctluser_array_sel1 = 1'b0;
	end
	assign `SIG201 = ncu_mb0_ctluser_array_sel1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32 = 1'bx;
	end
	assign `SIG68 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data32;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33 = 1'bx;
	end
	assign `SIG136 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data33;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30 = 1'bx;
	end
	assign `SIG69 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data30;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31 = 1'bx;
	end
	assign `SIG70 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data31;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36 = 1'bx;
	end
	assign `SIG137 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data36;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37 = 1'bx;
	end
	assign `SIG168 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data37;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34 = 1'bx;
	end
	assign `SIG138 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data34;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35 = 1'bx;
	end
	assign `SIG139 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data35;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9 = 1'b0;
	end
	assign `SIG229 = ncu_c2ifcd_ctlncu_c2ifc_ctltap_mondo_acc_addr9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr28 = 1'bx;
	end
	assign `SIG71 = ncu_c2ifcd_ctlpcx_ncu_addr28;

	initial begin
		#0 ncu_mb0_ctluser_incr_addr3 = 1'b0;
	end
	assign `SIG211 = ncu_mb0_ctluser_incr_addr3;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out31 = 1'b0;
	end
	assign `SIG0 = ncu_mb0_ctlcmpsel_pipe_out31;

	initial begin
		#0 ncu_mb0_ctlcmpsel_pipe_out30 = 1'b0;
	end
	assign `SIG1 = ncu_mb0_ctlcmpsel_pipe_out30;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr22 = 1'bx;
	end
	assign `SIG232 = ncu_c2ifcd_ctlpcx_ncu_addr22;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr23 = 1'bx;
	end
	assign `SIG233 = ncu_c2ifcd_ctlpcx_ncu_addr23;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr20 = 1'bx;
	end
	assign `SIG72 = ncu_c2ifcd_ctlpcx_ncu_addr20;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr21 = 1'bx;
	end
	assign `SIG73 = ncu_c2ifcd_ctlpcx_ncu_addr21;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr26 = 1'bx;
	end
	assign `SIG74 = ncu_c2ifcd_ctlpcx_ncu_addr26;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr27 = 1'bx;
	end
	assign `SIG75 = ncu_c2ifcd_ctlpcx_ncu_addr27;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr24 = 1'bx;
	end
	assign `SIG140 = ncu_c2ifcd_ctlpcx_ncu_addr24;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr25 = 1'bx;
	end
	assign `SIG141 = ncu_c2ifcd_ctlpcx_ncu_addr25;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104 = 1'bx;
	end
	assign `SIG180 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_104;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102 = 1'bx;
	end
	assign `SIG228 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_102;

	initial begin
		#0 ncu_mb0_ctlconfig_out8 = 1'b0;
	end
	assign `SIG114 = ncu_mb0_ctlconfig_out8;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr37 = 1'bx;
	end
	assign `SIG235 = ncu_c2ifcd_ctlpcx_ncu_addr37;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr38 = 1'bx;
	end
	assign `SIG239 = ncu_c2ifcd_ctlpcx_ncu_addr38;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_120 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_120 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_120 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_120 = 1'bx;
	end
	assign `SIG183 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_120;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req4 = 1'bx;
	end
	assign `SIG169 = ncu_c2ifcd_ctlpcx_ncu_req4;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr2 = 1'b0;
	end
	assign `SIG219 = ncu_mb0_ctluser_stop_addr2;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr3 = 1'b0;
	end
	assign `SIG220 = ncu_mb0_ctluser_stop_addr3;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr0 = 1'b0;
	end
	assign `SIG221 = ncu_mb0_ctluser_stop_addr0;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr1 = 1'b0;
	end
	assign `SIG222 = ncu_mb0_ctluser_stop_addr1;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr4 = 1'b0;
	end
	assign `SIG223 = ncu_mb0_ctluser_stop_addr4;

	initial begin
		#0 ncu_mb0_ctluser_stop_addr5 = 1'b0;
	end
	assign `SIG224 = ncu_mb0_ctluser_stop_addr5;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr33 = 1'bx;
	end
	assign `SIG238 = ncu_c2ifcd_ctlpcx_ncu_addr33;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req0 = 1'bx;
	end
	assign `SIG171 = ncu_c2ifcd_ctlpcx_ncu_req0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121 = 1'bx;
	end
	assign `SIG226 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_121;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr35 = 1'bx;
	end
	assign `SIG144 = ncu_c2ifcd_ctlpcx_ncu_addr35;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr34 = 1'bx;
	end
	assign `SIG30 = ncu_c2ifcd_ctlpcx_ncu_addr34;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'b0;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8 = 1'bx;
	end
	assign `SIG88 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data8;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9 = 1'bx;
	end
	assign `SIG89 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data9;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr31 = 1'bx;
	end
	assign `SIG145 = ncu_c2ifcd_ctlpcx_ncu_addr31;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req2 = 1'bx;
	end
	assign `SIG170 = ncu_c2ifcd_ctlpcx_ncu_req2;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_req1 = 1'bx;
	end
	assign `SIG105 = ncu_c2ifcd_ctlpcx_ncu_req1;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr32 = 1'bx;
	end
	assign `SIG80 = ncu_c2ifcd_ctlpcx_ncu_addr32;

	initial begin
		#0 ncu_mb0_ctlconfig_out1 = 1'b0;
	end
	assign `SIG35 = ncu_mb0_ctlconfig_out1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3 = 1'bx;
	end
	assign `SIG90 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data3;

	initial begin
		#0 ncu_mb0_ctlconfig_out3 = 1'b0;
	end
	assign `SIG39 = ncu_mb0_ctlconfig_out3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1 = 1'bx;
	end
	assign `SIG91 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data1;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6 = 1'bx;
	end
	assign `SIG34 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7 = 1'bx;
	end
	assign `SIG92 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data4 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data4 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data4 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data4 = 1'bx;
	end
	assign `SIG159 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data4;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'b1;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5 = 1'bx;
	end
	assign `SIG93 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data5;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_117 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_117 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_117 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_117 = 1'bx;
	end
	assign `SIG179 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_117;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_116 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_116 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_116 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_116 = 1'bx;
	end
	assign `SIG184 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_116;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_115 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_115 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_115 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_115 = 1'bx;
	end
	assign `SIG185 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_115;

	initial begin
		#0 ncu_mb0_ctlconfig_out2 = 1'b0;
	end
	assign `SIG130 = ncu_mb0_ctlconfig_out2;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44 = 1'bx;
	end
	assign `SIG246 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta44;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_addr39 = 1'bx;
	end
	assign `SIG81 = ncu_c2ifcd_ctlpcx_ncu_addr39;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'b1;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45 = 1'bx;
	end
	assign `SIG247 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta45;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_119 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_119 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_119 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_119 = 1'bx;
	end
	assign `SIG182 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_119;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_118 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_118 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_118 = 1'b0;
		#51000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_118 = 1'bx;
	end
	assign `SIG197 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_118;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail3 = 1'b0;
	end
	assign `SIG251 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail3;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12 = 1'b0;
	end
	assign `SIG186 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d12;

	initial begin
		#0 ncu_mb0_ctlconfig_out7 = 1'b0;
	end
	assign `SIG216 = ncu_mb0_ctlconfig_out7;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15 = 1'b0;
	end
	assign `SIG193 = ncu_c2ifcd_ctlncu_c2ifd_ctlwr1_a_d15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105 = 1'b1;
	end
	assign `SIG118 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_105;

	initial begin
		#0 ncu_mb0_ctlconfig_out6 = 1'b0;
	end
	assign `SIG217 = ncu_mb0_ctlconfig_out6;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106 = 1'b0;
	end
	assign `SIG22 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkta_106;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'b1;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10 = 1'bx;
	end
	assign `SIG87 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data10;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11 = 1'bx;
	end
	assign `SIG146 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data11;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12 = 1'b0;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12 = 1'bx;
	end
	assign `SIG147 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data12;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'b0;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13 = 1'bx;
	end
	assign `SIG148 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data13;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'b0;
		#49750.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14 = 1'bx;
	end
	assign `SIG149 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15 = 1'bx;
	end
	assign `SIG150 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data15;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'b0;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16 = 1'bx;
	end
	assign `SIG151 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data16;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17 = 1'b1;
		#50000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17 = 1'bx;
	end
	assign `SIG152 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data17;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'b0;
		#49000.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18 = 1'bx;
	end
	assign `SIG153 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data18;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'b0;
		#49250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19 = 1'bx;
	end
	assign `SIG154 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data19;

	initial begin
		#0 ncu_mb0_ctluser_incr_addr1 = 1'b0;
	end
	assign `SIG209 = ncu_mb0_ctluser_incr_addr1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail4 = 1'b0;
	end
	assign `SIG187 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail4;

	initial begin
		#0 ncu_mb0_ctlcontrol_out0 = 1'b0;
	end
	assign `SIG36 = ncu_mb0_ctlcontrol_out0;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'b0;
		#50250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114 = 1'bx;
	end
	assign `SIG178 = ncu_c2ifcd_ctlncu_c2ifd_ctlmondo2cpu_pkt_114;

	initial begin
		#0 ncu_mb0_ctluser_cmpsel1 = 1'b0;
	end
	assign `SIG198 = ncu_mb0_ctluser_cmpsel1;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d13 = 1'b0;
	end
	assign `SIG174 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d13;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d12 = 1'b0;
	end
	assign `SIG190 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d12;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d11 = 1'b0;
	end
	assign `SIG126 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d11;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d10 = 1'b0;
	end
	assign `SIG41 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d10;

	initial begin
		#0 ncu_mb0_ctluser_incr_addr4 = 1'b0;
	end
	assign `SIG212 = ncu_mb0_ctluser_incr_addr4;

	initial begin
		#0 ncu_mb0_ctluser_incr_addr5 = 1'b0;
	end
	assign `SIG213 = ncu_mb0_ctluser_incr_addr5;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d14 = 1'b0;
	end
	assign `SIG121 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_head_d14;

	initial begin
		#0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#12345.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'bx;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b1;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b0;
		#250.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'b1;
		#49500.0 ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50 = 1'bx;
	end
	assign `SIG103 = ncu_c2ifcd_ctlncu_c2ifd_ctlpcx_ncu_data50;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus101 = 1'b0;
	end
	assign `SIG131 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus101;

	initial begin
		#0 ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100 = 1'b0;
	end
	assign `SIG129 = ncu_i2cfcd_ctlncu_i2cfc_ctlintbuf_tail_plus100;

	initial begin
		#0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b0;
		#12345.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'bx;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b1;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b0;
		#250.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'b1;
		#49500.0 ncu_c2ifcd_ctlpcx_ncu_vld = 1'bx;
	end
	assign `SIG230 = ncu_c2ifcd_ctlpcx_ncu_vld;

	initial begin
		#0 ncu_mb0_ctluser_bisi_rd_mode = 1'b0;
	end
	assign `SIG10 = ncu_mb0_ctluser_bisi_rd_mode;

	initial begin
		#0 ncu_mb0_ctlconfig_out4 = 1'b0;
	end
	assign `SIG215 = ncu_mb0_ctlconfig_out4;

	initial begin
		 #87345.0 $finish;
	end

`endif
