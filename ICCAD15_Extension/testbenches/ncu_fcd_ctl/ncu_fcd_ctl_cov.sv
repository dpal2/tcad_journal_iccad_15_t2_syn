`ifdef _COV_
`include "ncu_fcd_ctl_tick_defines_netlist_PRankNetlist.v"
`include "ncu_fcd_ctl_tick_defines_netlist_SigSeT_1.v"
`include "ncu_fcd_ctl_tick_defines_netlist_HybrSel.v"
`endif

module ncu_fcd_ctl_cov_bench();

reg [7:0] mb1_wdata;
reg [5:0] mb1_addr;
reg [5:0] cpubuf_head_s;
reg [7:0] cpx_ncu_grant_cx;
reg [143:0] intbuf_dout;
reg [175:0] iobuf_dout;
reg [5:0] iobuf_tail_s;
reg [63:0] io_mondo_data0_din_s;
reg [63:0] io_mondo_data1_din_s;
reg [5:0] io_mondo_data_wr_addr_s;
reg [71:0] mondo_data0_dout;            // DONE
reg [71:0] mondo_data1_dout;            // DONE
reg [129:0] pcx_ncu_data_px2;
reg [21:0] tap_mondo_acc_addr_s;
reg [63:0] tap_mondo_din_s;
reg [7:0] cpubuf_mb0_data;
reg mb1_run;
reg mb1_cpubuf_wr_en;
reg tcu_mbist_user_mode;
reg tcu_mbist_bisi_en;
reg cmp_io_sync_en;
reg io_cmp_sync_en;
reg io_mondo_data_wr_s;
reg l2clk;
reg pcx_ncu_data_rdy_px1;
reg scan_in;
reg tap_mondo_acc_seq_s;
reg tap_mondo_wr_s;
reg tcu_aclk;
reg tcu_bclk;
reg tcu_clk_stop;
reg tcu_pce_ov;
reg tcu_scan_en;
reg mb0_start;
reg mb0_scanin;
reg iobuf_uei;
reg intbuf_uei;
reg mondotbl_pei;
reg array_wr_inhibit_io;
reg array_wr_inhibit_cmp;


wire [143:0] cpubuf_din;
wire [5:0] cpubuf_tail_f;
wire [4:0] cpubuf_tail_ptr;
wire [143:0] intbuf_din;
wire [4:0] intbuf_head_ptr;
wire [4:0] intbuf_tail_ptr;
wire [5:0] iobuf_head_f;
wire [4:0] iobuf_head_ptr;
wire [63:0] mondo_busy_vec_f;
wire [71:0] mondo_data0_din;
wire [71:0] mondo_data1_din;
wire [5:0] mondo_data_addr_p0;
wire [5:0] mondo_data_addr_p1;
wire [145:0] ncu_cpx_data_ca;
wire [7:0] ncu_cpx_req_cq;
wire [63:0] tap_mondo_dout_d2_f;
wire [7:0] mb0_wdata;
wire [5:0] mb0_addr;
wire cpubuf_wr;
wire intbuf_wr;
wire intbuf_rden;
wire iobuf_rden;
wire mondo_data0_wr;
wire mondo_data1_wr;
wire mondo_rd_en;
wire ncu_pcx_stall_pq;
wire tap_mondo_acc_addr_invld_d2_f;
wire tap_mondo_acc_seq_d2_f;
wire scan_out;
wire mb0_run;
wire mb0_iobuf_wr_en;
wire mb0_scanout;
wire mb0_done;
wire mb0_fail;
wire iobuf_ue_f;
wire intbuf_ue_f;
wire mondotbl_pe_f;
wire array_wr_inhibit_gate;

`ifdef _COV_
`include "ncu_fcd_ctl_trace_reg_PRankNetlist.v"
`include "ncu_fcd_ctl_trace_reg_SigSeT_1.v"
`include "ncu_fcd_ctl_trace_reg_HybrSel.v"
`endif

ncu_fcd_ctl ncu_fcd_ctl_ (.*);

initial begin
   mb1_run = 0;
   mb1_cpubuf_wr_en = 0;
   tcu_mbist_user_mode = 0;
   tcu_mbist_bisi_en = 0;
   mb0_scanin = 0;
   mb0_start = 0;
   //
   iobuf_uei = 0;
   intbuf_uei = 0;
   mondotbl_pei = 0;
   array_wr_inhibit_io = 0; 
   array_wr_inhibit_cmp = 0;
   //
   cmp_io_sync_en = 0;
   io_cmp_sync_en = 0;
   io_mondo_data_wr_s = 0;
   pcx_ncu_data_rdy_px1 = 0;
   tap_mondo_acc_seq_s = 0;
   tap_mondo_wr_s = 0;
   // clock and scan enabling signals
   l2clk = 0;
   scan_in = 0;
   tcu_aclk = 0;
   tcu_bclk = 0;
   tcu_clk_stop = 0;
   tcu_pce_ov = 0;
   tcu_scan_en = 0;
end

always begin
    // 400 MHz Clock
    #1.25 l2clk = ~l2clk;
end

`ifdef _COV_
`include "ncu_fcd_ctl_stim_netlist_PRankNetlist.v"
`include "ncu_fcd_ctl_stim_netlist_HybrSel.v"
`endif

endmodule
