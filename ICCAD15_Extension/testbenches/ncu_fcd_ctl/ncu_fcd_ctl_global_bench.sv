/* Testbench file for design ncu_fcd_ctl generated on 2017-07-24 23:55:52.994877 */
module ncu_fcd_ctl_global_bench();

reg [7:0] mb1_wdata;
reg [5:0] mb1_addr;
reg [5:0] cpubuf_head_s;
reg [7:0] cpx_ncu_grant_cx;
reg [143:0] intbuf_dout;
reg [175:0] iobuf_dout;
reg [5:0] iobuf_tail_s;
reg [63:0] io_mondo_data0_din_s;
reg [63:0] io_mondo_data1_din_s;
reg [5:0] io_mondo_data_wr_addr_s;
reg [71:0] mondo_data0_dout;            // DONE
reg [71:0] mondo_data1_dout;            // DONE
reg [129:0] pcx_ncu_data_px2;
reg [21:0] tap_mondo_acc_addr_s;
reg [63:0] tap_mondo_din_s;
reg [7:0] cpubuf_mb0_data;
reg mb1_run;
reg mb1_cpubuf_wr_en;
reg tcu_mbist_user_mode;
reg tcu_mbist_bisi_en;
reg cmp_io_sync_en;
reg io_cmp_sync_en;
reg io_mondo_data_wr_s;
reg l2clk;
reg pcx_ncu_data_rdy_px1;
reg scan_in;
reg tap_mondo_acc_seq_s;
reg tap_mondo_wr_s;
reg tcu_aclk;
reg tcu_bclk;
reg tcu_clk_stop;
reg tcu_pce_ov;
reg tcu_scan_en;
reg mb0_start;
reg mb0_scanin;
reg iobuf_uei;
reg intbuf_uei;
reg mondotbl_pei;
reg array_wr_inhibit_io;
reg array_wr_inhibit_cmp;


wire [143:0] cpubuf_din;
wire [5:0] cpubuf_tail_f;
wire [4:0] cpubuf_tail_ptr;
wire [143:0] intbuf_din;
wire [4:0] intbuf_head_ptr;
wire [4:0] intbuf_tail_ptr;
wire [5:0] iobuf_head_f;
wire [4:0] iobuf_head_ptr;
wire [63:0] mondo_busy_vec_f;
wire [71:0] mondo_data0_din;
wire [71:0] mondo_data1_din;
wire [5:0] mondo_data_addr_p0;
wire [5:0] mondo_data_addr_p1;
wire [145:0] ncu_cpx_data_ca;
wire [7:0] ncu_cpx_req_cq;
wire [63:0] tap_mondo_dout_d2_f;
wire [7:0] mb0_wdata;
wire [5:0] mb0_addr;
wire cpubuf_wr;
wire intbuf_wr;
wire intbuf_rden;
wire iobuf_rden;
wire mondo_data0_wr;
wire mondo_data1_wr;
wire mondo_rd_en;
wire ncu_pcx_stall_pq;
wire tap_mondo_acc_addr_invld_d2_f;
wire tap_mondo_acc_seq_d2_f;
wire scan_out;
wire mb0_run;
wire mb0_iobuf_wr_en;
wire mb0_scanout;
wire mb0_done;
wire mb0_fail;
wire iobuf_ue_f;
wire intbuf_ue_f;
wire mondotbl_pe_f;
wire array_wr_inhibit_gate;;

integer seed;

initial begin
    $random_seed_gen(seed);
end

ncu_fcd_ctl ncu_fcd_ctl_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("ncu_fcd_ctl_rtl.vcd");
        $dumpvars(0, ncu_fcd_ctl_);
    end
`endif
*/

// Neglect all mb0 signals for the time being. They are for Memory BIST related. Not functional

initial begin
   mb1_run = 0;
   mb1_cpubuf_wr_en = 0;
   tcu_mbist_user_mode = 0;
   tcu_mbist_bisi_en = 0;
   mb0_scanin = 0;
   mb0_start = 0;
   //
   iobuf_uei = 0;
   intbuf_uei = 0;
   mondotbl_pei = 0;
   array_wr_inhibit_io = 0; 
   array_wr_inhibit_cmp = 0;
   //
   cmp_io_sync_en = 0;
   io_cmp_sync_en = 0;
   io_mondo_data_wr_s = 0;
   pcx_ncu_data_rdy_px1 = 0;
   tap_mondo_acc_seq_s = 0;
   tap_mondo_wr_s = 0;
   // clock and scan enabling signals
   l2clk = 0;
   scan_in = 0;
   tcu_aclk = 0;
   tcu_bclk = 0;
   tcu_clk_stop = 0;
   tcu_pce_ov = 0;
   tcu_scan_en = 0;
end

class pcx_ncu_data;
    rand logic _pcx_ncu_data_rdy_px1 = 0;
    
    function new (int seed);
        this.srandom(seed);
        pcx_ncu_data_cover = new();
    endfunction

    covergroup pcx_ncu_data_cover;
        coverpoint _pcx_ncu_data_rdy_px1;
    endgroup 
endclass

task pcx_ncu_data_task(pcx_ncu_data _pcx_ncu_data);
    @(posedge l2clk)
    if (_pcx_ncu_data.randomize() == 1)
    begin
        pcx_ncu_data_rdy_px1 = _pcx_ncu_data._pcx_ncu_data_rdy_px1;
        _pcx_ncu_data.pcx_ncu_data_cover.sample();
    end
    else
    begin
        $display("Randomization at _pcx_ncu_data failed.\n");
    end
endtask

integer range;

initial begin
    pcx_ncu_data _pcx_ncu_data;
    #5;
    _pcx_ncu_data = new(seed);
    #600;
    repeat (500) begin
        range = $urandom_range(6, 4);
        pcx_ncu_data_task(_pcx_ncu_data);
        @(posedge l2clk);
        if (_pcx_ncu_data._pcx_ncu_data_rdy_px1 == 1)
        begin
            pcx_ncu_data_px2 = $random;
            repeat (range) @(posedge l2clk) begin
                pcx_ncu_data_px2 = $random;
            end
            pcx_ncu_data_rdy_px1 = 1'b0;
            @(posedge l2clk);
            pcx_ncu_data_px2 = {130{1'bX}};
        end
        #500;
    end
    $finish;
end


class ncu_fcd_ctl_mondo_data;
    
    rand logic [71:0] _mondo_data0_dout;
    rand logic [71:0] _mondo_data1_dout;

    constraint _mondo_data0_dout_range { _mondo_data0_dout inside {[0:72'hFF_FFFF_FFFF_FFFF_FFFF]};};
    constraint _mondo_data1_dout_range { _mondo_data1_dout inside {[0:72'hFF_FFFF_FFFF_FFFF_FFFF]};};

    function new (int seed);
        this.srandom(seed);
        ncu_fcd_ctl_mondo_data_cover = new();
    endfunction

    covergroup ncu_fcd_ctl_mondo_data_cover;
       coverpoint _mondo_data0_dout; 
       coverpoint _mondo_data1_dout; 
    endgroup

endclass

task ncu_fcd_ctl_mondo_data_task(ncu_fcd_ctl_mondo_data _ncu_fcd_ctl_mondo_data);
    @(posedge l2clk)
    if(_ncu_fcd_ctl_mondo_data.randomize() == 1)
    begin
       mondo_data0_dout = _ncu_fcd_ctl_mondo_data._mondo_data0_dout;
       mondo_data1_dout = _ncu_fcd_ctl_mondo_data._mondo_data1_dout;
    end
    else
    begin
        $display("Randomization at _ncu_fcd_ctl_mondo_data failed.\n");
    end
endtask

class tap_mondo;
    
    rand logic _tap_mondo_wr_s;
    rand logic _tap_mondo_acc_seq_s;
    rand logic [21:0] _tap_mondo_acc_addr_s;

    constraint tap_mondo_wr_range { _tap_mondo_wr_s dist {0:=6, 1:=4};};
    constraint tap_mondo_acc_range { _tap_mondo_acc_seq_s dist {0:=4, 1:=6};};
    constraint tap_mondo_acc_addr_range { _tap_mondo_acc_addr_s inside {[0:22'h3F_FFFF]};};

    constraint wr_acc_con { !( _tap_mondo_wr_s & _tap_mondo_acc_seq_s ); };

    function new (int seed);
        this.srandom(seed);
        tap_mondo_cover = new();
    endfunction

    covergroup tap_mondo_cover;
        coverpoint _tap_mondo_wr_s;
        coverpoint _tap_mondo_acc_seq_s;
        coverpoint _tap_mondo_acc_addr_s;
    endgroup

endclass

task tap_mondo_task(tap_mondo _tap_mondo);
    @(posedge l2clk)
    if(_tap_mondo.randomize() == 1)
    begin
        tap_mondo_wr_s = _tap_mondo._tap_mondo_wr_s;
        tap_mondo_acc_seq_s = _tap_mondo._tap_mondo_acc_seq_s;
        if(tap_mondo_wr_s == 1'b1)
            tap_mondo_acc_addr_s = _tap_mondo._tap_mondo_acc_addr_s;
        else
            tap_mondo_acc_addr_s = {22{1'bX}};
    end
    else
    begin
        $display("Randomization failed at _tap_mondo.\n");
    end
endtask

class io_mondo;
    
    rand logic _io_mondo_data_wr_s;
    rand logic [5:0] _io_mondo_data_wr_addr_s;

    constraint io_mondo_data_wr_s_range { _io_mondo_data_wr_s dist {0:=6, 1:=4};}; 
    constraint io_mondo_data_wr_addr_s_range { _io_mondo_data_wr_addr_s inside {[0:6'h3F]};};

    function new (int seed);
        this.srandom(seed);
        io_mondo_cover = new();
    endfunction

    covergroup io_mondo_cover;
        coverpoint  _io_mondo_data_wr_s;
        coverpoint _io_mondo_data_wr_addr_s;
    endgroup

endclass

task io_mondo_task(io_mondo _io_mondo);
    @(posedge l2clk)
    if(_io_mondo.randomize() == 1)
    begin
        io_mondo_data_wr_s = _io_mondo._io_mondo_data_wr_s;
        if (io_mondo_data_wr_s == 1'b1)
            io_mondo_data_wr_addr_s = _io_mondo._io_mondo_data_wr_addr_s;
        else
            io_mondo_data_wr_addr_s = {6{1'bX}};
    end
    else
    begin
        $display("Rnadomization at _io_mondo failed.\n");
    end
endtask

task reset_task();

endtask

always begin
    // 400 MHz Clock
    #1.25 l2clk = ~l2clk;
end

initial begin
    tap_mondo _tap_mondo;
    #5;
    _tap_mondo = new(seed);
    #1000;
    repeat(500) begin
        tap_mondo_task(_tap_mondo);
        @(posedge l2clk);
        if(_tap_mondo._tap_mondo_wr_s)
        begin
            tap_mondo_wr_s = 1'b0;
            tap_mondo_acc_addr_s = {22{1'bX}};
        end
        else if(_tap_mondo._tap_mondo_acc_seq_s)
        begin
            tap_mondo_acc_seq_s = 1'b0;
            tap_mondo_din_s = $random;
            @(posedge l2clk);
            tap_mondo_din_s = {64{1'bX}};
        end
    #600;
    end
end

initial begin
    io_mondo _io_mondo;
    #5;
    _io_mondo = new(seed);
    #500;
    repeat (500) begin
        io_mondo_task(_io_mondo);
        @(posedge l2clk);
        if (_io_mondo._io_mondo_data_wr_s == 1'b1)
        begin
            io_mondo_data_wr_s = 1'b0;
            io_mondo_data_wr_addr_s = {6{1'bX}};
            io_mondo_data0_din_s = $urandom;
            io_mondo_data1_din_s = $urandom;
            @(posedge l2clk);
            io_mondo_data0_din_s = {64{1'bX}};
            io_mondo_data1_din_s = {64{1'bX}};
        end
        #550;
    end
end

initial begin
    ncu_fcd_ctl_mondo_data _ncu_fcd_ctl_mondo_data;
    #5;
    _ncu_fcd_ctl_mondo_data = new(seed);
    #1000;
    repeat (300) begin
        ncu_fcd_ctl_mondo_data_task(_ncu_fcd_ctl_mondo_data);
        @(posedge l2clk);
        mondo_data0_dout = {72{1'bX}};
        mondo_data1_dout = {72{1'bX}};
        #1000;
    end
end

initial begin
    repeat (500) begin
        cmp_io_sync_en = 1'b1;
        @(posedge l2clk);
        cmp_io_sync_en = 1'b0;
        repeat (1000) @(posedge l2clk);
    end
end

initial begin
    repeat (200) begin
        io_cmp_sync_en = 1'b1;
        @(posedge l2clk);
        io_cmp_sync_en = 1'b0;
        repeat (500) @(posedge l2clk);
    end
end

`include "ncu_fcd_ctl_rtl_trace.v"

endmodule
