/* Testbench file for design dmu_clu generated on 2017-07-24 23:55:50.575997 */
module dmu_clu_global_bench();

reg [3:0] j2d_d_wrack_tag;
reg [1:0] j2d_di_cmd;
reg [15:0] j2d_di_ctag;
reg [3:0] j2d_p_cmd;
reg [35:0] j2d_p_addr;
reg [15:0] j2d_p_bmsk;
reg [10:0] j2d_p_ctag;
reg [127:0] j2d_d_data;
reg [3:0] j2d_d_data_par;
reg [127:0] j2d_p_data;
reg [3:0] j2d_p_data_par;
reg [40:0] ps2cl_e_rd_data;
reg [60:0] pm2cl_rcd;
reg [42:0] mm2cl_tcr_rcd;
reg [4:0] rm2cl_bufrel;
reg [5:0] tm2cl_dma_wptr;
reg [4:0] tm2cl_pio_wptr;
reg [127:0] di2cl_data;
reg [15:0] di2cl_bmask;
reg [4:0] di2cl_dpar;
reg [5:0] cr2cl_dbg_sel_a;
reg [5:0] cr2cl_dbg_sel_b;
reg [7:0] cr2cl_bus_num;
reg clk;
reg rst_l;
reg j2d_d_wrack_vld;
reg j2d_di_cmd_vld;
reg j2d_p_cmd_vld;
reg j2d_d_data_err;
reg j2d_d_data_vld;
reg j2d_p_data_vld;
reg ps2cl_e_gnt;
reg pm2cl_rcd_enq;
reg cm2cl_rcd_full;
reg mm2cl_tcr_req;
reg rm2cl_bufrel_enq;
reg ds2cl_stall;
reg p2d_npwr_stall_en;
reg rm2crm_npwr_wrack;
reg im2crm_bc_stall_en;
reg im2crm_ilu_stall_en;
reg il2cl_gr_16;


wire [3:0] d2j_cmd;
wire [36:0] d2j_addr;
wire [15:0] d2j_ctag;
wire [127:0] d2j_data;
wire [15:0] d2j_bmsk;
wire [4:0] d2j_data_par;
wire [4:0] k2y_dou_dptr;
wire [3:0] cl2ps_e_cmd_type;
wire [4:0] cl2ps_e_trn;
wire [6:0] cl2ps_e_wr_data;
wire [79:0] cl2cm_rcd;
wire [139:0] cl2mm_tdr_rcd;
wire [5:0] cl2tm_dma_rptr;
wire [4:0] cl2tm_int_rptr;
wire [8:0] cl2di_addr;
wire [127:0] cl2do_dma_data;
wire [3:0] cl2do_dma_dpar;
wire [6:0] cl2do_dma_addr;
wire [127:0] cl2do_pio_data;
wire [3:0] cl2do_pio_dpar;
wire [5:0] cl2do_pio_addr;
wire [7:0] cl2cr_dbg_a;
wire [7:0] cl2cr_dbg_b;
wire d2j_cmd_vld;
wire d2j_data_vld;
wire k2y_dou_err;
wire k2y_dou_vld;
wire cl2ps_e_req;
wire cl2pm_rcd_full;
wire cl2cm_rcd_enq;
wire cl2mm_tcr_ack;
wire cl2mm_tdr_vld;
wire cl2di_rd_en;
wire cl2do_dma_wr;
wire cl2do_pio_wr;;

integer seed;
initial begin
    $random_seed_gen(seed);
end

dmu_clu dmu_clu_ (.*);

/*
`ifdef _RTL_
    initial begin
        $dumpfile("dmu_clu_rtl.vcd");
        $dumpvars(0, dmu_clu_);
    end
`endif
*/

initial begin

    clk = 0;
    rst_l = 0;

    j2d_d_wrack_vld = 0;
    j2d_di_cmd_vld = 0;
    j2d_p_cmd_vld = 0;
    j2d_d_data_err = 0;
    j2d_d_data_vld = 0;
    j2d_p_data_vld = 0;

    ps2cl_e_gnt = 0;

    pm2cl_rcd_enq = 0;

    cm2cl_rcd_full = 0;

    mm2cl_tcr_req = 0;

    rm2cl_bufrel_enq = 0;

    ds2cl_stall = 0;

    p2d_npwr_stall_en = 0;

    rm2crm_npwr_wrack = 0;

    im2crm_bc_stall_en = 0;

    im2crm_ilu_stall_en = 0;

    il2cl_gr_16 = 0;
end

always begin
    #10000;
    p2d_npwr_stall_en = 1'b1;
    #3;
    p2d_npwr_stall_en = 1'b0;
end

always begin
    #15000;
    rm2crm_npwr_wrack = 1'b1;
    #3;
    rm2crm_npwr_wrack = 1'b0;
end

always begin
    #20000;
    im2crm_bc_stall_en = 1'b1;
    #3;
    im2crm_bc_stall_en = 1'b0;
end

always begin
    #25000;
    im2crm_ilu_stall_en = 1'b1;
    #3;
    im2crm_ilu_stall_en = 1'b0;
end

always begin
    #25000;
    il2cl_gr_16 = 1'b1;
    #3;
    il2cl_gr_16 = 1'b0;
end

always begin
    #1050;
    ds2cl_stall = 1'b1;
    cr2cl_bus_num = $random;
    #3;
    ds2cl_stall = 1'b0;
    cr2cl_bus_num = {8{1'bX}};
end

always begin
    #950;
    rm2cl_bufrel_enq = 1'b1;
    rm2cl_bufrel = $random;
    #3;
    rm2cl_bufrel_enq = 1'b0;
    rm2cl_bufrel = {5{1'bX}};
end

always begin
    #900;
    mm2cl_tcr_req = 1'b1;
    mm2cl_tcr_rcd = $random;
    #3;
    mm2cl_tcr_req = 1'b0;
    mm2cl_tcr_rcd = {43{1'bX}};
end

always begin
    #1000;
    ps2cl_e_gnt = 1'b1;
    ps2cl_e_rd_data = $random;
    #3;
    ps2cl_e_gnt = 1'b0;
    ps2cl_e_rd_data = {41{1'Bx}};
end

always begin
    #800;
    pm2cl_rcd_enq = 1'b1;
    pm2cl_rcd = $random;
    #3;
    pm2cl_rcd_enq = 1'b0;
    pm2cl_rcd = {61{1'bX}};
end
   
always begin
    #1200;
    cm2cl_rcd_full = 1'b1;
    #3;
    cm2cl_rcd_full = 1'b0;
end

task reset_task;
    begin
        $display("Initializing design\n");
        #100;
        rst_l = 1;
        $display("Reset sequence ended: %d\n", $time);
    end
endtask

class wrack_tag;
    
    rand logic _j2d_d_wrack_vld;
    rand logic [3:0] _j2d_d_wrack_tag;

    constraint wrack_vld_range { _j2d_d_wrack_vld dist {0:=6, 1:=4};};
    // At max 16 Credit IDs
    constraint wrack_tag_range { _j2d_d_wrack_tag inside {[0:4'hF]};};

    function new (int seed);
        this.srandom(seed);
        wrack_tag_cover = new();
    endfunction

    covergroup wrack_tag_cover;
        coverpoint _j2d_d_wrack_vld;
        coverpoint _j2d_d_wrack_tag;
    endgroup
endclass

task wrack_tag_task (wrack_tag _wt);
    @(posedge clk)
    if(_wt.randomize() == 1)
    begin
        j2d_d_wrack_vld = _wt._j2d_d_wrack_vld;
        if (j2d_d_wrack_vld)
            j2d_d_wrack_tag = _wt._j2d_d_wrack_tag;
        else
            j2d_d_wrack_tag = {4{1'bX}};
    end
    else
    begin
        $display("Randomization failed at wrac_tag.\n");
    end
endtask

class j2d_cmd_dma_int;
    
    rand logic _j2d_di_cmd_vld;
    rand logic [1:0] cmd_j2d_1;
    rand logic [1:0] cmd_j2d_0;

    rand logic [3:0] mnd_dout_1;
    rand logic [1:0] mnd_dout_2;

    logic [1:0] cmd_vals_1 [] = '{2'b00, 2'b01};
    logic [1:0] cmd_vals_0 [] = '{2'b10, 2'b11};

    constraint di_cmd_vld_range { _j2d_di_cmd_vld dist {0:=5, 1:=5};};
    constraint cmd_j2d_1_range {
        cmd_j2d_1 inside cmd_vals_1;
    }
    constraint cmd_j2d_0_range {
        cmd_j2d_0 inside cmd_vals_0;
    }

    function new (int seed);
        this.srandom(seed);
        j2d_cmd_dma_int_cover = new();
    endfunction

    covergroup j2d_cmd_dma_int_cover;
        coverpoint _j2d_di_cmd_vld;
        coverpoint cmd_j2d_1;
        coverpoint cmd_j2d_0;
        coverpoint mnd_dout_1;
        coverpoint mnd_dout_2;
    endgroup
endclass

task j2d_cmd_dma_int_task(j2d_cmd_dma_int cmd_di);
    @(posedge clk)
    if(cmd_di.randomize() == 1)
    begin
        j2d_di_cmd_vld = cmd_di._j2d_di_cmd_vld;
        if (j2d_di_cmd_vld)
        begin
            j2d_di_cmd = cmd_di.cmd_j2d_1;
            j2d_di_ctag = $urandom;
        end
        else
        begin
            j2d_di_cmd = cmd_di.cmd_j2d_0;
            j2d_di_ctag = {1'b0, cmd_di.mnd_dout_1, 8'b0, cmd_di.mnd_dout_2, 1'b0};
        end
    end
    else
    begin
        $display("Randomization failed at cmd_di.\n");
    end
endtask

class j2d_cmd_pio;

    rand logic _j2d_p_cmd_vld;
    rand logic j2d_p_cmd_1;
    rand logic j2d_p_cmd_0;

    rand logic j2d_p_ctag_10to7;
    rand logic j2d_p_ctag_6to0;

    rand logic ncu_dmu_pio_data_60;
    rand logic ncu_dmu_pio_data_50;
    rand logic ncu_dmu_pio_data_3;
    rand logic [7:0] ncu_dmu_pio_data_55to48;
    logic [7:0] bytemask;

    constraint p_cmd_vld_range { _j2d_p_cmd_vld dist {0:=4, 1:=6};};

    constraint j2d_p_cmd_1_range { j2d_p_cmd_1 dist {0:=3, 1:=7};}; 
    constraint j2d_p_cmd_0_range { j2d_p_cmd_0 dist {0:=1, 1:=9};}; 

    constraint ncu_dmu_pio_data_60_range { ncu_dmu_pio_data_60 dist {0:=6, 1:=4};};
    constraint ncu_dmu_pio_data_50_range { ncu_dmu_pio_data_50 dist {0:=3, 1:=7};};
    constraint ncu_dmu_pio_data_3_range { ncu_dmu_pio_data_3 dist {0:=2, 1:=8};};

    function new (int seed);
        this.srandom(seed);
        j2d_cmd_pio_cover = new();
    endfunction
    /*
    function byte2bits (logic [3:0] bytecount);
        integer bytes;
        bytes = bytecount;
        this.bytemask = {{(8 - bytes){1'b0}}, {bytes{1'b1}};
        return this.bytemask;
    endfunction
    */
    covergroup j2d_cmd_pio_cover;
        coverpoint _j2d_p_cmd_vld;
        coverpoint j2d_p_cmd_1;
        coverpoint j2d_p_cmd_0;
        coverpoint j2d_p_ctag_10to7;
        coverpoint j2d_p_ctag_6to0;
        coverpoint ncu_dmu_pio_data_60;
        coverpoint ncu_dmu_pio_data_50;
        coverpoint ncu_dmu_pio_data_3;
        coverpoint ncu_dmu_pio_data_55to48;
    endgroup
endclass

task j2d_cmd_pio_task (j2d_cmd_pio cmd_pio);
    @(posedge clk)
    if(cmd_pio.randomize() == 1)
    begin
        j2d_p_cmd_vld = cmd_pio._j2d_p_cmd_vld;
        if(j2d_p_cmd_vld)
        begin
            j2d_p_addr = $urandom;
            j2d_p_cmd = {cmd_pio.ncu_dmu_pio_data_60, 1'b1, cmd_pio.j2d_p_cmd_1, cmd_pio.j2d_p_cmd_0};
            j2d_p_ctag = {cmd_pio.j2d_p_ctag_10to7, cmd_pio.j2d_p_ctag_6to0};
            if(cmd_pio.ncu_dmu_pio_data_60 == 0) // PIO Writes
                if(cmd_pio.ncu_dmu_pio_data_3 == 1)
                    j2d_p_bmsk = {8'b0, cmd_pio.ncu_dmu_pio_data_55to48};
                else if(cmd_pio.ncu_dmu_pio_data_3 ==0)
                    j2d_p_bmsk = {cmd_pio.ncu_dmu_pio_data_55to48, 8'b0};
            else if(cmd_pio.ncu_dmu_pio_data_60 == 1) // PIO Reads
                if(cmd_pio.ncu_dmu_pio_data_3 == 1 & cmd_pio.ncu_dmu_pio_data_50 == 0)
                    j2d_p_bmsk = {8'b0, {{5{1'bX}}, cmd_pio.ncu_dmu_pio_data_55to48[2:0]}};
                else if(cmd_pio.ncu_dmu_pio_data_3 == 0 & cmd_pio.ncu_dmu_pio_data_50 == 0)
                    j2d_p_bmsk = {{{5{1'bX}}, cmd_pio.ncu_dmu_pio_data_55to48[2:0]}, 8'b0};
                else if(cmd_pio.ncu_dmu_pio_data_50 == 1)
                    j2d_p_bmsk = 16'b1;
        end
    end
    else
    begin
        $display("Randomization failed at cmd_pio.\n");
    end
endtask

always begin
    // 350 MHz clock
	#1.5 clk = ~clk;
end

integer index;

initial begin
    j2d_cmd_pio cmd_pio;
    #5;
    cmd_pio = new(seed);
    #100;
    repeat (500) begin
        j2d_cmd_pio_task(cmd_pio);
        @(posedge clk);
        if(cmd_pio._j2d_p_cmd_vld)
        begin
            j2d_p_cmd_vld = 1'b0;
            j2d_p_cmd = {4{1'bX}};
            j2d_p_addr = {36{1'bX}};
            j2d_p_ctag = {11{1'bX}};
            j2d_p_bmsk = {16{1'bX}};
            
            if(cmd_pio.ncu_dmu_pio_data_60 == 0)
            begin
                j2d_p_data_vld = 1'b1;
                j2d_p_data = $urandom;
                j2d_p_data_par[0] = ^j2d_p_data[31:0];
                j2d_p_data_par[1] = ^j2d_p_data[63:32];
                j2d_p_data_par[2] = ^j2d_p_data[95:64];
                j2d_p_data_par[3] = ^j2d_p_data[127:96];
                @(posedge clk);
                j2d_p_data_vld = 1'b0;
                j2d_p_data = {128{1'bX}};
                j2d_p_data_par = {4{1'bX}};
            end
        end
    #400;
    end
end


initial begin
    j2d_cmd_dma_int cmd_di;
    #5;
    cmd_di = new(seed);
    #50;
    repeat (500) begin
        j2d_cmd_dma_int_task(cmd_di);
        @(posedge clk);
        if(cmd_di._j2d_di_cmd_vld)
        begin
            j2d_di_cmd_vld = 1'b0;
            j2d_di_cmd = {2{1'bX}};
            j2d_di_ctag = {16{1'bX}};

            j2d_d_data_vld = 1'b1;
            j2d_d_data_err = $random;
            j2d_d_data = $urandom;
            j2d_d_data_par[0] = ^j2d_p_data[31:0];
            j2d_d_data_par[1] = ^j2d_d_data[63:32];
            j2d_d_data_par[2] = ^j2d_d_data[95:64];
            j2d_d_data_par[3] = ^j2d_d_data[127:96];
            repeat (4) @(posedge clk) begin
                j2d_d_data_vld = 1'b1;
                j2d_d_data_err = $random;
                j2d_d_data = $urandom;
                j2d_d_data_par[0] = ^j2d_p_data[31:0];
                j2d_d_data_par[1] = ^j2d_d_data[63:32];
                j2d_d_data_par[2] = ^j2d_d_data[95:64];
                j2d_d_data_par[3] = ^j2d_d_data[127:96];
            end
            j2d_d_data_vld = 1'b0;
            j2d_d_data_err = 1'b0;
            j2d_d_data = {128{1'bX}};
            j2d_d_data_par = {4{1'bX}};
        end
        #300;
    end
end

initial begin
    wrack_tag wt;
    #5;
    $display("Seed is: %d \n", seed);
    wt = new(seed);
    #10;
    reset_task;
    #300;
    repeat (500) begin
        wrack_tag_task(wt);
        @(posedge clk);
        if (wt._j2d_d_wrack_vld)
        begin
            j2d_d_wrack_vld = 1'b0;
            j2d_d_wrack_tag = {4{1'bX}};
        end
        #200;
    end
    $finish;
end

`include "dmu_clu_rtl_trace.v"

endmodule
