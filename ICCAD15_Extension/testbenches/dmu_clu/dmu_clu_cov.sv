`ifdef _COV_
`include "dmu_clu_tick_defines_netlist_PRankNetlist.v"
`include "dmu_clu_tick_defines_netlist_HybrSel.v"
`include "dmu_clu_tick_defines_netlist_SigSeT_1.v"
`endif

module dmu_clu_cov_bench;

reg [3:0] j2d_d_wrack_tag;
reg [1:0] j2d_di_cmd;
reg [15:0] j2d_di_ctag;
reg [3:0] j2d_p_cmd;
reg [35:0] j2d_p_addr;
reg [15:0] j2d_p_bmsk;
reg [10:0] j2d_p_ctag;
reg [127:0] j2d_d_data;
reg [3:0] j2d_d_data_par;
reg [127:0] j2d_p_data;
reg [3:0] j2d_p_data_par;
reg [40:0] ps2cl_e_rd_data;
reg [60:0] pm2cl_rcd;
reg [42:0] mm2cl_tcr_rcd;
reg [4:0] rm2cl_bufrel;
reg [5:0] tm2cl_dma_wptr;
reg [4:0] tm2cl_pio_wptr;
reg [127:0] di2cl_data;
reg [15:0] di2cl_bmask;
reg [4:0] di2cl_dpar;
reg [5:0] cr2cl_dbg_sel_a;
reg [5:0] cr2cl_dbg_sel_b;
reg [7:0] cr2cl_bus_num;
reg clk;
reg rst_l;
reg j2d_d_wrack_vld;
reg j2d_di_cmd_vld;
reg j2d_p_cmd_vld;
reg j2d_d_data_err;
reg j2d_d_data_vld;
reg j2d_p_data_vld;
reg ps2cl_e_gnt;
reg pm2cl_rcd_enq;
reg cm2cl_rcd_full;
reg mm2cl_tcr_req;
reg rm2cl_bufrel_enq;
reg ds2cl_stall;
reg p2d_npwr_stall_en;
reg rm2crm_npwr_wrack;
reg im2crm_bc_stall_en;
reg im2crm_ilu_stall_en;
reg il2cl_gr_16;


wire [3:0] d2j_cmd;
wire [36:0] d2j_addr;
wire [15:0] d2j_ctag;
wire [127:0] d2j_data;
wire [15:0] d2j_bmsk;
wire [4:0] d2j_data_par;
wire [4:0] k2y_dou_dptr;
wire [3:0] cl2ps_e_cmd_type;
wire [4:0] cl2ps_e_trn;
wire [6:0] cl2ps_e_wr_data;
wire [79:0] cl2cm_rcd;
wire [139:0] cl2mm_tdr_rcd;
wire [5:0] cl2tm_dma_rptr;
wire [4:0] cl2tm_int_rptr;
wire [8:0] cl2di_addr;
wire [127:0] cl2do_dma_data;
wire [3:0] cl2do_dma_dpar;
wire [6:0] cl2do_dma_addr;
wire [127:0] cl2do_pio_data;
wire [3:0] cl2do_pio_dpar;
wire [5:0] cl2do_pio_addr;
wire [7:0] cl2cr_dbg_a;
wire [7:0] cl2cr_dbg_b;
wire d2j_cmd_vld;
wire d2j_data_vld;
wire k2y_dou_err;
wire k2y_dou_vld;
wire cl2ps_e_req;
wire cl2pm_rcd_full;
wire cl2cm_rcd_enq;
wire cl2mm_tcr_ack;
wire cl2mm_tdr_vld;
wire cl2di_rd_en;
wire cl2do_dma_wr;
wire cl2do_pio_wr;

`ifdef _COV_
`include "dmu_clu_trace_reg_PRankNetlist.v"
`include "dmu_clu_trace_reg_HybrSel.v"
`include "dmu_clu_trace_reg_SigSeT_1.v"
`endif


dmu_clu dmu_clu_ (.*);

initial begin

    clk = 0;
    rst_l = 0;

    j2d_d_wrack_vld = 0;
    j2d_di_cmd_vld = 0;
    j2d_p_cmd_vld = 0;
    j2d_d_data_err = 0;
    j2d_d_data_vld = 0;
    j2d_p_data_vld = 0;

    ps2cl_e_gnt = 0;

    pm2cl_rcd_enq = 0;

    cm2cl_rcd_full = 0;

    mm2cl_tcr_req = 0;

    rm2cl_bufrel_enq = 0;

    ds2cl_stall = 0;

    p2d_npwr_stall_en = 0;

    rm2crm_npwr_wrack = 0;

    im2crm_bc_stall_en = 0;

    im2crm_ilu_stall_en = 0;

    il2cl_gr_16 = 0;
end

initial begin
    $display("Initializing design\n");
    #100;
    rst_l = 1;
    $display("Reset sequence ended: %d\n", $time);
end

always begin
    // 350 MHz clock
	#1.5 clk = ~clk;
end

`ifdef _COV_
`include "dmu_clu_stim_netlist_PRankNetlist.v"
`include "dmu_clu_stim_netlist_HybrSel.v"
`include "dmu_clu_stim_netlist_SigSeT_1.v"
`endif

endmodule
